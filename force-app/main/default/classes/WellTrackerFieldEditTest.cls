@isTest
private class WellTrackerFieldEditTest {

    @isTest
    static void controller_Test()
    {                                        
        //-- Setup of data --//                                                        

        //-- Setup Well Tracker
        Business_Unit__c businessUnit = new Business_Unit__c
            (           
                Name = 'Test Business Unit'
            );            
        insert businessUnit;

        Business_Department__c businessDepartment = new Business_Department__c
            (           
                Name = 'Test Business Department'
            );            
        insert businessDepartment;

        Operating_District__c operatingDistrict = new Operating_District__c
            (           
                Name = 'Test Field',
                Business_Department__c = businessDepartment.Id,
                Business_Unit__c = businessUnit.Id
            );            
        insert operatingDistrict;

        Id recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

        Field__c field = new Field__c
            (           
                Name = 'Test Field Control Centre',
                Operating_District__c = operatingDistrict.Id,
                RecordTypeId = recordTypeId
            );            
        insert field;
        
        Route__c route = new Route__c
              (
                  Name = '999'
              );     
        insert route;                  
                              
        Location__c location = 
            new Location__c
                (
                    Name = 'Test Location' ,
                    Route__c = route.id, 
                    Operating_Field_AMU__c = field.id
                );
        insert location;
                
        Well_Tracker__c wellTracker = new Well_Tracker__c
            (
                Oil_Prod_m3_day__c = 1,
                Well_down__c = Datetime.valueOf('2013-06-28 10:00:00'),
                Well_Up__c = Datetime.valueOf('2013-06-28 11:00:00'),
                Well_Status__c = 'Well Down',
                Well_Status_Reason__c = 'Maintenance Service',
                Comments__c = 'Testing Comments Box',
                Location__c = location.Id,
                Control_Centre__c = field.Id
            );        
        insert wellTracker;
        //--

        //-- End setup of data needed to test the WellTracker controller --//
        
        Test.startTest();

        PageReference pageRef = Page.WellTrackerFieldEdit;

        pageRef.getparameters().put('id', wellTracker.id);

        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(wellTracker);

        WellTrackerFieldEdit ext = new  WellTrackerFieldEdit(stdController);

        System.assertEquals('Testing Comments Box', wellTracker.Comments__c);

        wellTracker.Comments__c = 'Updated Comments box';

        ext.save();

        System.assertEquals('Updated Comments box', wellTracker.Comments__c);

        Test.stopTest();            
    
    }
    
 }