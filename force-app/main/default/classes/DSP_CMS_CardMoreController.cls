global virtual with sharing class DSP_CMS_CardMoreController extends cms.ContentTemplateController{
    global DSP_CMS_CardMoreController(cms.CreateContentController cc){
        super(cc);
    }
    
    global DSP_CMS_CardMoreController(cms.GenerateContent cc){
        super(cc);
    }
    
    global DSP_CMS_CardMoreController(){}

    global virtual override String getHTML(){return '';}

    public String website {
        get{
            return getProperty('website');
        }
        set;
    }

    public cms.Link parsedWebsite{
        get{
            return new cms.Link(this.website, this.page_mode, this.site_name);
        }
    }

    public String linkHTML(){
        String html='';
        
        html+='<a href="';
        if(this.parsedWebsite.targetPage != null)
            html+=parsedWebsite.targetPage;
        html+='"';
        if(this.parsedWebsite.target != null && this.parsedWebsite.target != '')
            html += 'target="' + this.parsedWebsite.target + '"';
        if(this.parsedWebsite.javascript != null && this.parsedWebsite.javascript != '')
            html += ' onlick=\''+this.parsedWebsite.javascript+'\'';
        html+=' class="small right">';
        html+= 'More</a>';
        
        return html;
    }
}