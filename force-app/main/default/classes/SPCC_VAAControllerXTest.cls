@isTest
private class SPCC_VAAControllerXTest {

	@testSetup static void setup() {
		SPCC_Utilities.executeTriggerCode = false;

		SPCC_Engineering_Work_Request__c ewr = SPCC_TestData.createEWR('TestProject', '12345', '','Other');
		insert ewr;

		List<Account> vendorAccounts = new List<Account>();
		for(Integer i=1; i <= 5; i++) {
			vendorAccounts.add(SPCC_TestData.createSPCCVendor('Test Account ' + i));
		}
		insert vendorAccounts;
	}
	
	@isTest static void testController() {
		//Query set records
		SPCC_Engineering_Work_Request__c ewr = [Select Id, Name, Project_Name__c, EWR_Number__c
												From SPCC_Engineering_Work_Request__c][0];
		List<Account> vendorAccounts = [Select Id, Name
										From Account];


		//Set Page
		PageReference pref = Page.SPCC_VAAAddVendor;
		pref.getParameters().put('EWR_Id', ewr.Id);
		Test.setCurrentPage(pref);

		Test.startTest();
			//Create VAA
			SPCC_Vendor_Account_Assignment__c vaa = SPCC_TestData.createVAA(null, null);

			//Create controller
			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(vaa);
			SPCC_VAAControllerX extCtrl = new SPCC_VAAControllerX(stdCtrl);

			//Test Controller initialized state
			System.assertEquals(extCtrl.projectName, ewr.Project_Name__c);
			System.assertEquals(extCtrl.duplicateFound, false);
			System.assertEquals(extCtrl.isError, false);

			//Test Account Assignment 
			extCtrl.vaa.Vendor_Account__c = vendorAccounts[0].Id;
			extCtrl.saveVAA();
			vaa = [Select Id, Name, Vendor_Account__c, Engineering_Work_Request__c
				   From SPCC_Vendor_Account_Assignment__c
				   Where Id =: vaa.Id];
			System.assertNotEquals(vaa, null);
			System.assertEquals(vaa.Engineering_Work_Request__c, ewr.Id);
			System.assertEquals(vaa.Vendor_Account__c, vendorAccounts[0].Id);

			//Test Duplicate Check
			SPCC_Vendor_Account_Assignment__c vaa2 = SPCC_TestData.createVAA(null, null);
			stdCtrl = new ApexPages.StandardController(vaa2);
			extCtrl = new SPCC_VAAControllerX(stdCtrl);
			extCtrl.vaa.Vendor_Account__c = vendorAccounts[0].Id;
			extCtrl.saveVAA();
			System.assertEquals(extCtrl.duplicateFound, true);
			System.assertEquals(extCtrl.vaa.Id, null);

		Test.stopTest();
	}
	
}