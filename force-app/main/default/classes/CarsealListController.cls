// KK 27/Feb/2014: remodelling the carseal, instead of going from area -> equipment -> carseal, it is now
 // plant -> unit -> carseal.
 // There are new variables and functsion in retrieving data for plants and units.
 
public class CarsealListController 
{

    public String CarsealID {get; set;}
    public String plantID {get; set;}
    public String unitID {get; set;}
    public Carseal__c SelectedCarseal {get; set;}

    // KK 19/Mar/2014: from customs settings, retrieve the max number of records to search
    public Integer maxSearchRows {get; private set;}
  
    // Car Seal ID Search string
    public String carsealSearchString {get; set;}
    // Car Seal Search option
    public String carSealSearchOption {get; set;}
  
    // get the number of records returned
    private Integer recordCount;
    public boolean statusIsWrong {get;set;}
    public String errorMsg {get;set;}
    // provide the frontend the number of records returned
    public String displayRecordCountInfo
    {
        get
        {
            String returnString = '';
            if (recordCount > maxSearchRows)
            {
                returnString = maxSearchRows + '+ carseal' + (recordCount > 1 ? 's' : '') + ' returned.';
            }
            else
            {
                returnString = recordCount + ' carseal' + (recordCount > 1 ? 's' : '') + ' returned.';
            }
            return returnString;
        } 
        private set;
    }
    
    // Flag to indicate whehter search has been requested either by plant unit hierarchy or through the search box.
    // When the screen loads the "Results" section should not show, not even the header.  But once a search has been
    // done via either method, the "Result" section will display.
    public Boolean CarsealSearchRequested {get; set;}
  
    public List<Carseal__c> carsealSearchResults
    {
        get 
        {
            if (carsealSearchResults == null) 
            {
                carsealSearchResults = new List<Carseal__c>();
            }
            return carsealSearchResults;
        }
        set;
    }

    // Status of last selected valve.  Use it to update valve status.
    public Carseal_Status__c currCarsealStatus
    {
        get
        {
            if (SelectedCarseal == null) 
            {
                currCarsealStatus = null;
            }
            return currCarsealStatus;
        }
        set;
    }

    public List<Carseal_Status__c> SelectedCarsealStatuses
    {
        get
        {
            if (SelectedCarseal == null)
            {
                SelectedCarsealStatuses = null;
            }
            else
            {
                SelectedCarsealStatuses = [SELECT Carseal__c, Date__c, Status_Lookup__c, View_Carseal_Status__c,
                    LastModifiedById, LastModifiedDate 
                    FROM Carseal_Status__c
                    WHERE Carseal__c = :SelectedCarseal.Id
                    ORDER BY LastModifiedDate DESC
                    ];
                
            }
            return SelectedCarsealStatuses;
        }
        set;
    }
    

    // KK 10/Mar/2014: add a constructor so that the search is defaulted to search by Reference Equipment as opposed to
    // Carseal ID
    public CarsealListController()
    {
        carSealSearchOption = 'name';   
        // inside the constructor also get the customs settings info.
        Carseal_Settings__c cSettings = Carseal_Settings__c.getOrgDefaults();
        maxSearchRows = Integer.valueOf(cSettings.Maximum_Search_Rows__c);
        CarsealSearchRequested = false;
        statusIsWrong = false;
    }
  
    // plant picklist
    public List<SelectOption> getPlantItems()
    {
        List<SelectOption> options = new List<SelectOption>();
        // 19/Mar/2014: KK Add a "Choose a Plant" option
        options.add(new SelectOption('-1', '-- Choose A Plant --'));
        for (List<Plant__c> pList : [SELECT Name FROM Plant__c ORDER BY Name])
        {
            for (Plant__c p : pList)
            {
                options.add(new SelectOption(p.ID, p.Name));
            }
        }
        return options;             
    }
  
    // unit picklist
    public List<SelectOption> getUnitItems()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        // 19/Mar/2014: KK Add a "Choose a Unit" option
        options.add(new SelectOption('-1', '-- Choose A Unit --'));
        
        if (plantId != null)
        {
            for (List<Unit__c> uList : [SELECT Name FROM Unit__c WHERE Plant__c = : plantID ORDER BY Name])
            {
                for (Unit__c unit : uList) 
                {
                    options.add(new SelectOption(unit.Id, unit.Name));
                }
            }
        }
        return options;
    }
  
    public PageReference  RefreshUnitList() 
    {
        unitID = null;
        CarsealID = null;
        SelectedCarseal = null;
        return null;
    } 
    
    public PageReference RefreshCarsealList()
    {
        CarsealID = null;
        SelectedCarseal = null;
        carSealSearchOption = 'unit';
        SearchCarseal();
        return null;
    } 
  
    // Search car seal given its car seal ID - invoked directly when the user clicks the "Search" button, or 
    // indirectly when the user chooses a Unit from the Unit picklist (via RefreshCarsealList() method)
    public PageReference SearchCarseal() 
    {
        // set the CarsealSearchRequested flag to true so the "Results" section will now display.
        CarsealSearchRequested = true;
        
        String refEquip ='';
        String carsealName = '';
        String pAndIDNumber = '';

        if (carSealSearchOption == 'name') 
        {
            carsealName = '%' + carsealSearchString + '%';
            carsealSearchResults = [SELECT Id, Name, Reference_Equipment__c, P_ID__c, Line__c, Size__c, Description__c, Current_Status_Text__c
                FROM Carseal__c  
                WHERE Name LIKE :carsealName
                ORDER BY Name 
                LIMIT :(maxSearchRows + 1)];
        }
        // KK 17/Mar/2014: added the P&ID Search, so need to check if this is search by Reference Equipment or P&ID #
        else if (carSealSearchOption == 'equip') 
        {
            refEquip = '%' + carsealSearchString + '%';
            // KK 6 Mar 2014:  added the ORDER BY 
            carsealSearchResults = [SELECT Id, Name, Reference_Equipment__c, P_ID__c, Line__c, Size__c, Description__c, Current_Status_Text__c 
                FROM Carseal__c  
                WHERE Reference_Equipment__c LIKE :refEquip
                ORDER BY Reference_Equipment__c 
                LIMIT :(maxSearchRows + 1)];
        }
        //KK 17/Mar/2014: add the P&ID Search
        else if (carSealSearchOption == 'pid') 
        {
            pAndIDNumber = '%' + carsealSearchString + '%';
            // KK 6 Mar 2014:  added the ORDER BY 
            carsealSearchResults = [SELECT Id, Name, Reference_Equipment__c, P_ID__c, Line__c, Size__c, Description__c, Current_Status_Text__c 
                FROM Carseal__c  
                WHERE P_ID__c LIKE :pAndIDNumber
                ORDER BY P_ID__c 
                LIMIT :(maxSearchRows + 1)];
        }
        //KK 19/Mar/2014: add the Unit (it's from Plant -> Unit navigation rather than the search)
        else if (carSealSearchOption == 'unit')
        {
            carsealSearchResults = [SELECT Id, Name, Reference_Equipment__c, P_ID__c, Line__c, Size__c, Description__c, Current_Status_Text__c 
                FROM Carseal__c 
                WHERE Plant__c = :plantID
                AND Unit__c = :unitID
                ORDER BY Name 
                LIMIT :(maxSearchRows + 1)]; 
        } 
        // Set the number of records returned
        recordCount = carsealSearchResults.size();
        return null;
    }
  
    // Show the detail of the carseal - invoked when the user clicks the carseal ID link in the search result.
    public PageReference showSelectedCarseal() 
    {
        CarsealID = Apexpages.currentPage().getParameters().get('currCarseal');
        prv_setCurrCarseal();
        System.debug('showSelectedCarseal  currCarsealStatus =' + currCarsealStatus);
        return null;
    }
  
    // Update status for the selected carseal - invoked when the user clicks the "Update Status" button
    public PageReference updateStatus() 
    {
        statusIsWrong = false;
        errorMsg = '';
        if (currCarsealStatus.Carseal__c == null) 
        {
            return null;
        }
        System.debug('update status currCarsealStatus =' + currCarsealStatus + '  selectedCarseal.Carseal_Status_Lookup__r.Status_Lookup__c=' + selectedCarseal.Carseal_Status_Lookup__r.Status_Lookup__c);
        //KK 18/Mar/2014: make sure the user has entered a status
        if (null == currCarsealStatus.Status_Lookup__c)
        {
            errorMsg = 'Error: Please provide a status.';
            statusIsWrong = true;
        }
        //KK 18/Mar/2014: check to see if the user has changed the status at all.  Compare what's on the form
        // (currCarSealStatus.Status_Lookup__c) and what's on the carseal record as the current status
        // (SelectedCarseal.Carseal_Status_Lookup__r.Status_Lookup__c)
        else if (currCarsealStatus.Status_Lookup__c == SelectedCarseal.Carseal_Status_Lookup__r.Status_Lookup__c)
        {
            System.debug('error');
            errorMsg = 'Error: You cannot create a new carseal status without updating the status.';
             statusIsWrong = true;
        }
        else
        {
            try 
            {
                insert currCarsealStatus;
                currCarsealStatus = new Carseal_Status__c(Carseal__c = SelectedCarseal.Id,
                    Date__c = currCarsealStatus.Date__c,
                    Status_Lookup__c = currCarsealStatus.Status_Lookup__c);

                // important to refresh SelectedCarseal because the current carseal status information has changed, 
                // i.e. Carseal_Status_Lookup__r.Status_Lookup__c
                SelectedCarseal = [SELECT Id, Name, Reference_Equipment__c, Unit__c, OwnerId, P_ID__c, Line__c, Size__c,
                    Elapsed__c, P_ID_Status__c, Description__c, Carseal_Status_Lookup__r.Status_Lookup__c,
                    Equipment_Protected__c, Unit__r.Plant__c, Justification__c, Special_Consideration__c 
                    FROM Carseal__c  
                    WHERE Id = :SelectedCarseal.Id];
                    
            }
            catch(Exception e) 
            {
                ApexPages.addMessages(e);
            }
        }
        return null;
    }
  
    // Set the currently selected valve
    private void prv_setCurrCarseal() 
    {
        // Get current carseal
        SelectedCarseal = [SELECT Id, Name, Reference_Equipment__c, Unit__c, OwnerId, P_ID__c, Line__c, Size__c,
            Elapsed__c, P_ID_Status__c, Description__c, Carseal_Status_Lookup__r.Status_Lookup__c,
            Equipment_Protected__c, Unit__r.Plant__c, Justification__c, Special_Consideration__c 
            FROM Carseal__c  
            WHERE Id = :carSealID];

        // Get current status
        try 
        {
            currCarsealStatus = new Carseal_Status__c(Status_Lookup__c = null, Date__c = date.today(),
                Carseal__c = SelectedCarseal.Id);
        }
        catch(QueryException ex) 
        {
            currCarsealStatus = new Carseal_Status__c(Carseal__c = SelectedCarseal.Id, Date__c = date.today());
        }
    }
    public List<SelectOption> getStatusCodes()
    {
    	List<SelectOption> options = new List<SelectOption>();
    	
	   options.add(new SelectOption('', '--None--')) ;    
	   for(Carseal_Status_Code__c c : [select Id, Name from Carseal_Status_Code__c order by Name])
	   {
	      options.add(new SelectOption(c.Id, c.Name));
	   }       
	   return options;
    }
}