public class SEP_TriggerUtility
{
    //Developed by Gangha Kaliyan
    //To have an utility class for all SEP triggers in the future
    
    //If this set has a trigger name, then that trigger is already executed
    public static Set<String> executedTriggers = new Set<String>();
    
    
    public static void triggerAfterInsertUpdateCommonTaskFields(Set<Id> taskIds, String dmlType)
    {
        //this method handles after insert updates for SEP_PopulateFieldsInCommonTask
        //variables used in this method
        List<Common_Task__c> ctList =  new List<Common_Task__c>();
        List<Common_Task__c> ctUpdateList = new List<Common_Task__c>();
        List<Junction_User_BusinessDepartment__c> userList =  new List<Junction_User_BusinessDepartment__c>();
        
        Set<Id> userIds = new Set<Id>();
        Map<Id, Id> userBusDeptMap = new Map<Id, Id>();
        
        
        ctList = [SELECT Id, Name, Project_Name__c, Business_Unit__c, Project_Name__r.Business_Unit__c, User__c, User__r.UserRole.Name 
                  FROM Common_Task__c 
                  WHERE Id IN: taskIds];

        for(Common_Task__c ct:ctList)
        {
            if(ct.User__c!=null)
            {
                userIds.add(ct.User__c);
            }
        } 

        for(Junction_User_BusinessDepartment__c u: [SELECT Id, Name, Business_Department__c, User__c 
                        FROM Junction_User_BusinessDepartment__c 
                        WHERE User__c IN:userIds])
        {
            userBusDeptMap.put(u.User__c, u.Business_Department__c);
        }
        
        try
        {
            for(Common_Task__c ct:ctList)
            {
                //check if this task has a project, if yes get the business unit from project and update that info to the task
                if(ct.Project_Name__c!=null)
                {
                    ct.Business_Unit__c = ct.Project_Name__r.Business_Unit__c;
                }
                else
                {
                    ct.Business_Unit__c = null;
                }
                
                if(userBusDeptMap.containsKey(ct.User__c))
                {
                    ct.Business_Department__c = userBusDeptMap.get(ct.User__c);
                }
                else
                {
                    ct.Business_Department__c = null;
                }
        
                ctUpdateList.add(ct);        
            }
           update ctUpdateList;  
        }

        catch(Exception e)
        {
            System.debug('\n'+e.getMessage()+'\n');
            System.debug('In class SEP_TriggerUtility \n');
            System.debug('Stack Trace '+e.getStackTraceString());
        }
           
    }
}