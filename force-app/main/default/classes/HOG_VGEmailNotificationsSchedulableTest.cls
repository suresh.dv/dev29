/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Unit Test for HOG_VGEmailNotificationsSchedulable.cls
History:        jschn 05.02.2018 - Created.
**************************************************************************************************/
@IsTest
private class HOG_VGEmailNotificationsSchedulableTest {
    
    @isTest static void testResheduledRun() {
        Test.startTest();
        //related job
        HOG_PVRUpdateBatchSchedulable relatedJob = new HOG_PVRUpdateBatchSchedulable();
        String schRJ = '0 30 7 * * ?';
        String rJobID = system.schedule(HOG_VGEmailNotificationsSchedulable.RELATED_JOB_NAME, schRJ, relatedJob);

        HOG_VGEmailNotificationsSchedulable m = new HOG_VGEmailNotificationsSchedulable();
        String sch = '0 0 8 * * ?';
        String jobID = system.schedule(HOG_VGEmailNotificationsSchedulable.JOB_NAME, sch, m);
        Test.stopTest();

        Datetime t = Datetime.now().addMinutes(30);
        String rescheduledJobCron = '0 ' 
                                + String.valueOf(t.minute()) 
                                + ' ' 
                                + String.valueOf(t.hour()) 
                                + ' ' 
                                + String.valueOf(t.day()) 
                                + ' ' 
                                + String.valueOf(t.month()) 
                                + ' ? ' 
                                + String.valueOf(t.year());     
        //this will fail if rescheduled part is not working.
        CronTrigger rescheduledJob = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                                    FROM CronTrigger
                                    WHERE CronExpression = :rescheduledJobCron];
                           

        //NOTE: There is no way to assert anything. TimesTrigger is not incrementing bcs CronTrigger is not fired which means value is not updated even thou scheduled class fires. 
        //and while there is no record update I don't have anything to assert really.
    }

    @isTest static void testRemovingCompletedRescheduledJobs() {
        Test.startTest();
        HOG_VGEmailNotificationsSchedulable m = new HOG_VGEmailNotificationsSchedulable();
        String sch = '0 0 8 * * ?';
        String jobID = system.schedule(HOG_VGEmailNotificationsSchedulable.JOB_NAME, sch, m);

        m = new HOG_VGEmailNotificationsSchedulable();
        String schRsch = '0 30 8 * * ?';
        String jobIDRsch = system.schedule(HOG_VGEmailNotificationsSchedulable.RESHEDULED_JOB_NAME, schRsch, m);
        Test.stopTest();

        //NOTE: There is no way to assert anything. TimesTrigger is not incrementing bcs CronTrigger is not fired which means value is not updated even thou scheduled class fires. 
        //and while there is no record update I don't have anything to assert really.
        // plus while there is no cronTrigger table update which means that rescheduled job runs without setting DELETED state on itself after finish, I don't have ability to test
        // and cover System.abortJob part.
    }
    
    @isTest static void testDirectSheduledRun() {
        Test.startTest();
        HOG_VGEmailNotificationsSchedulable m = new HOG_VGEmailNotificationsSchedulable();
        String sch = '0 0 8 * * ?';
        String jobID = system.schedule(HOG_VGEmailNotificationsSchedulable.JOB_NAME, sch, m);
        Test.stopTest();

        //NOTE: There is no way to assert anything. TimesTrigger is not incrementing bcs CronTrigger is not fired which means value is not updated even thou scheduled class fires. 
        //and while there is no record update I don't have anything to assert really.
    }
    
}