global class PurgeVolumeWeeklyBatchSchedule implements Database.Batchable<SObject>, Schedulable, Database.Stateful
{
	
/*******************************************************************************************************
*******************************************************************************************************
*******************************************************************************************************
NOTE: 
For now the result emails are sent to Chris Sembaluk
SELECT Email FROM User WHERE Profile.Name = 'System Administrator' And IsActive = true AND FirstName = 'Chris' AND Lastname = 'Sembaluk'
Need to make sure that clause is not there when rolled into production.
*******************************************************************************************************
*******************************************************************************************************
*******************************************************************************************************/
	
	// 2013-09-06: logic has changed so now it will read the custom settings to see how many weeks of volume weekly
	// data to keep.  That is, the data to be purged is not based on a date datatype, but rather a number representing
	// the number of weeks of weekly data to keep, coming in from the custom settings.
	
	global Integer numWeeksOfWeeklyDataToKeep;
	global List<Date> datesToDelete = new List<Date>();
	@TestVisible private static Boolean isMissingWeekEMailsSent = false;
	@TestVisible private static Boolean isPurgeSuccessEMailSent = false;

	global PurgeVolumeWeeklyBatchSchedule()
	{
		Cardlock_Settings__c cs = Cardlock_Settings__c.getOrgDefaults();
		numWeeksOfWeeklyDataToKeep = (Integer)cs.Num_Weeks_Of_Weekly_Data_To_Keep__c;
		
		// first get a list of distinct transaction week ending date from volume transaction file.
		// Set it to return in descending order
		List<AggregateResult> distinctWeekInWeeklyVolume = new List<AggregateResult>();
		for (List<AggregateResult> temp : [SELECT Transaction_Week_Ending_Date__c 
			FROM Volume_Transaction__c GROUP BY Transaction_Week_Ending_Date__c ORDER BY Transaction_Week_Ending_Date__c DESC])
			{
				distinctWeekInWeeklyVolume = temp;
			}

		// go through each of them and ensure there is no more than one week of data that is not summarized.
		List<Date> datesNotInSummary = new List<Date>();
		List<Date> datesInSummary = new List<Date>();
		Date checkDate;
		for (AggregateResult tmpDate : distinctWeekInWeeklyVolume)
		{
			checkDate = (Date)tmpDate.get('Transaction_Week_Ending_Date__c');
			List<Volume_Transaction_Summary__c> tempVtsList = new List<Volume_Transaction_Summary__c>();
			tempVtsList = [SELECT Id FROM Volume_Transaction_Summary__c WHERE Transaction_Week_Ending_Date__c = :checkDate LIMIT 1];
			if (tempVtsList.size() == 0)
			{
				datesNotInSummary.add(checkDate);
			}
			else
			{
				datesInSummary.add(checkDate);
			}
		}
		
		// if there are more than one week of weekly data that has not been summarized, then the system admin needs to be notified.
		if (datesNotInSummary.size() > 1)
		{
	    	String sysMessage = '';
	    	sysMessage += 'Dear Sir/Madam\n\r';
	    	sysMessage += 'This is a message generated from batch job PurgeVolumeWeeklyBatchSchedule.\n\r';
	    	sysMessage += 'It has been noticed that more than one week of Volume Weekly data has not been processed yet.\n\r';
	    	sysMessage += 'Please execute the batch job for these dates as soon as possible, as the volume weekly data takes up a lot of disk space:\n\r';
	    	for (Date tmpDate : datesNotInSummary)
	    	{
	    		sysMessage += '- ' + Datetime.newInstance(tmpDate, time.newinstance(0,0,0,0)).format('yyyy-MM-dd') + '\n';
	    	}
	        sysMessage += '\rThank you very much.\n\r';
	        sysMessage += 'Husky Retail Team';
	        
	        List<String> systemAdmins = new List<String>();
	        for (List<User> users : [ SELECT Email FROM User WHERE Profile.Name = 'System Administrator' And IsActive = true AND FirstName = 'Chris' AND Lastname = 'Sembaluk'])
	        {
	            for (User user: users)
	            {
	                systemAdmins.add(user.Email);
	            }
	        }
	        if (systemAdmins.size() > 0)
	        {
		        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
		        mail.setToAddresses(systemAdmins);
		        mail.setSenderDisplayName('Husky Retail Team');
		        mail.setSubject('URGENT:  Purge Volume Weekly Batch Schedule notifications');
		        mail.setPlainTextBody(sysMessage);
		        mail.setBccSender(false);
		        mail.setSaveAsActivity(false);
		        mail.setUseSignature(false);
		        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		        PurgeVolumeWeeklyBatchSchedule.isMissingWeekEMailsSent = true;
	        }
		}
		
		if (datesInSummary.size() > this.numWeeksOfWeeklyDataToKeep)
		{
			// datesInSummary is constructed such that they are in descending order.
			// If the list is longer than n, then we should delete data related to datesInSummary[n] and after.
			datesToDelete = datesInSummary;
			for (Integer i = 0 ; i < this.numWeeksOfWeeklyDataToKeep; i++)
			{
				datesToDelete.remove(0);
			}
		} 
	}
	
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([SELECT Id FROM Volume_Transaction__c WHERE 
        	Transaction_Week_Ending_Date__c IN : this.datesToDelete]);
    }
    
    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
    	List<Volume_Transaction__c> vtlist = new List<Volume_Transaction__c>();
    	for (SObject s : scope)
    	{
    		Volume_Transaction__c vt = (Volume_Transaction__c)s;
    		vtlist.add(vt);
    	}
    	delete vtlist;
    }
    
    global void finish(Database.BatchableContext BC)
    {
    	String sysMessage = '';
    	sysMessage += 'Dear Sir/Madam\n\r';
    	sysMessage += 'This is a message generated from batch job PurgeVolumeWeeklyBatchSchedule.\n\r';
    	sysMessage += 'The job has been executed successfully.\n\r';
        sysMessage += 'Thank you very much.\n\r';
        sysMessage += 'Husky Retail Team';
        
        List<String> systemAdmins = new List<String>();
        for (List<User> users : [ SELECT Email FROM User WHERE Profile.Name = 'System Administrator' And IsActive = true AND FirstName = 'Chris' AND Lastname = 'Sembaluk'])
        {
            for (User user: users)
            {
                systemAdmins.add(user.Email);
            }
        }
        if (systemAdmins.size() > 0)
        {
	        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
	        mail.setToAddresses(systemAdmins);
	        mail.setSenderDisplayName('Husky Retail Team');
	        mail.setSubject('Purge Volume Weekly Batch Schedule results');
	        mail.setPlainTextBody(sysMessage);
	        mail.setBccSender(false);
	        mail.setSaveAsActivity(false);
	        mail.setUseSignature(false);
	        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	        PurgeVolumeWeeklyBatchSchedule.isPurgeSuccessEMailSent = true;
        }
    }

	// This is for the scheduling
    global void execute(SchedulableContext SC)
    {
        PurgeVolumeWeeklyBatchSchedule b = new PurgeVolumeWeeklyBatchSchedule();
        Database.executeBatch(b);
    }
    

}