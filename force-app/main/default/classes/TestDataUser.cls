@isTest
public class TestDataUser {
	public static User createAdmin(String role) {
		User user = new User();
        Profile p = [Select Id from Profile where Name='System Administrator'];
        
        Double random = Math.Random();
        
        user.Email = 'pb.pb' +  random + '@shield.com';
        user.Alias = 'eyepatch' ;
        user.EmailEncodingKey = 'UTF-8';
        user.LastName = 'PB';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        user.ProfileId = p.Id;        
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.UserName = 'pb.pb' + random + '@shield.com.avengers';
        
        if(role != 'default'){
			user.UserRoleId = role;
        }
        
        insert user;
        return user;
	}
}