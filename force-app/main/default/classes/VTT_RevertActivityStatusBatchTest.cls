@isTest
public class VTT_RevertActivityStatusBatchTest {

	static testMethod void whenActivityIn2SCHFIX_batchWillRevertItTo1RTS() {
		//Conduct Test
		Test.startTest();
			VTT_RevertActivityStatusBatch activityTest = new VTT_RevertActivityStatusBatch();
			Database.executeBatch(activityTest,200);
		Test.stopTest();

		// Verify
		List<Work_Order_Activity__c> activities = [SELECT Id, Name, Operating_Field_AMU__c, Operating_Field_AMU__r.Is_Thermal__c,
														Status__c, Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c,
														Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c,
														Operation_Number__c, Sub_Operation_Number__c, User_Status__c, Description__c,
														Assigned_Text__c, Assigned_Vendor__c, 
														Maintenance_Work_Order__r.Planner_Group__c, Work_Center__c, 
														Maintenance_Work_Order__r.Work_Order_Number__c 
													FROM Work_Order_Activity__c LIMIT 50];
		System.debug('DEBUG activities ' + activities);
		for(Work_Order_Activity__c wo: activities){
			System.assertEquals('1RTS', wo.User_Status__c, 'Incorrect user status on activity');
			System.assertEquals('New', wo.Status__c, 'Incorrect status on activity');
		}
	}

	static testMethod void whenBatchFailsAndTimeIsAfterLimitForMoreThen20Activities_itWillSendAnEmail() {
		List<Work_Order_Activity__c> activities = [SELECT Id, Name, Operating_Field_AMU__c, Operating_Field_AMU__r.Is_Thermal__c,
															Status__c, Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c,
															Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c,
															Operation_Number__c, Sub_Operation_Number__c, User_Status__c, Description__c,
															Maintenance_Work_Order__r.Planner_Group__c, Work_Center__c, Assigned_Text__c, 
															Assigned_Vendor__c, 
															Maintenance_Work_Order__r.Work_Order_Number__c, Maintenance_Work_Order__r.Order_Type__c 
													FROM Work_Order_Activity__c LIMIT 50];

		//Conduct Test
		Test.startTest();
			VTT_RevertActivityStatusBatch activityTest = new VTT_RevertActivityStatusBatch();
			activityTest.retryRequired = true;
			activityTest.currentHour = 4;
			activityTest.failedActivities = activities;
			Database.executeBatch(activityTest,200);
		Test.stopTest();

	}

	static testMethod void whenBatchFailsAndTimeIsAfterLimitForLessThen20Activities_itWillSendAnEmail() {
		List<Work_Order_Activity__c> activities = [SELECT Id, Name, Operating_Field_AMU__c, Operating_Field_AMU__r.Is_Thermal__c,
															Status__c, Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c,
															Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c,
															Operation_Number__c, Sub_Operation_Number__c, User_Status__c, Description__c,
															Maintenance_Work_Order__r.Planner_Group__c, Work_Center__c, Assigned_Text__c, 
															Assigned_Vendor__c, 
															Maintenance_Work_Order__r.Work_Order_Number__c, Maintenance_Work_Order__r.Order_Type__c 
													FROM Work_Order_Activity__c LIMIT 10];

		//Conduct Test
		Test.startTest();
			VTT_RevertActivityStatusBatch activityTest = new VTT_RevertActivityStatusBatch();
			activityTest.retryRequired = true;
			activityTest.currentHour = 4;
			activityTest.failedActivities = activities;
			Database.executeBatch(activityTest,200);
		Test.stopTest();

	}

	static testMethod void whenBatchFailsAndNeedRetry_itWillRecheduleAnotherRun() {
		//Conduct Test
		Test.startTest();
			VTT_RevertActivityStatusBatch activityTest = new VTT_RevertActivityStatusBatch();
			activityTest.retryRequired = true;
			activityTest.currentHour = 1;
			Database.executeBatch(activityTest,200);
		Test.stopTest();

	}
	
	@testSetup static void setupTestData() {

		Account vendor1 = VTT_TestData.createVendorAccount('Vendor1');
		Contact tradesman1 = VTT_TestData.createTradesmanContact('Tom', 'Cruise',  vendor1.id, UserInfo.getUserID());
		
		MaintenanceServicingUtilities.executeTriggerCode = false; 		
		HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest();       
        
        // Thermal 
        Business_Unit__c businessUnit = new Business_Unit__c(Name = 'Test Business Unit');
        insert businessUnit;

        Business_Department__c businessDepartment = BusinessDepartmentTestData.createBusinessDepartment('Test Business Department');    
        insert businessDepartment;

        Operating_District__c operatingDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Field', businessDepartment.Id, businessUnit.Id);
        insert operatingDistrict;

        Id recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;
        Field__c field = FieldTestData.createField('Test Field Control Centre', operatingDistrict.Id, recordTypeId);
        field.Planner_Group__c = '634';
        insert field; 


        HOG_Maintenance_Servicing_Form__c workOrder1 = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
		 workOrder1.Planner_Group__c = '634';
		 update workOrder1;
        // Create work order activities and assignements
        List<Work_Order_Activity__c> activityList1 = VTT_TestData.createWorkOrderActivitiesWithAssignments(workOrder1.ID, tradesman1.ID, 50);
        
        // Set correct status and make it Thermal Actvitity
        for(Work_Order_Activity__c woa: activityList1){
        	woa.User_Status__c = '2SCH FIX';
        	woa.Status__c = VTT_Utilities.ACTIVITY_STATUS_SCHEDULED;
        	woa.Operating_Field_AMU__c = field.Id;
        	woa.Status__c = VTT_Utilities.ACTIVITY_STATUS_SCHEDULED;
        	woa.Scheduled_Start_Date__c = Date.today().addDays(-4);
        }

        update activityList1;

        HOG_Planner_Group__c plannerGroup = VTT_TestData.createHogPlannerGroup('634');
        HOG_Planner_Group_Notification_Type__c plannerGroupNotificationType2 = VTT_TestData.createHogPlannerGroupNotificationType(plannerGroup.Id, null, null,'Field Scheduler', tradesman1.Id);
	}
}