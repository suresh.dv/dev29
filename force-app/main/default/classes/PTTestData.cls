@isTest
public class PTTestData {

    public static void createPTTestRecords(){
        Field__c amu = new Field__c();//Instace AMU object
        Utility_Requests__c utilityRequest = new Utility_Requests__c();//Instance Utility Request object
        Operating_District__c operatingDistrict = new Operating_District__c();//Instance Operating District object
        Utility_Meter__c utilityMeter = new Utility_Meter__c();//Instance Utility Meter object
        Service__c service = new Service__c();//Instance Service object
        Business_unit__c businessUnit = new Business_unit__c();//Instance of Business Unit object
        Business_department__c businessDepartment = new Business_department__c();//Instance of Business Department object
        Activiy_Tracking__c activityTracking = new Activiy_Tracking__c();
        businessUnit.Name = 'CWCC000001 - Northwest';
        insert businessUnit;
        //Creation of Business Department Record
        String recordTypeBusinessDepartment = [Select id from recordtype where developername='Power_Tracker_Business_Departments'].id;
        businessDepartment.RecordTypeId = recordTypeBusinessDepartment;
        businessDepartment.Name = 'CWCC000001 - Northwest';
        String businessUnitId=[select id from Business_Unit__c where name = 'CWCC000001 - Northwest' Limit 1].id;
        businessDepartment.Business_Unit__c = businessUnitId;
        insert businessDepartment;
        //Creation of Operating District Record
        String recordTypeOperatingDistrict = [Select id from recordtype where developername='Power_Tracker_Operating_District'].id;
        operatingDistrict.RecordTypeId = recordTypeOperatingDistrict;
        operatingDistrict.Name = 'CWCC000007 - Athabasca';
        String businessDepartmentId=[select id from Business_Department__c where name = 'CWCC000001 - Northwest' Limit 1].id;
        operatingDistrict.Business_Department__c  =businessDepartmentId;
        operatingDistrict.Business_Unit__c = businessUnitId;
        insert operatingDistrict;
        //Creation of AMU
        String recordTypeAmu = [Select id from recordtype where developername='Power_Tracker_AMU'].id;
        amu.RecordTypeId = recordTypeAmu;
        amu.Name = 'Redwater Oil Bty - Oil Res. - CWCC000541';
        String operatingDistrictId = [select id from Operating_District__c where name='CWCC000007 - Athabasca' Limit 1].id;
        amu.Operating_District__c = operatingDistrictId;
        insert amu;
        //Creation of Service
        service.Term__c = '05 Years';
        service.Contract_Start_Date__c = date.parse('08/03/2016');
        service.Site_Id__c = '0040001421539';
        service.LSD_id__c = '11-05-048-21W3';
        String amuId=[Select id from field__c where name = 'Redwater Oil Bty - Oil Res. - CWCC000541' Limit 1].id;
        service.AMU_id__c = amuId;
        service.Site_Name__c = 'Redwater';
        service.Service_Status__c='Active';
        insert service;
        //Creation of Meter
        String serviceId = [Select id from service__c where site_id__c='0040001421539' Limit 1].id;
        utilityMeter.Name = '000000000001800314';
        utilityMeter.Service_id__c = serviceId;
        insert utilityMeter;
        //Creation of Request
        utilityRequest.Utility_Ref_Num__c = 'TSTGDLT1';
        utilityRequest.Utility_Request_Type__c = 'New Service - Firm';
        utilityRequest.Request_Status__c = 'Quotation';
        utilityRequest.Required_Date__c = date.parse('03/12/2016');
        utilityRequest.service_id__c = serviceId;
        insert utilityRequest;
        //Creation of Activity Tracking
        activityTracking.Request_Date__c = date.parse('03/12/2016');
		activityTracking.Date_Type__c = 'Project request Date';
		String utilityRequestId = [Select id from Utility_Requests__c where service_id__c=:serviceId Limit 1].id;
		activityTracking.Utility_Request_id__c = utilityRequestId;
		insert activityTracking;
  }
    public Static void createZMBillingTestRecords(){
     	Utility_Rate__c ura = new Utility_Rate__c();
     	String seid = [Select id from service__C where site_id__c = '0040001421539' Limit 1].id;
     	//Creation of Utility Rate
     	ura.Name = 'D32';
     	ura.utility_rate_description__c = 'Rate Description test';
     	insert ura;
     	//Creation of Utility Billings
     	String uraid = [Select id from utility_rate__c where name='D32' Limit 1].id;
        InsertBillingTestRecords('0040001421539',uraid,0.00,0.00,39.6,1012.83,0.00,date.parse('01/01/2016'),500.35,600.75);
        InsertBillingTestRecords('0040001421539',uraid,0.00,0.00,39.6,1082.68,0.00,date.parse('01/02/2016'),500.35,600.75);
        InsertBillingTestRecords('0040001421539',uraid,0.00,0.00,39.6,1044.59,0.00,date.parse('01/03/2016'),500.35,600.75);
    }
        public Static void createPFBillingTestRecords(){
     	Utility_Rate__c ura = new Utility_Rate__c();
     	String seid = [Select id from service__C where site_id__c='0040001421539' Limit 1].id;
     	//Creation of Utility Rate
     	ura.Name='D33';
     	ura.utility_rate_description__c = 'Rate Description test2';
     	insert ura;
     	//Creation of Utility Billings
     	String uraid = [Select id from utility_rate__c where name='D33' Limit 1].id;
        InsertBillingTestRecords('0040001421539',uraid,1230.72,1565.00,1443.84,26131.71,13000.00,date.parse('01/04/2016'),500.35,600.75);
        InsertBillingTestRecords('0040001421539',uraid,1254.72,1588.93,1470.72,24671.40,13000.00,date.parse('01/05/2016'),500.35,600.75);
		InsertBillingTestRecords('0040001421539',uraid,1256.16,1585.08,1443.84,26459.64,13000.00,date.parse('01/06/2016'),500.35,600.75);            

     }
    public static void createVBBillingTestRecords(){
        //Create new account
        Account utilityCompany = new Account();
        utilityCompany.name = 'ATCO Electric';
        insert utilityCompany;       	        
     	String seid = [Select id from service__C where site_id__c='0040001421539' Limit 1].id;
    	//Creation of Utility Rate
    	Utility_Rate__c ura = new Utility_Rate__c();
     	ura.Name = 'D34';
     	ura.utility_rate_description__c = 'Rate Description test3';
        ura.Rate_Minimum__c = 0;
     	insert ura;     	
     	String uraid = [Select id from utility_rate__c where name='D34' Limit 1].id;
        //Update service
        Service__c serviceUpdate = new Service__c();
        String aid = [Select id from account where name = 'ATCO Electric' limit 1].id;
        serviceUpdate.id = seid;
        serviceUpdate.Utility_Company__c = aid;
        serviceUpdate.utility_rate_id__c = uraid;
        serviceUpdate.Minimum_Contract_Demand__c = 0;
        update serviceUpdate;
        //Creation of Utility Billings
        InsertBillingTestRecords('0040001421539',uraid,1230.72,1565.00,1443.84,26131.71,13000.00,date.parse('01/01/2017'),500.35,600.75);
    }
    
    public static void createVBBillingTestRecord2(){
        //Create new account
        Account utilityCompany = new Account();
        utilityCompany.name = 'FortisAlberta';
        insert utilityCompany;          
        String seid = [Select id from service__C where site_id__c='0040001421539' Limit 1].id;
        //Update service
        Service__c serviceUpdate = new Service__c();
        String aid = [Select id from account where name = 'FortisAlberta' limit 1].id;
        serviceUpdate.Id = seid;
        serviceUpdate.Utility_Company__c = aid;
        update serviceUpdate;
        //Creation of Utility Billings
        Utility_Rate__c ura = new Utility_Rate__c();
        String uraid = [Select id from utility_rate__c where name='D34' Limit 1].id;
        InsertBillingTestRecords('0040001421539',uraid,1254.72,1588.93,1470.72,24671.40,13000.00,date.parse('01/02/2017'),500.35,600.75);
    }
    
    public static void createVBBillingTestRecord3(){
        String seid = [Select id from service__C where site_id__c='0040001421539' Limit 1].id;
        String uraid = [Select id from utility_rate__c where name='D34' Limit 1].id;
        //Creation of Utility Billings
        InsertBillingTestRecords('0040001421539',uraid,0.00,1565.00,10.00,0.00,13000.00,date.parse('01/03/2017'),500.35,600.75);
    }    
    
        public static void createNEWVBBillingTestRecords(){
     	Utility_Rate__c ura = new Utility_Rate__c();
     	String seid = [Select id from service__C where site_id__c='0040001421539' Limit 1].id;
     	//Creation of Utility Billings
     	String uraid = [Select id from utility_rate__c where name='D34' Limit 1].id;
        InsertBillingTestRecords('0040001421539',uraid,0.00,1565.00,0.00,0.00,13000.00,date.parse('01/01/2017'),500.35,600.75);
        InsertBillingTestRecords('0040001421539',uraid,1254.72,1588.93,1470.72,24671.40,13000.00,date.parse('01/02/2017'),500.35,600.75);
    }
    
    public static void createRCMBillingTestRecords(){
        Utility_Rate__c ura = new Utility_Rate__c();
        String seid = [Select id from service__C where site_id__c='0040001421539' Limit 1].id;
        //Creation of Utility Rate
        ura.Name = 'D35';
        ura.utility_rate_description__c = 'Rate Description test4';
        insert ura;
        //Creation of Utility Billings
        String uraid = [Select id from utility_rate__c where name='D35' Limit 1].id;
        InsertBillingTestRecords('0040001421539',uraid,1254.72,1470.72,1588.93,24671.40,13000.00,date.parse('01/01/2016'),500.35,600.75);
        InsertBillingTestRecords('0040001421539',uraid,1254.72,1470.72,1588.93,24671.40,13000.00,date.parse('01/02/2016'),500.35,600.75);
		InsertBillingTestRecords('0040001421539',uraid,1254.72,1470.72,1588.93,24671.40,13000.00,date.parse('01/03/2016'),500.35,600.75);
        InsertBillingTestRecords('0040001421539',uraid,1254.72,1470.72,1588.93,24671.40,13000.00,date.parse('01/04/2016'),500.35,600.75);
    }
    
    public static void testZMPCSOImplemented(){
        Utility_Rate__c ura = new Utility_Rate__c();
        String seid = [Select id from service__C where site_id__c = '0040001421539' Limit 1].id;
     	ura.Name = 'D32';
     	ura.utility_rate_description__c = 'Rate Description test';
     	insert ura;          
        String uraid = [Select id from utility_rate__c where name='D32' Limit 1].id;
		InsertBillingTestRecords('0040001421539',uraid,0.00,0.00,39.6,1012.83,0.00,date.parse('01/01/2016'),500.35,600.75);
        InsertBillingTestRecords('0040001421539',uraid,0.00,0.00,39.6,1082.68,0.00,date.parse('01/02/2016'),500.35,600.75);
        InsertBillingTestRecords('0040001421539',uraid,0.00,0.00,39.6,1044.59,0.00,date.parse('01/03/2016'),500.35,600.75);
		InsertPCSO(seid,'Idle/Salvage','Implemented','Valid',null,null,null,null,0);
        InsertPCSO(seid,'Idle/Salvage','Created','Valid',null,null,null,null,0);                
    }
    
    public static void testRCMPCSOImplemented(){
        Utility_Rate__c ura = new Utility_Rate__c();
        String seid = [Select id from service__C where site_id__c = '0040001421539' Limit 1].id;
     	ura.Name = 'D32';
     	ura.utility_rate_description__c = 'Rate Description test';
     	insert ura;        
        String uraid = [Select id from utility_rate__c where name='D32' Limit 1].id;
		InsertBillingTestRecords('0040001421539',uraid,0.00,0.00,39.6,1012.83,0.00,date.parse('01/01/2016'),500.35,600.75);
        InsertBillingTestRecords('0040001421539',uraid,0.00,0.00,39.6,1082.68,0.00,date.parse('01/02/2016'),500.35,600.75);
        InsertBillingTestRecords('0040001421539',uraid,0.00,0.00,39.6,1044.59,0.00,date.parse('01/03/2016'),500.35,600.75);
		InsertPCSO(seid,'Review Contract Minimum','Implemented','Valid',null,null,null,null,0);
        InsertPCSO(seid,'Review Contract Minimum','Created','Valid',null,null,null,null,0);                
    }
    
    public static void testPFPCSOImplemented(){
        Utility_Rate__c ura = new Utility_Rate__c();
        String seid = [Select id from service__C where site_id__c = '0040001421539' Limit 1].id;
     	ura.Name = 'D32';
     	ura.utility_rate_description__c = 'Rate Description test';
     	insert ura;          
        String uraid = [Select id from utility_rate__c where name='D32' Limit 1].id;
        InsertBillingTestRecords('0040001421539',uraid,0.00,0.00,39.6,1012.83,0.00,date.parse('01/01/2016'),500.35,600.75);
        InsertBillingTestRecords('0040001421539',uraid,0.00,0.00,39.6,1082.68,0.00,date.parse('01/02/2016'),500.35,600.75);
        InsertBillingTestRecords('0040001421539',uraid,0.00,0.00,39.6,1044.59,0.00,date.parse('01/03/2016'),500.35,600.75);
        InsertPCSO(seid,'Power Factor Review','Implemented','Valid',null,null,null,null,0);
        InsertPCSO(seid,'Power Factor Review','Created','Valid',null,null,null,null,0);            
    }
    
    public static void CreateInvalidPCSOTestRecord(String rate){
        String seid = [Select id from service__C where site_id__c='0040001421539' Limit 1].id;
        //Creation of Utility Billings
        String uraid = [Select id from utility_rate__c where name=:rate Limit 1].id;
        InsertBillingTestRecords('0040001421539',uraid,1443.84,1443.84,1443.84,26131.71,13000.00,date.parse('01/12/2017'),500.35,600.75);
    }
    
    public static void TestUtilityMeterInactivation(String utilityName){
        Utility_Meter__c utilityMeter = new Utility_Meter__c();
        String seid = [Select id from service__c where site_id__c='0040001421539' Limit 1].id;
        utilityMeter.Service_id__c= seid;
        utilityMeter.Name=utilityName;
        utilityMeter.Meter_Status__c='Active';
        insert utilityMeter;
    }
    
    public static void TestDuplicateSiteId(){
        Service__c duplicateSiteId = new Service__c();
        duplicateSiteId.Site_Id__c = '0040001421539';
        insert duplicateSiteId;
    }
     
    public static void TestModifyingServiceDuplicateSiteID(){
      	Service__c newService = new Service__c();
        newService.Site_Id__c = '1140001421539';
        insert newService;
        
        Service__c updateService = new Service__c();
        String seid = [Select id from service__c where site_id__c='1140001421539' Limit 1].id;
        updateService.Id = seid;
        updateService.Site_Id__c = '0040001421539';
        update updateService;
    }
    
        public static void testBillingRateCheck(){
        Utility_Rate__c ura = new Utility_Rate__c();
        Utility_Rate__c ura2 = new Utility_Rate__c();        
     	ura.Name = 'D32';
     	ura.utility_rate_description__c = 'Rate Description test';
     	insert ura;
     	ura2.Name = 'D33';
     	ura2.utility_rate_description__c = 'Rate Description test2';
     	insert ura2;            
        String uraid = [Select id from utility_rate__c where name='D32' Limit 1].id;
        String uraid2 = [Select id from utility_rate__c where name='D33' Limit 1].id;
        Service__c seid = [Select id from service__C where site_id__c = '0040001421539' Limit 1];
        seid.utility_rate_id__c = uraid;
        update seid;            
		InsertBillingTestRecords('0040001421539',uraid,0.00,0.00,39.6,1012.83,0.00,date.parse('01/01/2016'),500.35,600.75);
        InsertBillingTestRecords('0040001421539',uraid2,0.00,0.00,39.6,1082.68,0.00,date.parse('01/02/2016'),500.35,600.75);
        InsertBillingTestRecords('0040001421539',uraid,0.00,0.00,39.6,1044.59,0.00,date.parse('01/03/2016'),500.35,600.75);               
    }

    public static void ControllerCreateCustomSetting(String name,string requestStatus,string cType){
        if (PT_RequestController_Settings__c.getInstance(name) == null){
        	PT_RequestController_Settings__c controllerCS = new PT_RequestController_Settings__c();
        	controllerCS.name = name;
        	controllerCS.requestStatusController__c = requestStatus;
        	controllerCS.Type__c = cType;
        	insert controllerCS;
        }
    }
    
    public static void PCSOCreateCustomSetting(String name,decimal PCSOValue){
        if (PT_PCSO_Settings__c.getInstance(name) == null){
            PT_PCSO_Settings__c PCSOCS = new PT_PCSO_Settings__c();
            PCSOCS.name = name;
            PCSOCS.PCSO_Value__c = PCSOValue;
            insert PCSOCS;
        }
    }
    
    public static void createPTauditControllerRecords(){
        string abRecordTypeId = [select id from recordtype where developername='AB_Field_Report_Record'].id;
        String skRecordTypeId = [select id from recordtype where developername='SK_Field_Report_Record'].id;
        Field_Report_Record__c abFRR = new Field_Report_Record__c();
        Field_Report_Record__c skFRR = new Field_Report_Record__c();
        
        abFRR.Business_Unit__c = 'CCDN000000 - Canadian Downstream';
        abFRR.Operating_District__c = 'CHLR000000 - HLR Marketing & Refining';
        abFRR.AMU__c = 'HLR Operations - CHLR000022';
        abFRR.Initial__c = date.parse('01/01/2016');
        abFRR.Interim__c = date.parse('02/01/2016');
        abFRR.Final__c = date.parse('03/01/2016');
        abFRR.Site_ID__c = '0010007660014';
        abFRR.UWI__c = '../52- ST- LLOYD.../..';
        abFRR.Cost_Center__c = '10063019';
        abFRR.Percentage__c = 100;
        abFRR.Initial_Energy_Dollars__c = 0;
        abFRR.Interim_Energy_Dollars__c = 0;
        abFRR.Final_Energy_Dollars__c = 0;
        abFRR.Wire_Dollars__c = 0;
        abFRR.PFAM_Dollars__c = 0;
        abFRR.Retail_Fee_Total_Dollars__c = 7.50;
        abFRR.Total_Cost__c = 7.50;
        abFRR.Rate_Code__c = 'D21';
        abFRR.Initial_Volume__c = 0;
        abFRR.Interim_Volume__c = 0;
        abFRR.Final_Volume__c = 0;
        abFRR.Total_Volume__c = 0;
        abFRR.RecordTypeId = abRecordTypeID;
        insert abFRR;
        
        skFRR.Business_Unit__c = 'CCDN000000 - Canadian Downstream';
        skFRR.Operating_District__c = 'CPIP000002 - Pipeline Op & Butane';
        skFRR.AMU__c = 'Big Gully Chops - CLYD000238';
        skFRR.Account_Group__c = 'Pipeline';
        skFRR.Site_ID__c = '500003794368';
        skFRR.UWI__c = '../14-27-049-25W3/..';
        skFRR.Cost_Center__c = '11064152';
        skFRR.Consumption_Month__c = date.parse('01/01/2016');
        skFRR.Start_Date__c = date.parse('02/01/2016');
        skFRR.End_Date__c = date.parse('03/01/2016');
        skFRR.Percentage__c = 100;
        skFRR.Demand_Charge_Dollars__c = 1;
        skFRR.Consumption_Charge_Dollars__c = 0 ;
        skFRR.Other_Dollars__c = 0;
        skFRR.PST_Dollars__c = 0;
        skFRR.Total_Dollars__c = 0;
        skFRR.Consumption__c = 0;
        skFRR.Billing_Demand__c = 0;
        skFRR.Metered_Demand__c = 0;
        skFRR.Rate_Code__c = 'E43';
        skFRR.File_Type__c = 'SPN';
        insert skFRR;
        
    }
    
    public static void CreatePCSOSnapshot(){
        String seid = [Select id from service__C where site_id__c = '0040001421539' Limit 1].id;
    	InsertPCSO(seid,'Power Factor Review','Implemented','Valid',null,null,null,null,0);  
    }
    
    public static void CreateBuydownFlagRecord(){
        String seid = [Select id from service__C where site_id__c = '0040001421539' Limit 1].id;
        InsertPCSO(seid,'Power Factor Review','Implemented','Valid',null,null,null,null,0);  
    }
    
    public static void UpdateBuydownFlagRecord(){
    	String pcsoId= [Select id from pcso__c order by createddate DESC limit 1].id;
        UpdatePCSO(pcsoId,100.00,50.00,100.00,50.00,'Created');
    }
    
    public static void CreatePCSOControllerRecords(){
    	integer count = 1500;
        List<PCSO__c> pcsoList = new List<PCSO__c>();
        String seid = [Select id from service__C where site_id__c = '0040001421539' Limit 1].id;
        for(integer i = 0; i<count; i++){
            PCSO__c pcso = new PCSO__c();
            pcso.Service_id__c = seid;
            pcso.PCSO_type__c = 'Power Factor Review';
            pcso.PCSO_Status__c = 'Created';
            pcso.System_Review__c = 'Valid';
            pcso.Cost_Saving__c = 100.00;
            pcso.Buydown_Cost__c = 50.00;
            pcso.Audited_Buydown_cost__c = 100.00;
            pcso.Audited_Cost_Saving__c = 50.00;
            pcsoList.add(pcso);
        }    
        insert pcsoList;
    }
    public static void CreatePCSOHistory(){
    	String pcsoId = [Select id from pcso__C where site_id__c = '0040001421539' Limit 1].id;        
        PCSO__history pcsoHistory = new PCSO__history(ParentId = pcsoId,field = 'Cost_Saving__c');
        insert pcsoHistory;
    }
    public static void ModifyPCSOSnapshot(){
    	String pcsoId= [Select id from pcso__c order by createddate DESC limit 1].id;
        PTTestData.PCSOSnapshotCreateCustomSetting('Created', '1P', '1 Created');
        UpdatePCSO(pcsoId,100.00,50.00,100.00,50.00,'Created');
		UpdatePCSO(pcsoId,80.00,30.00,30.00,80.00,'Created');         
        UpdatePCSO(pcsoId,80.00,30.00,30.00,80.00,'Working On');
        PTTestData.PCSOSnapshotCreateCustomSetting('Working On', '2P', '2 Working On');
        UpdatePCSO(pcsoId,null,null,null,null,'Working On');
        UpdatePCSO(pcsoId,80.00,30.00,30.00,80.00,'Working On');
    }
    
    public static void InsertBillingTestRecords(string siteId,string rateId, decimal meteredDemandKW,decimal meteredDemandKVA,decimal billingDemandKW,decimal wireCost,decimal totalVolume, date billingDate,decimal totalCost,decimal energyDollars){
        Utility_Billing__c ub = new Utility_Billing__c();
        ub.Site_ID__c = siteId;
        ub.Utility_Rate__c = rateId;
        ub.Metered_Demand_KW__c = meteredDemandKW;
        ub.Metered_Demand_KVA__c = meteredDemandKVA;
        ub.Billing_Demand_KW__c = billingDemandKW;
        ub.Wire_Cost__c = wireCost;
        ub.Total_Volume__c = totalVolume;
        ub.Billing_Date__c = billingDate;
        ub.Total_Cost__c = totalCost;
        ub.Energy_Dollars__c = energyDollars;
        insert ub;
    }
    
    public static void InsertPCSO(String serviceID, string pcsoType, string pcsoStatus, string systemReview,decimal costSaving,decimal buydownCost,decimal auditedCostSaving,decimal auditedBuydownCost,integer monthsValid){
        PCSO__c pcso = new PCSO__c();
        pcso.Service_id__c = serviceID;
        pcso.PCSO_type__c = pcsoType;
        pcso.PCSO_Status__c = pcsoStatus;
        pcso.System_Review__c = systemReview;
        pcso.Cost_Saving__c = costSaving;
        pcso.Buydown_Cost__c = buydownCost;
        pcso.Audited_Buydown_cost__c = auditedBuydownCost;
        pcso.Audited_Cost_Saving__c = auditedCostSaving;
        pcso.Implemented_Date__c = date.parse('01/01/2017');
        pcso.Months_Valid__c = monthsValid;        
        insert pcso;
    }
    
    public static void UpdatePCSO(id pcsoId,decimal costSaving,decimal buydownCost,decimal auditedCostSaving,decimal auditedBuydownCost,string pcsoStatus){
      	PCSO__c pcso = new PCSO__c();
        pcso.Id = pcsoId;
        pcso.Cost_Saving__c = costSaving;
        pcso.Buydown_Cost__c = buydownCost;
        pcso.Audited_Buydown_cost__c = auditedBuydownCost;
        pcso.Audited_Cost_Saving__c = auditedCostSaving;
        pcso.PCSO_Status__c = pcsoStatus;
        update pcso;
    }
    
    public static void PCSOSnapshotCreateCustomSetting(String name,String statusGrouping,String statusTranslation){
        if (PT_PCSO_Snapshot_Settings__c.getInstance(name) == null){
            PT_PCSO_Snapshot_Settings__c PCSOSnapshot = new PT_PCSO_Snapshot_Settings__c();
            PCSOSnapshot.name = name;
            PCSOSnapshot.Status_Grouping__c = statusGrouping;
            PCSOSnapshot.Status_Translation__c = statusTranslation;
            insert PCSOSnapshot;
        }
    }   
}