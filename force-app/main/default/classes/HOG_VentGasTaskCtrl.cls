/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller for Vent Gas Alert Task Functionality
Test class:     HOG_VentGasTaskCtrlTest
History:        jschn 04.23.2018 - Created.
**************************************************************************************************/
public with sharing class HOG_VentGasTaskCtrl {

    private String returnUrl;
    private String saveUrl;
    private String alertId;
    private String taskId;

    private Id routeOperator;
	
    public HOG_Vent_Gas_Alert_Task__c ventGasTask     {get;set;}

    public static final String ERROR_MSG_MISSING_PARAMS = 'You can create task only from Vent Gas Alert.';

    public Boolean editMode {get {
            return String.isNotBlank(taskId);
        }
        private set;
    }

    public HOG_VentGasTaskCtrl(ApexPages.StandardController stdController) {
        loadUrlParameters();
        if(!getIsMissingParameters()) {
            if(editMode)
                ventGasTask = getVentGasTask();
            else initVentGasTask();
        } else HOG_VentGas_Utilities.addErrorMsg(ERROR_MSG_MISSING_PARAMS);
    }

/**
INITIATIONS
*/
    /**
    * Loading URL parameters.
    */
    private void loadUrlParameters() {
        returnUrl   = HOG_VentGas_Utilities.getReturnUrlParameter();
        alertId     = HOG_VentGas_Utilities.getVentGasAlertParameter();
        taskId      = HOG_VentGas_Utilities.getVentGasTaskParameter();
        saveUrl     = HOG_VentGas_Utilities.getSaveUrlParameter();
    }

    /**
    * Initialize task with defaul values for Vent Gas Alert when creating new task.
    */
    private void initVentGasTask() {
        ventGasTask = new HOG_Vent_Gas_Alert_Task__c();
        ventGasTask.Status__c = HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED;
        ventGasTask.Vent_Gas_Alert__r = getVentGasAlert();
        ventGasTask.Remind__c = true;
        if(ventGasTask.Vent_Gas_Alert__r != null)
            initTaskValuesFromParent();
    }

    /**
    * Initialize task with values from Alert (parent)
    */
    private void initTaskValuesFromParent() {
        ventGasTask.Vent_Gas_Alert__c = ventGasTask.Vent_Gas_Alert__r.Id;
        ventGasTask.Priority__c = ventGasTask.Vent_Gas_Alert__r.Priority__c;
        ventGasTask.Name = ventGasTask.Vent_Gas_Alert__r.Well_Location__r.Name 
                            + ' - ' + ventGasTask.Vent_Gas_Alert__r.Type__c 
                            + ' - ' + String.valueOf(Date.today());
        routeOperator = HOG_VentGas_Utilities.getRouteOperatorUserId(ventGasTask.Vent_Gas_Alert__r.Well_Location__r.Route__c);
    }

/**
GETTERS
*/
    /**
    * Retrieve Vent Gas alert based on ventGasId variable loaded from URL Parameter.
    */
    private HOG_Vent_Gas_Alert__c getVentGasAlert() {
        List<HOG_Vent_Gas_Alert__c> alerts = HOG_VentGas_Utilities.retrieveAlert(alertId);
        return (HOG_Vent_Gas_Alert__c) HOG_VentGas_Utilities.getFirstOrNull(alerts);
    }

    /**
    * Retrieve Vent Gas Task based on taskId variable loaded from URL Parameter.
    */
    private HOG_Vent_Gas_Alert_Task__c getVentGasTask() {
        List<HOG_Vent_Gas_Alert_Task__c> tasks = HOG_VentGas_Utilities.retrieveTask(taskId);
        return (HOG_Vent_Gas_Alert_Task__c) HOG_VentGas_Utilities.getFirstOrNull(tasks);
    }

/**
PAGE ACTIONS
*/
    /**
    * Handle when assignee type is changed.
    * If assignee type is Route Operator prefill Assignee1 with Operator on call.
    */
    public void handleAssigneeTypeChange() {
        if(String.isNotBlank(ventGasTask.Assignee_Type__c))
            loadAssigneeBasedOnType(ventGasTask.Assignee_Type__c);
    }

    /**
    * Save method.
    * Check if task is valid, then update related records(Alert), upsert task record and redirect if success.
    */
    public PageReference save() {
        if(isTaskValid()) {
            System.Savepoint sp = Database.setSavepoint();
            try{
                if(updateRelatedRecords()) {
                    upsert ventGasTask;
                    return redirectAfterSave();
                }
            } catch(System.DmlException ex) {
                Database.rollback(sp);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getDmlMessage(0)));
            }
        }
        return null;
    }

    /**
    * Redirect page. 
    * use returnUrl if present
    * otherwise redirect to task if edit mode
    * otherwise redirect to ventGas record.
    */
    public PageReference cancel() {
        PageReference pr;
        if(String.isNotBlank(returnUrl))    pr = new PageReference(returnUrl);
        else if(editMode)                   pr = new PageReference('/' + ventGasTask.Id);
        else                                pr = new PageReference('/' + alertId);
        pr.setRedirect(true);
        return pr;
    }

/**
HELPER METHODS
*/
    /**
    * Load route operator as Assignee1 if Assigne Type is Route Operator.
    */
    private void loadAssigneeBasedOnType(String type) {
        if(HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR.equals(ventGasTask.Assignee_Type__c))
            ventGasTask.Assignee1__c = routeOperator;
        else ventGasTask.Assignee1__c = null;
    }

    /**
    * Redirect page.
    * use save url if presnet
    * otherwise redirect to task if edit mode
    * otherwise redirect to ventGas record
    */
    private PageReference redirectAfterSave() {
        PageReference pr;
        if(String.isNotBlank(saveUrl))  pr = new PageReference(saveUrl);
        else if(editMode)               pr = new PageReference('/' + ventGasTask.Id);
        else                            pr = new PageReference('/' + ventGasTask.Vent_Gas_Alert__c);
        pr.setRedirect(true);
        return pr;
    }

    /**
    * Update related records.
    * only related record is Alert. if update unsuccessful add error msg to page.
    */
    private Boolean updateRelatedRecords() {
        Boolean updated = updateVentGasAlert();
        if (!updated) HOG_VentGas_Utilities.addErrorMsg('Unable to update Vent Gas Alert.');
        return updated;
    }

    /**
    * Update Vent Gas alert.
    * If parent vent gas alert has status not started update it to in progress. 
    */
    private Boolean updateVentGasAlert() {
        if (ventGasTask.Vent_Gas_Alert__r != null) {
            if(isAlertNotStarted(ventGasTask.Vent_Gas_Alert__r)) {
                ventGasTask.Vent_Gas_Alert__r.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS;
                update ventGasTask.Vent_Gas_Alert__r;
            }
            return true;
        }
        return false;
    }

/**
VALIDATIONS
*/
    /**
    * Task validation. 
    * Check required fields.
    */
    private Boolean isTaskValid() {
        return requiredFieldsAreFilled();
    }

    /**
    * Check required fields.
    * Is Assign to set validation
    * Is Assignee Type set.
    */
    private Boolean requiredFieldsAreFilled() {
        return isAssignToSet()
            && isAssigneeTypeSet();
    }

    /**
    * Check if assignee type set. 
    * If no, put error msg.
    */
    private Boolean isAssigneeTypeSet() {
        Boolean valid = String.isNotBlank(ventGasTask.Assignee_Type__c);
        if (!valid) HOG_VentGas_Utilities.addErrorMsg('You have to set Assignee Type.');
        return valid;
    }

    /**
    * Check is assignee set.
    * If no, put error msg.
    */
    private Boolean isAssignToSet() {
        Boolean valid = String.isNotBlank(ventGasTask.Assignee1__c);
        if (!valid) HOG_VentGas_Utilities.addErrorMsg('You have to set Assignee');
        return valid;
    }

    /**
    * Check if alert has Not Started status.
    */
    private Boolean isAlertNotStarted(HOG_Vent_Gas_Alert__c alert) {
        return HOG_VentGas_Utilities.isAlertNotStarted(alert);
    }

    /**
    * Check if all required parameters are populated.
    * Restrict to create new tasks from other place then Vent Gas Alert.
    */
    public Boolean getIsMissingParameters() {
        return String.isBlank(alertId) && String.isBlank(taskId);
    }


}