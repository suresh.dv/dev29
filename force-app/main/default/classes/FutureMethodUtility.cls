public class FutureMethodUtility {

    @future
    public static void createAccountShare(List<Id> UserIdList, List<Id> AccountIdList) {
       
       List<AccountShare> accountShareToCreate = new List<AccountShare>();
       
       for(Integer i = 0; i < UserIdList.size(); i++) {
           AccountShare accountShare = new AccountShare();
           accountShare.AccountId = AccountIdList[i];
           accountShare.UserOrGroupId = UserIdList[i];
           accountShare.AccountAccessLevel = 'Read';
           accountShare.OpportunityAccessLevel = 'Read';
           accountShareToCreate.add(accountShare);
       }
       System.debug('accountSharetoCreate : ' +accountSharetoCreate);
       insert accountSharetoCreate;
    }
    
    /*
    @future
    public static void initiateEndorsementProcess(Set<Id> changeRequestIds) {
        List<Change_Request__c> changeRequests = new List<Change_Request__c>();
        
        Map<Id, CR_Endorsers__c> CRIdToEndorserMap = new Map<Id, CR_Endorsers__c>();
        
        for(Change_Request__c changeRequest : [SELECT Id, Change_Status__c, inFutureMethod__c FROM Change_Request__c WHERE Id in :changeRequestIds]) {
            changeRequest.Change_Status__c = 'Endorsement'; 
            changeRequest.inFutureMethod__c = True;
            changeRequests.add(changeRequest);
        }
        
        update changeRequests;
        System.debug('ChangeRequests : ' + changeRequests);
        
        for(CR_Endorsers__c endorser : [SELECT Id, Endorser__c, Order__c, isCurrentEndorser__c, Change_Request__c FROM CR_Endorsers__c WHERE Change_Request__c in :changeRequestIds
                                            AND isCurrentEndorser__c = False ORDER BY Order__c]) {
        
            CR_Endorsers__c endorserRec = CRIdToEndorserMap.get(endorser.Change_Request__c);
            
            if(endorserRec == Null) {
                CRIdToEndorserMap.put(endorser.Change_Request__c, endorser);
            }                                           
        }
        
        List<Approval.ProcessSubmitRequest> requestList = new List<Approval.ProcessSubmitRequest>();
        List<CR_Endorsers__c> endorsersToUpdate = new List<CR_Endorsers__c>();
        
        for(Id changeRequestId : changeRequestIds) {
            CR_Endorsers__c endorser = CRIdToEndorserMap.get(changeRequestId);
            Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
            request.setComments('Endorse Change Request');
            request.setObjectId(changeRequestId);
            request.setNextApproverIds(new Id[] {endorser.Endorser__c});
            endorser.isCurrentEndorser__c = True;
            
            requestList.add(request);
            endorsersToUpdate.add(endorser);
        }
        
        
        List<Approval.Processresult> resultList = Approval.process(requestList);
        
        if(endorsersToUpdate.size() > 0) {
            update endorsersToUpdate;
        }
    }
    */
    
    /*@future
    public static void initiateReviewProcess(Set<Id> changeRequestIds) {
        List<Change_Request__c> changeRequests = new List<Change_Request__c>();
        
        Map<Id, CR_Review__c> CRIdToReviewsMap = new Map<Id, CR_Review__c>();
        
        for(Change_Request__c changeRequest : [SELECT Id, Change_Status__c, inFutureMethod__c FROM Change_Request__c WHERE Id in :changeRequestIds]) {
            changeRequest.Change_Status__c = 'Under Review';    
            changeRequest.inFutureMethod__c = True;
            changeRequests.add(changeRequest);
        }
        
        update changeRequests;
        System.debug('ChangeRequests : ' + changeRequests);
        
        for(CR_Review__c review : [SELECT Id, reviewer__c, Order__c, isCurrentReviewer__c, Change_Request__c FROM CR_Review__c WHERE Change_Request__c in :changeRequestIds
                                            AND isCurrentReviewer__c = False ORDER BY Order__c]) {
        
            CR_Review__c reviewRec = CRIdToReviewsMap.get(review.Change_Request__c);
            
            if(reviewRec == Null) {
                CRIdToReviewsMap.put(review.Change_Request__c, review);
            }                                           
        }
        
        List<Approval.ProcessSubmitRequest> requestList = new List<Approval.ProcessSubmitRequest>();
        List<CR_Review__c> reviewsToUpdate = new List<CR_Review__c>();
        
        for(Id changeRequestId : changeRequestIds) {
            CR_Review__c review = CRIdToReviewsMap.get(changeRequestId);
            Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
            request.setComments('Review Change Request');
            request.setObjectId(changeRequestId);
            request.setNextApproverIds(new Id[] {review.reviewer__c});
            review.isCurrentReviewer__c = True;
            
            requestList.add(request);
            reviewsToUpdate.add(review);
        }       
        
        List<Approval.Processresult> resultList = Approval.process(requestList);
        
        if(reviewsToUpdate.size() > 0) {
            update reviewsToUpdate;
        }
    }*/
    
    /*
    @future
    public static void updateChangeRequestStatusToImpactAssessment(Set<Id> changeRequestIds) {
        
        List<Change_Request__c> changeRequestsToUpdate = new List<Change_Request__c> ();
        for(Change_Request__c changeRequest : [SELECT Id, Change_Status__c FROM Change_Request__c WHERE Id in : changeRequestIds]) {
            changeRequest.Change_Status__c = 'Impact Assessment and Review';
            changeRequestsToUpdate.add(changeRequest);
        }
        
        update changeRequestsToUpdate;
        */
        /*List<Approval.ProcessSubmitRequest> requestList = new List<Approval.ProcessSubmitRequest>();
        
        for(Id changeRequestId : changeRequestIds) {
            Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
            request.setComments('Impact Assessment of Change Request');
            request.setObjectId(changeRequestId);
            
            requestList.add(request);   
        }
        
        
        List<Approval.Processresult> resultList = Approval.process(requestList);
        System.debug('Approval Submitted ');*/
    /*} */
    
    /*@future
    public static void initiateReviewRecommendation(Set<Id> changeRequestIds) {
        List<Change_Request__c> changeRequestsToUpdate = new List<Change_Request__c> ();
        for(Change_Request__c changeRequest : [SELECT Id, Change_Status__c FROM Change_Request__c WHERE Id in : changeRequestIds]) {
            changeRequest.Change_Status__c = 'Review Recommendation';
            changeRequestsToUpdate.add(changeRequest);
        }
        
        update changeRequestsToUpdate;
        
        List<Approval.ProcessSubmitRequest> requestList = new List<Approval.ProcessSubmitRequest>();
        
        for(Id changeRequestId : changeRequestIds) {
            Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
            request.setComments('Submit for Approval');
            request.setObjectId(changeRequestId);
            
            requestList.add(request);   
        }
        
        
        List<Approval.Processresult> resultList = Approval.process(requestList);
        System.debug('Approval Submitted ');
    }*/
    
    /*
    @future
    public static void initiateSPAReview(Set<Id> changeRequestIds) {
        List<Approval.ProcessSubmitRequest> requestList = new List<Approval.ProcessSubmitRequest>();
        
        for(Id changeRequestId : changeRequestIds) {
            Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
            request.setComments('Submit for SPA Review');
            request.setObjectId(changeRequestId);
            
            requestList.add(request);   
        }
        
        
        List<Approval.Processresult> resultList = Approval.process(requestList);
        System.debug('Approval Submitted ');        
    }
    
    @future
    public static void updateChangeRequestStatusToApproved(Set<Id> changeRequestIds) {
        List<Change_Request__c> changeRequestsToUpdate = new List<Change_Request__c> ();
        for(Change_Request__c changeRequest : [SELECT Id, Change_Status__c FROM Change_Request__c WHERE Id in : changeRequestIds]) {
            changeRequest.Change_Status__c = 'Approved';
            changeRequestsToUpdate.add(changeRequest);
        }
        
        update changeRequestsToUpdate;
    }
    
    @future
    public static void initiateApprovalProcess(Set<Id> changeRequestIds) {
        List<Change_Request__c> changeRequests = new List<Change_Request__c>();
        
        for(Change_Request__c changeRequest : [SELECT Id, Change_Status__c, inFutureMethod__c FROM Change_Request__c WHERE Id in :changeRequestIds]) {
            changeRequest.Change_Status__c = 'Sent for Approval';   
            changeRequest.inFutureMethod__c = True;
            changeRequests.add(changeRequest);
        }
        
        update changeRequests;
        System.debug('ChangeRequests : ' + changeRequests);
        
        Map<Id, CR_Approver__c> CRIdToApproverMap = new Map<Id, CR_Approver__c>();
        
        List<CR_Approver__c> approversToUpdate = new List<CR_Approver__c>();
        
        List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
        
        for(CR_Approver__c approver : [SELECT Id, Approver__c, Order__c, isCurrentApprover__c, Change_Request__c FROM CR_Approver__c WHERE Change_Request__c in :changeRequestIds
                                            AND isCurrentApprover__c = False ORDER BY Order__c]) {
        
            CR_Approver__c approverRec = CRIdToApproverMap.get(approver.Change_Request__c);
            
            if(approverRec == Null) {
                CRIdToApproverMap.put(approver.Change_Request__c, approver);
            }                                           
        }
        
        for(Id changeRequestId : changeRequestIds) {
            CR_Approver__c approver = CRIdToApproverMap.get(changeRequestId);
            
            Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
            request.setComments('Approve Change Request');
            request.setObjectId(changeRequestId);
            request.setNextApproverIds(new Id[] {approver.Approver__c});
            
            requests.add(request);
            System.debug('Approval Submitted ');
            approver.isCurrentApprover__c = True;
            
            approversToUpdate.add(approver);
        }
        
        if(requests.size() > 0) {
            System.debug('Initiating Approval Process');
            System.debug('requests :' + requests );
            List<Approval.ProcessResult> results = Approval.process(requests);
        }
        
        if(approversToUpdate.size() > 0) {
            update approversToUpdate;
        }
    }    
    // ------ YEN added Oct 30, 2014
    @future
    public static void initiateCRVerification(Set<Id> changeRequestIds) {
        List<Approval.ProcessSubmitRequest> requestList = new List<Approval.ProcessSubmitRequest>();
        List<Change_Request__c> changeRequests = new List<Change_Request__c>();
        
        for(Change_Request__c changeRequest : [SELECT Id, Change_Status__c, Document_Number__c, inFutureMethod__c FROM Change_Request__c WHERE Id in :changeRequestIds]) {
            
            changeRequest.inFutureMethod__c = True;
            changeRequests.add(changeRequest);
            
            Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
            request.setComments('Submit for SPA verify');
            request.setObjectId(changeRequest.Id);
            
            requestList.add(request);  
        
        }
        update changeRequests;
        
        List<Approval.Processresult> resultList = Approval.process(requestList);
        System.debug('Approval Submitted ');        
    }
    */
}