public without sharing class VendorPortalUtility {
	public final static String VENDOR_PORTAL_PROFILE = 'HOG Vendor Community';
    public final static String PERMISSIONSET_ID = 'HOG_Vendor_Community_VTT_User';
    public final static String VENDOR_PORTAL_SUPERVISOR = 'HOG_Vendor_Community_VTT_Vendor_Supervisor';
	public final static String GROUP_NAME = 'Vendor_Portal_Users';
    
	public static Boolean portalExecuteTriggerCode = true;

	//This method is used in the apex trigger on User that
    //Adds portal users to Public Group that is used for 
    //Sharing HOG Notifcation Type records to portal users.
    public static void AddToGroups(List<User> portalUsers )
    {
    	Set<Id> userIds = new Set<Id>();
    	for(User u:portalUsers){
    		System.debug('Adding user to group HERE '+u.IsPortalEnabled + ' ' + u.ContactId);
    		if(u.IsPortalEnabled && u.ContactId != null){
    			userIds.add(u.Id);
    		}
    	}

    	if(userIds != null && userIds.size() > 0){
			//Profile that Vendor portal users use
		    Profile p = [SELECT Id, Name FROM Profile WHERE Name =: VendorPortalUtility.VENDOR_PORTAL_PROFILE];

			List<User> users=[SELECT Id, Name, IsPortalEnabled, ProfileId, ContactID, Vendor_Portal_User_Type__c
		    					FROM user 
		    					WHERE Id IN :userIds 
		    					AND IsPortalEnabled = true
		    					AND ProfileId =: p.Id
		    					AND ContactID <> null
		    					AND Vendor_Portal_User_Type__c <> null];
		    
		    System.debug('Users that will be added to group: ' + users);
		    
		    List<GroupMember>listGroupMember = new List<GroupMember>(); 

		    if (users != null && users.size() > 0){
		    	//Group used for sharing containing Vendor portal users
		   		Group g=[SELECT Id FROM Group WHERE DeveloperName=:VendorPortalUtility.GROUP_NAME];

				// loop the users that have been created
				for (User user : users){
			          GroupMember gm= new GroupMember(); 
			          gm.GroupId=g.id;
			          gm.UserOrGroupId = user.id;
			          listGroupMember.add(gm);

			           
			     } 
			     System.debug('listGroupMember: ' + listGroupMember);
			     insert listGroupMember;
			     //System.debug('Updating Contacts...');
			     //UpdatePortalContacts(userIds);
	             System.debug('Adding Permission Sets');
	             AddPermissionSets(userIds);
		    }
	    }
	    
	}

	//This method update contacts records of vendor portal users - it will update User__c field 
	//on contact to reference portal user that is assigned to this contact (needed for VTT)
	@future
    public static void UpdatePortalContacts(Set<Id> userIds, Id profileId)
    {
    	List<User> users=[SELECT Id, Name, 
    						IsPortalEnabled, ProfileId, 
    						ContactID, Vendor_Portal_User_Type__c, IsActive
	    					FROM user 
	    					WHERE Id IN :userIds 
	    					AND IsPortalEnabled = true
	    					AND ProfileId =: profileId
	    					AND ContactID <> null
	    					AND Vendor_Portal_User_Type__c <> null];

    	List<Contact>contactsToUpdate = new List<Contact>();

    	if (users != null && users.size() > 0){
 			for (User user : users){
				  //update relevant contact associated with user, 
		          //so that Contact records User__c field correctly references portal user
		          Contact c = new Contact();
		          c.Id = user.ContactID;
		          c.User__c = user.Id;
		          
		          contactsToUpdate.add(c);  
			}
			System.debug('contactsToUpdate: ' + contactsToUpdate);
			update contactsToUpdate;
    	}
	
	}

	//Check if user that has been updated and disabled is vendor portal user, if so then also update contacts
	//Which will fire trigger on contact to recount number of vendor portal account users
	public static void IsContactVendorPortalUser(Map<Id, User> newMap, Profile p){
		Set<Id> disabledUsersIds = new Set<Id>();

		//Check if value on Vendor_Portal_User_Type__c has changed
		for(User u: newMap.values()){
			if(u.ProfileId == p.Id
				&& u.ContactId != null
				&& u.Vendor_Portal_User_Type__c != null
				&& u.IsPortalEnabled){

				disabledUsersIds.add(u.Id);
			}
		}

		//Call future method to update contacts
		if(disabledUsersIds.size() > 0){
			UpdatePortalContacts(disabledUsersIds, p.Id);
		}
	}
    
    //This method adds permission set to Vendor portal users based on Vendor_Portal_User_Type__c value 
    //which is name of permission set and is  set on vendor portal management 
    //page upon clicking tradesmen or supervisor button and creating new user
    public static void AddPermissionSets(Set<Id> userIds)
    {
        //Profile that Vendor portal users use
	    Profile p = [SELECT Id, Name FROM Profile WHERE Name =: VendorPortalUtility.VENDOR_PORTAL_PROFILE];

    	List<User> users=[SELECT Id, Name, IsPortalEnabled, ProfileId, ContactID, Vendor_Portal_User_Type__c
	    					FROM user 
	    					WHERE Id IN :userIds 
	    					AND IsPortalEnabled = true
	    					AND ProfileId =: p.Id
	    					AND ContactID <> null
	    					AND Vendor_Portal_User_Type__c <> null];

	    Set<String> setPermissionSetNames = new Set<String>();
		if(!users.isEmpty() && users.size() > 0){
			for(User user : users)
			{
				setPermissionSetNames.add(user.Vendor_Portal_User_Type__c);
			}

		    Map<String, Id> permissionSetMap = new Map<String, Id>();
		    for(PermissionSet permissionSet : [SELECT Id, Name FROM PermissionSet WHERE Name IN  :setPermissionSetNames]) {
	                permissionSetMap.put(permissionSet.Name, permissionSet.Id);            
	        }

			List<PermissionSetAssignment> permissionSetAssignments = new List<PermissionSetAssignment>();
	    	if (users != null){
	 			for (User user : users){
	                PermissionSetAssignment psa = new PermissionSetAssignment();
	                psa.PermissionSetId = permissionSetMap.get(user.Vendor_Portal_User_Type__c);
	                psa.AssigneeId = user.id;
					//update relevant permission sets associated with user, 
			        permissionSetAssignments.add(psa);
	                
						 
				}
	    	}

	    	System.debug('Updating Users: ' + userIds + ' with permission set: ' +permissionSetAssignments);
			insert permissionSetAssignments;

	    	//update contact record for these user
	    	System.debug('Updating contacts for portal users...');
	    	UpdatePortalContacts(userIds, p.Id);
		}

	}

    //This method is used in update trigger on user object - it will PSA for users
    //This is all build with 1 PS per VendorPortal User in mind!!!!
    public static void UpdatePortalUserPermissionSets (Map<Id, User> newMap, Map<Id, User> oldMap){
    	Set<Id> userPSAtoUpdate = new Set<Id>();
		Profile vendorPortalProfile = [SELECT Id, Name FROM Profile WHERE Name =: VendorPortalUtility.VENDOR_PORTAL_PROFILE];
		
		//Check if value on Vendor_Portal_User_Type__c has changed
		for(User u: oldMap.values()){
			 if(!u.IsActive 
				&& newMap.get(u.Id).Vendor_Portal_User_Type__c != oldMap.get(u.Id).Vendor_Portal_User_Type__c
				&& u.ProfileId == vendorPortalProfile.Id
				&& u.ContactId != null
				&& u.Vendor_Portal_User_Type__c != null
				&& u.IsPortalEnabled){

				userPSAtoUpdate.add(u.Id);
			}
		}

		if(userPSAtoUpdate.size() > 0){
			System.debug('Vendor portal user type has been changed for these users..: ' + userPSAtoUpdate);
			
			Set<String> permissionSetNames = new Set<String>();
			
			for (User us: newMap.values()){
	    		permissionSetNames.add(us.Vendor_Portal_User_Type__c);
	    	}

	    	//Map of permission name and its id so we get Id based on Vendor_Portal_User_Type__c value
		    Map<String, Id> permissionSetMap = new Map<String, Id>();
		    for(PermissionSet permissionSet : [SELECT Id, Name FROM PermissionSet WHERE Name IN  :permissionSetNames]) {
	                permissionSetMap.put(permissionSet.Name, permissionSet.Id);            
	        }

	        //Owned by profile check need to be in place because 
		    //there is some sort of system permission set attached to users that cannot be deleted
			List<PermissionSetAssignment> permissionSetAssignments = [SELECT Id, AssigneeId, Assignee.Vendor_Portal_User_Type__c 
																		FROM PermissionSetAssignment 
																		WHERE AssigneeId IN :userPSAtoUpdate
																		AND PermissionSetId 
																			IN (SELECT Id FROM PermissionSet WHERE IsOwnedByProfile = false)];
			System.debug('Deleting old PS: ' + permissionSetAssignments);
			delete permissionSetAssignments;

			List<PermissionSetAssignment> newPSA = new List<PermissionSetAssignment>();

			for(PermissionSetAssignment psa : permissionSetAssignments){
				PermissionSetAssignment nPs = new PermissionSetAssignment();
	            nPs.PermissionSetId = permissionSetMap.get(psa.Assignee.Vendor_Portal_User_Type__c);
	            nPs.AssigneeId = psa.AssigneeId;
	            newPSA.add(nPs);
		    }

			System.debug('Inserting new permision sets for these users: ' + newPSA);
			insert newPSA;
			System.debug('Updating related contacts for these users' + newMap.keySet());
			//UpdatePortalContacts(newMap.keySet());
		}

		IsContactVendorPortalUser(newMap, vendorPortalProfile);
	}

    //This method is used on contact trigger - it basicly counts number of vendor portal users that partner account has enabled
    public static void UpdateUserCountOnAccount( List<Contact> contacts) {
    	//Account ids
    	Set<Id> aId = new Set<Id>();
    	RecordType recId = [SELECT Id FROM RecordType WHERE DeveloperName = 'HOG_Vendor_Contact' LIMIT 1];
    	for(Contact c:contacts) {
    		if(c.RecordTypeId == recId.Id && c.User__c != null ){

    			aId.add(c.AccountId);
    		}
    	}

    	if(aId != null && aId.size() > 0){
    		//List of Portal enabled Accounts and related Contacts
	    	List<Account> acc = [
						    SELECT id, Used_Licenses__c, IsPartner, 
						    (SELECT id FROM Contacts WHERE is_Vendor_Portal_User__c = true) 
						    FROM Account 
						    WHERE Id IN :aId
						    AND IsPartner = true
							];
			if(acc != null && acc.size() > 0){
				for(Account a : acc){ 
					a.Used_Licenses__c = a.Contacts.size();
				}
				System.debug('Updating used licence count on account: ' + acc);
				update acc;
			}
    	}
    	
	
	}

	public static void createOrReactivatePortalUser(String contactId, String email, String firstName, String lastName, String profileId, String userType){
	    String orgId = UserInfo.getOrganizationId();
        Profile p = [SELECT Id, Name FROM Profile WHERE Name =: VendorPortalUtility.VENDOR_PORTAL_PROFILE];

        //Check if portal user is already present for this contact (previously enabled)
        List<User> enabledUser=[SELECT Id FROM user 
	    					WHERE contactId =: contactId
	    					AND IsActive = false
	    					AND ProfileId =: p.Id
	    					AND ContactID <> null];
      	String space = ' ';
      	String noSpace = '';
      	firstName = firstName.replace(space, noSpace);
      	lastName = lastName.replace(space, noSpace);
        if(!enabledUser.isEmpty() || enabledUser.size() < 0){
            //We will have to reactivate this user
            User reactivatedUser = enabledUser.get(0);
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.EmailHeader.triggerUserEmail = true; 
            //reactivatedUser.username = email + '.vendor.' + orgId;
            reactivatedUser.Email = email;
            reactivatedUser.FirstName = firstName;
            reactivatedUser.LastName = lastName;
            reactivatedUser.Alias = string.valueof(firstName.substring(0,1) + lastName.substring(0,1));
            reactivatedUser.ProfileId = profileId;
            reactivatedUser.emailencodingkey='UTF-8';
            reactivatedUser.languagelocalekey='en_US';
            reactivatedUser.localesidkey='en_CA';
            reactivatedUser.timezonesidkey='America/Denver';
            reactivatedUser.Vendor_Portal_User_Type__c = userType;
            reactivatedUser.IsActive =true;
            System.debug('Re-enabling this user: ' + reactivatedUser);
            try {
                update reactivatedUser;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'User has been re-enabled. Please refresh this page after a while'));
            }catch(Exception ex){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Error when reactivating user - ' + ex.getMessage()));
                System.debug('EXCEPTION: ' + ex);
            }
        } else {
            User u = new User();
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.EmailHeader.triggerUserEmail = true;         
            u.ContactId = contactId;
            u.username = firstName + '.' + lastName + '@huskyvttportal.com';
            u.Email = email;
            u.FirstName = firstName;
            u.LastName = lastName;
            u.Alias = string.valueof(firstName.substring(0,1) + lastName.substring(0,1));
            u.ProfileId = profileId;
            u.emailencodingkey='UTF-8';
            u.languagelocalekey='en_US';
            u.localesidkey='en_CA';
            u.timezonesidkey='America/Denver';
            u.Vendor_Portal_User_Type__c = userType;
            try {
                insert u;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'User has been created. Please refresh this page after a while'));
            }catch(Exception ex){
            	if(ex.getMessage().contains('DUPLICATE_USERNAME')){
            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please change firstName or lastName of this contacts before enabling him as portal user'));
            		System.debug('Duplicate: ' + ex);
            	}else{
            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Error when creating new user - ' + ex.getMessage()));
            		System.debug('EXCEPTION: ' + ex);
            	
                }
            }
        }
	}
}