@isTest
private class WeldingTrackerSelfRegistrationTest {
    static testmethod void registrationTest() {
        Account acc = new Account( Name = 'Master Unassigned Welding Tracker Users' );
        insert acc;
        
        SelfRegistrationCodes__c sc = new SelfRegistrationCodes__c( Name = 'Welding Tracker Community User',
                                        Alert_Email__c = 'welding@huskyenergy.com',
                                        CAPTCHA_Private_Key__c = '6Lc9U3QUAAAAAA6iS89fuhBc5wqSjtf6ECrzT5IP',
                                        CAPTCHA_Public_Key__c = '6Lc9U3QUAAAAAL2UBEEjUcApq3M-XPFZy4_OK5qy',
                                        Max_Licenses_Count__c = 400, Registration_Code__c = '548896118798712' );
        insert sc;
        
        Test.startTest();
            WeldingTrackerSelfRegistration controller = new WeldingTrackerSelfRegistration();
            controller.firstName = 'Neetu';
            controller.lastName = 'Bansal';
            controller.email = 'neetu.bansal@huskyenergy.com';
            controller.password = 'test123';
            controller.confirmPassword = 'test123';
            controller.communityNickname = 'bansan';
            controller.registrationCode = '548896118798712';
            controller.userName = 'neetu.bansal@huskyenergy.com';
            
            WeldingTrackerSelfRegistration.isValidEmail( 'neetu.bansal@huskyenergy.com' );
            controller.registerUser();
            String rs = controller.response;
            String rH = controller.remoteHost;
            WeldingTrackerSelfRegistration.sendAlertEmail( sc, 'Test', 'Test' );
            WeldingTrackerSelfRegistration.makeRequest( '', '' );
        Test.stopTest();
    }
    
    static testmethod void invalidEmail() {
        Account acc = new Account( Name = 'Master Unassigned Welding Tracker Users' );
        insert acc;
        
        SelfRegistrationCodes__c sc = new SelfRegistrationCodes__c( Name = 'Welding Tracker Community User',
                                        Alert_Email__c = 'welding@huskyenergy.com',
                                        CAPTCHA_Private_Key__c = '6Lc9U3QUAAAAAA6iS89fuhBc5wqSjtf6ECrzT5IP',
                                        CAPTCHA_Public_Key__c = '6Lc9U3QUAAAAAL2UBEEjUcApq3M-XPFZy4_OK5qy',
                                        Max_Licenses_Count__c = 0, Registration_Code__c = '548896118798712' );
        insert sc;
        
        Test.startTest();
            WeldingTrackerSelfRegistration controller = new WeldingTrackerSelfRegistration();
            controller.firstName = 'Neetu';
            controller.lastName = 'Bansal';
            controller.email = 'neetu.bansalhuskyenergy.com';
            controller.password = '12';
            controller.confirmPassword = '12';
            controller.communityNickname = 'bansan';
            controller.registrationCode = '548896118798712';
            controller.userName = 'neetu.bansalhuskyenergy.com';
            
            WeldingTrackerSelfRegistration.isValidEmail( 'neetu.bansalhuskyenergy.com' );
            //controller.challenge = null;
            controller.registerUser();
        Test.stopTest();
    }
    
    static testmethod void LicenseTest() {
        Account acc = new Account( Name = 'Master Unassigned Welding Tracker Users' );
        insert acc;
        
        SelfRegistrationCodes__c sc = new SelfRegistrationCodes__c( Name = 'Welding Tracker Community User',
                                        Alert_Email__c = 'welding@huskyenergy.com',
                                        CAPTCHA_Private_Key__c = '6Lc9U3QUAAAAAA6iS89fuhBc5wqSjtf6ECrzT5IP',
                                        CAPTCHA_Public_Key__c = '6Lc9U3QUAAAAAL2UBEEjUcApq3M-XPFZy4_OK5qy',
                                        Max_Licenses_Count__c = 0, Registration_Code__c = '548896118798712' );
        insert sc;
        
        Test.startTest();
            WeldingTrackerSelfRegistration controller = new WeldingTrackerSelfRegistration();
            controller.firstName = 'Neetu';
            controller.lastName = 'Bansal';
            controller.email = 'neetu.bansal@huskyenergy.com';
            controller.password = 'test123';
            controller.confirmPassword = 'test123';
            controller.communityNickname = 'bansan';
            controller.registrationCode = '548896118798712';
            controller.userName = 'neetu.bansal@huskyenergy.com';
            
            controller.registerUser();
        Test.stopTest();
    }
    
    static testmethod void invalidPassword() {
        Account acc = new Account( Name = 'Master Unassigned Welding Tracker Users' );
        insert acc;
        
        SelfRegistrationCodes__c sc = new SelfRegistrationCodes__c( Name = 'Welding Tracker Community User',
                                        Alert_Email__c = 'welding@huskyenergy.com',
                                        CAPTCHA_Private_Key__c = '6Lc9U3QUAAAAAA6iS89fuhBc5wqSjtf6ECrzT5IP',
                                        CAPTCHA_Public_Key__c = '6Lc9U3QUAAAAAL2UBEEjUcApq3M-XPFZy4_OK5qy',
                                        Max_Licenses_Count__c = 400, Registration_Code__c = '548896118798712' );
        insert sc;
        
        Test.startTest();
            WeldingTrackerSelfRegistration controller = new WeldingTrackerSelfRegistration();
            controller.firstName = 'Neetu';
            controller.lastName = 'Bansal';
            controller.email = 'neetu.bansal@huskyenergy.com';
            controller.password = '34';
            controller.confirmPassword = '12';
            controller.communityNickname = 'bansan';
            controller.registrationCode = '548896118798712';
            controller.userName = 'neetu.bansal@huskyenergy.com';
            
            controller.registerUser();
        Test.stopTest();
    }
}