/*---------------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: Test class for ServiceNotificationRequestGlobal
----------------------------------------------------------------------------------------------------------*/  
@isTest
private class ServiceNotificationRequestGlobalTest
{
    static List<HOG_Maintenance_Servicing_Form__c> riglessServicingForm;
    static List<HOG_Notification_Type__c> notificationType;
    static List<HOG_Work_Order_Type__c> workOrderType;
    static List<HOG_Service_Request_Notification_Form__c> serviceRequest;
    static List<HOG_Notification_Type_Priority__c> notificationTypePriority;
    static List<HOG_Service_Code_MAT__c> serviceCodeMAT;
	static HOG_Service_Rig_Program__c serviceRigProgram;
    static HOG_Service_Category__c serviceCategory;
    static HOG_Service_Code_Group__c serviceCodeGroup;
    static HOG_Service_Required__c serviceRequiredList;
    static HOG_User_Status__c userStatus;
    static HOG_Service_Priority__c servicePriorityList;
    static Business_Unit__c businessUnit;
    static Business_Department__c businessDepartment;
    static Operating_District__c operatingDistrict;
    static Field__c field;
    static Route__c route;
    static Location__c location; 
    static Facility__c facility;
    static System__c sys;
    static Sub_System__c subSys;
    static Functional_Equipment_Level__c fel;
    static Yard__c yard;
    static Well_Event__c wellEvent;
    static Account account;         
    static Well_Tracker__c wellTracker;
    static Equipment__c equipment;
    static HOG_Settings__c settings;
    static Id recordTypeId;
	static String NOTIFICATIONOBJECTNAME = 'HOG_Service_Request_Notification_Form__c';
	static List<String> WORKORDERRECORDTYPE = new List<String>{'Flushby - Production Flush', 'Coil Tubing Unit - Rod Side Entry'};

    @isTest (SeeAllData = false) 
    static void ServiceNotificationRequestGlobal_Non_Callouts_Test()
    {                
        ServiceRequestNotificationUtilities.executeTriggerCode = false;
        WellServicingUtilities.executeTriggerCode = false;
    	MaintenanceServicingUtilities.executeTriggerCode = false;        

        SetupTestData();

        Test.startTest();
    
        // read recurring service before inserting Task
        ServiceNotificationRequestGlobal.readURLRecurringService(serviceRequest[0].Id);
        
        // --------- start Insert Task records
        String taskSubject = 'Create Work Order';
        String taskStatus = 'Not Started';
        String taskRecurrenceType = 'RecursDaily';
        String taskRecurrenceInstance;
        String taskRecurrenceMonthOfYear;
        Integer taskRecurrenceDayOfWeekMask;
        Integer taskRecurrenceInterval = 1;
        Integer taskRecurrenceDayOfMonth;                    
        Date taskRecurrenceStartDateOnly = serviceRequest[0].Recurring_Start_Date__c;
        Date taskRecurrenceEndDateOnly = serviceRequest[0].Recurring_End_Date__c;
        Id taskWhatId = serviceRequest[0].Id;
        Id taskOwnerId = UserInfo.getUserId();
                
        Integer frequencyTaskQuantity = 1;
        String frequency = 'Daily';
        String frequencyDaily = 'Every weekday';
                                    
        for (Integer taskQuantity = 0; taskQuantity < frequencyTaskQuantity; taskQuantity++)
        {
            Task taskRecurringRecord = new Task
                (               
                    Subject = taskSubject,
                    Status = taskStatus,
                    RecurrenceType = taskRecurrenceType,
                    RecurrenceMonthOfYear = taskRecurrenceMonthOfYear,
                    RecurrenceDayOfMonth = taskRecurrenceDayOfMonth,
                    RecurrenceDayOfWeekMask = taskRecurrenceDayOfWeekMask,
                    RecurrenceInterval = taskRecurrenceInterval,
                    RecurrenceInstance = taskRecurrenceInstance,
                    RecurrenceStartDateOnly = taskRecurrenceStartDateOnly,
                    RecurrenceEndDateOnly = taskRecurrenceEndDateOnly,
                    IsRecurrence = true,
                    WhatId = taskWhatId,
                    OwnerId = taskOwnerId
                );
                
            insert taskRecurringRecord;

        }                        
        // --------- end Insert Task records if reccuring

	    System.assertNotEquals(null, ServiceNotificationRequestGlobal.readServiceNotificationWorkOrders(serviceRequest[1].Id));
	    
		System.assertEquals(null, ServiceNotificationRequestGlobal.readNotificationTypePriority(serviceRequest[1].HOG_Work_Order_Type__r.HOG_Notification_Type__c, '1', True));
		
		System.assertNotEquals(null, ServiceNotificationRequestGlobal.readServiceWorkOrders(riglessServicingForm[1].Id));

        // read recurring service after inserting Task
        System.assertNotEquals(null, ServiceNotificationRequestGlobal.readURLRecurringService(serviceRequest[0].Id));
        
        // cancel scheduled recurring service
        System.assertNotEquals(null, ServiceNotificationRequestGlobal.cancelScheduledRecurringService(serviceRequest[0].Id));
        
        //-- update work order number, test for Generate_Numbers_From_SAP__c = false and recurring
        System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateWorkOrderNumber(riglessServicingForm[0].Id, serviceRequest[0].Id, 'YYYYYYYY'));

        //-- update work order number, test for Generate_Numbers_From_SAP__c = true and recurring
        System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateWorkOrderNumber(riglessServicingForm[3].Id, serviceRequest[3].Id, 'ZZZZZZZZ'));
        
        //-- update work order number, test for Generate_Numbers_From_SAP__c = false and not recurring
        System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateWorkOrderNumber(riglessServicingForm[1].Id, serviceRequest[1].Id, 'ZZZZZZZZ'));

        //-- update work order number, test for Generate_Numbers_From_SAP__c = true and not recurring
        System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateWorkOrderNumber(riglessServicingForm[2].Id, serviceRequest[2].Id, 'ZZZZZZZZ'));
        
        //-- update work order number, test for TECO
        riglessServicingForm[3].TECO__c = true;
        update riglessServicingForm;        
        System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateWorkOrderNumber(riglessServicingForm[3].Id, serviceRequest[3].Id, 'DDDDDDDD'));

        Test.stopTest();        
    }
    
    @isTest (SeeAllData = false) 
    static void ServiceNotificationRequestGlobal_Workorder_Callouts_Test()
    {                
        ServiceRequestNotificationUtilities.executeTriggerCode = false;
        WellServicingUtilities.executeTriggerCode = false;
    	MaintenanceServicingUtilities.executeTriggerCode = false;

        SetupTestData();

        Test.startTest();

        // Not empty Rig Program, not empty notification and SAP_Generated_Notification_Number__c = true
 	 	System.assertNotEquals(null, ServiceNotificationRequestGlobal.createServiceRigWorkOrder
 	 		(serviceRigProgram.Id, serviceRigProgram.HOG_Service_Request_Notification_Form__c, serviceRigProgram.RecordTypeId));         

        // Not empty Rig Program, not empty notification and SAP_Generated_Notification_Number__c = false
        serviceRequest[2].SAP_Generated_Notification_Number__c = false;
        update serviceRequest;
 	 	System.assertNotEquals(null, ServiceNotificationRequestGlobal.createServiceRigWorkOrder
 	 		(serviceRigProgram.Id, serviceRigProgram.HOG_Service_Request_Notification_Form__c, serviceRigProgram.RecordTypeId));         
        serviceRequest[2].SAP_Generated_Notification_Number__c = true;
        update serviceRequest; 

        // Not empty Rig Program, not empty notification and empty notification number 
        serviceRequest[2].Notification_Number__c = '';
        update serviceRequest;
 	 	System.assertNotEquals(null, ServiceNotificationRequestGlobal.createServiceRigWorkOrder
 	 		(serviceRigProgram.Id, serviceRigProgram.HOG_Service_Request_Notification_Form__c, serviceRigProgram.RecordTypeId));         
        serviceRequest[2].Notification_Number__c = '0000000';
        update serviceRequest;

        // Not empty Rig Program and empty notification
 	 	System.assertNotEquals(null, ServiceNotificationRequestGlobal.createServiceRigWorkOrder
 	 		(serviceRigProgram.Id, null, serviceRigProgram.RecordTypeId));         
		
		// Empty Rig Program
 	 	System.assertNotEquals(null, ServiceNotificationRequestGlobal.createServiceRigWorkOrder
 	 		(null, null, null));         

		// Update Work Order
        System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateServiceWorkOrderFromSAP(riglessServicingForm[2].Id, serviceRequest[2].Id, 'XXXXXXXX'));
        
		// Update Work Order
        System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateServiceWorkOrderFromSAP(null, serviceRequest[2].Id, 'XXXXXXXX'));

		// Update Work Order
        System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateServiceWorkOrderFromSAP(riglessServicingForm[2].Id, null, 'XXXXXXXX'));

        // Empty Work_Details__c
        System.assertNotEquals(null, ServiceNotificationRequestGlobal.createServiceWorkOrderInSAP(riglessServicingForm[2].Id, serviceRequest[2].Id));

        // Not empty Work_Details__c
        riglessServicingForm[2].Work_Details__c = 'Test Detail';
        update riglessServicingForm;
        System.assertNotEquals(null, ServiceNotificationRequestGlobal.createServiceWorkOrderInSAP(riglessServicingForm[2].Id, serviceRequest[2].Id));

        // Empty work order when creating work order
        System.assertNotEquals(null, ServiceNotificationRequestGlobal.createServiceWorkOrderInSAP(null, serviceRequest[2].Id));

        // Empty notification order when creating work order
        System.assertNotEquals(null, ServiceNotificationRequestGlobal.createServiceWorkOrderInSAP(riglessServicingForm[2].Id, null));

  //      //Set Mock Service
  //      Test.setMock(WebServiceMock.class, new SAPHOGGetNotificationResponseMock(equipment, location, null));

  //      // Update Notification From SAP
  //      System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(serviceRequest[2].Id, '1234567'));

  //      // Update Notification From SAP
  //      System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(null, '1234567'));

  //      // Empty Work_Details__c
		//System.assertNotEquals(null, ServiceNotificationRequestGlobal.createNoticationOrderInSAP(serviceRequest[2].Id));

  //      // Not empty Work_Details__c
  //      serviceRequest[2].Work_Details__c = 'Test Detail';
  //      update serviceRequest;
		//System.assertNotEquals(null, ServiceNotificationRequestGlobal.createNoticationOrderInSAP(serviceRequest[2].Id));

  //      // Empty notification order when creating notification
		//System.assertNotEquals(null, ServiceNotificationRequestGlobal.createNoticationOrderInSAP(null));

        Test.stopTest();
    }
    

    @isTest (SeeAllData = false)
    static void ServiceNotificationRequestGlobal_Notification_Callouts_Test()
    {
        
        ServiceRequestNotificationUtilities.executeTriggerCode = false;
        WellServicingUtilities.executeTriggerCode = false;
        MaintenanceServicingUtilities.executeTriggerCode = false;
        
        ServiceNotificationRequestGlobal.isTestRespType = 'wellLocation';

        SetupTestData();
        
        Test.startTest();

            // Update Notification From SAP
            
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(serviceRequest[2].Id, '1234567'));
    
            // Update Notification From SAP
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(null, '1234567'));
    
            // Empty Work_Details__c
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.createNoticationOrderInSAP(serviceRequest[2].Id));
    
            // Not empty Work_Details__c
            serviceRequest[2].Work_Details__c = 'Test Detail';
            update serviceRequest;
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.createNoticationOrderInSAP(serviceRequest[2].Id));
    
            // Empty notification order when creating notification
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.createNoticationOrderInSAP(null));
        
        Test.stopTest();
        
    }
    
    
    @isTest (SeeAllData = false)
    static void ServiceNotificationRequestGlobal_Notification_Callouts_Test_Facility()
    {
        
        
        ServiceRequestNotificationUtilities.executeTriggerCode = false;
        WellServicingUtilities.executeTriggerCode = false;
        MaintenanceServicingUtilities.executeTriggerCode = false;
        
        ServiceNotificationRequestGlobal.isTestRespType = 'facility';
        
        SetupTestData();
        
        Test.startTest();
        
            // Update Notification From SAP
            
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(serviceRequest[2].Id, '0000001'));
    
            // Update Notification From SAP
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(null, '0000001'));
    
        Test.stopTest();
        
    }
    
    @isTest (SeeAllData = false)
    static void ServiceNotificationRequestGlobal_Notification_Callouts_Test_System()
    {
        
        
        ServiceRequestNotificationUtilities.executeTriggerCode = false;
        WellServicingUtilities.executeTriggerCode = false;
        MaintenanceServicingUtilities.executeTriggerCode = false;
        
        ServiceNotificationRequestGlobal.isTestRespType = 'system';
        
        SetupTestData();
        
        Test.startTest();
        
            // Update Notification From SAP
            
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(serviceRequest[2].Id, '0000002'));
    
            // Update Notification From SAP
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(null, '0000002'));
    
        Test.stopTest();
    }
    
    @isTest (SeeAllData = false)
    static void ServiceNotificationRequestGlobal_Notification_Callouts_Test_SubSystem()
    {
        
        
        ServiceRequestNotificationUtilities.executeTriggerCode = false;
        WellServicingUtilities.executeTriggerCode = false;
        MaintenanceServicingUtilities.executeTriggerCode = false;
        
        ServiceNotificationRequestGlobal.isTestRespType = 'sub-system';
        
        SetupTestData();
        
        Test.startTest();
        
            // Update Notification From SAP
            
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(serviceRequest[2].Id, '0000003'));
    
            // Update Notification From SAP
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(null, '0000003'));
    
        Test.stopTest();
    }

    @isTest (SeeAllData = false)
    static void ServiceNotificationRequestGlobal_Notification_Callouts_Test_FEL()
    {
        
        
        ServiceRequestNotificationUtilities.executeTriggerCode = false;
        WellServicingUtilities.executeTriggerCode = false;
        MaintenanceServicingUtilities.executeTriggerCode = false;
        
        ServiceNotificationRequestGlobal.isTestRespType = 'fel';
        
        SetupTestData();
        
        Test.startTest();
        
            // Update Notification From SAP
            
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(serviceRequest[2].Id, '0000006'));
    
            // Update Notification From SAP
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(null, '0000006'));
    
        Test.stopTest();
    }

    @isTest (SeeAllData = false)
    static void ServiceNotificationRequestGlobal_Notification_Callouts_Test_Yard()
    {
        
        
        ServiceRequestNotificationUtilities.executeTriggerCode = false;
        WellServicingUtilities.executeTriggerCode = false;
        MaintenanceServicingUtilities.executeTriggerCode = false;
        
        ServiceNotificationRequestGlobal.isTestRespType = 'yard';
        
        SetupTestData();
        
        Test.startTest();
        
            // Update Notification From SAP
            
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(serviceRequest[2].Id, '0000005'));
    
            // Update Notification From SAP
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(null, '0000005'));
    
        Test.stopTest();
    }
    
    @isTest (SeeAllData = false)
    static void ServiceNotificationRequestGlobal_Notification_Callouts_Test_WellEvent()
    {
        
        
        ServiceRequestNotificationUtilities.executeTriggerCode = false;
        WellServicingUtilities.executeTriggerCode = false;
        MaintenanceServicingUtilities.executeTriggerCode = false;
        
        ServiceNotificationRequestGlobal.isTestRespType = 'wellEvent';
        
        SetupTestData();
        
        Test.startTest();
        
            // Update Notification From SAP
            
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(serviceRequest[2].Id, '0000004'));
    
            // Update Notification From SAP
            System.assertNotEquals(null, ServiceNotificationRequestGlobal.updateNotificationOrderFromSAP(null, '0000004'));
    
        Test.stopTest();
    }
    

    private static void SetupTestData()
    {        
        //-- Begin setup of data needed to test the Service Request Notification controller --//
                
        //-- Setup Service Category
        serviceCategory = ServiceCategoryTestData.createServiceCategory('Test Category');
        insert serviceCategory;
        //--

        //-- Setup Service Code Group
        serviceCodeGroup = ServiceCodeGroupTestData.createServiceCodeGroup('Test Group', true);
        insert serviceCodeGroup;
        //--

        //-- Setup Notification Type
        notificationType = new List<HOG_Notification_Type__c>();
        notificationType.add((HOG_Notification_Type__c)NotificationTypeTestData.createNotificationType(serviceCategory.Id, false, false, false, true));
        notificationType.add((HOG_Notification_Type__c)NotificationTypeTestData.createNotificationType(serviceCategory.Id, true, true, true, true)); 
        insert notificationType;
        //--
 
        //-- Setup Service Code MAT  
        serviceCodeMAT = new List<HOG_Service_Code_MAT__c>();      
        serviceCodeMAT.add((HOG_Service_Code_MAT__c)ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', 'TST', WORKORDERRECORDTYPE[0]));
        serviceCodeMAT.add((HOG_Service_Code_MAT__c)ServiceCodeMATTestData.createServiceCodeMAT('Test MAT2', 'TST', WORKORDERRECORDTYPE[1]));
        insert serviceCodeMAT;
        //--

        //-- Setup Service Required
        serviceRequiredList = ServiceRequiredTestData.createServiceRequired('Test Service Required', true, true, true);
        insert serviceRequiredList;
        //--

        //-- Setup Service User Status
        userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;
        //--

        //-- Setup Service - Priority
        servicePriorityList = ServicePriorityTestData.createServicePriority('Service Priority 1', 1, null);
        insert servicePriorityList;
        //--

        //-- Setup Notification Type - Priority Detail
        notificationTypePriority = new List<HOG_Notification_Type_Priority__c>();
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[0].Id, servicePriorityList.Id, false));                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[1].Id, servicePriorityList.Id, true));                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[1].Id, servicePriorityList.Id, false));                
        insert notificationTypePriority;        
        //--
                
        workOrderType = new List<HOG_Work_Order_Type__c>();
        workOrderType.add((HOG_Work_Order_Type__c)WorkOrderTypeTestData.createWorkOrderType(notificationType[0].Id, serviceCodeMAT[0].Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true));
        workOrderType.add((HOG_Work_Order_Type__c)WorkOrderTypeTestData.createWorkOrderType(notificationType[1].Id, serviceCodeMAT[1].Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[1], true));
        insert workOrderType;
        //--
                                        
        //-- Setup objects for Well Tracker                                
        businessUnit = new Business_Unit__c(Name = 'Test Business Unit');
        insert businessUnit;

        businessDepartment = BusinessDepartmentTestData.createBusinessDepartment('Test Business Department');    
        insert businessDepartment;

        operatingDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Field', businessDepartment.Id, businessUnit.Id);
        insert operatingDistrict;

        recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

        field = FieldTestData.createField('Test Field Control Centre', operatingDistrict.Id, recordTypeId);
        insert field;

        route = RouteTestData.createRoute('999');
        insert route;                  

        location = LocationTestData.createLocation('Test Location', route.Id, field.Id);
        location.SAP_Object_ID__c = '?012345';        
        insert location;
        
        facility = FacilityTestData.createFacility('Test Facility', route.Id, field.Id);
        facility.SAP_Object_ID__c = '?000001';        
        insert facility;
        
        
        sys = new System__c();
        sys.Name = 'Test System';
        sys.Operating_Field_AMU__c = field.Id;
        sys.SAP_Object_ID__c = '?000002'; 
        insert sys;
               
        
        subSys = new Sub_System__c();
        subSys.Name = 'Test Sub-System';
        subSys.SAP_Object_ID__c = '?000003';        
        insert subSys;

        fel = new Functional_Equipment_Level__c();
        fel.Name = 'Test FEL';
        fel.SAP_Object_ID__c = '?000006';
        fel.Operating_Field_AMU__c = field.Id;        
        insert fel;

        yard = new Yard__c();
        yard.Name = 'Test yard';
        yard.SAP_Object_ID__c = '?000005'; 
        yard.Operating_Field_AMU__c = field.Id;       
        insert yard;
        
        wellEvent = LocationTestData.createEvent('Test Well Event', location.Id);
        wellEvent.SAP_Object_ID__c = '?000004';        
        insert wellEvent;
        
        account = AccountTestData.createAccount('Test Account', null);          
        insert account;

        wellTracker = WellTrackerTestData.createWellTracker(location.Id, null, field.Id, 1, Datetime.valueOf('2013-06-20 12:00:00'), 'Producing', 'Engineering');
        insert wellTracker;                                                    
        //--

        //-- Setup equipment
        equipment = EquipmentTestData.createEquipment(location);
        equipment.Equipment_Number__c = '12345678';
        update equipment;
        //--

        serviceRequest = new List<HOG_Service_Request_Notification_Form__c>();

        serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
            (
                wellTracker.Id,
                workOrderType[0].Id,
                null,
                null,
                null,
                null,
                null,
                null,
                notificationTypePriority[0].Id,
                notificationTypePriority[1].Id,
                account.Id,
                '7521256',
                'AAAAAAAA',
                'Test Detail',
                Date.today(),
                Date.today() + 10
            ));
            
        serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
            (
                wellTracker.Id,
                workOrderType[0].Id,
                null,
                null,
                null,
                null,
                null,
                null,
                notificationTypePriority[0].Id,
                notificationTypePriority[1].Id,
                account.Id,
                '7521257',
                'BBBBBBBB',
                'Test Detail',
                null,
                null
            ));

        serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
            (
                wellTracker.Id,
                workOrderType[1].Id,
                null,
                null,
                null,
                null,
                null,
                null,
                notificationTypePriority[2].Id,
                notificationTypePriority[1].Id,
                account.Id,
                '7521258',
                'CCCCCCCC',
                'Test Detail',
                null,
                null
            ));

        serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
            (
                wellTracker.Id,
                workOrderType[1].Id,
                null,
                null,
                null,
                null,
                null,
                null,
                notificationTypePriority[2].Id,
                notificationTypePriority[1].Id,
                account.Id,
                '9999999',
                'DDDDDDDD',
                'Test Detail',
                Date.today(),
                Date.today() + 10
            ));

        serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
            (
                wellTracker.Id,
                workOrderType[1].Id,
                null,
                null,
                null,
                null,
                null,
                null,
                notificationTypePriority[2].Id,
                notificationTypePriority[1].Id,
                account.Id,
                '8888888',
                'EEEEEEEE',
                'Test Detail',
                Date.today(),
                Date.today() + 10
            ));

        serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
            (
                wellTracker.Id,
                workOrderType[1].Id,
                null,
                null,
                null,
                null,
                null,
                null,
                notificationTypePriority[2].Id,
                notificationTypePriority[1].Id,
                account.Id,
                '7777777',
                'FFFFFFFF',
                'Test Detail',
                Date.today(),
                Date.today() + 10
            ));

        serviceRequest[0].RecordTypeId = ServiceRequestNotificationUtilities.readObjectRecordTypeId(NOTIFICATIONOBJECTNAME, 'Well Id');                
        serviceRequest[1].RecordTypeId = ServiceRequestNotificationUtilities.readObjectRecordTypeId(NOTIFICATIONOBJECTNAME, 'Well Id');                
        serviceRequest[2].RecordTypeId = ServiceRequestNotificationUtilities.readObjectRecordTypeId(NOTIFICATIONOBJECTNAME, 'Well Id');                
        serviceRequest[2].SAP_Changed_By__c = 'Test';
        serviceRequest[2].SAP_Generated_Notification_Number__c = true;
        //-- End setup of data needed to test the Service Request Notification controller --//
         
        insert serviceRequest;

        //-- Start setup data for Work Order        
        riglessServicingForm = new List<HOG_Maintenance_Servicing_Form__c>();

		ServiceRequestNotificationUtilities.ServiceRequest request = new ServiceRequestNotificationUtilities.ServiceRequest(); 

    	request.workOrderName = 'Test Location';		
    	request.workOrderCount = 1;
		request.notificationType = notificationType[0].Id;
        request.serviceTimeIsRequired = false;
		request.vendorInvoicingPersonnelIsRequired = false;
        					
		request.record = serviceRequest[0];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[1];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[2];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[3];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[4];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
        
        riglessServicingForm[2].SAP_Request_Returned_Critical_Error__c = true;
        riglessServicingForm[3].SAP_Request_Returned_Critical_Error__c = true;
        insert riglessServicingForm;                               
        //-- End setup data for Work Order        

        // this will return 1 record or none 
        serviceRigProgram = new HOG_Service_Rig_Program__c
        (
        	HOG_Service_Request_Notification_Form__c = serviceRequest[2].Id
        );
        insert serviceRigProgram;

        settings = HOGSettingsTestData.createHOGSettings();
        insert settings;
    }    
}