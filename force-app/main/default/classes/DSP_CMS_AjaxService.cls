/*
 * Called from javascript in the Downstream Portal (both the home page and the news page).
 * Returns a JSON package with content to display on those pages.
 */
global with sharing class DSP_CMS_AjaxService implements cms.ServiceInterface
{
    
    private String action;
    private System.JSONGenerator response;
    
    public class calEvent{
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public String startString {get;set;}
        public String startStringIE {get;set;}
        public String endString {get;set;}
        public String endStringIE {get;set;}
        public String url {get;set;}
        public String location {get;set;}
        public String createdBy {get;set;}
        public String description {get;set;}
    }
    
    public list<calEvent> events {get;set;}
    public Id calendarId {get;set;}
    
    String dtFormat = 'EEE, d MMM yyyy HH:mm:ss z';
    String dtFormatIE = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
    
    public DSP_Settings__c customSettings {get;set;}
    
    public List<Idea> ideaList {get;set;}
    
    public List<DSP01_Scorecard__c> Scorecards {get;set;}
    public System.Type getType() {
        return DSP_CMS_AjaxService.class;
    }
    
    public void fetchIdeas()
    {
        // Get a list of all the ideas to display on the main page
        try
        {
            Community community = [SELECT Id
                                   FROM Community
                                   WHERE Name =: customSettings.Idea_Zone_Name__c ];
            
            ideaList = [SELECT Title,
                               NumComments,
                               Id,
                               VoteTotal,
                               CreatedDate,
                               CreatorName,
                               Body,
                               LastModifiedDate
                        FROM Idea
                        WHERE CommunityId =: community.Id
                        ORDER BY LastModifiedDate DESC LIMIT 3];
        }
        catch (Exception e)
        {
            ideaList = new List<Idea>();
        }
    }
    
    public void fetchScorecards()
    {
        // Get a list of all the Scorecards to display on the main page
        try
        {
            
            
            Scorecards = [SELECT ID,
                               DSP01_fx_Arrow__c,
                               DSP01_dt_CalDay__c,
                               DSP01_tx_Category__c,
                               DSP01_nu_Direction__c,
                               DSP01_tx_KPI__c,
                               DSP01_nu_Status__c,
                               DSP01_fx_Symbol__c,
                               DSP01_tx_Trend__c
                        FROM DSP01_Scorecard__c
                        ];
        }
        catch (Exception e)
        {
            Scorecards = new List<DSP01_Scorecard__c>();
        }
    }
 
    public void fetchEventsForHomePage()
    {
        // Get ID of DS Portal Calendar it cannot be queried so we need to hardcode it or put it in a custom setting
        String calendarId = ID.valueOf(customSettings.Public_Calendar_ID__c);
        events = new List<calEvent>();
        
        // Get a list of upcoming events (limit 8)
        for(Event evnt: [SELECT Id,
                                Subject,
                                description,
                                OwnerId,
                                location,
                                IsAllDayEvent,
                                StartDateTime,
                                EndDateTime,
                                createdBy.name
                         FROM Event
                         WHERE OwnerId =: calendarId AND
                               CALENDAR_YEAR(convertTimezone(EndDateTime)) >=: Date.Today().Year()    AND
                               CALENDAR_MONTH(convertTimezone(EndDateTime)) >=: Date.Today().Month()  AND
                               DAY_IN_YEAR(convertTimezone(EndDateTime)) >=: Date.Today().dayOfYear()
                         ORDER BY StartDateTime ASC LIMIT 8])
        {
            DateTime startDT = evnt.StartDateTime;
            DateTime endDT = evnt.EndDateTime;
            calEvent myEvent = new calEvent();
             
            myEvent.title = evnt.Subject;
            myEvent.location = evnt.Location;
            myEvent.createdBy = evnt.createdBy.name;
            myEvent.description = evnt.description;
            myEvent.allDay = evnt.IsAllDayEvent;
            // Keep date/time as GMT if it is an all day event
            if(myEvent.allDay){
                myEvent.startString = startDT.formatGmt(dtFormat);
                myEvent.endString = endDT.addDays(1).formatGmt(dtFormat);
            } else { // Set time as local time of the salesforce instance
                myEvent.startString = startDT.format(dtFormat);
                myEvent.endString = endDT.format(dtFormat);
            }
            myEvent.url = '/' + evnt.Id;
            events.add(myEvent);
        }
    }
    
    global String executeRequest(Map<String, String> p)
    {
        action = p.get('action');
        
        // Fetch data from custom settings
        
        customSettings = DSP_Settings__c.getInstance();
        
        if(action == 'home')
        {
            fetchIdeas();
            fetchScorecards();
            fetchEventsForHomePage();
            
            response = System.JSON.createGenerator(false);
            
            response.writeStartObject();
            
            // ideas
            
            response.writeFieldName('ideas');
            response.writeStartArray();
            
            for(Idea eachIdea : ideaList)
            {
                response.writeStartObject();
                response.writeStringField('Id', eachIdea.Id);
                response.writeStringField('title', eachIdea.title);
                if(eachIdea.body != null) response.writeStringField('body', eachIdea.body);
                else                      response.writeStringField('body', '');
                response.writeStringField('VoteTotal', String.valueOf(eachIdea.VoteTotal));
                response.writeStringField('NumComments', String.valueOf(eachIdea.NumComments));
                response.writeStringField('CreatorName', eachIdea.CreatorName);
                response.writeStringField('LastModifiedDate', eachIdea.LastModifiedDate.format());
                response.writeEndObject();
            }
            
            
            
            response.writeEndArray();
            
            
            //Scorecards
            
            response.writeFieldName('Scorecards');
            response.writeStartArray();
            
            for(DSP01_Scorecard__c Scorecard : Scorecards)
            {
                response.writeObject(Scorecard);
            }
            
            response.writeEndArray();
            
            // weather
            
            response.writeStringField('WeatherCodeForecast', customSettings.Weather_Code_Forecast__c);
            response.writeStringField('WeatherCodeToday',    customSettings.Weather_Code_Today__c);
            response.writeStringField('WeatherCodeTommorow', customSettings.Weather_Code_Tommorow__c);
            response.writeStringField('WeatherCodeTonight',  customSettings.Weather_Code_Tonight__c);
            response.writeStringField('WeatherLocation',     customSettings.Weather_Location__c);
            response.writeStringField('WeatherTempToday',    customSettings.Weather_Temp_Today__c);
            response.writeStringField('WeatherTempForecast', customSettings.Weather_Temp_Forecast__c);
            response.writeStringField('WeatherTempTonight',  customSettings.Weather_Temp_Tonight__c);
            response.writeStringField('WeatherTempTommorow', customSettings.Weather_Temp_Tommorow__c);
            response.writeStringField('WeatherWind',         customSettings.Weather_Wind__c);
            response.writeStringField('WeatherDayTommorow',  customSettings.Weather_Day_Tommorow__c);
            
            // calendar (home page)
            
            response.writeFieldName('events');
            response.writeStartArray();
            
            for(calEvent eachEvent : events)
            {
                response.writeObject(eachEvent);
            }
            
            response.writeEndArray();
            
            response.close();
            return (response.getAsString());
        }
        else // if(action == 'news')
        {
            response = System.JSON.createGenerator(false);
            
            response.writeStartObject();
            
            // alberta oil magazine
            
            if(customSettings.Alberta_Oil_Magazine_Article_1__c      != null) response.writeStringField('AOM1',     customSettings.Alberta_Oil_Magazine_Article_1__c);
            if(customSettings.Alberta_Oil_Magazine_Article_1_URL__c  != null) response.writeStringField('AOM1URL',  customSettings.Alberta_Oil_Magazine_Article_1_URL__c);
            if(customSettings.Alberta_Oil_Magazine_Article_1_Date__c != null) response.writeStringField('AOM1Date', customSettings.Alberta_Oil_Magazine_Article_1_Date__c);
            
            if(customSettings.Alberta_Oil_Magazine_Article_2__c      != null) response.writeStringField('AOM2',     customSettings.Alberta_Oil_Magazine_Article_2__c);
            if(customSettings.Alberta_Oil_Magazine_Article_2_URL__c  != null) response.writeStringField('AOM2URL',  customSettings.Alberta_Oil_Magazine_Article_2_URL__c);
            if(customSettings.Alberta_Oil_Magazine_Article_2_Date__c != null) response.writeStringField('AOM2Date', customSettings.Alberta_Oil_Magazine_Article_2_Date__c);
            
            if(customSettings.Alberta_Oil_Magazine_Article_3__c      != null) response.writeStringField('AOM3',     customSettings.Alberta_Oil_Magazine_Article_3__c);
            if(customSettings.Alberta_Oil_Magazine_Article_3_URL__c  != null) response.writeStringField('AOM3URL',  customSettings.Alberta_Oil_Magazine_Article_3_URL__c);
            if(customSettings.Alberta_Oil_Magazine_Article_3_Date__c != null) response.writeStringField('AOM3Date', customSettings.Alberta_Oil_Magazine_Article_3_Date__c);
            
            if(customSettings.Alberta_Oil_Magazine_Article_4__c      != null) response.writeStringField('AOM4',     customSettings.Alberta_Oil_Magazine_Article_4__c);
            if(customSettings.Alberta_Oil_Magazine_Article_4_URL__c  != null) response.writeStringField('AOM4URL',  customSettings.Alberta_Oil_Magazine_Article_4_URL__c);
            if(customSettings.Alberta_Oil_Magazine_Article_4_Date__c != null) response.writeStringField('AOM4Date', customSettings.Alberta_Oil_Magazine_Article_4_Date__c);
            
            response.close();
            return (response.getAsString());
        }
    }
}