@isTest
global with sharing class SAPHOGCreateNotificationsResponseMock implements WebServiceMock {
	
	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
		
		SAPHOGNotificationServices.CreateNotificationsResponse responseElement = new SAPHOGNotificationServices.CreateNotificationsResponse();
   		responseElement.Message = 'SAPHOGNotificationServices.CreateNotificationsResponse Test Message';
   		responseElement.type_x = True;
   		
   		responseElement.NotificationResponseList = new SAPHOGNotificationServices.CreateNotificationResponse[]{};

		SAPHOGNotificationServices.CreateNotificationResponse item = new SAPHOGNotificationServices.CreateNotificationResponse();
		item.Notification = new SAPHOGNotificationServices.Notification();		
		item.Notification.NotificationNumber = '7654321';

		responseElement.NotificationResponseList.add(item);
   		
   		response.put('response_x', responseElement);
	}
}