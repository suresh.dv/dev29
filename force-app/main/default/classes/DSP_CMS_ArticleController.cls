global virtual with sharing class DSP_CMS_ArticleController extends cms.ContentTemplateController{
    global DSP_CMS_ArticleController(cms.CreateContentController cc){
        super(cc);
    }

    global DSP_CMS_ArticleController(){}
    
    global DSP_CMS_ArticleController(cms.GenerateContent cc)
    {
        super(cc);
    }
    
    global virtual override String getHTML(){return '';}
    
    public String image{
        get{
            return getProperty('image');
        }
        set;
    }
    
    public String icon{
        get{
            return getProperty('icon');
        }
        set;
    }
    
    public String title{
        get{
            return String.isNotBlank(getProperty('title')) ?  getProperty('title') : '';
        }
        set;
    }
    
    public String tagline{
        get{
            return getProperty('tagline');
        }
        set;
    }
    
    public String text{
        get{
            return String.isNotBlank(getProperty('text')) ?  getProperty('text') : '';
        }
        set;
    }
    
    public String summary{
        get{
            return String.isNotBlank(getProperty('summary')) ?  getProperty('summary') : '';
        }
        set;
    }
    
    public String articleDetailHTML(){
        String html='';
        
        html += '<div class="col s12 m12 l12">';
        if(icon != null)
            html += '<h2><img src="'+icon+'"/>'+title+'</h2>';
        else
            html += '<h2>'+title+'</h2>';
        html += '</div>';
        html += '<div class="col s12 m12 l12">';
        if(image != null && image != '')
        {
            html += '<div class="col s12 m12 l12">';
            html += '<img src="'+image+'"/>';
            html += '</div>';
        }
        else
        {
            String url = '';
            
            List<StaticResource> resourceList = [SELECT Name,
                                                        NamespacePrefix,
                                                        SystemModStamp
                                                 FROM StaticResource
                                                 WHERE Name =: 'DownstreamPortalHuskyLogo'];
            if(resourceList.size() == 1)
            {
               String namespace = resourceList[0].NamespacePrefix;
               url = '/resource/'
                  + resourceList[0].SystemModStamp.getTime() + '/'
                  + (namespace != null && namespace != '' ? namespace + '__' : '')
                  + 'DownstreamPortalHuskyLogo';
                html +=             '<div class="col s12 m12 l12">';
                html +=                 '<img src="'+url+'"/>';
                html +=             '</div>';
            }
        }
        if(tagline != null)
        {
            html += '<div class="col s12 m12 l12">';
            html += '<h5>'+tagline+'</h5>';
            html += '</div>';
        }
        html += '<div class="col s12 m12 l12">';
        html += '<p>'+text+'</p>';
        html += '</div>';
        html += '</div>';
        
        return html;
    }
    
    public String articleSummaryHTML(){
        
        DSP_Settings__c settings = DSP_Settings__c.getInstance();
        String articleDetailURL = settings.Article_Detail_URL__c;
        
        if(articleDetailURL == null)
            articleDetailURL = '';
        
        String contentId = '';
        if(content != null)
            contentId = content.Id;
        
        String html='';
        
        html += '<div class="col s12 m12 l6">';
        html +=     '<div class="card">';
        html +=         '<div class="card-content">';
        
        if(image != null && image != '')
        {
            html +=             '<div class="center-align col s12 m12 l12" style="height:267px;" >';
            html +=                 '<img src="'+image+'" style="max-height:267px;"/>';
            html +=             '</div>';
        }
        else
        {
            String url = '';
            
            List<StaticResource> resourceList = [SELECT Name,
                                                        NamespacePrefix,
                                                        SystemModStamp
                                                 FROM StaticResource
                                                 WHERE Name =: 'DownstreamPortalHuskyLogo'];
            if(resourceList.size() == 1)
            {
               String namespace = resourceList[0].NamespacePrefix;
               url = '/resource/'
                  + resourceList[0].SystemModStamp.getTime() + '/'
                  + (namespace != null && namespace != '' ? namespace + '__' : '')
                  + 'DownstreamPortalHuskyLogo';
                html +=             '<div class="center-align col s12 m12 l12" style="height:267px;" >';
                html +=                 '<img src="'+url+'" style="max-height:267px;" />';
                html +=             '</div>';
            }
        }
        html +=             '<div class="col s12 m12 l12">';
        html +=                 '<a href="'+articleDetailURL+contentId+'"><h4>'+title+'</h4></a>';
        html +=             '</div>';
        html +=             '<div class="col s12 m12 l12">';
        html +=                 '<p>'+summary+'</p>';
        html +=             '</div>';
        html +=             '<div class="card-date col s12 m12 l12">';
        if(content != null)
            html +=                 getOriginalPublishedStartDate().format('MMMMM d, yyyy HH:mm');
        html +=             '</div>';
        html +=         '</div>';
        html +=     '</div>';
        html += '</div>';

        return html;
    }
    
    public String articleTitleHTML(){
        DSP_Settings__c settings = DSP_Settings__c.getInstance();
        String articleDetailURL = settings.Article_Detail_URL__c;
        
        if(articleDetailURL == null)
            articleDetailURL = '';
        
        String contentId = '';
        if(content != null)
            contentId = content.Id;
        
        String html='';
        
        html += '<div class="col s12 m12 l12">';
        html +=     '<div class="card inner">';
        html +=         '<div class="center-align card-content">';
        if(image != null && image != '')
        {
            html +=             '<div class="col s12 m12 l12" style="height:267px;">';
            html +=                 '<img src="'+image+'" style="max-height:267px;" />';
            html +=             '</div>';
        }
        else
        {
            String url = '';
            
            List<StaticResource> resourceList = [SELECT Name,
                                                        NamespacePrefix,
                                                        SystemModStamp
                                                 FROM StaticResource
                                                 WHERE Name =: 'DownstreamPortalHuskyLogo'];
            if(resourceList.size() == 1)
            {
               String namespace = resourceList[0].NamespacePrefix;
               url = '/resource/'
                  + resourceList[0].SystemModStamp.getTime() + '/'
                  + (namespace != null && namespace != '' ? namespace + '__' : '')
                  + 'DownstreamPortalHuskyLogo';
                html +=             '<div class="center-align col s12 m12 l12" style="height:267px;">';
                html +=                 '<img src="'+url+'" style="max-height:267px;"/>';
                html +=             '</div>';
            }
        }
        html +=             '<div class="col s12 m12 l12">';
        html +=                 '<a href="'+articleDetailURL+contentId+'"><h5>'+title+'</h5></a>';
        html +=             '</div>';
        html +=             '<div class="left-align card-date col s12 m12 l12">';
        if(content != null)
            html +=                 '<p><em>'+getOriginalPublishedStartDate().format('MMMMM d, yyyy HH:mm')+'</em></p>';
        else
            html +=                 '<p><em></em></p>';
        html +=             '</div>';
        html +=         '</div>';
        html +=     '</div>';
        html += '</div>';
        
        return html;
    }
    
    public String articleListItemHTML(){
        DSP_Settings__c settings = DSP_Settings__c.getInstance();
        String articleDetailURL = settings.Article_Detail_URL__c;
        
        if(articleDetailURL == null)
            articleDetailURL = '';
        
        String contentId = '';
        if(content != null)
            contentId = content.Id;
        
        String html='';
        
        html += '<div style="clear:both;border-bottom:1px solid #DDD;padding-top:5px;padding-bottom:5px;margin-bottom:10px !important;" class="row ArticleListItem">';
        if(icon != null && icon != '')
        {
            html += '<div style="float:left">';
            html +=     '<img src="'+icon+'" />';
            html += '</div>';
        }
        html +=     '<div class="col s10 m10 l10">';
        html +=         '<a href="'+articleDetailURL+contentId+'"><h5>'+title+'</h5></a>';
        html +=         '<div class="card-date">';
        if(content != null)
            html +=         '<p style="margin-top:0px"><em>'+getOriginalPublishedStartDate().format('MMMMM d, yyyy HH:mm')+'</em></p>';
        else
            html +=         '<p style="margin-top:0px"><em></em></p>';
        html +=         '</div>';
        html +=     '</div>';
        html += '</div>';
        
        return html;
    }
}