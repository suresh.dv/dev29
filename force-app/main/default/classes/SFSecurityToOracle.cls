global class SFSecurityToOracle implements Schedulable
{
    global void execute(SchedulableContext SC)
    {
        DOM.Document doc = fetchSecurity();
        sendXMLtoMule(doc.toXmlString());
    }
 
    global DOM.Document fetchSecurity()
    {
        // Query all users/permission sets/roles where role is a WCP role *and* they are in a WCP permisison set
        
        Set<String> validPermissionSets = new Set<String>{'WCP_Editor',
                                                          'WCP_Read_Only',
                                                          'WCP_Well_Entry'};
        
        Map<String,String> cleanPermissionName = new Map<String,String>{'WCP_Editor'     => 'Editor',
                                                                        'WCP_Read_Only'  => 'Read Only',
                                                                        'WCP_Well_Entry' => 'Well Entry'};

        Map<String,Integer> permissionWeight = new Map<String,Integer>{'Editor'     => 3,
                                                                       'Read Only'  => 1,
                                                                       'Well Entry' => 2}; 
        
        Map<Id,String> usersWithWCPPermissionSet = new Map<Id,String>();
        
        for(PermissionSetAssignment psa : [SELECT Id,
                                                  AssigneeId,
                                                  PermissionSet.Name
                                           FROM PermissionSetAssignment
                                           WHERE PermissionSet.Name IN: validPermissionSets])
        {
            String permission = cleanPermissionName.get(psa.PermissionSet.Name);
            
            if(!usersWithWCPPermissionSet.containsKey(psa.AssigneeId))
            {
                // New user, add their permission
                usersWithWCPPermissionSet.put(psa.AssigneeId, permission);
            }
            else if(permissionWeight.get(usersWithWCPPermissionSet.get(psa.AssigneeId)) < permissionWeight.get(permission))
            {
                // Update user to have higher permission
                usersWithWCPPermissionSet.put(psa.AssigneeId, permission);
            }
        }
        
        Set<String> validRoles = new Set<String>{'Western_Canada_Production_Director',
                                                 'GRD_Director',
                                                 'GRD_Manager',
                                                 'GRD_User',
                                                 'WCP_RB_Director',
                                                 'WCP_RB_Manager',
                                                 'WCP_RB_User',
                                                 'Western_Canada_Conventional_Director',
                                                 'Northwest_Director',
                                                 'WCP_GP_Manager',
                                                 'WCP_GP_User',
                                                
                                                 'WCP_SL_Manager',
                                                 'WCP_SL_User',  
                                                 'Southeast_Director',
                                                 'WCP_PV_Manager',
                                                 'WCP_PV_User',
                                                 'WCP_SC_Manager',
                                                 'WCP_SC_User',
                                                 'WCP_TB_Manager',
                                                 'WCP_TB_User'};
        
        List<User> usersToSend = [SELECT Id,
                                         FederationIdentifier,
                                         UserRoleId
                                  FROM User
                                  WHERE UserRole.DeveloperName IN: validRoles AND
                                        Id IN: usersWithWCPPermissionSet.keySet() AND
                                        FederationIdentifier != null AND
                                        FederationIdentifier != ''];
        
        Map<Id,UserRole> roleId2Name = new Map<Id,UserRole>([SELECT Id,
                                                                    DeveloperName
                                                             FROM UserRole
                                                             WHERE DeveloperName IN: validRoles]);
        
        // Generate XML
        DOM.Document doc = new DOM.Document();
        
        // Set up the URL to the namespace
        String namespace = 'http://wcp.huskyenergy.com/salesforcesecurity';
        
        /* Start well_message block */
        // Create the root element wellMessage and add attributes needed
        dom.XmlNode salesforcesecurity = doc.createRootElement('salesforce_security', namespace, 'wcp');
        salesforcesecurity.setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        salesforcesecurity.setAttribute('xsi:schemaLocation', 'http://wcp.huskyenergy.com/salesforcesecurity http://wcp.huskyenergy.com/salesforcesecurity');
        
        // message_dttm
        // This is the date/time for the message
        salesforcesecurity.addChildElement('message_dttm', namespace, 'wcp').addTextNode(string.valueof(System.now()).replace(' ', 'T'));
        
        dom.XmlNode users = salesforcesecurity.addChildElement('users', namespace, 'wcp');
        
        for(User eachUser : usersToSend)
        {
            if(eachUser.FederationIdentifier != null)
            {
                dom.XmlNode eachUserNode = users.addChildElement('user', namespace, 'wcp');
                
                eachUserNode.addChildElement('permission', namespace, 'wcp').addTextNode(usersWithWCPPermissionSet.get(eachUser.Id));
                
                dom.XmlNode schemas = eachUserNode.addChildElement('schemas', namespace, 'wcp');
                
                for(String schema : WCPRoles.roleDeveloperName2schemaNames(roleId2Name.get(eachUser.UserRoleId).DeveloperName))
                {
                    schemas.addChildElement('schema', namespace, 'wcp').addTextNode(schema);
                }
                
                eachUserNode.addChildElement('user_samid', namespace, 'wcp').addTextNode(eachUser.FederationIdentifier);
            }
        }
        return doc;
    }
    
   @future(callout=true)
    // Sends an XML string to Mulesoft to the passed to a WCP database
    public static void sendXMLtoMule(String xml)
    {
        // Get certificate name
        USCP_Settings__c mc = USCP_Settings__c.getOrgDefaults();
        String WS_CertificateName = mc.HEI_WS_CertificateName__c;
        String muleHost = getMuleHost();
        
        // Where we're sending the XML to
        //String endpointUrl = 'https://salesforce-wcp.huskyenergy.com:8001/biPortalGrd/WCPIAP/security';
        String endpointUrl = 'https://salesforce-wcp' + muleHost + '.huskyenergy.com:8001/biPortalGrd/WCPIAP/security';
        
        // Instantiate a new http object
        Http h = new Http();
        
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        
        // The body is the XML string
        req.setBody(xml);
        
        // Tell the http request where to go
        req.setEndpoint(endpointUrl);
        
        // Specify certificate
        if(WS_CertificateName != null) req.setClientCertificateName(WS_CertificateName);
        
        req.setTimeout(120000);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'text/xml');
        
        // This is the response we'll get back
        HttpResponse res;
        
        // Send the request, and return a response
        if(!Test.isRunningTest())
        {
            res = h.send(req);
        
            System.debug('XML to Mule reponse is '+res);
            System.debug('res.getBody() '+res.getBody());
            System.debug(res.getStatusCode()+' : '+res.getStatus());
        }
    }
    
    private static String getMuleHost(){
        String hostName = URL.getSalesforceBaseUrl().getHost().split('\\.')[0].toLowerCase();
        
        //TODO : Use enum ??
        if(hostName.containsIgnoreCase('--dev')){
            return '-dv';
        }
        
        if(hostName.containsIgnoreCase('--test')){
            return '-qa';
        }  
        
        if(hostName.containsIgnoreCase('--ps')){
            return '-ps';
        }                 
        
        return '';
    }     
}