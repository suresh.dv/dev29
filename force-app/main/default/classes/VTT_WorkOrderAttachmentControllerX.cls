/**
 * VTT_WorkOrderAttachmentControllerX
 *
 * Used for displaying all attachments for WOAs displayed on parent WO
 * 
 * Test: VTT_WorkOrderAttachmentControllerXTest
 *
 * Created: 2018/05/03
 * Author: MBrimus
 */
public without sharing class VTT_WorkOrderAttachmentControllerX {

	public static String NO_ATTACHMENTS_AND_NOTE_MESSAGE = 'There are no attachments on any of the Work Order or Activities';
	public static String NOTE_TYPE = 'Note';
	public static String ATTACHMENT_TYPE = 'Attachment';

	public HOG_Maintenance_Servicing_Form__c record;
	public String clickedId { get;set; }
	public String redirectUrl { public get; private set; }
	public String clickedType { public get; set; }

	// Used for buttons on vf page since we cannot add 18 digit id to link on New Note or New Attachment
	public String recordId {get;set;}
	
	public VTT_WorkOrderAttachmentControllerX(ApexPages.StandardController stdController) {
		record = (HOG_Maintenance_Servicing_Form__c) stdController.getRecord();
		record = [SELECT Id,
					(SELECT Id From Work_Order_Activities__r)
					FROM HOG_Maintenance_Servicing_Form__c 
					WHERE ID =: record.Id];

		recordId = String.valueOf(record.Id).substring(0, 15);
	}

	public List<AttachmentWrapper> getAttachments() {
		// Based on business requirements we are also adding Work Order notes and attachments
		Set<Id> recordIds = new Set<Id>{ record.id };
		
		for(Work_Order_Activity__c woa: record.Work_Order_Activities__r){
			recordIds.add(woa.id);
		}

		List<Attachment> attachments = [SELECT Id, Name, ParentID, Parent.Name, SystemModstamp, LastModifiedDate, Owner.Name  
										FROM Attachment 
										WHERE ParentID IN :recordIds];

		List<Note> notes = [SELECT Id, Title, ParentID, Parent.Name, SystemModstamp, LastModifiedDate, Owner.Name  
							FROM Note
							WHERE ParentID IN :recordIds];

		if(attachments.size() <= 0 && notes.size() <= 0) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, NO_ATTACHMENTS_AND_NOTE_MESSAGE));
		}

		return getAttachmentWrapper(attachments, notes);
	}

	public List<AttachmentWrapper> getAttachmentWrapper(List<Attachment> attachments, List<Note> notes) {
		List<AttachmentWrapper> attachmentWrapperList = new List<AttachmentWrapper>();
		
		for(Attachment at: attachments) {
			attachmentWrapperList.add(new AttachmentWrapper(at, null, ATTACHMENT_TYPE));
		}

		for(Note note: notes) {
			attachmentWrapperList.add(new AttachmentWrapper(null, note, NOTE_TYPE));
		}

		return attachmentWrapperList;
	}

	public void deleteAttachment(){
		if (clickedType == NOTE_TYPE){
			try {
				delete new Note( id = clickedId );
			} catch (Exception e){
				System.debug('Exception ' + e.getMessage());
			}
		} else {
			try {
				delete new Attachment( id = clickedId );
			} catch (Exception e){
				System.debug('Exception ' + e.getMessage());
			}
		}
	}

	public class AttachmentWrapper {
		public Attachment attachment {get;set;}
		public Note note {get;set;}
		public String sourceType {get;set;}

		public AttachmentWrapper(Attachment attachment, Note note, String sourceType) {
			this.attachment = attachment;
			this.note = note;
			this.sourceType = sourceType;
		}
	}

}