/*------------------------------------------------------------------------------------------------------------
Author     : Marek Kosar
Company    : IBM
Description: Unit Test Class for WOEquipmentHierarchyCtrl apex class
History    :
            09.05.16    Class Created  
            08-Mar-17   Miro Zelina - Added Methods to test Equipment Hierarchy for Well Event
            06-Jul-17   Miro Zelina - Added Methods to test Equipment Hierarchy for System & Sub-System
-------------------------------------------------------------------------------------------------------------*/

@isTest
private class WOEquipmentHierarchyCtrlTest {
    
    static Testmethod void testEquipmentHierarchyWOLocation() {

        SetupTestData();

        maintenanceRecord.Location__c = location.Id;
        update maintenanceRecord;

        WOEquipmentHierarchyCtrl WOHierarchyCtrl = new WOEquipmentHierarchyCtrl(
            new ApexPages.StandardController(maintenanceRecord));
        
        List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();

        //Test Hierarchy Structure
        System.assertEquals(5,equipmentHierarchyResult.size());
        for(Integer i=0; i < 5; i++) {
            System.debug('Superior Equipment: ' +testEquipmentHierarchy[i].Superior_Equipment__c);
            System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchy[i].Id);
            System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
            if(i == 0) {
                //Top Level tests
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            } else if (i == 4) {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
            } else {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            }
        }

        //Test Hierarchy Behaviour
        ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchy[0].Id);
        WOHierarchyCtrl.toggleNodeState();
        equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();
        System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());
    }

    static Testmethod void testEquipmentHierarchyWOFacility() {

        SetupTestData();

        maintenanceRecord.Facility__c = facility.Id;
        update maintenanceRecord;

        List<Equipment__c> eqList = [select Id,Facility__c from Equipment__c];
        for(Equipment__c eq:eqList){
            eq.Facility__c = facility.Id;
        }
        update eqList;


        WOEquipmentHierarchyCtrl WOHierarchyCtrl = new WOEquipmentHierarchyCtrl(
            new ApexPages.StandardController(maintenanceRecord));
        
        List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();

        //Test Hierarchy Structure
        System.assertEquals(5,equipmentHierarchyResult.size());
        for(Integer i=0; i < 5; i++) {
            System.debug('Superior Equipment: ' +testEquipmentHierarchy[i].Superior_Equipment__c);
            System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchy[i].Id);
            System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
            if(i == 0) {
                //Top Level tests
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            } else if (i == 4) {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
            } else {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            }
        }

        //Test Hierarchy Behaviour
        ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchy[0].Id);
        WOHierarchyCtrl.toggleNodeState();
        equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();
        System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());

        WOHierarchyCtrl.getEquipmentHierarchyVisible();

    }
    
    static Testmethod void testEquipmentHierarchyWOEvent() {

        SetupTestData();

        maintenanceRecord.Well_Event__c = wellEvent.Id;
        update maintenanceRecord;
        
        List<Equipment__c> eqList = [select Id,Facility__c, Well_Event__c from Equipment__c];
        for(Equipment__c eq : eqList){
            eq.Well_Event__c = wellEvent.Id;
        }
        update eqList;

        WOEquipmentHierarchyCtrl WOHierarchyCtrl = new WOEquipmentHierarchyCtrl(
            new ApexPages.StandardController(maintenanceRecord));
        
        List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();

        //Test Hierarchy Structure
        System.assertEquals(5,equipmentHierarchyResult.size());
        for(Integer i=0; i < 5; i++) {
            System.debug('Superior Equipment: ' +testEquipmentHierarchy[i].Superior_Equipment__c);
            System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchy[i].Id);
            System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
            if(i == 0) {
                //Top Level tests
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            } else if (i == 4) {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
            } else {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            }
        }

        //Test Hierarchy Behaviour
        ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchy[0].Id);
        WOHierarchyCtrl.toggleNodeState();
        equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();
        System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());
    }
    
        static Testmethod void testEquipmentHierarchyWOSystem() {

        SetupTestData();

        maintenanceRecord.System__c = sys.Id;
        update maintenanceRecord;
        
        List<Equipment__c> eqList = [select Id,Facility__c, Well_Event__c from Equipment__c];
        for(Equipment__c eq : eqList){
            eq.System__c = sys.Id;
        }
        update eqList;

        WOEquipmentHierarchyCtrl WOHierarchyCtrl = new WOEquipmentHierarchyCtrl(
            new ApexPages.StandardController(maintenanceRecord));
        
        List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();

        //Test Hierarchy Structure
        System.assertEquals(5,equipmentHierarchyResult.size());
        for(Integer i=0; i < 5; i++) {
            System.debug('Superior Equipment: ' +testEquipmentHierarchy[i].Superior_Equipment__c);
            System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchy[i].Id);
            System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
            if(i == 0) {
                //Top Level tests
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            } else if (i == 4) {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
            } else {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            }
        }

        //Test Hierarchy Behaviour
        ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchy[0].Id);
        WOHierarchyCtrl.toggleNodeState();
        equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();
        System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());
    }
    
    static Testmethod void testEquipmentHierarchyWOSubSystem() {

        SetupTestData();

        maintenanceRecord.Sub_System__c = subSys.Id;
        update maintenanceRecord;
        
        List<Equipment__c> eqList = [select Id,Facility__c, Well_Event__c from Equipment__c];
        for(Equipment__c eq : eqList){
            eq.Sub_System__c = subSys.Id;
        }
        update eqList;

        WOEquipmentHierarchyCtrl WOHierarchyCtrl = new WOEquipmentHierarchyCtrl(
            new ApexPages.StandardController(maintenanceRecord));
        
        List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();

        //Test Hierarchy Structure
        System.assertEquals(5,equipmentHierarchyResult.size());
        for(Integer i=0; i < 5; i++) {
            System.debug('Superior Equipment: ' +testEquipmentHierarchy[i].Superior_Equipment__c);
            System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchy[i].Id);
            System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
            if(i == 0) {
                //Top Level tests
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            } else if (i == 4) {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
            } else {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            }
        }

        //Test Hierarchy Behaviour
        ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchy[0].Id);
        WOHierarchyCtrl.toggleNodeState();
        equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();
        System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());
    }

    static Testmethod void testEquipmentHierarchyWOFEL() {

        SetupTestData();

        maintenanceRecord.Functional_Equipment_Level__c = fel.Id;
        update maintenanceRecord;
        
        List<Equipment__c> eqList = [select Id,Facility__c, Well_Event__c from Equipment__c];
        for(Equipment__c eq : eqList){
            eq.Functional_Equipment_Level__c = fel.Id;
        }
        update eqList;

        WOEquipmentHierarchyCtrl WOHierarchyCtrl = new WOEquipmentHierarchyCtrl(
            new ApexPages.StandardController(maintenanceRecord));
        
        List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();

        //Test Hierarchy Structure
        System.assertEquals(5,equipmentHierarchyResult.size());
        for(Integer i=0; i < 5; i++) {
            System.debug('Superior Equipment: ' +testEquipmentHierarchy[i].Superior_Equipment__c);
            System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchy[i].Id);
            System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
            if(i == 0) {
                //Top Level tests
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            } else if (i == 4) {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
            } else {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            }
        }

        //Test Hierarchy Behaviour
        ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchy[0].Id);
        WOHierarchyCtrl.toggleNodeState();
        equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();
        System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());
    }

    static Testmethod void testEquipmentHierarchyWOYard() {

        SetupTestData();

        maintenanceRecord.Yard__c = yard.Id;
        update maintenanceRecord;
        
        List<Equipment__c> eqList = [select Id,Facility__c, Well_Event__c from Equipment__c];
        for(Equipment__c eq : eqList){
            eq.Yard__c = yard.Id;
        }
        update eqList;

        WOEquipmentHierarchyCtrl WOHierarchyCtrl = new WOEquipmentHierarchyCtrl(
            new ApexPages.StandardController(maintenanceRecord));
        
        List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();

        //Test Hierarchy Structure
        System.assertEquals(5,equipmentHierarchyResult.size());
        for(Integer i=0; i < 5; i++) {
            System.debug('Superior Equipment: ' +testEquipmentHierarchy[i].Superior_Equipment__c);
            System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchy[i].Id);
            System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
            if(i == 0) {
                //Top Level tests
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            } else if (i == 4) {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
            } else {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            }
        }

        //Test Hierarchy Behaviour
        ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchy[0].Id);
        WOHierarchyCtrl.toggleNodeState();
        equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();
        System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());
    }

    static Testmethod void testEquipmentHierarchyWOActivityLocation() {

        SetupTestData();

        //Setup Data
        //Account vendor = VTT_TestData.createVendorAccount('Vendor1');
        //Contact tradesman = VTT_TestData.createTradesmanContact('Test', 'Tradesman', vendor.Id);

        //Create and assign work order activity
        List<Work_Order_Activity__c> activityList = VTT_TestData.createWorkOrderActivitiesWithAssignments(maintenanceRecord.Id, null, 1);
        activityList[0].Scheduled_Start_Date__c = Date.today();
        //activityList[0].Assigned_Vendor__c = vendor.id;
        activityList[0].Location__c = location.Id;
        update activityList;

        WOEquipmentHierarchyCtrl WOHierarchyCtrl = new WOEquipmentHierarchyCtrl(
            new ApexPages.StandardController(activityList[0]));
        
        List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();

        //Test Hierarchy Structure
        System.assertEquals(5,equipmentHierarchyResult.size());
        for(Integer i=0; i < 5; i++) {
            System.debug('Superior Equipment: ' +testEquipmentHierarchy[i].Superior_Equipment__c);
            System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchy[i].Id);
            System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
            if(i == 0) {
                //Top Level tests
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            } else if (i == 4) {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
            } else {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            }
        }

        //Test Hierarchy Behaviour
        ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchy[0].Id);
        WOHierarchyCtrl.toggleNodeState();
        equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();
        System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());

        WOHierarchyCtrl.getEquipmentHierarchyVisible();

    }

    static Testmethod void testEquipmentHierarchyWOActivityFacility() {

        SetupTestData();

        List<Equipment__c> eqList = [select Id,Facility__c from Equipment__c];
        for(Equipment__c eq:eqList){
            eq.Facility__c = facility.Id;
        }
        update eqList;

        //Create and assign work order activity
        List<Work_Order_Activity__c> activityList = VTT_TestData.createWorkOrderActivitiesWithAssignments(maintenanceRecord.Id, null, 1);
        activityList[0].Scheduled_Start_Date__c = Date.today();
        activityList[0].Facility__c = facility.Id;
        update activityList;

        WOEquipmentHierarchyCtrl WOHierarchyCtrl = new WOEquipmentHierarchyCtrl(
            new ApexPages.StandardController(activityList[0]));
        
        List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();

        //Test Hierarchy Structure
        System.assertEquals(5,equipmentHierarchyResult.size());
        for(Integer i=0; i < 5; i++) {
            System.debug('Superior Equipment: ' +testEquipmentHierarchy[i].Superior_Equipment__c);
            System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchy[i].Id);
            System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
            if(i == 0) {
                //Top Level tests
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            } else if (i == 4) {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
            } else {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            }
        }

        //Test Hierarchy Behaviour
        ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchy[0].Id);
        WOHierarchyCtrl.toggleNodeState();
        equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();
        System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());

        WOHierarchyCtrl.getEquipmentHierarchyVisible();

    }
    
    static Testmethod void testEquipmentHierarchyWOActivityEvent() {

        SetupTestData();

        List<Equipment__c> eqList = [select Id,Facility__c, Well_Event__c from Equipment__c];
        for(Equipment__c eq : eqList){
            eq.Well_Event__c = wellEvent.Id;
        }
        update eqList;

        //Create and assign work order activity
        List<Work_Order_Activity__c> activityList = VTT_TestData.createWorkOrderActivitiesWithAssignments(maintenanceRecord.Id, null, 1);
        activityList[0].Scheduled_Start_Date__c = Date.today();
        activityList[0].Well_Event__c = wellEvent.Id;
        update activityList;

        WOEquipmentHierarchyCtrl WOHierarchyCtrl = new WOEquipmentHierarchyCtrl(
            new ApexPages.StandardController(activityList[0]));
        
        List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();

        //Test Hierarchy Structure
        System.assertEquals(5,equipmentHierarchyResult.size());
        for(Integer i=0; i < 5; i++) {
            System.debug('Superior Equipment: ' +testEquipmentHierarchy[i].Superior_Equipment__c);
            System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchy[i].Id);
            System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
            if(i == 0) {
                //Top Level tests
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            } else if (i == 4) {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
            } else {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            }
        }

        //Test Hierarchy Behaviour
        ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchy[0].Id);
        WOHierarchyCtrl.toggleNodeState();
        equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();
        System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());

        WOHierarchyCtrl.getEquipmentHierarchyVisible();

    }
    
    static Testmethod void testEquipmentHierarchyWOActivitySystem() {

        SetupTestData();

        List<Equipment__c> eqList = [select Id,Facility__c, Well_Event__c from Equipment__c];
        for(Equipment__c eq : eqList){
            eq.System__c = sys.Id;
        }
        update eqList;

        //Create and assign work order activity
        List<Work_Order_Activity__c> activityList = VTT_TestData.createWorkOrderActivitiesWithAssignments(maintenanceRecord.Id, null, 1);
        activityList[0].Scheduled_Start_Date__c = Date.today();
        activityList[0].System__c = sys.Id;
        update activityList;

        WOEquipmentHierarchyCtrl WOHierarchyCtrl = new WOEquipmentHierarchyCtrl(
            new ApexPages.StandardController(activityList[0]));
        
        List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();

        //Test Hierarchy Structure
        System.assertEquals(5,equipmentHierarchyResult.size());
        for(Integer i=0; i < 5; i++) {
            System.debug('Superior Equipment: ' +testEquipmentHierarchy[i].Superior_Equipment__c);
            System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchy[i].Id);
            System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
            if(i == 0) {
                //Top Level tests
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            } else if (i == 4) {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
            } else {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            }
        }

        //Test Hierarchy Behaviour
        ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchy[0].Id);
        WOHierarchyCtrl.toggleNodeState();
        equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();
        System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());

        WOHierarchyCtrl.getEquipmentHierarchyVisible();

    }
    
    static Testmethod void testEquipmentHierarchyWOActivitySubSystem() {

        SetupTestData();

        List<Equipment__c> eqList = [select Id,Facility__c, Well_Event__c from Equipment__c];
        for(Equipment__c eq : eqList){
            eq.Sub_System__c = subSys.Id;
        }
        update eqList;

        //Create and assign work order activity
        List<Work_Order_Activity__c> activityList = VTT_TestData.createWorkOrderActivitiesWithAssignments(maintenanceRecord.Id, null, 1);
        activityList[0].Scheduled_Start_Date__c = Date.today();
        activityList[0].Sub_System__c = subSys.Id;
        update activityList;

        WOEquipmentHierarchyCtrl WOHierarchyCtrl = new WOEquipmentHierarchyCtrl(
            new ApexPages.StandardController(activityList[0]));
        
        List<WellLocationequipmentHierarchyCtrlX.EquipmentNode> equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();

        //Test Hierarchy Structure
        System.assertEquals(5,equipmentHierarchyResult.size());
        for(Integer i=0; i < 5; i++) {
            System.debug('Superior Equipment: ' +testEquipmentHierarchy[i].Superior_Equipment__c);
            System.assertEquals(equipmentHierarchyResult[i].getId(), testEquipmentHierarchy[i].Id);
            System.assertEquals(equipmentHierarchyResult[i].getLevel(), i);
            if(i == 0) {
                //Top Level tests
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), null);
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), false);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            } else if (i == 4) {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), false);
            } else {
                System.assertEquals(equipmentHierarchyResult[i].getParentId(), equipmentHierarchyResult[i-1].getId());
                System.assertEquals(equipmentHierarchyResult[i].getIsParentClosed(), true);
                System.assertEquals(equipmentHierarchyResult[i].getHasChildren(), true);
            }
        }

        //Test Hierarchy Behaviour
        ApexPages.currentPage().getParameters().put('nodeId', testEquipmentHierarchy[0].Id);
        WOHierarchyCtrl.toggleNodeState();
        equipmentHierarchyResult = WOHierarchyCtrl.getEquipmentHierarchy();
        System.assertEquals(equipmentHierarchyResult[1].getIsParentClosed(), equipmentHierarchyResult[0].getClosed());

        WOHierarchyCtrl.getEquipmentHierarchyVisible();

    }

    private static HOG_Service_Code_MAT__c serviceCodeMAT;
    private static HOG_Work_Order_Type__c workOrderType;
    private static HOG_Notification_Type_Priority__c notificationTypePriority;
    private static HOG_Service_Category__c serviceCategory;
    private static HOG_Service_Code_Group__c serviceCodeGroup;
    private static HOG_Notification_Type__c notificationType;
    private static HOG_Service_Required__c serviceRequiredList;
    private static HOG_User_Status__c userStatus;
    private static HOG_Service_Priority__c servicePriorityList;
    private static Business_Unit__c businessUnit;
    private static Business_Department__c businessDepartment;
    private static Operating_District__c operatingDistrict;
    private static Field__c field;
    private static Route__c route;
    private static Location__c location;
    private static Well_Event__c wellEvent;
    private static System__c sys;
    private static Sub_System__c subSys;
    private static Functional_Equipment_Level__c fel;
    private static Yard__c yard;
    private static Facility__c facility;    
    private static HOG_Maintenance_Servicing_Form__c maintenanceRecord;
    private static Work_Order_Activity__c workOrderActivity;
    private static Id recordTypeId;
    private static String workOrderNumber = 'TEST_WORK';
    private static List<String> WORKORDERRECORDTYPE = new List<String>{'Flushby - Production Flush', 'Coil Tubing Unit - Rod Side Entry'};
    private static List<Equipment__c> testEquipmentHierarchy;


    public static void SetupTestData()
    {  

        final String serviceCategoryCode = 'MNT';  // Maintenance service category code
        final String notificationTypeCode = 'WP';
        final String orderTypeCode = 'WP01';
        final String matCode = 'TXN';
        final String notificationNumber = 'TEST_NOTI';
        final String catalogueCode = 'CATCODE';
        final String partCode = 'PARTCODE';

        //-- Begin setup of data needed for Maintenance Servicing --//                
        //-- Setup Service Category
        serviceCategory = new HOG_Service_Category__c
            (
                Name = 'Test Category',
                Service_Category_Code__c = serviceCategoryCode                                
            );
        insert serviceCategory;

        //-- Setup Service Code Group
        serviceCodeGroup = ServiceCodeGroupTestData.createServiceCodeGroup('Test Group', true);
        insert serviceCodeGroup;
        //--

        //-- Setup Notification Type
        notificationType = new HOG_Notification_Type__c
            (
                HOG_Service_Category__c = serviceCategory.Id,
                Notification_Type__c = notificationTypeCode,
                Order_Type__c = orderTypeCode
            );
        insert notificationType;
        //--
  
        //-- Setup Service Code MAT        
       serviceCodeMAT = ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', matCode, 'Master');       
        insert serviceCodeMAT;        
        //--

        //-- Setup Service Required
        serviceRequiredList = ServiceRequiredTestData.createServiceRequired('Test Service Required', true, true, true);
        insert serviceRequiredList;
        //--

        //-- Setup Service User Status
        userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;
        //--

        //-- Setup Service - Priority
        servicePriorityList = ServicePriorityTestData.createServicePriority('Service Priority 1', 1, '1');
        insert servicePriorityList;
        //--
                
        //-- Setup Notification Type - Priority Detail
        notificationTypePriority = NotificationTypePriorityTestData.createNotificationTypePriority(notificationType.Id, servicePriorityList.Id, false);
        insert notificationTypePriority;        
        //--

        //-- Setup Work Order Type
        workOrderType = WorkOrderTypeTestData.createWorkOrderType(notificationType.Id, serviceCodeMAT.Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true);
        insert workOrderType;
        //--

        //-- Setup objects for Maintenance Work Order                                 
        businessUnit = new Business_Unit__c(Name = 'Test Business Unit');
        insert businessUnit;

        businessDepartment = BusinessDepartmentTestData.createBusinessDepartment('Test Business Department');    
        insert businessDepartment;

        operatingDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Field', businessDepartment.Id, businessUnit.Id);
        insert operatingDistrict;
        
        recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

        field = FieldTestData.createField('Test Field Control Centre', operatingDistrict.Id, recordTypeId);
        insert field;

        route = RouteTestData.createRoute('999');
        insert route;                  

        location = LocationTestData.createLocation('Test Location', route.Id, field.Id);    
        insert location;
        
        wellEvent = LocationTestData.createEvent('Test Well Event', location.Id);    
        insert wellEvent;
        
        sys = LocationTestData.createSystem('Test System');
        subSys = LocationTestData.createSubSystem('Test Sub System');    
        fel = LocationTestData.createFEL('Test FEL');
        yard = LocationTestData.createYard('Test Sub Yard');    

        facility = FacilityTestData.createFacility('Test Facility', route.Id, field.Id);
        insert facility;        
        
        testEquipmentHierarchy = EquipmentTestData.createEquipmentHierarchy(location);

        //-- Setup HOG_Maintenance_Servicing_Form__c        
        maintenanceRecord = new HOG_Maintenance_Servicing_Form__c
        (
            Name = 'Test Name',
            Functional_Location__c = 'Test FLOC',
            Work_Order_Number__c = workOrderNumber,
            Notification_Number__c = notificationNumber,
            Notification_Type__c = notificationTypeCode,
            Order_Type__c = orderTypeCode,
            Priority_Number__c = 1,
            MAT_Code__c = matCode,
            ALT_Confirmed__c = false,
            Plant_Section__c = 'PS'
        );        
        insert maintenanceRecord;
        
        system.debug(maintenanceRecord);
        
        system.debug([select HOG_Notification_Type__c from HOG_Maintenance_Servicing_Form__c where Functional_Location__c = 'Test FLOC' limit 1]);

    }
}