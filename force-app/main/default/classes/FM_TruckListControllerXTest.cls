/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class FM_TruckListControllerXTest
{
    
    private static String sYesterdayComments = 'testing comments';
    private static Integer iLoadCount = 5;
    
    @isTest
    public static void testTruckListCommentsController(){
        // Create test Objects
        CreateTestData();

        PageReference pageRef = Page.FM_TruckListComments;
        Test.setCurrentPage(pageRef);
        
        FM_Truck_List_Comments__c tlComments = [select Id from FM_Truck_List_Comments__c limit 1];

        ApexPages.StandardController con = new ApexPages.StandardController(tlComments);
        FM_TruckListCommentsControllerX custController = new FM_TruckListCommentsControllerX(con);
        
        custController.save();
        
        system.assert(custController.truckListComments != null);
        
    }
    
    @isTest
    public static void testTruckListEmailController(){
        // Create test Objects
        CreateTestData();

        // get TruckTrip, add dispatcher, update and get record again
        FM_Truck_Trip__c tt = [select Truck_Trip_Status__c, Run_Sheet_Date__c, Planned_Dispatch_Date__c, Shift__c, Unit__c, TL_Dispatched_By__r.Lastname from FM_Truck_Trip__c limit 1];

        PageReference pageRef = Page.FM_TruckListEmail;
        pageRef.getParameters().put('dv_date',String.valueOf(tt.Planned_Dispatch_Date__c));
        pageRef.getParameters().put('dv_status',tt.Truck_Trip_Status__c);
        pageRef.getParameters().put('dv_shift',tt.Shift__c);
        pageRef.getParameters().put('dv_unit',tt.Unit__c);
        Test.setCurrentPage(pageRef);

        FM_TruckTripEmailComponentController custController = new FM_TruckTripEmailComponentController();
        
        system.assert(custController.oComments != null);
        //system.assert(custController.getFormattedDate() != null);
        
        List<FM_Truck_Trip__c> lFirstPage = custController.lFirstPage;
        List<FM_Truck_Trip__c> lNextPages = custController.lNextPages;
        Integer nextPageSize = custController.getNextPagesSize();

        String upperStatus = custController.getUpperStatus();
        System.assertEquals('Your Dispatcher:', upperStatus);
        
        // Load request for 1 oil tomorrow
        Date testDate = Date.today().addDays(1);
        DateTime dFormat = DateTime.newInstance(testDate.year(), testDate.month(), testDate.day());
        System.assertEquals(dFormat.format('MMMM dd yyyy'), custController.getFormattedDate());
    }
    
    private static List<FM_Truck_Trip__c> GetTruckTripData(FM_Truck_List__c oTruckList)
    {
        List<FM_Truck_Trip__c> lTruckTrips = [SELECT Id, Name, Axle__c, Carrier__c, Standing_Comments__c, 
                                            Facility__c, Facility_Lookup__c, Facility_Type__c, 
                                            Location_Lookup__c, Order__c, Priority__c, Product__c, 
                                            Run_Sheet_Date__c, 
                                            Run_Sheet_Lookup__c, 
                                            Shift__c, Shift_Route_Product__c, Sour__c, Tank__c, 
                                            Truck_List_Lookup__c, Truck_Trip_Status__c, Unit__c
                                            FROM FM_Truck_Trip__c WHERE Run_Sheet_Date__c = 
                                            :oTruckList.Run_Sheet_Date__c AND Shift__c = 
                                            :oTruckList.Shift__c AND Unit__c = 
                                            :oTruckList.Unit__c];

        for(FM_Truck_Trip__c oOneTrip : lTruckTrips)
        {
            System.debug('Association is: ' + oOneTrip.Truck_List_Lookup__c);
        }

        return lTruckTrips;
    }
	
    private static void CreateTestData()
    {
        // Test Account
        Account oAccount = new Account();
        oAccount.Name = 'Carrier Account';
        insert oAccount;

        // Test Carrier
        Carrier__c oCarrier = new Carrier__c();
        oCarrier.Carrier__c = oAccount.Id;
        oCarrier.Carrier_Name_For_Fluid__c = 'Test Carrier';
        insert oCarrier;

        // Test Carrier Unit
        RecordType oCarrierUnitRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Carrier_Unit__c' AND DeveloperName = 'Fluid_Carrier'];
        Carrier_Unit__c oCarrierUnit = new Carrier_Unit__c();
        oCarrierUnit.Carrier__c = oCarrier.Id;
        oCarrierUnit.RecordTypeId = oCarrierUnitRecordType.Id;
        oCarrierUnit.Unit_Email__c = 'testing@testing.com';
        oCarrierUnit.Name = 'Unit1';
        insert oCarrierUnit;

        // create another Unit with no email
        oCarrierUnit = new Carrier_Unit__c();
        oCarrierUnit.Carrier__c = oCarrier.Id;
        oCarrierUnit.RecordTypeId = oCarrierUnitRecordType.Id;
        oCarrierUnit.Unit_Email__c = null;
        oCarrierUnit.Name = 'Unit2';
        insert oCarrierUnit;

        // Test Carrier Desk
        RecordType oDispatchDeskRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Dispatch_Desk__c' AND DeveloperName = 'Fluid_Dispatch'];
        Dispatch_Desk__c oDispatchDesk = new Dispatch_Desk__c();
        oDispatchDesk.Name = 'Desk 1';
        oDispatchDesk.Phone__c = '403-555-1234';
        oDispatchDesk.RecordTypeId = oDispatchDeskRecordType.Id;
        insert oDispatchDesk;

        // Test Business Department
        Business_Department__c oBusiness = new Business_Department__c();
        oBusiness.Name = 'Husky Business';
        insert oBusiness;

        // Test Operating District
        Operating_District__c oDistrict = new Operating_District__c();
        oDistrict.Name = 'District 1234';
        oDistrict.Business_Department__c = oBusiness.Id;
        insert oDistrict;

        // Test AMU Field
        Field__c oField = new Field__c();
        oField.Name = 'AMU Field';
        oField.Operating_District__c = oDistrict.Id;
        insert oField;

        // Test Route
        Route__c oRoute = new Route__c();
        oRoute.Fluid_Management__c = true;
        oRoute.Name = '999';
        oRoute.Route_Number__c = '999';
        insert oRoute;

        // Test Location
        Location__c oLocation = new Location__c();
        oLocation.Name = 'Husky Well';
        oLocation.Fluid_Location_Ind__c = true;
        oLocation.Location_Name_for_Fluid__c = 'Husky Well Fluid';
        oLocation.Operating_Field_AMU__c = oField.Id;
        oLocation.Route__c = oRoute.Id;
        oLocation.Functional_Location_Category__c = 4;
        insert oLocation;

        // Test Facility
        Facility__c oFacility = new Facility__c();
        oFacility.Fluid_Facility_Ind__c = true;
        oFacility.Facility_Name_for_Fluid__c = 'Fluid Facility B';
        oFacility.Name = 'Facility B';
        insert oFacility;

        // Test Run Sheet - this run sheet (because of Date__c and Tomorrow_Oil__c and iLoadCount will result in a truck list with the same
        // date and Day shift to be created, and 2 truck trips with the same info to be created.
        FM_Run_Sheet__c oRunSheet = new FM_Run_Sheet__c();
        oRunSheet.Date__c = Date.today();
        oRunSheet.Well__c = oLocation.Id;
        oRunSheet.Act_Tank_Level__c = 40;
        oRunSheet.Act_Flow_Rate__c = 3;
        oRunSheet.Tank__c = 'Tank 1';
        oRunSheet.Tank_Low_Level__c = 1;
        oRunSheet.Tank_Size__c = 42;

        //oRunSheet.Tomorrow_Oil__c = iLoadCount;
        insert oRunSheet;

        //Create Load Requests
        List<FM_Load_Request__c> loadRequests = new List<FM_Load_Request__c>();
        for(Integer i=1; i <= iLoadCount; i++) {
            loadRequests.add(FM_TestData.createLoadRequest(oRunSheet.Id, 'Standard Load', 
                'O', 'day', null, 'T5X', 'Primary', null, 
                'Load Request created from Runsheet', null, oLocation.Id, FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, null, 
                'Tank 1', oRunSheet.Act_Tank_Level__c, 20, 120, 4, 5));
        }

        // Create Comment Object
        FM_Truck_List_Comments__c oComment = new FM_Truck_List_Comments__c();
        oComment.Run_Sheet_Date__c = Date.Today() - 1;
        oComment.Dispatch_Comments__c = sYesterdayComments;
        insert oComment;

        // Custom Settings
        HOG_Settings__c oSettings = new HOG_Settings__c();
        oSettings.Fluid_Truck_List_Blank_Lines__c = 3;
        insert oSettings;
    }

  private static User createUser() {
    User user = new User();
    Profile p = [Select Id from Profile where Name='Standard HOG - General User'];

    Double random = Math.Random();

    user.Email = 'TestUser' +  random + '@hog.com';
    user.Alias = 'TestUser' ;
    user.EmailEncodingKey = 'UTF-8';
    user.LastName = 'User';
    user.LanguageLocaleKey = 'en_US';
    user.LocaleSidKey = 'en_US';
    user.ProfileId = p.Id;
    user.TimeZoneSidKey = 'America/Los_Angeles';
    user.UserName = 'TestUser' + random + '@hog.com.unittest';

    insert user;
    return user;
  }

}