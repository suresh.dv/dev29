global virtual with sharing class DSP_CMS_PageTitleController extends cms.ContentTemplateController{
    global DSP_CMS_PageTitleController(cms.CreateContentController cc){
        super(cc);
    }
    
    global DSP_CMS_PageTitleController(cms.GenerateContent cc){
        super(cc);
    }

    global DSP_CMS_PageTitleController(){}

    global virtual override String getHTML(){return '';}

    public String title {
        get{
            return getProperty('title');
        }
        set;
    }

    public String titleHTML(){
        return title;
    }
}