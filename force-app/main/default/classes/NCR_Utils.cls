public class NCR_Utils
{
    //if Owner is a portal user, share this record with his Poral role group 
    public static boolean manualNCRCaseShareReadOwner(Id recordId, ID OwnerID)
    {
       System.debug('NCR_Utils.manualNCRCaseShareReadOwner');
       System.debug('Create sharing for record: ' + recordId);
       
       USer userObj = [select id, AccountID, ContactID, PortalRole, UserRoleID from User where id= :OwnerID LIMIT 1];
       if(userObj.AccountID != null)
       {
           Group grp = [select id from group where type = 'RoleAndSubordinates' and relatedid = :userObj.UserRoleID LIMIT 1];         
           return manualNCRCaseShareRead(recordId, grp.ID);
       }
       else
       {
           System.debug('User ID: ' + OwnerID + ' is not portal user');
       }
       return false;    
    }
    
    //Share this record with any portal roles if Owner, Originator, QA LEad or Assignee are portal users
    public static boolean manualNCRCaseShareRead(Id recordId)
    {
    
       System.debug('NCR_Utils.manualNCRCaseShareRead');
       System.debug('Create sharing for record: ' + recordId);
       if(recordId == null)  
       {
       
           return false;
       }
    
       NCR_Case__c ncr_record =  [SELECT OwnerID, Originator__c,  QA_Lead__c, Assignee__c from NCR_Case__c where id = :recordId LIMIT 1];
    

       List<User> userList = [select id, AccountID, ContactID, PortalRole, UserRoleID from User 
                           where id in (:ncr_record.OwnerID, :ncr_record.Originator__c, :ncr_record.QA_Lead__c, :ncr_record.Assignee__c) AND  AccountID<>null ];
       
      
       for(User userObj: userList) 
       {       
           Group grp = [select id from group where type = 'RoleAndSubordinates' and relatedid = :userObj.UserRoleID LIMIT 1];  
           System.debug('Sharing Group: ' +  grp.ID); 
           return NCR_Utils.manualNCRCaseShareRead(RecordID, grp.ID);
       }    
    

        return null;
    }

    public static boolean manualNCRCaseShareRead(Id recordId, Id userOrGroupId){
          // Create new sharing object for the custom object Job.
          NCR_Case__Share ncrShr  = new NCR_Case__Share ();
       
          // Set the ID of record being shared.
          ncrShr.ParentId = recordId;
            
          // Set the ID of user or group being granted access.
          ncrShr.UserOrGroupId = userOrGroupId;
            
          // Set the access level.
          ncrShr.AccessLevel = 'Read';
            
          // Set rowCause to 'manual' for manual sharing.
          // This line can be omitted as 'manual' is the default value for sharing objects.
          ncrShr.RowCause = Schema.NCR_Case__Share.RowCause.Manual;
            
          // Insert the sharing record and capture the save result. 
          // The false parameter allows for partial processing if multiple records passed 
          // into the operation.
          Database.SaveResult sr = Database.insert(ncrShr,false);
    
          // Process the save results.
          if(sr.isSuccess()){
             // Indicates success
              System.debug('Success');
              
              
             List<NCR_Case__Share> shareresult = [SELECT Id, ParentId, UserOrGroupId, AccessLevel, RowCause, LastModifiedDate, LastModifiedById, IsDeleted FROM NCR_Case__Share where ParentId = :recordId];
             system.debug(shareresult);             
              
             return true;
          }
          else {
          
                     
             // Get first save result error.
             Database.Error err = sr.getErrors()[0];
             System.debug(err);  
             // Check if the error is related to trival access level.
             // Access levels equal or more permissive than the object's default 
             // access level are not allowed. 
             // These sharing records are not required and thus an insert exception is acceptable. 
             if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&  
                      err.getMessage().contains('AccessLevel')){
                // Indicates success.
                return true;
             }
             else{
                // Indicates failure.
                return false;
             }
           }
       }

    //Share this record with any portal roles if Owner, Originator, QA LEad or Assignee are portal users
    public static boolean manualNCRCARShareRead(Id recordId)
    {
    
       System.debug('NCR_Utils.manualNCRCARShareRead');
       System.debug('Create sharing for record: ' + recordId);
       if(recordId == null)  
       {
       
           return false;
       }
    
       NCR_CAR__c ncr_record =  [SELECT OwnerID, QA_Lead__c, Assignee__c from NCR_CAR__c where id = :recordId LIMIT 1];
    

       List<User> userList = [select id, AccountID, ContactID, PortalRole, UserRoleID from User 
                           where id in (:ncr_record.OwnerID, :ncr_record.QA_Lead__c, :ncr_record.Assignee__c) AND  AccountID<>null ];
       
      
       for(User userObj: userList) 
       {       
           Group grp = [select id from group where type = 'RoleAndSubordinates' and relatedid = :userObj.UserRoleID LIMIT 1];  
           System.debug('Sharing Group: ' +  grp.ID); 
           return NCR_Utils.manualNCRCARShareRead(RecordID, grp.ID);
       }    
    

        return null;
    }

    public static boolean manualNCRCARShareRead(Id recordId, Id userOrGroupId){
          // Create new sharing object for the custom object Job.
          NCR_CAR__Share ncrShr  = new NCR_CAR__Share ();
       
          // Set the ID of record being shared.
          ncrShr.ParentId = recordId;
            
          // Set the ID of user or group being granted access.
          ncrShr.UserOrGroupId = userOrGroupId;
            
          // Set the access level.
          ncrShr.AccessLevel = 'Read';
            
          // Set rowCause to 'manual' for manual sharing.
          // This line can be omitted as 'manual' is the default value for sharing objects.
          ncrShr.RowCause = Schema.NCR_Case__Share.RowCause.Manual;
            
          // Insert the sharing record and capture the save result. 
          // The false parameter allows for partial processing if multiple records passed 
          // into the operation.
          Database.SaveResult sr = Database.insert(ncrShr,false);
    
          // Process the save results.
          if(sr.isSuccess()){
             // Indicates success
              System.debug('Success');
              
              
             List<NCR_CAR__Share> shareresult = [SELECT Id, ParentId, UserOrGroupId, AccessLevel, RowCause, LastModifiedDate, LastModifiedById, IsDeleted FROM NCR_CAR__Share where ParentId = :recordId];
             system.debug(shareresult);             
              
             return true;
          }
          else {
          
                     
             // Get first save result error.
             Database.Error err = sr.getErrors()[0];
             System.debug(err);  
             // Check if the error is related to trival access level.
             // Access levels equal or more permissive than the object's default 
             // access level are not allowed. 
             // These sharing records are not required and thus an insert exception is acceptable. 
             if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&  
                      err.getMessage().contains('AccessLevel')){
                // Indicates success.
                return true;
             }
             else{
                // Indicates failure.
                return false;
             }
           }
       }



    Public Static Boolean IsNCRAdminUser()
    {    
    
        //lets check if user sysadmin
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String MyProflieName = PROFILE[0].Name;
        if(MyProflieName  == 'System Administrator')
        {
            return true;
        }

        //lets check if user has NCR Admin permissionset
        PermissionSet ps = [select id, name from PermissionSet where name = 'NCR_Admin' limit 1];
        List<PermissionSetAssignment> psalist = [select id from PermissionSetAssignment where AssigneeId =:userinfo.getUserId() and PermissionSetId =:ps.id LIMIT 1];
        if(psalist.size()>0)
        {
            return true;        
        }
        return false;
    }

    public static void logError(Exception ex)
    {
    
        string message = ex.getMessage();
        logError('Unexpected error:' +message );
    }
    
    public static void logError(string errorMessage)
    {
    
        /*
        USCP_Settings__c  mc = USCP_Settings__c.getOrgDefaults();
        String csEmail  = mc.Customer_Support_Email__c;    
        String csPhone  = mc.Customer_Support_Phone__c;  
        String csMsg  =  mc.Customer_Support_Message__c;
        csMsg = csMsg.replace('{PHONE}', csPhone);
        csMsg = csMsg.replace('{EMAIL}', '<a href= mailto:'+ csEmail + '>' + csEmail + '</a>' );          
        */
    
        String message = errorMessage; // +  '<br/>' +  csMsg;
        system.debug(message);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, message));
    }    


}