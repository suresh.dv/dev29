public class OperationalEventListController {
    private static final String ACTIVE_EVENTS_LIST_PAGE_URI = '/apex/ActiveOperationalEventListView';
  //  private static Boolean activeTeamAllocationEventsLoaded = false;
    
    public Id selectedTAId {get;set;}
    public List<SelectOption> activeTAOptions {get;set;}
    public List<Operational_Event__c> activeTeamAllocationEvents {get;set;}
    public List<Team_Allocation__c> activeTeamAllocations {get;set;}
    public OperationalEventListController()
    {
        getActiveTeamAllocations();
        if(activeTAOptions.size() > 0)
        {   
            selectedTAId = activeTAOptions[0].getValue();
            getActiveTeamAllocationEvents();
        }
    }
    public void getActiveTeamAllocationEvents() 
    {
   /*     if (!activeTeamAllocationEventsLoaded) {
            // Lookup active team allocations
            activeTeamAllocation = TeamAllocationUtilities.getActiveTeamAllocation();
            if (activeTeamAllocation != null) {
                activeTeamAllocationEvents = TeamAllocationUtilities.getOperationalEvents(activeTeamAllocation.Id);
            }
        }*/
        activeTeamAllocationEvents = new List<Operational_Event__c>();
        System.debug('****** selectedTAId =' + selectedTAId);
        if (selectedTAId != null)
            activeTeamAllocationEvents = TeamAllocationUtilities.getOperationalEvents(selectedTAId);
    }
    
    /*
    private void getActiveTeamAllocations()
    {
        activeTAOptions = new List<SelectOption>();
        activeTeamAllocations = TeamAllocationUtilities.getActiveTeamAllocations();
        for(Team_Allocation__c ta : activeTeamAllocations)
        {
            activeTAOptions.add(new SelectOption(ta.Id, ta.Team_Allocation_Name__c + ' - ' + ta.RecordType.Name));
        }
    }*/
    
    private void getActiveTeamAllocations()
    {
        List<RecordTypeInfo> infos = Operational_Event__c.SObjectType.getDescribe().getRecordTypeInfos();
        activeTAOptions = new List<SelectOption>();
        activeTeamAllocations = TeamAllocationUtilities.getActiveTeamAllocations();
        System.debug('==> activeTeamAllocations  are=='+activeTeamAllocations );
        if (infos.size() > 1) {
            for (RecordTypeInfo i : infos) {
                System.debug('==>> i is '+i);
                if(i.getName() != 'Master'){
                    System.debug('The name is ==> '+i.getName());
                    for(Team_Allocation__c ta : activeTeamAllocations)
                    {
                        if(i.isAvailable() && ta.RecordType.Name == i.getName()){
                            activeTAOptions.add(new SelectOption(ta.Id, ta.Team_Allocation_Name__c + ' - ' + ta.RecordType.Name));
                        }
                    }                 
                } 
            }
        }
    }    
    
 /*   public Team_Allocation__c getActiveTeamAllocation() {
        if (!activeTeamAllocationEventsLoaded) {
            getActiveTeamAllocationEvents();
        } 
        
        return activeTeamAllocation;
    } */
    
    public PageReference newOperationalEvent() 
    {
        Team_Allocation__c currentTA = null;
        for(Team_Allocation__c ta : activeTeamAllocations)
           if(ta.Id == selectedTAId)
               currentTA = ta;
        
        if (currentTA == null) return null;
        System.debug('-------- newOperationalEvent = ' + currentTA.RecordType.Name);       
        Operational_Event__c newEvent = new Operational_Event__c();
        Map<String, Schema.Recordtypeinfo> recTypesByName = Operational_Event__c.SObjectType.getDescribe().getRecordTypeInfosByName();
     
        Schema.DescribeSObjectResult describeResult = newEvent.getSObjectType().getDescribe();
        String safeReturnUrl = EncodingUtil.urlEncode(ACTIVE_EVENTS_LIST_PAGE_URI, 'UTF-8');
        String addNewUrl = '/apex/OperationalEventEdit?' 
         //   + describeResult.getKeyPrefix() + '/e?' 
            + 'retURL=' + safeReturnUrl
        //    + '&rt=' + recTypesByName.get(currentTA.RecordType.Name).getRecordTypeId()
            + '&ta=' + currentTA.Id ;
        
        System.debug(
            '\n**********************************\n'
            + 'METHOD: newOperationalEvent()'
            + '\n'
            + 'addNewUrl = ' + addNewUrl
            + '\n**********************************\n');
            
        PageReference pr = new PageReference(addNewUrl);
        pr.setRedirect(true);
        return pr;
    }
    
    public Boolean hasActiveTeamAllocationEvents {
        get {
            return (activeTeamAllocationEvents == null) ? false : activeTeamAllocationEvents.size() > 0;
        }
    }
    
    public Boolean hasActiveTeamAllocation {
        get {
            return (activeTeamAllocations != null);
        }
    }  
}