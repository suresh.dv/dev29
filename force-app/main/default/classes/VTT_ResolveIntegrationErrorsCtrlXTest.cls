@isTest
private class VTT_ResolveIntegrationErrorsCtrlXTest {
	
	@isTest static void testUpdateSAPHappyPath() {
		User runningUser = VTT_TestData.createVTTUser();

        System.runAs(runningUser) {
        	MaintenanceServicingUtilities.executeTriggerCode = false;  
            VTT_TestData.SetupRelatedTestData(false);

            System.AssertNotEquals(runningUser.Id, Null);

            Account vendor1 = VTT_TestData.createVendorAccount('Vendor1');
            Contact tradesman1 = VTT_TestData.createTradesmanContact('Tom', 'Cruise',  vendor1.id, runningUser.id);           

            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);       
            HOG_Maintenance_Servicing_Form__c workOrder1 = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            workOrder1.User_Status_Code__c = '5X';
            workOrder1.Order_Type__c = 'WP01';
            workOrder1.Plant_Section__c  = '200';       
            workOrder1.Work_Order_Priority_Number__c  = '1';    
            workOrder1.Equipment__c = VTT_TestData.equipment.Id;
            workOrder1.Location__c = VTT_TestData.location.Id;
            workOrder1.Operating_Field_AMU_Lookup__c = VTT_TestData.field.Id;  
            update workOrder1;

            MaintenanceServicingUtilities.executeTriggerCode = true; 

            List<Work_Order_Activity__c> activityList1 =  VTT_TestData.createWorkOrderActivitiesWithAssignments(workOrder1.ID, tradesman1.ID, 1);
            Work_Order_Activity__c woActivity = activityList1[0];
            woActivity.Operation_Number__c = '0010';
            update woActivity;

	        //Create Logs and Log Entries
	        VTT_Utilities.executeTriggerCode = false;
            List<Work_Order_Activity_Log_Entry__c> logEntryList = new List<Work_Order_Activity_Log_Entry__c>();
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
            	woActivity, VTT_Utilities.LOGENTRY_STARTJOB, null, 
            	null, null, 'Start Job'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
            	woActivity, VTT_Utilities.LOGENTRY_STARTATSITE, null,
            	null, null, 'Start at Equipment'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                    woActivity, VTT_Utilities.LOGENTRY_FINISHEDATSITE, null,
                    null, null, 'Finish Job at Equipment'));
    	    logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
            	woActivity, VTT_Utilities.LOGENTRY_JOBCOMPLETE, null,
            	null, null, 'Job Complete'));
    		insert logEntryList;

            Id updateAcStatusRecordTypeId = [Select Id From RecordType 
                Where DeveloperName =: VTT_IntegrationUtilities.RECORDTYPE_UPDATE_ACTIVITY_INTEGRATION_ERROR].Id;
    		VTT_Integration_Error__c error = new VTT_Integration_Error__c(
	        	Work_Order_Activity__c = woActivity.Id,
	        	Status__c = VTT_IntegrationUtilities.STATUS_ERROR,
	        	Error_Log__c = 'Error',
                RecordTypeId = updateAcStatusRecordTypeId
	        );
	        List<VTT_Integration_Error__c> errorList = new List<VTT_Integration_Error__c>{error};
	        insert errorList;
    		//Create VTT_Integration 
    		VTT_Utilities.executeTriggerCode = true;

    		//Test
	        Test.startTest();
	            Test.setMock(WebServiceMock.class, new HOG_SAPUpdateWorkOrderActivitiesMockImpl(false));
		        Test.setCurrentPage(Page.VTT_ResolveIntegrationErrors);
		        ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(errorList);
	            stdController.setSelected(errorList);
		        VTT_ResolveIntegrationErrorsCtrlX extController = new VTT_ResolveIntegrationErrorsCtrlX(stdController);

	            //Test
		        extController.UpdateSAP();
		    Test.stopTest();

		    //Validate Result
		    error = [Select Id, Name, Error_Log__c, Work_Order_Activity__c, Status__c
	    			 From VTT_Integration_Error__c
	    			 Where Id =: error.Id];
	    	System.assertNotEquals(null, error.Id);
	    	System.assertEquals(error.Status__c, VTT_IntegrationUtilities.STATUS_SUCCESS);
        }
	}
	
	@isTest static void testUpdateSAPError() {
		User runningUser = VTT_TestData.createVTTUser();

        System.runAs(runningUser) {
        	MaintenanceServicingUtilities.executeTriggerCode = false;  
            VTT_TestData.SetupRelatedTestData(false);

            System.AssertNotEquals(runningUser.Id, Null);

            Account vendor1 = VTT_TestData.createVendorAccount('Vendor1');
            Contact tradesman1 = VTT_TestData.createTradesmanContact('Tom', 'Cruise',  vendor1.id, runningUser.id);           

            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);       
            HOG_Maintenance_Servicing_Form__c workOrder1 = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            workOrder1.User_Status_Code__c = '5X';
            workOrder1.Order_Type__c = 'WP01';
            workOrder1.Plant_Section__c  = '200';       
            workOrder1.Work_Order_Priority_Number__c  = '1';    
            workOrder1.Equipment__c = VTT_TestData.equipment.Id;
            workOrder1.Location__c = VTT_TestData.location.Id;
            workOrder1.Operating_Field_AMU_Lookup__c = VTT_TestData.field.Id;  
            update workOrder1;

            MaintenanceServicingUtilities.executeTriggerCode = true; 

            List<Work_Order_Activity__c> activityList1 =  VTT_TestData.createWorkOrderActivitiesWithAssignments(workOrder1.ID, tradesman1.ID, 1);
            Work_Order_Activity__c woActivity = activityList1[0];
            woActivity.Operation_Number__c = '0010';
            update woActivity;

	        //Create Logs and Log Entries
	        VTT_Utilities.executeTriggerCode = false;
            List<Work_Order_Activity_Log_Entry__c> logEntryList = new List<Work_Order_Activity_Log_Entry__c>();
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
            	woActivity, VTT_Utilities.LOGENTRY_STARTJOB, null, 
            	null, null, 'Start Job'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
            	woActivity, VTT_Utilities.LOGENTRY_STARTATSITE, null,
            	null, null, 'Start at Equipment'));
            logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                    woActivity, VTT_Utilities.LOGENTRY_FINISHEDATSITE, null,
                    null, null, 'Finish Job at Equipment'));
    	    logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
            	woActivity, VTT_Utilities.LOGENTRY_JOBCOMPLETE, null,
            	null, null, 'Job Complete'));
    		insert logEntryList;

            Id updateAcStatusRecordTypeId = [Select Id From RecordType 
                Where DeveloperName =: VTT_IntegrationUtilities.RECORDTYPE_UPDATE_ACTIVITY_INTEGRATION_ERROR].Id;
    		VTT_Integration_Error__c error = new VTT_Integration_Error__c(
	        	Work_Order_Activity__c = woActivity.Id,
	        	Status__c = VTT_IntegrationUtilities.STATUS_ERROR,
	        	Error_Log__c = 'Error',
                RecordTypeId = updateAcStatusRecordTypeId
	        );
	        List<VTT_Integration_Error__c> errorList = new List<VTT_Integration_Error__c>{error};
	        insert errorList;
    		//Create VTT_Integration 
    		VTT_Utilities.executeTriggerCode = true;

    		//Test
	        Test.startTest();
	            Test.setMock(WebServiceMock.class, new HOG_SAPUpdateWorkOrderActivitiesMockImpl(true));
		        Test.setCurrentPage(Page.VTT_ResolveIntegrationErrors);
		        ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(errorList);
	            stdController.setSelected(errorList);
		        VTT_ResolveIntegrationErrorsCtrlX extController = new VTT_ResolveIntegrationErrorsCtrlX(stdController);

	            //Test
		        extController.UpdateSAP();
		    Test.stopTest();

		    //Validate Result
		    error = [Select Id, Name, Error_Log__c, Work_Order_Activity__c, Status__c
	    			 From VTT_Integration_Error__c
	    			 Where Id =: error.Id];
	    	System.assertNotEquals(null, error.Id);
	    	System.assertEquals(error.Status__c, VTT_IntegrationUtilities.STATUS_ERROR);
        }
	}
}