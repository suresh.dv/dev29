global virtual with sharing class DSP_CMS_FAQ extends DSP_CMS_FAQController
{
    global override String getHTML()
    {
        return FAQHTML();
    }
}