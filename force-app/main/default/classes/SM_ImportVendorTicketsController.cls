public with sharing class SM_ImportVendorTicketsController {

public Blob csvFileBody{get;set;}
public string csvAsString{get;set;}
public Integer csvFileSize {get;set;}
public String[] csvFileLines{get;set;}

public List<ImportVendorTicket> ticketList{get;set;}

public static Integer COLLUMN_INDEX_SAND_REQUEST = 0;
public static Integer COLLUMN_INDEX_VENDOR = 1;
public static Integer COLLUMN_INDEX_INVOICE_NUMBER = 2;
public static Integer COLLUMN_INDEX_TICKET_DATE = 4;
public static Integer COLLUMN_INDEX_TICKET_NUMBER = 5;
public static Integer COLLUMN_INDEX_TRUCK_TYPE = 6;
public static Integer COLLUMN_INDEX_UNIT = 7;
public static Integer COLLUMN_INDEX_TICKET_HOURS = 8;
public static Integer COLLUMN_INDEX_TICKET_VOLUME = 9;
public static Integer COLLUMN_INDEX_DISPOSAL_LOCATION = 10;
public static Integer COLLUMN_INDEX_DISPOSAL_LOCATION_INVOICE = 11;
public static Integer COLLUMN_INDEX_DISPOSAL_FLUID_VOLUME = 12;
public static Integer COLLUMN_INDEX_DISPOSAL_SOLIDS_VOLUME = 13;
public static Integer COLLUMN_INDEX_DISPOSAL_LOCATION_INVOICED_VOLUME = 14;

public Map<String, HOG_Picklist_Manager__c> VendorMap {get; set;} 
public Map<String, HOG_Picklist_Manager__c> UnitTypeMap {get; set;} 

  public SM_ImportVendorTicketsController(){
    csvFileLines = new String[]{};

    ticketList = new List<ImportVendorTicket>();

    PopulateVendorMap(); 
    PopulateUnitTypeMap(); 
  }
  
    public void importCSVFile()
    {
        String[] aDataCheck;

       try{
           ticketList = new List<ImportVendorTicket>();

           csvAsString = csvFileBody.toString();
           csvFileLines = csvAsString.split('\n'); 

           Set<String> sandRequestNameSet = new Set<String>();
           Set<String> disposalLocationNameSet = new Set<String>();           

           if(csvFileLines.size()>=1000)
           {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'Maximum size is 1000 rows. Please make sure input csv file is correct.');
            ApexPages.addMessage(errorMessage);             
           }

            FindCollumnIndexes(csvFileLines[0]);

            for(Integer i=1;i<Math.min(csvFileLines.size(), 1000);i++)
            {
                aDataCheck = csvFileLines[i].replace('\r','').split(',');

                if(aDataCheck.size() > 0)
                {
                    ImportVendorTicket vtWrapperObj = new ImportVendorTicket(csvFileLines[i], 
                        sandRequestNameSet, 
                        disposalLocationNameSet, 
                        VendorMap,
                        UnitTypeMap);

                    ticketList.add(vtWrapperObj);
                }
            }

            //prepare sand request map
            List<SM_Run_Sheet_Detail__c> sandRequestList = [select id, name, Sand_Id__c from SM_Run_Sheet_Detail__c WHERE Sand_Id__c IN :sandRequestNameSet LIMIT 5000];
            Map<String, SM_Run_Sheet_Detail__c> sandRequestMap = new Map<String, SM_Run_Sheet_Detail__c>(); 
            for(SM_Run_Sheet_Detail__c sandRequestRec : sandRequestList)            
            {
                sandRequestMap.put(sandRequestRec.Sand_Id__c, sandRequestRec);
            }

            //prepare sand facility map
            List<Facility__c> existingFacilityList =[select id, name from facility__c where Sand_Destination_Ind__c = true and name in :disposalLocationNameSet];
            List<HOG_Picklist_Manager__c> pmDisposalList = [SELECT Id, Picklist_Name__c, Picklist_Value__c FROM HOG_Picklist_Manager__c WHERE Picklist_Name__c = 'Disposal'];
            
            Map<String, Facility__c> facilityMap = new Map<String, Facility__c>(); 
            Map<String, HOG_Picklist_Manager__c> pmDisposalMap = new Map<String, HOG_Picklist_Manager__c>();  

            for(Facility__c facRec :existingFacilityList)
            {
                facilityMap.put(facRec.name.toLowerCase(), facRec);
            }

            for(HOG_Picklist_Manager__c pm :pmDisposalList)
            {
                pmDisposalMap.put(pm.Picklist_Value__c.toLowerCase(), pm);
            }
			
            // populate sand request and disposal map ids
            for(ImportVendorTicket ticketRec :ticketList)
            {
                //try to find Sand Request
                String sandRequestnName =  GetStringValue(ticketRec.RecordData,COLLUMN_INDEX_SAND_REQUEST); 
                if(sandRequestMap.containsKey(sandRequestnName))
                {
                    ticketRec.ticket.Sand_Request__c = sandRequestMap.get(sandRequestnName).id;
                }
                else
                {
                    ticketRec.isError = true;
                    ticketRec.errorMessage +='Unable to find Sand Request:' + sandRequestnName + '\n';
                }

                //try to find Disposal Location
                String disposalLocationName = GetStringValue(ticketRec.RecordData,COLLUMN_INDEX_DISPOSAL_LOCATION);
                if(disposalLocationName<> null && disposalLocationName <> '')
                {
                    if(facilityMap.containsKey(disposalLocationName.toLowerCase()))
                    {
                        ticketRec.ticket.Disposal_Facility__c = facilityMap.get(disposalLocationName.toLowerCase()).id;
                        ticketRec.ticket.Disposal_Location_Name__c = facilityMap.get(disposalLocationName.toLowerCase()).Name;
                    }
                    else if(pmDisposalMap.containsKey(disposalLocationName.toLowerCase())){
                         ticketRec.ticket.Disposal_Facility_Ext__c = pmDisposalMap.get(disposalLocationName.toLowerCase()).Id;  
                         ticketRec.ticket.Disposal_Location_Name__c = pmDisposalMap.get(disposalLocationName.toLowerCase()).Picklist_Value__c; 
                    }
                    else
                    {
                        ticketRec.isError = true;
                        ticketRec.errorMessage +='Unable to find Disposal Location:' + disposalLocationName + ' \n';
                    }
                }

                //build source_system_id
                if(!ticketRec.isError)
                {
                    //ticketRec.ticket.source_system_id__c = sandRequestnName +
                    //'~' + ticketRec.ticket.Vendor__c + 
                    //'~' + ticketRec.ticket.Ticket_Number__c;
                    ticketRec.ticket.source_system_id__c = ticketRec.ticket.Sand_Request__c + '-' + ticketRec.ticket.Vendor_PM__c /*Vendor__c*/ + '-' + ticketRec.ticket.Ticket_Number__c;

                    system.debug('ticketRec.ticket.source_system_id__c: ' + ticketRec.ticket.source_system_id__c);
                }               
            }
        }
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importing data. Please make sure input csv file is correct.' + e.getMessage());
            ApexPages.addMessage(errorMessage);
            system.debug(e);
        } 
  }

    public void importTicketList(){
        List<SM_Sand_Request_Vendor_Ticket__c> vendorTicketList = new List<SM_Sand_Request_Vendor_Ticket__c>();
        List<ImportVendorTicket> updatedTicketList = new List<ImportVendorTicket>();

        try
        {
            for(ImportVendorTicket ticketRec :ticketList)
            {
                //try to find Sand Request
                if(ticketRec <> null && ticketRec.isError <> true)
                {
                    vendorTicketList.add(ticketRec.ticket);

                    updatedTicketList.add(ticketRec);
                }
            }
            Schema.SObjectField f = SM_Sand_Request_Vendor_Ticket__c.Fields.source_system_id__c;
            Database.UpsertResult[] upsertResults = Database.upsert(vendorTicketList, f, false);

            Integer importedSuccessfully = 0;
            for(Integer i=0;i<upsertResults.size();i++){

                if (!upsertResults.get(i).isSuccess()){
                    // DML operation failed
                    updatedTicketList[i].isError = true;
                    for(Database.Error error : upsertResults.get(i).getErrors())
                    {
                        String failedDML = error.getMessage();
                        updatedTicketList[i].errorMessage += error.getMessage() + '\n';                     
                    }
                    //Database.Error error = upsertResults.get(i).getErrors().get(0);
                    //String failedDML = error.getMessage();

                    //ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while saving data:' + failedDML);
                    //ApexPages.addMessage(errorMessage);

                    


                }
                else
                {
                    importedSuccessfully++;
                }

            }
            ApexPages.Message successMessage = new ApexPages.Message(ApexPages.severity.Confirm,'Imported Tickets:' + importedSuccessfully);
            ApexPages.addMessage(successMessage);    

        }
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while saving data:' + e.getMessage());
            ApexPages.addMessage(errorMessage);
        }       

    }


    public static void FindCollumnIndexes(String pCsvHeaderLine)
    {
        COLLUMN_INDEX_SAND_REQUEST = -1;
        COLLUMN_INDEX_VENDOR = -1;
        COLLUMN_INDEX_INVOICE_NUMBER = -1;
        COLLUMN_INDEX_TICKET_DATE = -1;
        COLLUMN_INDEX_TICKET_NUMBER = -1;
        COLLUMN_INDEX_TRUCK_TYPE = -1;
        COLLUMN_INDEX_UNIT = -1;
        COLLUMN_INDEX_TICKET_HOURS = -1;
        COLLUMN_INDEX_TICKET_VOLUME = -1;
        COLLUMN_INDEX_DISPOSAL_LOCATION = -1;
        COLLUMN_INDEX_DISPOSAL_LOCATION_INVOICE = -1;
        COLLUMN_INDEX_DISPOSAL_LOCATION_INVOICED_VOLUME = -1;
        COLLUMN_INDEX_DISPOSAL_FLUID_VOLUME = -1;
        COLLUMN_INDEX_DISPOSAL_SOLIDS_VOLUME = -1;      

        String[] pCsvRecordHeader = pCsvHeaderLine.replace('\r','').split(',');

        for(Integer i=0;i<pCsvRecordHeader.size();i++){

            String collumnName = pCsvRecordHeader[i].trim().tolowercase();
            if(collumnName.startsWith('sand request'))
            {
                COLLUMN_INDEX_SAND_REQUEST = i;
            }
            if(collumnName.startsWith('vendor name'))
            {
                COLLUMN_INDEX_VENDOR = i;
            }
            if(collumnName.startsWith('vendor invoice #'))
            {
                COLLUMN_INDEX_INVOICE_NUMBER = i;
            }
            if(collumnName.startsWith('vendor ticket date'))
            {
                COLLUMN_INDEX_TICKET_DATE = i;
            }
            if(collumnName.startsWith('vendor ticket #'))
            {
                COLLUMN_INDEX_TICKET_NUMBER = i;
            }
            if(collumnName.startsWith('vendor truck type'))
            {
                COLLUMN_INDEX_TRUCK_TYPE = i;
            }   
            if(collumnName.startsWith('vendor unit #'))
            {
                COLLUMN_INDEX_UNIT = i;
            }                       
            if(collumnName.startsWith('vendor ticket hours'))
            {
                COLLUMN_INDEX_TICKET_HOURS = i;
            }           
            if(collumnName.startsWith('vendor load volume'))
            {
                COLLUMN_INDEX_TICKET_VOLUME = i;
            }           
            if(collumnName=='disposal location')
            {
                COLLUMN_INDEX_DISPOSAL_LOCATION = i;
            }           
            if(collumnName=='disposal location invoice #')
            {
                COLLUMN_INDEX_DISPOSAL_LOCATION_INVOICE = i;
            }
            if(collumnName.startsWith('disposal location invoiced volume'))
            {
                COLLUMN_INDEX_DISPOSAL_LOCATION_INVOICED_VOLUME = i;
            }   
            if(collumnName.startsWith('disposal fluid volume'))
            {
                COLLUMN_INDEX_DISPOSAL_FLUID_VOLUME = i;
            }   
            if(collumnName.startsWith('disposal solids volume'))
            {
                COLLUMN_INDEX_DISPOSAL_SOLIDS_VOLUME = i;
            }
        }
    }

    public static Date parseDate(String inDate) {
        Date    dateRes     = null;


        // grab date portion only m[m]/d[d]/yyyy , ignore time
        try {
            String[] candDate   = inDate.substring(0,Math.min(10,inDate.length())).split('/');

            Integer intYear = Integer.valueOf(candDate[2]);
            Integer intMonth = Integer.valueOf(candDate[0]);
            Integer intDay = Integer.valueOf(candDate[1]);

            if(intMonth>12 || intMonth<1)
            {
                return null;
            }
            if(intDay>31 || intDay<1)
            {
                return null;
            }

            dateRes = Date.newInstance(intYear,intMonth,intDay);
            return dateRes;
        }
        catch (Exception e) {}


        ////    1 - Try locale specific mm/dd/yyyy or dd/mm/yyyy    
        //try {
        //  String candDate     = inDate.substring(0,Math.min(10,inDate.length()));// grab date portion only m[m]/d[d]/yyyy , ignore time
        //  dateRes     = Date.parse(candDate);
        //  return dateRes;
        //}
        //catch (Exception e) {}

        if (dateRes == null) {
        //  2 - Try yyyy-mm-dd          
            try {
                String candDate     = inDate.substring(0,Math.min(10,inDate.length()));         // grab date portion only, ignore time, if any
                dateRes             = Date.valueOf(candDate);
                return dateRes;
            }
            catch (Exception e) {} 
        }
        
        return dateRes;
    }

    public static String GetStringValue(String[] pCsvRecordData, Integer pCollumnIndex)
    {
        if(pCsvRecordData.size()<=pCollumnIndex || pCollumnIndex == -1)
        {
            return null;
        }

        return pCsvRecordData[pCollumnIndex].trim();
    }
    public static Decimal GetDecimalValue(String[] pCsvRecordData, Integer pCollumnIndex)
    {
        if(pCsvRecordData.size()<=pCollumnIndex  
            || pCollumnIndex == -1 
            || pCsvRecordData[pCollumnIndex] == null
            || pCsvRecordData[pCollumnIndex] == '')
        {
            return null;
        }
        return Decimal.valueOf(pCsvRecordData[pCollumnIndex]);
    }
    public static Date GetDateValue(String[] pCsvRecordData, Integer pCollumnIndex)
    {
        if(pCsvRecordData.size()<=pCollumnIndex  || pCollumnIndex == -1)
        {
            return null;
        }
        return parseDate(pCsvRecordData[pCollumnIndex]);
    }

    private void PopulateVendorMap()
    {
        VendorMap = new Map<String, HOG_Picklist_Manager__c>();
         /*
         Schema.DescribeFieldResult fieldResult =
         SM_Sand_Request_Vendor_Ticket__c.Vendor__c.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
           for( Schema.PicklistEntry f : ple)
           {
              VendorMap.put(f.getValue().tolowercase(), f.getValue());  
           }    
        */  
        List<HOG_Picklist_Manager__c> pmVendorList = [SELECT Id, Picklist_Name__c, Picklist_Value__c 
                                                      FROM HOG_Picklist_Manager__c 
                                                      WHERE Picklist_Name__c = 'Vendor'];

        for(HOG_Picklist_Manager__c pmVendor : pmVendorList)
           {
              VendorMap.put(pmVendor.Picklist_Value__c.tolowercase(), pmVendor);  
           }                                                  
    } 
    
    private void PopulateUnitTypeMap()
    {
        
        UnitTypeMap = new Map<String, HOG_Picklist_Manager__c>();
        /*
        Schema.DescribeFieldResult fieldResult =
         SM_Sand_Request_Vendor_Ticket__c.Truck_Type__c.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
           for( Schema.PicklistEntry f : ple)
           {
              UnitTypeMap.put(f.getValue().tolowercase(), f.getValue());    
           } 
           */ 
        List<HOG_Picklist_Manager__c> pmUnitTypeList = [SELECT Id, Picklist_Name__c, Picklist_Value__c 
                                                        FROM HOG_Picklist_Manager__c 
                                                        WHERE Picklist_Name__c = 'Unit Type'];

            for(HOG_Picklist_Manager__c pmUnitType : pmUnitTypeList){
                    
                    UnitTypeMap.put(pmUnitType.Picklist_Value__c.tolowercase(), pmUnitType);  
                }                
    } 

  //wrapper class
  public class ImportVendorTicket
  {
    public SM_Sand_Request_Vendor_Ticket__c ticket {get; set;}
    public Boolean isError {get; private set;}
    public String errorMessage {get; set;}
    public String[] RecordData {get; set;}
    public String SandId {get; private set;}

    public ImportVendorTicket(String pCsvLine, 
            Set<String> pSandRequestNameSet, 
            Set<String> pDisposalLocationNameSet, 
            Map<String, HOG_Picklist_Manager__c> pVendorMap,
            Map<String, HOG_Picklist_Manager__c> pUnitTypeMap)
    {
        SM_Sand_Request_Vendor_Ticket__c vtObj = new SM_Sand_Request_Vendor_Ticket__c();
        this.errorMessage = '';
        this.isError = false;
        this.ticket = vtObj;

        String[] csvRecordData = pCsvLine.replace('\r','').split(',');
        this.RecordData = csvRecordData;
		
        if(csvRecordData.size() > 0)
        {
            this.SandId = csvRecordData[COLLUMN_INDEX_SAND_REQUEST].trim();
            pSandRequestNameSet.add(csvRecordData[COLLUMN_INDEX_SAND_REQUEST].trim());
    
            try
            {
                
                String dateValue = '';
                String fieldValue = '';

                String vendorName = GetStringValue(csvRecordData,COLLUMN_INDEX_VENDOR); 
                if(vendorName==null || vendorName=='')
                {
                    this.isError = true;
                    this.errorMessage+= 'Vendor is required\n';             
                }
                else
                {
    				
                    if(pVendorMap <> null && pVendorMap.containsKey(vendorName.toLowerCase()))
                    {
                        vtObj.Vendor_PM__c = pVendorMap.get(vendorName.toLowerCase()).Id;
                        vtObj.Vendor__c = pVendorMap.get(vendorName.toLowerCase()).Picklist_Value__c; 
                    }
                    else
                    {
                        this.isError = true;
                        this.errorMessage+= 'Vendor [' + vendorName +'] is not valid\n';    
                    }
                }

                //vtObj.Vendor__c = GetStringValue(csvRecordData,COLLUMN_INDEX_VENDOR); 
                //if(vtObj.Vendor__c==null || vtObj.Vendor__c=='')
                //{
                //  this.isError = true;
                //  this.errorMessage+= 'Vendor is required\n';             
                //}
    
                //fieldValue = GetStringValue(csvRecordData,COLLUMN_INDEX_TRUCK_TYPE);
                //if(fieldValue <> null && fieldValue <> '' ) vtObj.Truck_Type__c = fieldValue;

                String unitTypeName = GetStringValue(csvRecordData,COLLUMN_INDEX_TRUCK_TYPE);
/*
                if(unitTypeName==null || unitTypeName=='')
                {
                    this.isError = true;
                    this.errorMessage+= 'Unit Type is required\n';              
                }
                else
                {
*/

                    if(unitTypeName != null && unitTypeName != '' && pUnitTypeMap <> null && pUnitTypeMap.containsKey(unitTypeName.toLowerCase()))
                    {
                        vtObj.Truck_Type_PM__c = pUnitTypeMap.get(unitTypeName.toLowerCase()).Id;
                        vtObj.Truck_Type__c = pUnitTypeMap.get(unitTypeName.toLowerCase()).Picklist_Value__c;
                    }
                    else if(unitTypeName != null && unitTypeName != '')
                    {
                        this.isError = true;
                        this.errorMessage+= 'Unit Type [' + unitTypeName +'] is not valid\n';   
                    }
//              }

                fieldValue = GetStringValue(csvRecordData,COLLUMN_INDEX_UNIT);
                if(fieldValue <> null && fieldValue <> '' ) vtObj.Unit_Number__c = fieldValue;          

                dateValue =  GetStringValue(csvRecordData,COLLUMN_INDEX_TICKET_DATE);
                if(dateValue<> null && dateValue <> '')
                {
                    vtObj.Ticket_Date__c = GetDateValue(csvRecordData, COLLUMN_INDEX_TICKET_DATE);
    
                    if(vtObj.Ticket_Date__c == null && dateValue <> null && dateValue <> '')
                    {
                        this.isError = true;
                        this.errorMessage += 'Could not parse Ticket Date [' + dateValue +']\n';                    
                    }
                }

                fieldValue = GetStringValue(csvRecordData,COLLUMN_INDEX_TICKET_NUMBER);
                if(fieldValue <> null && fieldValue <> '' ) 
                {
                    vtObj.Ticket_Number__c = fieldValue;
                }
                else
                {
                    this.isError = true;
                    this.errorMessage+= 'Ticket # is required\n';               
                }
    
                Decimal ticketHours = GetDecimalValue(csvRecordData,COLLUMN_INDEX_TICKET_HOURS);
                if(ticketHours <> null) vtObj.Ticket_Hours__c = ticketHours;
                
                String disposalLocationName = GetStringValue(csvRecordData,COLLUMN_INDEX_DISPOSAL_LOCATION); 
                if(disposalLocationName <> null && disposalLocationName <> '')
                {
                    pDisposalLocationNameSet.add(disposalLocationName);
                }
    
                Decimal ticketVolume = GetDecimalValue(csvRecordData,COLLUMN_INDEX_TICKET_VOLUME);
                if(ticketVolume <> null) vtObj.Ticket_Volume_m3__c = ticketVolume;              
    
                fieldValue = GetStringValue(csvRecordData,COLLUMN_INDEX_INVOICE_NUMBER);
                if(fieldValue <> null && fieldValue <> '' ) vtObj.Invoice_Number__c = fieldValue;
    
                fieldValue = GetStringValue(csvRecordData,COLLUMN_INDEX_DISPOSAL_LOCATION_INVOICE);
                if(fieldValue <> null && fieldValue <> '' ) vtObj.Disposal_Location_Invoice_Number__c = fieldValue;
    
                Decimal invoicedVolume = GetDecimalValue(csvRecordData,COLLUMN_INDEX_DISPOSAL_LOCATION_INVOICED_VOLUME);
                if(invoicedVolume <> null) vtObj.Disposal_Location_Invoiced_Volume__c = invoicedVolume;
    
                Decimal fluidVolume = GetDecimalValue(csvRecordData,COLLUMN_INDEX_DISPOSAL_FLUID_VOLUME);
                if(fluidVolume <> null) vtObj.Disposal_Fluid_Volume__c = fluidVolume;
    
                Decimal solidsVolume = GetDecimalValue(csvRecordData,COLLUMN_INDEX_DISPOSAL_SOLIDS_VOLUME);
                if(solidsVolume <> null) vtObj.Disposal_Solids_Volume__c = solidsVolume;
    
    
            }
            catch(Exception ex)
            {
                this.isError = true;
                this.errorMessage = ex.getMessage()+'\n';
            }
        }
    }
  }
}