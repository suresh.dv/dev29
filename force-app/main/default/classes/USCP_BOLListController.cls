public with sharing class USCP_BOLListController extends DynamicListController {

    public Boolean IsAdmin {get;set;}
    public String AccountId {get;set;}    
    public String Account {get;set;}   

    public String InvoiceNumber {get;set;}
    public String BOLNumber {get;set;}
    public String Carrier{get;set;}      
    public String Product {get;set;}  
    public String selectedProductID {get;set;}     
    public String Location {get;set;}      
    public String selectedTerminalID{get;set;}      
    public string DateRangeType {get;set;}
    public Date dateFrom {get;set;}
    public Date dateTo {get;set;}  
    public String dateFromStr 
    {
        get
        {
            Date d = dateFrom;
            if(d==null)
            {
                return null;
            }
            return d.format() ;
        }
        set
        {
            dateFrom = USCP_Utils.convertDate(value);    //Date.valueOf(value);
        }
    }
    public String dateToStr 
    {
        get
        {
            Date d = dateTo ;
            if(d==null)
            {
                return null;
            }            
            return d.format() ;
        }
        set
        {
            dateTo =  USCP_Utils.convertDate(value); //Date.valueOf(value);
        }
    }


    public USCP_BOLListController () {
        // TODO: call the parent class constructor with a base query
        super(''); 
        String sql='';
        sql+='select Id, Name, BOL__c, Account__c, Account__r.Name,'; 
        sql+='BOL_Number__c, Line_Number__c, ';
        sql+='Carrier__c,';
        sql+='Destination_Location__c,';
        sql+='Lifting_Number__c,';
        sql+='Location__c,Location__r.Name,';
        sql+='Movement_Date__c,';
        sql+='Movement_Time__c, ';
        sql+='Type__c, ';
        sql+='Net_Quantity__c,';
        sql+='Gross_Quantity__c,';
        sql+='Product__c,Product__r.Name,';
        sql+='Invoice__c, Invoice__r.Name ';
        //sql+='(select id, Invoice__c, Invoice__r.Name from AccTransactions__r)';
        sql+='from USCP_BOL_AccTransaction__c';
        this.BaseQuery = sql;        
        pageSize = 20;
        
        ResetFilters();

        IsAdmin = USCP_Utils.IsAdminUser();   
        
        InvoiceNumber  = ApexPages.currentPage().getParameters().get('InvoiceNumber');
        BOLNumber = ApexPages.currentPage().getParameters().get('BOLNumber');

        //sortColumn = 'BOL_Number__c, Line_Number__c';        
        sortColumn = 'Movement_Date__c';        
        sortAsc = false;

        if(InvoiceNumber==null) InvoiceNumber='';
        if(BOLNumber ==null) BOLNumber ='';   
            
        if(InvoiceNumber !='' || BOLNumber !='')
        {        
            //query();               
            RefreshData();
        }            
    }

    private void ResetFilters()
    {
        AccountId      ='';
        Account = '';
            
        InvoiceNumber ='';
        BOLNumber = '';    
        Product = '';
        selectedProductID = '';
        Location = '';  
        selectedTerminalID= '';            
        Carrier = '';   
        DateRangeType ='na';
        dateFrom = null; //Date.Today()-1;
        dateTo = null; //Date.Today()-1; 
    }
    // cast the resultset
    public List<USCP_BOL_AccTransaction__c> getBOLRecords() {
        // TODO: cast returned records into list of Accounts
        return (List<USCP_BOL_AccTransaction__c>) getRecords();
    }
    // cast the resultset
    public List<USCP_BOL_AccTransaction__c> getAllBOLRecords() {
        // TODO: cast returned records into list of Accounts
        return (List<USCP_BOL_AccTransaction__c>) getAllRecords();
    }
    
    
    public PageReference ClearFilter() {
        ResetFilters();  
        this.WhereClause  = ' Name=null '; //always false, no data load
        query();
    
        return null;
    }    
    public PageReference RefreshData() {  
            String FilteredSOQL = ' Name!=null '; // always true
            Boolean filtersON = false; 

            if(AccountId!='')
                {
                filteredSOQL  = filteredSOQL + ' and Account__c = \'' +  String.escapeSingleQuotes(AccountId) + '\'';
                filtersON = true;
                }   


            if(InvoiceNumber !='')
                {
                filteredSOQL  = filteredSOQL + ' and Invoice__r.Name like \'%' +  String.escapeSingleQuotes(InvoiceNumber ) + '%\'';
                //filteredSOQL  = filteredSOQL + ' and id in (select BOL_Line_Item__c from USCP_BOL_AccTransaction__c where Invoice__r.Name like \'%' +  String.escapeSingleQuotes(InvoiceNumber ) + '%\')';
                filtersON = true;                
                }

            if(BOLNumber !='')
                {
                filteredSOQL  = filteredSOQL + ' and BOL_Number__c like \'%' +  String.escapeSingleQuotes(BOLNumber ) + '%\'';
                filtersON = true;                
                }
                
            if(Carrier!='')
                {
                filteredSOQL  = filteredSOQL + ' and Carrier__c like \'%' +  String.escapeSingleQuotes(Carrier) + '%\'';
                filtersON = true;                
                }
            /*                
            if(Product !='')
                {
                filteredSOQL  = filteredSOQL + ' and Product__r.name like \'%' +  String.escapeSingleQuotes(Product) + '%\'';
                filtersON = true;                
                }
            */
            if(selectedProductID  !='')
                {
                filteredSOQL  = filteredSOQL + ' and Product__c = \'' +  String.escapeSingleQuotes(selectedProductID ) + '\'';
                filtersON = true;                
                }
            else
                if(Product !='')
                {
                filteredSOQL  = filteredSOQL + ' and Product__r.name like \'%' +  String.escapeSingleQuotes(Product) + '%\'';
                filtersON = true;                
                }
            
            if(selectedTerminalID!='')
                {
                filteredSOQL  = filteredSOQL + ' and Location__c = \'' +  String.escapeSingleQuotes(selectedTerminalID) + '\'';
                filtersON = true;                
                }              
            else if(Location !='')
                {
                filteredSOQL  = filteredSOQL + ' and Location__r.name like \'%' +  String.escapeSingleQuotes(Location ) + '%\'';
                filtersON = true;                
                } 
                 
                                               
            if(/*DateRangeType =='movementdate' && */ dateFrom != null) 
                {
                filteredSOQL  = filteredSOQL + ' and Movement_Date__c >= ' +  String.valueof(dateFrom);               
                filtersON = true;                
                }
                        
            if(/*DateRangeType =='movementdate' && */ dateTo != null)    
                {
                //DateTime dTimeFrom = DateTime.newinstance(dateFrom, Time.newinstance(0,0,0,0) );               
                //DateTime dTimeTo = DateTime.newinstance(dateTo , Time.newinstance(23,59,0,0) );                
                //filteredSOQL  = filteredSOQL + ' and Movement_Date__c >= ' + dTimeFrom.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'')  + ' and Movement_Date__c <= ' +  dTimeTo.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                filteredSOQL  = filteredSOQL + ' and Movement_Date__c <= ' +  String.valueof(dateTo );
                filtersON = true;                
                }           
                  
         if(filtersON )
         {         
             this.WhereClause  = FilteredSOQL;
         }  
         else
         {
            this.WhereClause  = ' Name=null '; //always false, no data load
         }                   
         query();
    
         return null;
    }
     
    @RemoteAction
    public static List<Product2> searchProduct(String searchProduct) {
        System.debug('Product Name is: '+searchProduct);
        List<Product2> products = Database.query('Select Id, Name from Product2 where id in (select product__c from USCP_BOL_AccTransaction__c) and name like \'%' + String.escapeSingleQuotes(searchProduct) + '%\'');
        return products ;
    }
    @RemoteAction
    public static List<USCP_Terminal__c> searchTerminal(String searchTerminal) {
        System.debug('Terminal Name is: '+searchTerminal);
        List<USCP_Terminal__c> result = Database.query('Select Id, Name from USCP_Terminal__c where id in (select Location__c from USCP_BOL_AccTransaction__c) and name like \'%' + String.escapeSingleQuotes(searchTerminal) + '%\'');
        return result ;
    }   
   
    @RemoteAction
    public static List<ResultSet> searchCarrier(String searchCarrier) {
        System.debug('Carrier Name is: '+searchCarrier);
        List<AggregateResult> result = Database.query('select Carrier__c from USCP_BOL_AccTransaction__c where Carrier__c != null and Carrier__c like \'%' + String.escapeSingleQuotes(searchCarrier) + '%\' group by Carrier__c');

        List<ResultSet> distinctCarriers = new List<ResultSet> ();
        for (AggregateResult rec: result ) {
            String carrier = String.valueOf(rec.get('Carrier__c'));        
            distinctCarriers.add( new ResultSet(carrier ,carrier ) );
        }
        return distinctCarriers ;
    }
     
    @RemoteAction
    public static List<Account > searchAccount(String searchAccount) {
        System.debug('Account Name is: '+searchAccount);
        List<Account> result = Database.query('Select id, Name, SMS_Number__c from Account where id in (select Account__c from USCP_BOL_AccTransaction__c) and (name like \'%' + String.escapeSingleQuotes(searchAccount) + '%\' or Customer_Number__c  like \'%'  + String.escapeSingleQuotes(searchAccount) + '%\' )  order by Name ');
        //RecordType.DeveloperName = \'USCP_Account_Record\' and 
        return result ;
    }         
       
            
    /*Record Wrapper*/
 
    public class ResultSet{
        public String Id {get;set;} 
        public String Name{get;set;}
        ResultSet(String Id,String Name){
            this.Id = Id;
            this.Name = Name;
        }
    }        

    public List<List<USCP_BOL_AccTransaction__c>> getAllRecordPages() {
        List<USCP_BOL_AccTransaction__c> result = new List<USCP_BOL_AccTransaction__c>();
        List<List<USCP_BOL_AccTransaction__c>> resultPages =new List<List<USCP_BOL_AccTransaction__c>>();

        //store existing page settings
        integer currentPageSize =  this.pageSize;   
        integer currentPage =  this.getPageNumber();
        this.pageSize = 1000; 
        
        this.first();
        do
        {
            if(resultPages.size() <> 0 ) this.next();   
            result = new List<USCP_BOL_AccTransaction__c>();
            for(USCP_BOL_AccTransaction__c rec: (List<USCP_BOL_AccTransaction__c>) getRecords())
            {
                  result.add(rec);
            } 
            resultPages.add(result);
        }
        while(this.getHasNext());
        
        //restore existing page settings
        this.pageSize = currentPageSize ; 
        this.setPageNumber(currentPage );
        
        System.debug('Returning Pages:' +resultPages.size() );
        
        return resultPages ;
    }   


        
  public PageReference SaveToExcel() 
   {
        return page.USCP_BOLExcel;
   }    
}