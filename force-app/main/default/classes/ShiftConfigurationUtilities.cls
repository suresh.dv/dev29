public class ShiftConfigurationUtilities {
    public static Shift_Configuration__c lookupShift(ID shiftId) {
        return 
          [SELECT Name,
              Start_Time__c,
              End_Time__c,
              Operating_District__c,
              Enabled__c
           FROM Shift_Configuration__c
           WHERE ID = :shiftId]; 
    }
    
    
    public static List<Shift_Configuration__c> lookupValidShifts(String rtName) {
        return [
            SELECT 
                Id, 
                Name,
                Start_Time__c,
                End_Time__c, 
                Operating_District__c,
                Enabled__c,
                Sunday__c,
                Monday__c,
                Tuesday__c,
                Wednesday__c,
                Thursday__c,
                Friday__c,
                Saturday__c, RecordType.Name 
            FROM Shift_Configuration__c
            WHERE Enabled__c = True and RecordType.Name = :rtName];
    }
    
    // Added By Sanchivan Sivadasan
    public static List<Shift_Configuration__c> findActiveShift(Datetime refTime, String rtName) {
        Integer mins = refTime.minute();
        String minsStr = (mins < 10) ? ('0' + mins) : ('' + mins);
        Integer currentHrMin = Integer.valueof(refTime.hour() + minsStr);
        System.debug(currentHrMin);
        String dayOfWeek = refTime.format('E');
        System.debug('dayOfWeek =' + dayofWeek);       
        String refDay = DayFieldBasedonDay(dayOfWeek);
        System.debug('refDay = ' + refDay);
        List<Shift_Configuration__c> actiaveShifts = new List<Shift_Configuration__c>();
        for (Shift_Configuration__c sc : lookupValidShifts(rtName)) {
        	
        	if(Boolean.valueOf(sc.get(refDay))) {
        		
	        	Integer startTime = Integer.valueof(sc.Start_Time__c);
	            Integer endTime = Integer.valueof(sc.End_Time__c);
	            if (startTime <= endTime) {
	                // Same day shift.
	                //YEN fix Sep 15, 2014: find active shift that match current time, remove condition (currentHrMin - startTime <= 30)
	                if ((currentHrMin >= startTime) &&  (currentHrMin < endTime)) {
	                    actiaveShifts.add(sc);
	                }
	            } else { 
	                // Overnight shift.
	                if (currentHrMin >= startTime) {
	                    actiaveShifts.add(sc);
	                }
	            }
        	}
        }
        
        return actiaveShifts; 
    }
    
    // Added By Sanchivan Sivadasan
    public static String DayFieldBasedonDay(String dayOfWeek) {
    	if(dayOfWeek == 'Sun')
    		return 'Sunday__c';
		else if(dayOfWeek == 'Mon')
			return 'Monday__c';
		else if(dayOfWeek == 'Tue')
			return 'Tuesday__c';
		else if(dayOfWeek == 'Wed')
			return 'Wednesday__c';
		else if(dayOfWeek == 'Thu')
			return 'Thursday__c';
		else if(dayOfWeek == 'Fri')
			return 'Friday__c';
		else if(dayOfWeek == 'Sat')
			return 'Saturday__c';
			
		return null;
    }
    /*
    public static Shift_Configuration__c findActiveShift(Datetime refTime) {
        Integer mins = refTime.minute();
        String minsStr = (mins < 10) ? ('0' + mins) : ('' + mins);
        Integer currentHrMin = Integer.valueof(refTime.hour() + minsStr);
        
        for (Shift_Configuration__c sc : lookupValidShifts()) {
            Integer startTime = Integer.valueof(sc.Start_Time__c);
            Integer endTime = Integer.valueof(sc.End_Time__c);
            if (startTime <= endTime) {
                // Same day shift.
                if ((currentHrMin >= startTime) &&  (currentHrMin < endTime)) {
                    return sc;
                }
            } else {
                // Overnight shift.
                if (currentHrMin >= startTime) {
                    return sc;
                }
            }
        }
        
        return null;
    }*/
}