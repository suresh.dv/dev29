/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Extension for recreating load request based on LR template
                priveded as id Functionality
Test Class:     FM_LoadRequestRecreateCtrlTest
History:        jschn 06.01.2018 - Created.
**************************************************************************************************/
public with sharing class FM_LoadRequestRecreateCtrl {

    public static final String CLASS_NAME = 'FM_LoadRequestRecreateCtrl';
    public static final String PAGE_NAME = 'FM_LoadRequestRecreate'; 

    private String loadRequestId;

    public final Boolean oldLoadRequestValid {get;private set;}
    public FM_Load_Request__c   oldLoadRequest {get;set;}
    public FM_Load_Request__c   newLoadRequest {get;set;}

    private String oldCancelComments = '';
    private String newCreateComments = '';

    private Map<String, Equipment_Tank__c> equipmentTankMap;

    private String cancelUrl;
    private String saveUrl;

    private static final String STRING_REPLACE_PLACEHOLDER = '&&&';

    public static final String URL_PARAM_ID             = 'Id';
    public static final String URL_PARAM_SAVE           = 'saveUrl';
    public static final String URL_PARAM_CANCEL         = 'cancelUrl';
    public static final String URL_PARAM_SOURCE_TYPE    = 'sourceType';
    public static final String URL_PARAM_SOURCE_ID      = 'sourceId';
    public static final String URL_PARAM_ROUTE_ID       = 'routeId';

    private static final String OTHER_REASON = 'Other';

    private static final String IS_REQUIRED = STRING_REPLACE_PLACEHOLDER + ' is required.';

    private static final String ERROR_MSG_MISSING_CANCEL_REASON     = IS_REQUIRED.replace(STRING_REPLACE_PLACEHOLDER, 
                                                                                        'Cancel Reason for Old Load Request');
    private static final String ERROR_MSG_MISSING_CANCEL_COMMENTS   = 'Cancel Comments for Old Load Request is '
                                                                        + 'required when Cancel Reason is set to ' + OTHER_REASON;
    private static final String ERROR_MSG_MISSING_LOAD_WEIGHT       = 'Load Weight is required.';
    private static final String ERROR_MSG_MISSING_STANDING_COMMENTS = 'Standing Comments are required when Service Load created.';
    private static final String ERROR_MSG_MISSING_TANK              = IS_REQUIRED.replace(STRING_REPLACE_PLACEHOLDER, 'Tank');
    private static final String ERROR_MSG_MISSING_LOAD_TYPE         = IS_REQUIRED.replace(STRING_REPLACE_PLACEHOLDER, 'Load Type');
    private static final String ERROR_MSG_MISSING_PRODUCT           = IS_REQUIRED.replace(STRING_REPLACE_PLACEHOLDER, 'Product');
    private static final String ERROR_MSG_MISSING_FLOW_RATE         = IS_REQUIRED.replace(STRING_REPLACE_PLACEHOLDER, 'Flow Rate');
    private static final String ERROR_MSG_MISSING_TANK_LEVEL        = IS_REQUIRED.replace(STRING_REPLACE_PLACEHOLDER, 'Tank Level');
    private static final String ERROR_MSG_MISSING_TANK_LOW_LEVEL    = IS_REQUIRED.replace(STRING_REPLACE_PLACEHOLDER, 'Tank Low Level');
    private static final String ERROR_MSG_MISSING_TANK_SIZE         = IS_REQUIRED.replace(STRING_REPLACE_PLACEHOLDER, 'Tank Size');
    private static final String ERROR_MSG_MISSING_AXLE              = IS_REQUIRED.replace(STRING_REPLACE_PLACEHOLDER, 'Axle');
    private static final String ERROR_MSG_MISSING_CARRIER           = IS_REQUIRED.replace(STRING_REPLACE_PLACEHOLDER, 'Carrier');
    private static final String ERROR_MSG_MISSING_CREATE_REASON     = IS_REQUIRED.replace(STRING_REPLACE_PLACEHOLDER, 'Create Reason');
    private static final String ERROR_MSG_MISSING_CREATE_COMMENTS   = 'Create Comments is required when Create Reason is set to ' 
                                                                        + OTHER_REASON;
    private static final String ERROR_MSG_NO_LOADREQUEST            = 'Couldn\'t find load request to recreate.';
    private static final String ERROR_MSG_NOT_SUPPORTED_STATUS      = 'Load request with ' 
                                                                        + STRING_REPLACE_PLACEHOLDER
                                                                        + ' status is not supported to recreate.';

    /**
     * Storing value for tank picklist for new load request.
     * Using setter and getter to set it properly.
     * Standard tanks have ID delimitered in value, are loading that value into Equipment_Tank__c field
     * Otherwise we are using Tank__c as value.
     */
    public String tank {
        get {
            String tankValue = '';
            if(newLoadRequest != null) {
                if(newLoadRequest.Equipment_Tank__c != null) {
                    tankValue += newLoadRequest.Equipment_Tank__c + FM_LoadRequest_Utilities.STANDARD_TANK_DELIMITER;
                }
                tankValue += newLoadRequest.Tank__c;
            }
            return tankValue;
        }
        set {
            if(value != null) {
                if(value.contains(FM_LoadRequest_Utilities.STANDARD_TANK_DELIMITER)) {
                    newLoadRequest.Equipment_Tank__c = value.split(FM_LoadRequest_Utilities.STANDARD_TANK_DELIMITER)[0];
                    newLoadRequest.Tank__c = value.split(FM_LoadRequest_Utilities.STANDARD_TANK_DELIMITER)[1];
                } else {
                    newLoadRequest.Equipment_Tank__c = null;
                    newLoadRequest.Tank__c = value;
                }
            }
        }
    }

    public List<SelectOption> tankOptions {
        get {
            if(tankOptions == null)
                tankOptions = FM_LoadRequest_Utilities.getStandardTanks(newLoadRequest);
            return tankOptions;
        }
        private set;
    }

    public List<SelectOption> extraTankOptions {
        get {
            if(extraTankOptions == null)
                extraTankOptions = FM_LoadRequest_Utilities.getExtraTanks(newLoadRequest, tankOptions); 
            return extraTankOptions;
        }
        private set;
    }

    public List<SelectOption> createReasonOptions {
        get {
            return FM_LoadRequest_Utilities.getCreateReasonOptionsWithoutDefault();
        }
        private set;
    }

    public List<SelectOption> shiftOptions {
        get {
            return FM_LoadRequest_Utilities.getShiftOptions();
        } 
        private set;
    }

    public String newLoadRequestShift {
        get {
            return FM_LoadRequest_Utilities.getShiftString(newLoadRequest);
        } set {
            newLoadRequest = FM_LoadRequest_Utilities.setShift(newLoadRequest, value);
        }
    }

    /**
     * Constructor for Controller extension.
     * First it will get parameters from URL.
     * Then it will load old load request based on ID from URL and validate it status.
     * If valid, it will create Equipment Tank map, clone new Load Requet from old one,
     * clear field that are not suppose to be cloned, set some default values, loads 
     * equipment tank info from created map if we have it and sets old LR to cancelled.
     */
    public FM_LoadRequestRecreateCtrl(ApexPages.StandardController stdController) {
        getUrlParameters();
        oldLoadRequest = getOldLoadRequest();
        if(oldLoadRequest != null) {
            oldLoadRequestValid = validateLoadRequestStatus(oldLoadRequest);

            if(oldLoadRequestValid){
                createEquipmentTankMap();
                cloneNewLoadRequest();
                clearNewLoadRequestFields();
                setDefaultForNewLoadRequest();
                updateLoadRequestTankInfo();
                setOldRequestCancelled();
            }
        } else {
            oldLoadRequestValid = false;
            HOG_GeneralUtilities.addPageError(ERROR_MSG_NO_LOADREQUEST);
        }
    }

    /*********************
    INITIALIZATION METHODS
    *********************/
    /**
     * Retrieves url paramesters.
     * SaveUrl, CancelUrl and load request Id.
     */
    private void getUrlParameters() {
        saveUrl = HOG_GeneralUtilities.getUrlParam(URL_PARAM_SAVE);
        cancelUrl = HOG_GeneralUtilities.getUrlParam(URL_PARAM_CANCEL);
        loadRequestId = HOG_GeneralUtilities.getUrlParam(URL_PARAM_ID);
    }

    /**
     * Retrieves old load request record based on ID URL param.
     * @return old Load Request Record
     */
    private FM_Load_Request__c getOldLoadRequest() {
        if(String.isNotBlank(loadRequestId))
            return FM_LoadRequest_Utilities.getLoadRequestById(loadRequestId);
        return null;
    }

    /**
     * Clones old load request into new one. (Deep clone without ID).
     * If no old request retrieved before it throws an error.
     */
    private void cloneNewLoadRequest() {
        if(oldLoadRequest != null)
            newLoadRequest = oldLoadRequest.clone(false, true, false, false);
    }

    /**
     * Clear new load request fields which are not supposted to stay
     * prepopulated after cloning from old load request
     */
    private void clearNewLoadRequestFields() {
        newLoadRequest.Create_Comments__c = '';
        newLoadRequest.Cancel_Reason__c = '';
        newLoadRequest.Cancel_Comments__c = '';
        newLoadRequest.Run_Sheet_Lookup__c = null;
    }

    /**
     * Sets status of Old Load Request to cancelled.
     */
    private void setOldRequestCancelled() {
        oldLoadRequest.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_CANCELLED;
    }

    /**
     * Sets default values for new load request.
     */
    private void setDefaultForNewLoadRequest() {
        newLoadRequest.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
    }

    /**
     * Creates equipment tank map based on old load request(Source location/facility)
     */
    private void createEquipmentTankMap() {
        equipmentTankMap = FM_LoadRequest_Utilities.getEquipmentTankMap(oldLoadRequest);
    }

    /******
    GETTERS
    ******/
    /**
     * Get true if old load request has Cancel Reason set to Other
     * @return Boolean
     */
    public Boolean getIsOldLoadRequestOtherCancelReason() {
        return oldLoadRequest != null
            && oldLoadRequest.Cancel_Reason__c == OTHER_REASON;
    }

    /**
     * Get true if new load request has Create Reason set to Other
     * @return Boolean
     */
    public Boolean getIsNewLoadRequestOtherCreateReason() {
        return newLoadRequest != null
            && newLoadRequest.Create_Reason__c == OTHER_REASON;
    }

    /**
     * Get true if new load request has Standard Load Type.
     * @return Boolean
     */
    public Boolean getIsStandardLoad() {
        return newLoadRequest != null
            && newLoadRequest.Load_Type__c == FM_LoadRequest_Utilities.LOADREQUEST_LOADTYPE_STANDARD;
    }

    /***********
    PAGE ACTIONS
    ***********/
    /**
     * Handler for Old Load Request's Cancel Reason change.
     * When Cancel reason changes to Other, it will set original Cancel Comments into field.
     * If Cancel Reason changes to anything but other, it will load content
     * of Cancel comments into oldCancelComments and clean Cancel Comments field. 
     */
    public void handleCancelReasonChange() {
        if(!OTHER_REASON.equals(oldLoadRequest.Cancel_Reason__c)) {
            if(String.isNotBlank(oldLoadRequest.Cancel_Comments__c)) {
                oldCancelComments = oldLoadRequest.Cancel_Comments__c;
                oldLoadRequest.Cancel_Comments__c = '';
            }
        } else {
            oldLoadRequest.Cancel_Comments__c = oldCancelComments;
        }
    }

    /**
     * Handler for new Load Request's Create Reason change.
     * When Create reason changes to Other, it will set original Create Comments into field.
     * If Create Reason changes to anything but other, it will load content of 
     * Create Comments into oldCreateComments and clean Create Comments field.
     */
    public void handleCreateReasonChange() {
        if(!OTHER_REASON.equals(newLoadRequest.Create_Reason__c)) {
            if(String.isNotBlank(newLoadRequest.Create_Comments__c)) {
                newCreateComments = newLoadRequest.Create_Comments__c;
                newLoadRequest.Create_Comments__c = '';
            }
        } else {
            newLoadRequest.Create_Comments__c = newCreateComments;
        }
    }

    /**
     * It will extract tank id from tank field and set values from
     * equipment tank map record based on tank Id.
     * if no equipment tank with that ID has been found in that 
     * map, it will blank all 3 fields to null.
     */
    public void updateLoadRequestTankInfo() {
        String tankId = getTankId();
        newLoadRequest.Act_Tank_Level__c = getActualTankLevel(tankId);
        newLoadRequest.Tank_Low_Level__c = getTankLowLevel(tankId);
        newLoadRequest.Tank_Size__c = getTankSize(tankId);
    }

    /**
     * This method will validate if all fields are properly populated, update(cancel)
     * old load request, insert new load request and redirect page accordingly.
     * If saveUrl is present it will redirect page into that url if not it will redirect page
     * to new load request detail page.
     * @return Page reference
     */
    public PageReference recreate() {
        if(isValid()) {
            Savepoint sp = Database.setSavepoint();
            try{
                if(updateOldLoadRequestAndValidate()
                && insertNewLoadRequestAndValidate()) {
                    if(String.isNotBlank(saveUrl))
                        return new PageReference('/' + saveUrl + buildReturnFilterString());
                    return new ApexPages.StandardController(newLoadRequest).view();
                }
            } catch(Exception ex) {
                System.debug(CLASS_NAME + ' EXCEPTION: ' + ex.getMessage());
                HOG_GeneralUtilities.addPageError(ex.getMessage());
            }
            Database.rollback(sp);
        }
        return null;
    }

    /**
     * Redirects page back. If cancel URL presnet, it will redirect to that page.
     * Otherwise it will redirect back to old load request detail.
     * @return Page Reference
     */
    public PageReference cancel() {
        if(String.isNotBlank(cancelUrl))
            return new PageReference('/' + cancelUrl + buildReturnFilterString());
        return new PageReference('/' + loadRequestId);
    }

    /**********
    VALIDATIONS
    **********/
    /**
     * Validate Load Request status.
     * Load request status is valid only when it is new, submitted or booked.
     * Otherwise it will throw error.
     * @param  loadRequest to validate
     * @return             Boolean
     */
    private Boolean validateLoadRequestStatus(FM_Load_Request__c loadRequest) {
        Boolean valid = (loadRequest != null
                    && (loadRequest.Status__c == FM_LoadRequest_Utilities.LOADREQUEST_STATUS_NEW
                        || loadRequest.Status__c == FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED
                        || loadRequest.Status__c == FM_LoadRequest_Utilities.LOADREQUEST_STATUS_BOOKED));
        if(!valid) 
            HOG_GeneralUtilities.addPageError(ERROR_MSG_NOT_SUPPORTED_STATUS.replace(STRING_REPLACE_PLACEHOLDER, 
                                                                                    loadRequest.Status__c));
        return valid;
    }

    /**
     * Runs validation on old and new load requests.
     * @return Boolean
     */
    private Boolean isValid() {
        return isOldLoadRequestValid()
            && isNewLoadRequestValid();
    }

    /**
     * Checks if old load request is valid.
     * @return Boolean
     */
    private Boolean isOldLoadRequestValid() {
        return isOldRequestCancelReasonValid()
            && isOldRequestCancelCommentsValid();
    }

    /**
     * Checks if old request Cancel Reason is populated.
     * If no, throws error.
     * @return Boolean
     */
    private Boolean isOldRequestCancelReasonValid() {
        Boolean valid = oldLoadRequest != null
                    && String.isNotBlank(oldLoadRequest.Cancel_Reason__c);
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_CANCEL_REASON);
        return valid;
    }

    /**
     * Check if old request cancel comments is populated when required.
     * Cancel Comments is required only when Cancel Reason is set to Other
     * @return Boolean
     */
    private Boolean isOldRequestCancelCommentsValid() {
        Boolean valid = oldLoadRequest != null
                    && (!getIsOldLoadRequestOtherCancelReason() 
                        || (String.isNotBlank(oldLoadRequest.Cancel_Comments__c)
                            && getIsOldLoadRequestOtherCancelReason()));
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_CANCEL_COMMENTS);
        return valid;
    }

    /**
     * Checks if New Load Request fields are populated correctly.
     * @return Boolean
     */
    private Boolean isNewLoadRequestValid() {
        return isLoadWeightValid()
            && isStandingCommentsValid()
            && isTankValid()
            && isLoadTypeValid()
            && isProductValid()
            && isFlowRateValid()
            && isTankLevelValid()
            && isTankLowLevelValid()
            && isTankSizeValid()
            && isAxleValid()
            && isCarrierValid()
            && isCreateReasonValid()
            && isCreateCommentsValid();
    }

    /**
     * Checks if new load request Load Weight is populated.
     * If no, throws error.
     * @return Boolean
     */
    private Boolean isLoadWeightValid() {
        Boolean valid = newLoadRequest != null
                    && String.isNotBlank(newLoadRequest.Load_Weight__c);
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_LOAD_WEIGHT);
        return valid;
    }

    /**
     * Checks if new load request Standing comments is populated.
     * This field is required only for nonstandard loads.
     * If no, throws error.
     * @return Boolean
     */
    private Boolean isStandingCommentsValid() {
        Boolean valid = newLoadRequest != null
                    && (getIsStandardLoad()
                        || (!getIsStandardLoad()
                            && String.isNotBlank(newLoadRequest.Standing_Comments__c)));
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_STANDING_COMMENTS);
        return valid;
    }

    /**
     * Checks if new load request Tank is populated.
     * If no, throws error.
     * @return Boolean
     */
    private Boolean isTankValid() {
        Boolean valid = newLoadRequest != null
                    && (String.isNotBlank(newLoadRequest.Tank__c)
                        || String.isNotBlank(newLoadRequest.Equipment_Tank__c));
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_TANK);
        return valid;
    }

    /**
     * Checks if new load request Flow Rate is populated.
     * If no, throws error.
     * @return Boolean
     */
    private Boolean isFlowRateValid() {
        Boolean valid = newLoadRequest != null
                    && newLoadRequest.Act_Flow_Rate__c != null;
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_FLOW_RATE);
        return valid;
    }

    /**
     * Checks if new load request Tank Level is populated.
     * If no, throws error.
     * @return Boolean
     */
    private Boolean isTankLevelValid() {
        Boolean valid = newLoadRequest != null
                    && newLoadRequest.Act_Tank_Level__c != null;
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_TANK_LEVEL);
        return valid;
    }

    /**
     * Checks if new load request Tank Low Level is populated.
     * If no, throws error.
     * @return Boolean
     */
    private Boolean isTankLowLevelValid() {
        Boolean valid = newLoadRequest != null
                    && newLoadRequest.Tank_Low_Level__c != null;
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_TANK_LOW_LEVEL);
        return valid;
    }

    /**
     * Checks if new load request Tank Size is populated.
     * If no, throws error.
     * @return Boolean
     */
    private Boolean isTankSizeValid() {
        Boolean valid = newLoadRequest != null
                    && newLoadRequest.Tank_Size__c != null;
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_TANK_SIZE);
        return valid;
    }

    /**
     * Checks if new load request Axle is populated.
     * If no, throws error.
     * @return Boolean
     */
    private Boolean isAxleValid() {
        Boolean valid = newLoadRequest != null
                    && String.isNotBlank(newLoadRequest.Axle__c);
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_AXLE);
        return valid;
    }

    /**
     * Checks if new load request Carrier is populated.
     * If no, throws error.
     * @return Boolean
     */
    private Boolean isCarrierValid() {
        Boolean valid = newLoadRequest != null
                    && String.isNotBlank(newLoadRequest.Carrier__c);
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_CARRIER);
        return valid;
    }

    /**
     * Checks if new load request Create Reason is populated.
     * If no, throws error.
     * @return Boolean
     */
    private Boolean isCreateReasonValid() {
        Boolean valid = newLoadRequest != null
                    && String.isNotBlank(newLoadRequest.Create_Reason__c);
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_CREATE_REASON);
        return valid;
    }

    /**
     * Checks if new load request Create Comments is populated.
     * Create Comments is required only when Create Reason is set to Other
     * If no, throws error.
     * @return Boolean
     */
    private Boolean isCreateCommentsValid() {
        Boolean valid = newLoadRequest != null
                    && (!getIsNewLoadRequestOtherCreateReason()
                        || (String.isNotBlank(newLoadRequest.Create_Comments__c)
                            && getIsNewLoadRequestOtherCreateReason()));
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_CREATE_COMMENTS);
        return valid;
    }

    /**
     * Checks if new load request Load Type is populated.
     * If no, throws error.
     * @return Boolean
     */
    private Boolean isLoadTypeValid() {
        Boolean valid = newLoadRequest != null
                    && String.isNotBlank(newLoadRequest.Load_Type__c);
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_LOAD_TYPE);
        return valid;
    }

    /**
     * Checks if new load request Product is populated.
     * If no, throws error.
     * @return Boolean
     */
    private Boolean isProductValid() {
        Boolean valid = newLoadRequest != null
                    && String.isNotBlank(newLoadRequest.Product__c);
        if(!valid)
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_PRODUCT);
        return valid;
    }

    /******
    HELPERS
    ******/
    /**
     * Extract tank id from selected Tank.
     * @return Tank Id/label
     */
    private String getTankId() {
        if(tank.contains(FM_LoadRequest_Utilities.STANDARD_TANK_DELIMITER)) 
            return tank.split(FM_LoadRequest_Utilities.STANDARD_TANK_DELIMITER)[0];
        return tank;
    }

    /**
     * Retrieves SCADA Tank level from EquipmentTankMap based on TankId
     * @param  tankId Id of tank for which we want to get Tank Levels
     * @return        Actual Tank Level
     */
    private Integer getActualTankLevel(String tankId) {
        if(equipmentTankMap.containsKey(tankId)
        && equipmentTankMap.get(tankId).SCADA_Tank_Level__c != null) {
            return Math.round(equipmentTankMap.get(tankId).SCADA_Tank_Level__c); 
        }
        return null;
    }

    /**
     * Retrieves Tank Low Level from EquipmentTankMap based on TankId
     * @param  tankId Id of tank for which we want to get tank low level
     * @return        Tank Low Level
     */
    private Decimal getTankLowLevel(String tankId) {
        if(equipmentTankMap.containsKey(tankId))
            return equipmentTankMap.get(tankId).Low_Level__c;
        return null;
    }

    /**
     * Retrieves Tank Size from EquipmentTankMap based on TankId
     * @param  tankId Id of tank for which we want to get tank size
     * @return        Tank size
     */
    private Decimal getTankSize(String tankId) {
        if(equipmentTankMap.containsKey(tankId))
            return equipmentTankMap.get(tankId).Tank_Size_m3__c ;
        return null;
    }

    /**
     * Update old load request in Database and returns false if any errors occured during update.
     * @return Successfull update.
     */
    private Boolean updateOldLoadRequestAndValidate() {
        System.debug(CLASS_NAME + '-> Cancel Reason: ' + oldLoadRequest.Cancel_Reason__c);
        System.debug(CLASS_NAME + '-> Cancel Comments: ' + oldLoadRequest.Cancel_Comments__c);
        update oldLoadRequest;
        return !ApexPages.hasMessages();
    }

    /**
     * Insert new load request into Database and return false if any errors occured during process.
     * @return Sucessfull insert
     */
    private Boolean insertNewLoadRequestAndValidate() {
        insert newLoadRequest;
        return !ApexPages.hasMessages();
    }

    private String buildReturnFilterString() {
        String returnFilterString = '';
        String sourceType = HOG_GeneralUtilities.getUrlParam(URL_PARAM_SOURCE_TYPE);
        String sourceId = HOG_GeneralUtilities.getUrlParam(URL_PARAM_SOURCE_ID);
        String routeId = HOG_GeneralUtilities.getUrlParam(URL_PARAM_ROUTE_ID);
        if(String.isNotBlank(sourceType))
            returnFilterString += '?' + URL_PARAM_SOURCE_TYPE + '=' + sourceType;
        if(string.isNotBlank(sourceId))
            returnFilterString += '&' + URL_PARAM_SOURCE_ID + '=' + sourceId;
        if(string.isNotBlank(routeId))
            returnFilterString += '&' + URL_PARAM_ROUTE_ID + '=' + routeId;
        return returnFilterString;
    }

}