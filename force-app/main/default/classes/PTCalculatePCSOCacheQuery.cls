public class PTCalculatePCSOCacheQuery {
	public static List<Service__c> pcsoInfo = [select id,ownerid,utility_company__r.name,(select billing_demand_kw__c,metered_demand_kva__c,metered_demand_kw__c,total_volume__c,wire_cost__c,ratchet_billing__c,invalid_billing__c from utility_billings__r order by billing_date__c desc limit 12),(select id,name,pcso_status__c,pcso_type__c,system_review__c,months_valid__c from pcsos__r where system_review__c='Valid') from service__c where service_status__c in ('Active','Idle') and utility_company__r.name!='Saskpower'];
    public static boolean pcsoInfoQueried = true;
    
    public static List<Service__c> queryPcsoInfo(){
        if(pcsoInfoQueried == false){
        	pcsoInfo = [select id,ownerid,utility_company__r.name,(select billing_demand_kw__c,metered_demand_kva__c,metered_demand_kw__c,total_volume__c,wire_cost__c,ratchet_billing__c,invalid_billing__c from utility_billings__r order by billing_date__c desc limit 12),(select id,name,pcso_status__c,pcso_type__c,system_review__c,months_valid__c from pcsos__r where system_review__c='Valid') from service__c where service_status__c in ('Active','Idle') and utility_company__r.name!='Saskpower'];    
        }
        pcsoInfoQueried = true;
        return pcsoInfo;
    }
}