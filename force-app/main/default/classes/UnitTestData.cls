@isTest
public class UnitTestData {

	public static List<Unit__c> createUnits(List<Plant__c> plantList, Integer numberOfUnitsForEachPlant) {
		
		List<Unit__c> recList = new List<Unit__c>();
		
		for(Plant__c plant : plantList) {
			for(Integer i = 0; i < numberOfUnitsForEachPlant; i++) {
				Unit__c unit = new Unit__c();
				unit.Name = 'Unit Name ' + i;
				unit.Functional_Location__c = 'Unit Funtional Location ' + i;
				unit.Plant__c = plant.Id;
				
				recList.add(unit);	
			}
		}
		
		insert recList;
		return recList;
	}
}