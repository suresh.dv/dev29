public class TaberHussarController
{
    public Boolean hasChatterAccess {get;set;}
    
    public String spotfireURL {get;set;}
    
    public String TaberHussarTeamChatterGroup {get;set;}
    
    public List<AssetWrapper> assets {get;set;}
    
    public class AssetWrapper
    {
        public String URL  {get;set;}
        public String name {get;set;}
        public String icon {get;set;}
        public Boolean isActive {get;set;}
        
        public AssetWrapper(String URL, String name, String icon, Boolean active)
        {
            this.name = name;
            this.URL = URL;
            this.icon = icon;
            isActive = active;
        }
    }
    
    public List<TeamWrapper> teams {get;set;}
    
    public class TeamWrapper
    {
        public String URL  {get;set;}
        public String name {get;set;}
        public String pic  {get;set;}
        
        public TeamWrapper(String URL, String name, String pic)
        {
            this.name = name;
            this.URL = URL;
            this.pic = pic;
        }
    }
    
    public TaberHussarController()
    {
        AURASpotfireURLs__c spotfireURLs = AURASpotfireURLs__c.getOrgDefaults();
        
        spotfireURL = spotfireURLs.TaberHussarSpotfireSession__c;
        
        List<String> chatterNames = new List<String>{'Taber/Hussar Team','Taber/Hussar Reservoir','Taber/Hussar Development','Taber/Hussar D&C','Taber/Hussar Production Ops','Taber/Hussar Finance','Taber/Hussar G&G','Taber/Hussar BPI'};
        
        Map<String,Id> chatterName2Id = new Map<String,Id>();
        
        for(CollaborationGroup cg : [SELECT Id,Name FROM CollaborationGroup WHERE Name IN: chatterNames])
        {
            chatterName2Id.put(cg.Name,cg.Id);
        }
        
        TaberHussarTeamChatterGroup = '';
        if(chatterName2Id.containsKey('Taber/Hussar Team'))
            TaberHussarTeamChatterGroup = chatterName2Id.get('Taber/Hussar Team');
        
        // Does the current user have access to the Taber/Hussar Chatter Group?
        hasChatterAccess = false;
        List<CollaborationGroupMember> TaberHussarGroupMembers = [SELECT Id FROM CollaborationGroupMember WHERE CollaborationGroupId =: TaberHussarTeamChatterGroup AND MemberId =: UserInfo.getUserId()];
        if(TaberHussarGroupMembers.size() > 0)
            hasChatterAccess = true;
        
        assets = new List<AssetWrapper>();
        assets.add(new AssetWrapper('/apex/WCPWells?show=Western Canada Production', 'Wells',  'wells.png', true));
        assets.add(new AssetWrapper('#','Routes / Pipelines', 'pipelines.png', false));
        assets.add(new AssetWrapper('#','Facilities', 'facilities.png', false));
        
        teams = new List<TeamWrapper>();
        
        if(chatterName2Id.containsKey('Taber/Hussar Reservoir'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Taber/Hussar Reservoir'),'Reservoir','Reservoir.png'));
        else
            teams.add(new TeamWrapper('#','Reservoir','Reservoir.png'));
        
        if(chatterName2Id.containsKey('Taber/Hussar G&G'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Taber/Hussar G&G'),'G & G','GandG.jpg'));
        else
            teams.add(new TeamWrapper('#','G & G','GandG.jpg'));
        
        if(chatterName2Id.containsKey('Taber/Hussar Development'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Taber/Hussar Development'),'Development','Development.jpg'));
        else
            teams.add(new TeamWrapper('#','Development','Development.jpg'));
        
        if(chatterName2Id.containsKey('Taber/Hussar Production Ops'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Taber/Hussar Production Ops'),'Production Ops','Production.jpg'));
        else
            teams.add(new TeamWrapper('#','Production Ops','Production.jpg'));
        
        if(chatterName2Id.containsKey('Taber/Hussar BPI'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Taber/Hussar BPI'),'BPI','BPI.jpg'));
        else
            teams.add(new TeamWrapper('#','BPI','BPI.jpg'));
        
        if(chatterName2Id.containsKey('Taber/Hussar Finance'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Taber/Hussar Finance'),'Finance','Finance.jpg'));
        else
            teams.add(new TeamWrapper('#','Finance','Finance.jpg'));
        
        if(chatterName2Id.containsKey('Taber/Hussar D&C'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Taber/Hussar D&C'),'D & C','DandC.png'));
        else
            teams.add(new TeamWrapper('#','D & C','DandC.png'));
    }
}