@isTest
private class SetAccount_Test
{
    @isTest static void TestSetAccount()
    {
        RecordType recType = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Welding_Partner_Account'];
        Account acct = AccountTestData.createAccount('accountName', null);
        acct.RecordTypeId = recType.Id;
        acct.AccountNumber= '302010';        
        insert acct;
        Bill_To__c billTo = new Bill_To__c(Account__c=acct.Id, Name='BillTo',Bill_To_Key__c='900990', Customer__c='302010');
        insert billTo;
        acct.AccountNumber= '302010';
        acct.Customer_Number__c =  '302012';
        update acct;       
        billTo.Bill_To_Key__c='900992';
        billTo.Customer__c='302012';
        update billTo;
    }
}