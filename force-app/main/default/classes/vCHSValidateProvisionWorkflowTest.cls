@isTest(SeeAllData = true)
public class vCHSValidateProvisionWorkflowTest {
    static testMethod void myUnitTest() {
         
            Date currentDate = Date.Today();
            User runningUser = UserTestData.createTestsysAdminUser();
            insert runningUser;
            System.AssertNotEquals(runningUser.Id, Null);
            
            System.runAs(runningUser) {
                System.debug('++RUNNING USER: ' + UserInfo.getUserName());
                System.debug('Current Profile: ' + UserInfo.getProfileId()); 

				Request__c request1 = RequestTestData.createLabRequest();
                
                User fakeUser = UserTestData.createTestUser('Standard Platform User');
                insert fakeUser;                
                fakeUser = [select id,Email from User where id=:fakeUser.id];
                       
                //Submit the Request for "SUBMIT FOR APPROVAL"
                request1 = RequestTestData.setProvisionStage(1, request1.Id);     
                update request1;                
                request1 = [select id, Name, Program_Manager__r.Id, Provisioning_Stage__c from Request__c where id=:request1.id];
                System.assertEquals(request1.Provisioning_Stage__c, 'Submit for Approval');
                
                //CHANGE OWNER to PgM
                request1 = RequestTestData.changeOwner(request1.Id, request1.Program_Manager__r.Id);
                update request1;                 
                request1 = [select id, Name, EA_Approved__c  , PgM_Approved__c, Program_Manager__c, Provisioning_Stage__c from Request__c where id=:request1.id];               
                
                //Set the Approvers to TRUE
                request1 = RequestTestData.setApprovers(request1.Id, true,true);
                update request1;                 
                request1 = [select id, Name, EA_Approved__c  , PgM_Approved__c, Program_Manager__c, Provisioning_Stage__c from Request__c where id=:request1.id];               
                System.assertEquals(request1.PgM_Approved__c, true);
                System.assertEquals(request1.EA_Approved__c, true);
                
                //Attempt to set the Request to "Provisioning: In Progress"
                //
                System.debug('****Setting Provision Stage!!');              
                request1 = RequestTestData.setProvisionStage(2, request1.Id);    
                System.debug('****Setting Provision Stage - 2!!');
                try{
                    update request1;                
                    request1 = [select id, Provisioning_Stage__c from Request__c where id=:request1.id];                                          
                    System.assertNotEquals(request1.Provisioning_Stage__c, 'Provisioning:  In Progress');
                }
                catch(Dmlexception e)
                {
                    System.debug('#@@@@@Got here - caught dml exception!!');
                    return;
                } 
        }
        
	}
}