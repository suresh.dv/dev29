@isTest
public class CaseCloseAndSaveControllerTest {
    
    @isTest static void updateCaseTest(){
        Test.startTest();

        Case testCase = new Case(status='New');
        insert testCase;
        Id caseId = testCase.Id;
        CaseCloseAndSaveController.updateCase(caseId);
        testCase = [Select Id,status from Case Where id=:caseId ] ;
        Test.stopTest();
        List<Case_data__mdt> caseMetaData = [SELECT Completed_Status__c FROM Case_data__mdt Limit 1];
    	string closedStatus = caseMetaData[0].Completed_Status__c;
        System.assertEquals(closedStatus, testCase.Status);
    }
}