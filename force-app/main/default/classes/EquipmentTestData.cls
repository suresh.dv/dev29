/*History:
 *
 *  09-Mar-17 Miro Zelina added method for create equipment for Well Event
 *  06-jul-17 Miro Zelina added methods for create equipment for System & Sub-System
 *  28-Jul-17 Miro Zelina added create user method 
 *
 */

@isTest
public class EquipmentTestData {
    
    public static User createUser(String uRole, String uProfile, String uName){

        User user = new User();
       
        Profile p = [SELECT Id FROM Profile WHERE Name =: uProfile];
        UserRole r = [SELECT Id FROM UserRole WHERE Name =: uRole];

        Double random = Math.Random();

        user.email = 'testUser' +  random + '@test.com';
        user.Alias = 'tu' ;
        user.EmailEncodingKey = 'UTF-8';
        user.LastName = 'User';
        user.LanguageLocaleKey = 'en_US'; 
        user.LocaleSidKey = 'en_US'; 
        user.ProfileId = p.Id;
        user.userRoleId = r.Id;
        user.TimeZoneSidKey = 'America/Los_Angeles'; 
        user.UserName = uName + random;

        insert user;
        return user;    
    }
    
    //Permission Set Assignemnt
    public static User assignPermissionSet(User user, String userPermissionSetName){
    
        PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name =: userPermissionSetName]; 
        PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();
        
        permissionSetAssignment.PermissionSetId = permissionSet.Id;
        permissionSetAssignment.AssigneeId = user.Id; 
        
        insert permissionSetAssignment;
            
        return user;
    }
    

	public static List<Equipment__c> createEquipment(List<Unit__c> unitList, Integer numberOfEquipmentsForEachUnit) {
		
		List<Equipment__c> recList = new List<Equipment__c>();
		
		for(Unit__c unit : unitList) {
			for(Integer i = 0; i < numberOfEquipmentsForEachUnit; i++) {
				Equipment__c equipment = new Equipment__c();
				equipment.Name = 'Equipment Name ' + i;
				equipment.Tag_Number__c = 'Equipment Name ' + i;
				equipment.Functional_Location__c = 'Equi Fun Location ' + i;
				equipment.Unit__c = unit.Id; 
				
				recList.add(equipment);	
			}
		}
		
		insert recList;
		return recList;
	}
	public static Equipment__c createEquipment(Location__c loc)
	{
		Equipment__c equipment = new Equipment__c();
        equipment.Name = 'Equipment Test on Location';
        equipment.Description_of_Equipment__c = 'Description';
        equipment.Manufacturer__c = 'Manufacturer A';
        equipment.Model_Number__c = 'SONY';
        equipment.Manufacturer_Serial_No__c = '123456';
        equipment.Tag_Number__c = 'Equipment Test';
        equipment.Location__c = loc.Id;
        insert equipment;
        return equipment;
	}
	
	public static Equipment__c createEquipment(Facility__c fac)
	{
		Equipment__c equipment = new Equipment__c();
        equipment.Name = 'Equipment Test on Facility';
        equipment.Description_of_Equipment__c = 'Description';
        equipment.Manufacturer__c = 'Manufacturer A';
        equipment.Model_Number__c = 'SONY';
        equipment.Manufacturer_Serial_No__c = '123456';
        equipment.Tag_Number__c = 'Equipment Test';
        equipment.Facility__c = fac.Id;
        insert equipment;
        return equipment;
	}
	
	public static Equipment__c createEquipment(Well_Event__c eve)
	{
		Equipment__c equipment = new Equipment__c();
        equipment.Name = 'Equipment Test on Event';
        equipment.Description_of_Equipment__c = 'Description';
        equipment.Manufacturer__c = 'Manufacturer C';
        equipment.Model_Number__c = 'Siemens';
        equipment.Manufacturer_Serial_No__c = '123999';
        equipment.Tag_Number__c = 'Equipment Test';
        equipment.Well_Event__c = eve.Id;
        insert equipment;
        return equipment;
	}
	
	public static Equipment__c createEquipment(System__c sys)
	{
		Equipment__c equipment = new Equipment__c();
        equipment.Name = 'Equipment Test on System';
        equipment.Description_of_Equipment__c = 'Description';
        equipment.Manufacturer__c = 'Manufacturer D';
        equipment.Model_Number__c = 'JVC';
        equipment.Manufacturer_Serial_No__c = '123888';
        equipment.Tag_Number__c = 'Equipment Test';
        equipment.System__c = sys.Id;
        insert equipment;
        return equipment;
	}
	
	public static Equipment__c createEquipment(Sub_System__c subSys)
	{
		Equipment__c equipment = new Equipment__c();
        equipment.Name = 'Equipment Test on Sub-System';
        equipment.Description_of_Equipment__c = 'Description';
        equipment.Manufacturer__c = 'Manufacturer E';
        equipment.Model_Number__c = 'Samsung';
        equipment.Manufacturer_Serial_No__c = '123777';
        equipment.Tag_Number__c = 'Equipment Test';
        equipment.Sub_System__c = subSys.Id;
        insert equipment;
        return equipment;
	}

	public static Equipment__c createEquipment(Functional_Equipment_Level__c fel)
	{
		Equipment__c equipment = new Equipment__c();
        equipment.Name = 'Equipment Test on FEL';
        equipment.Description_of_Equipment__c = 'Description';
        equipment.Manufacturer__c = 'Manufacturer E';
        equipment.Model_Number__c = 'Samsung';
        equipment.Manufacturer_Serial_No__c = '123777';
        equipment.Tag_Number__c = 'Equipment Test';
        equipment.Functional_Equipment_Level__c = fel.Id;
        insert equipment;
        return equipment;
	}

	public static Equipment__c createEquipment(Yard__c yard)
	{
		Equipment__c equipment = new Equipment__c();
        equipment.Name = 'Equipment Test on Yard';
        equipment.Description_of_Equipment__c = 'Description';
        equipment.Manufacturer__c = 'Manufacturer E';
        equipment.Model_Number__c = 'Samsung';
        equipment.Manufacturer_Serial_No__c = '123777';
        equipment.Tag_Number__c = 'Equipment Test';
        equipment.Yard__c = yard.Id;
        insert equipment;
        return equipment;
	}

	public static List<Equipment__c> createEquipmentHierarchy(Location__c loc) {
		List<Equipment__c> equipmentList = new List<Equipment__c>();

		for(Integer i=0; i<5; i++) {
			Equipment__c equipment = new Equipment__c();
	        equipment.Name = 'Equipment Test ' + i;
	        equipment.Description_of_Equipment__c = 'Description';
	        equipment.Manufacturer__c = 'Manufacturer A';
	        equipment.Model_Number__c = 'SONY';
	        equipment.Manufacturer_Serial_No__c = String.valueOf(Math.random());
	        equipment.Tag_Number__c = 'Equipment Test' + i;
	        equipment.Location__c = loc.Id;
	        equipmentList.add(equipment);
	    }
	    insert equipmentList;

	    //Create Hierarchy
	    equipmentList[1].Superior_Equipment__c = equipmentList[0].Id;
	    equipmentList[2].Superior_Equipment__c = equipmentList[1].Id;
	    equipmentList[3].Superior_Equipment__c = equipmentList[2].Id;
	    equipmentList[4].Superior_Equipment__c = equipmentList[3].Id;
	
	    update equipmentList;
	    return equipmentList;
	}
	
	public static List<Equipment__c> createEquipmentHierarchyEvent(Well_Event__c eve) {
		List<Equipment__c> equipmentListEvent = new List<Equipment__c>();

		for(Integer i=0; i<5; i++) {
			Equipment__c equipment = new Equipment__c();
	        equipment.Name = 'Equipment Test Event' + i;
	        equipment.Description_of_Equipment__c = 'Description';
	        equipment.Manufacturer__c = 'Manufacturer B';
	        equipment.Model_Number__c = 'PHILIPS';
	        equipment.Manufacturer_Serial_No__c = String.valueOf(Math.random());
	        equipment.Tag_Number__c = 'Equipment Test Event' + i;
	        equipment.Well_Event__c = eve.Id;
	        equipmentListEvent.add(equipment);
	    }
	    insert equipmentListEvent;

	    //Create Hierarchy
	    equipmentListEvent[1].Superior_Equipment__c = equipmentListEvent[0].Id;
	    equipmentListEvent[2].Superior_Equipment__c = equipmentListEvent[1].Id;
	    equipmentListEvent[3].Superior_Equipment__c = equipmentListEvent[2].Id;
	    equipmentListEvent[4].Superior_Equipment__c = equipmentListEvent[3].Id;
	
	    update equipmentListEvent;
	    return equipmentListEvent;
	}
	
	public static List<Equipment__c> createEquipmentHierarchySystem(System__c sys) {
		List<Equipment__c> equipmentListSystem = new List<Equipment__c>();

		for(Integer i=0; i<5; i++) {
			Equipment__c equipment = new Equipment__c();
	        equipment.Name = 'Equipment Test System' + i;
	        equipment.Description_of_Equipment__c = 'Description';
	        equipment.Manufacturer__c = 'Manufacturer D';
	        equipment.Model_Number__c = 'JVC';
	        equipment.Manufacturer_Serial_No__c = String.valueOf(Math.random());
	        equipment.Tag_Number__c = 'Equipment Test System' + i;
	        equipment.System__c = sys.Id;
	        equipmentListSystem.add(equipment);
	    }
	    insert equipmentListSystem;

	    //Create Hierarchy
	    equipmentListSystem[1].Superior_Equipment__c = equipmentListSystem[0].Id;
	    equipmentListSystem[2].Superior_Equipment__c = equipmentListSystem[1].Id;
	    equipmentListSystem[3].Superior_Equipment__c = equipmentListSystem[2].Id;
	    equipmentListSystem[4].Superior_Equipment__c = equipmentListSystem[3].Id;
	
	    update equipmentListSystem;
	    return equipmentListSystem;
	}
	
	public static List<Equipment__c> createEquipmentHierarchySubSystem(Sub_System__c subSys) {
		List<Equipment__c> equipmentListSubSystem = new List<Equipment__c>();

		for(Integer i=0; i<5; i++) {
			Equipment__c equipment = new Equipment__c();
	        equipment.Name = 'Equipment Test Sub-System' + i;
	        equipment.Description_of_Equipment__c = 'Description';
	        equipment.Manufacturer__c = 'Manufacturer E';
	        equipment.Model_Number__c = 'Samsung';
	        equipment.Manufacturer_Serial_No__c = String.valueOf(Math.random());
	        equipment.Tag_Number__c = 'Equipment Test Sub-System' + i;
	        equipment.Sub_System__c = subSys.Id;
	        equipmentListSubSystem.add(equipment);
	    }
	    insert equipmentListSubSystem;

	    //Create Hierarchy
	    equipmentListSubSystem[1].Superior_Equipment__c = equipmentListSubSystem[0].Id;
	    equipmentListSubSystem[2].Superior_Equipment__c = equipmentListSubSystem[1].Id;
	    equipmentListSubSystem[3].Superior_Equipment__c = equipmentListSubSystem[2].Id;
	    equipmentListSubSystem[4].Superior_Equipment__c = equipmentListSubSystem[3].Id;
	
	    update equipmentListSubSystem;
	    return equipmentListSubSystem;
	}

	public static List<Equipment__c> createEquipmentHierarchyFEL(Functional_Equipment_Level__c fel) {
		List<Equipment__c> equipmentListFEL = new List<Equipment__c>();

		for(Integer i=0; i<5; i++) {
			Equipment__c equipment = new Equipment__c();
	        equipment.Name = 'Equipment Test FEL' + i;
	        equipment.Description_of_Equipment__c = 'Description';
	        equipment.Manufacturer__c = 'Manufacturer E';
	        equipment.Model_Number__c = 'Samsung';
	        equipment.Manufacturer_Serial_No__c = String.valueOf(Math.random());
	        equipment.Tag_Number__c = 'Equipment Test FEL' + i;
	        equipment.Functional_Equipment_Level__c = fel.Id;
	        equipmentListFEL.add(equipment);
	    }
	    insert equipmentListFEL;

	    //Create Hierarchy
	    equipmentListFEL[1].Superior_Equipment__c = equipmentListFEL[0].Id;
	    equipmentListFEL[2].Superior_Equipment__c = equipmentListFEL[1].Id;
	    equipmentListFEL[3].Superior_Equipment__c = equipmentListFEL[2].Id;
	    equipmentListFEL[4].Superior_Equipment__c = equipmentListFEL[3].Id;
	
	    update equipmentListFEL;
	    return equipmentListFEL;
	}

	public static List<Equipment__c> createEquipmentHierarchyYard(Yard__c yard) {
		List<Equipment__c> equipmentListYard = new List<Equipment__c>();

		for(Integer i=0; i<5; i++) {
			Equipment__c equipment = new Equipment__c();
	        equipment.Name = 'Equipment Test Sub-System' + i;
	        equipment.Description_of_Equipment__c = 'Description';
	        equipment.Manufacturer__c = 'Manufacturer E';
	        equipment.Model_Number__c = 'Samsung';
	        equipment.Manufacturer_Serial_No__c = String.valueOf(Math.random());
	        equipment.Tag_Number__c = 'Equipment Test Sub-System' + i;
	        equipment.Yard__c = yard.Id;
	        equipmentListYard.add(equipment);
	    }
	    insert equipmentListYard;

	    //Create Hierarchy
	    equipmentListYard[1].Superior_Equipment__c = equipmentListYard[0].Id;
	    equipmentListYard[2].Superior_Equipment__c = equipmentListYard[1].Id;
	    equipmentListYard[3].Superior_Equipment__c = equipmentListYard[2].Id;
	    equipmentListYard[4].Superior_Equipment__c = equipmentListYard[3].Id;
	
	    update equipmentListYard;
	    return equipmentListYard;
	}
	
}