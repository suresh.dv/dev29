public class ProjectAreaTestData {
    
    public static User spa {get; set;}
    public static User projectManager {get; set;}
    public static User changeCoordinator {get; set;}
    
    public static Project_Area__c createProjectArea(Id projectId, String Name) {
        spa = UserTestData.createSunriseTestUser(1)[0];
                
        projectManager = UserTestData.createSunriseTestUser(1)[0];
        
        changeCoordinator = UserTestData.createSunriseTestAdminUser(1)[0];  
                
        Project_Area__c projectArea = new Project_Area__c();
        //projectArea.Project__c = projectId;
        projectArea.Name = Name;
        projectArea.SPA__c = spa.Id;
        projectArea.Project_Manager__c = projectManager.Id;
        projectArea.Change_Coordinator__c = changeCoordinator.Id;
        //projectArea.Endorser__c = projectManager.Id;
        
        return projectArea;
    }
}