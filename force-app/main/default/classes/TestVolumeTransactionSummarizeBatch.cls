@isTest
public class TestVolumeTransactionSummarizeBatch
{
	private class TestData
	{
		Id AccountId;
		Date EndDate;
	}
	
	private static TestData createDataForOneAccountOneVolume(Date pbDate)
	{
		// This test only checks for one account and one volume record.  At the end the summary
		// should only have one record for one account.
		
		Cardlock_Settings__c csSettings = new Cardlock_Settings__c();
		csSettings.Num_Errors__c = 0;
		csSettings.Volume_Transaction_Scheduled_Job_Id__c = '0';
		insert csSettings;
		
		Account acct = new Account(Name = 'Account 1');
		insert acct;
		
		Volume_Transaction__c vt = new Volume_Transaction__c(
			Account__c = acct.Id,
			Card_Type__c = 'RC',
			Volume_Transaction__c = 100,
			YTD_Volume__c = 200,
			PYTD_Volume__c = 300,
			Total_Profit__c = 400,
			Total_YTD_Profit__c = 500,
			Total_PYTD_Profit__c = 600,
			Transaction_Week_Ending_Date__c = pbDate
		);
		insert vt;
		
		TestData a = new TestData();
		a.AccountId = acct.Id;
		a.EndDate = pbDate;
		return a;
	}
	
	@isTest
	public static void testWrongDayOfTheWeek()
	{
		// this results in error condition 1
		Date txnDate = Date.newInstance(2013, 1, 5);
		TestData a = createDataForOneAccountOneVolume(txnDate);
		System.assertEquals(0, [SELECT Count() FROM Volume_Transaction_Summary__c]);
		System.assertEquals(0, VolumeTransactionSummarizeBatchSchedule.errCode);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent);
        Test.startTest();
        Database.executeBatch(new VolumeTransactionSummarizeBatchSchedule(txnDate));
        Test.stopTest();
		System.assertEquals(0, [SELECT Count() FROM Volume_Transaction_Summary__c]);
		System.assertEquals(1, VolumeTransactionSummarizeBatchSchedule.errCode);
		System.assertEquals(true, VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent);
	}
	
	@isTest
	public static void testOneAccountOneVolumeRecord()
	{
		// this results in no error condition.
		Date txnDate = Date.newInstance(2013, 1, 6);
		TestData a = createDataForOneAccountOneVolume(txnDate);
		System.assertEquals(0, [SELECT Count() FROM Volume_Transaction_Summary__c]);
		System.assertEquals(0, VolumeTransactionSummarizeBatchSchedule.errCode);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent);
        Test.startTest();
        Database.executeBatch(new VolumeTransactionSummarizeBatchSchedule(txnDate));
        Test.stopTest();
		System.assertEquals(1, [SELECT Count() FROM Volume_Transaction_Summary__c]);
		System.assertEquals(0, VolumeTransactionSummarizeBatchSchedule.errCode);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent);
		Volume_Transaction_Summary__c vts = [SELECT Volume__c, Volume_YTD__c, Volume_PYTD__c, Total_Profit__c, Total_Profit_YTD__c, Total_Profit_PYTD__c
			FROM 
				Volume_Transaction_Summary__c 
			WHERE 
				Account__c = :a.AccountId AND Transaction_Week_Ending_Date__c = :a.EndDate];
		System.assertEquals(100, vts.Volume__c);
		System.assertEquals(200, vts.Volume_YTD__c);
		System.assertEquals(300, vts.Volume_PYTD__c);
		System.assertEquals(400, vts.Total_Profit__c);
		System.assertEquals(500, vts.Total_Profit_YTD__c);
		System.assertEquals(600, vts.Total_Profit_PYTD__c);
	}

	private static TestData createDataForOneAccountButNoVolume(Date pbDate)
	{
		// This test only checks for one account and but no volume record for the week specified.
		// Note we still create a volume weekly record, but for the week before.
		
		Cardlock_Settings__c csSettings = new Cardlock_Settings__c();
		csSettings.Num_Errors__c = 0;
		csSettings.Volume_Transaction_Scheduled_Job_Id__c = '0';
		insert csSettings;
		
		Account acct = new Account(Name = 'Account 1');
		insert acct;

		Volume_Transaction__c vt = new Volume_Transaction__c(
			Account__c = acct.Id,
			Card_Type__c = 'RC',
			Volume_Transaction__c = 100,
			YTD_Volume__c = 200,
			PYTD_Volume__c = 300,
			Total_Profit__c = 400,
			Total_YTD_Profit__c = 500,
			Total_PYTD_Profit__c = 600,
			Transaction_Week_Ending_Date__c = pbDate.addDays(-7)
		);
		insert vt;
		
		TestData a = new TestData();
		a.AccountId = acct.Id;
		a.EndDate = pbDate;
		return a;
	}
	
	@isTest
	public static void testOneAccountButNoVolumeRecord()
	{
		// this results in error condition 2
		Date txnDate = Date.newInstance(2013, 1, 6);
		TestData a = createDataForOneAccountButNoVolume(txnDate);
		System.assertEquals(0, [SELECT Count() FROM Volume_Transaction_Summary__c]);
		System.assertEquals(0, VolumeTransactionSummarizeBatchSchedule.errCode);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent);
        Test.startTest();
        Database.executeBatch(new VolumeTransactionSummarizeBatchSchedule(txnDate));
        Test.stopTest();
		System.assertEquals(0, [SELECT Count() FROM Volume_Transaction_Summary__c]);
		System.assertEquals(2, VolumeTransactionSummarizeBatchSchedule.errCode);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent);
		System.assertEquals(true, VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent);
	}

	private static TestData createDataForOneAccountWithVolumeAlreadySummarized(Date pbDate)
	{
		// This test only checks for one account and the volume data has already been summarized.
		
		Cardlock_Settings__c csSettings = new Cardlock_Settings__c();
		csSettings.Num_Errors__c = 0;
		csSettings.Volume_Transaction_Scheduled_Job_Id__c = '0';
		insert csSettings;
		
		Account acct = new Account(Name = 'Account 1');
		insert acct;

		Volume_Transaction__c vt = new Volume_Transaction__c(
			Account__c = acct.Id,
			Card_Type__c = 'RC',
			Volume_Transaction__c = 100,
			YTD_Volume__c = 200,
			PYTD_Volume__c = 300,
			Total_Profit__c = 400,
			Total_YTD_Profit__c = 500,
			Total_PYTD_Profit__c = 600,
			Transaction_Week_Ending_Date__c = pbDate
		);
		insert vt;
		
		Volume_Transaction_Summary__c vts = new Volume_Transaction_Summary__c(
			Account__c = acct.Id,
			Total_Profit__c = 100, 
			Total_Profit_PYTD__c = 200,
			Total_Profit_YTD__c = 300,
			Volume__c = 400,
			Volume_PYTD__c = 500,
			Volume_YTD__c = 600,
			Transaction_Week_Ending_Date__c = pbDate
		);
		insert vts;

		TestData a = new TestData();
		a.AccountId = acct.Id;
		a.EndDate = pbDate;
		return a;
	}
		
	@isTest
	public static void testOneAccountWithVolumeAlreadySummarized()
	{
		// this results in error condition 4.
		Date txnDate = Date.newInstance(2013, 1, 6);
		TestData a = createDataForOneAccountWithVolumeAlreadySummarized(txnDate);
		List<Volume_Transaction_Summary__c> vtsBefore = [SELECT LastModifiedDate FROM Volume_Transaction_Summary__c];
		System.assertEquals(vtsBefore.size(), 1);
		Datetime beforeTimestamp;
		beforeTimestamp = (Datetime)vtsBefore[0].get('LastModifiedDate');

		System.assertEquals(0, VolumeTransactionSummarizeBatchSchedule.errCode);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent);

        Test.startTest();
        Database.executeBatch(new VolumeTransactionSummarizeBatchSchedule(txnDate));
        Test.stopTest();

		List<Volume_Transaction_Summary__c> vtsAfter = [SELECT LastModifiedDate FROM Volume_Transaction_Summary__c];
		System.assertEquals(vtsAfter.size(), 1);
		Datetime afterTimestamp;
		afterTimestamp = (Datetime)vtsAfter[0].get('LastModifiedDate');

		// Check the record has not been touched.
		System.assertEquals(beforeTimestamp, afterTimestamp);

		System.assertEquals(4, VolumeTransactionSummarizeBatchSchedule.errCode);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent);
	}


	private static TestData createDataForAccountWithWrongCardType(Date pbDate)
	{
		// This test checks for wrong card, i.e. non-RC.  At the end the summary will not have
		// any record because it is of the wrong card type.
		
		// This assumes the date is a Sunday, e.g. 2013-01-06
		
		Cardlock_Settings__c csSettings = new Cardlock_Settings__c();
		csSettings.Num_Errors__c = 0;
		csSettings.Volume_Transaction_Scheduled_Job_Id__c = '0';
		insert csSettings;

		Account acct = new Account(Name = 'Account 1');
		insert acct;
		
		Volume_Transaction__c vt = new Volume_Transaction__c(
			Account__c = acct.Id,
			Card_Type__c = 'RD',
			PYTD_Volume__c = 100,
			Total_Profit__c = 200,
			Total_PYTD_Profit__c = 300,
			Total_YTD_Profit__c = 400,
			Transaction_Week_Ending_Date__c = pbDate,
			Volume_Transaction__c = 500,
			YTD_Volume__c = 600
		);
		insert vt;

		TestData a = new TestData();
		a.AccountId = acct.Id;
		a.EndDate = pbDate;
		return a;
	}
	
	@isTest
	public static void testAccountWithWrongProduct()
	{
		// this does not result in error, just nothing happens
		Date txnDate = Date.newInstance(2013, 1, 6);
		TestData a = createDataForAccountWithWrongCardType(txnDate);
		System.assertEquals(0, [SELECT Count() FROM Volume_Transaction_Summary__c]);
		System.assertEquals(0, VolumeTransactionSummarizeBatchSchedule.errCode);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent);
        Test.startTest();
        Database.executeBatch(new VolumeTransactionSummarizeBatchSchedule(txnDate));
        Test.stopTest();
		System.assertEquals(0, [SELECT Count() FROM Volume_Transaction_Summary__c]);
		System.assertEquals(0, VolumeTransactionSummarizeBatchSchedule.errCode);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent);
	}
	
	private static TestData createDataForOneAccountSeveralVolumes(Date pbDate)
	{
		// This test checks for one account and 5 volume records.  At the end the summary
		// should only have one record for one account, and other data rolledup properly.
		// After 5 runs, volume should be 100 + 200 + 300 + 400 + 500 = 1500
		// YTD is 200 + 400 + 600 + 800 + 1000 = 3000
		// PYTD is 300 + 600 + 900 + 1200 + 1500 = 4500, and so on

		// This assumes the date is a Sunday, e.g. 2013-01-06

		Cardlock_Settings__c csSettings = new Cardlock_Settings__c();
		csSettings.Num_Errors__c = 0;
		csSettings.Volume_Transaction_Scheduled_Job_Id__c = '0';
		insert csSettings;

		Account acct = new Account(Name = 'Account 1');
		insert acct;
		
		List<Volume_Transaction__c> vtrans = new List<Volume_Transaction__c>();
		for (Integer i = 1 ; i <= 5 ; i++)
		{
			Volume_Transaction__c vtran = new Volume_Transaction__c(
				Account__c = acct.Id,
				Card_Type__c = 'RC',
				Volume_Transaction__c = 100 * i,
				YTD_Volume__c = 200 * i,
				PYTD_Volume__c = 300 * i,
				Total_Profit__c = 400 * i,
				Total_YTD_Profit__c = 500 * i,
				Total_PYTD_Profit__c = 600 * i,
				Transaction_Week_Ending_Date__c = pbDate
			);
			vtrans.add(vtran);	
		}
		insert vtrans;
		
		TestData a = new TestData();
		a.AccountId = acct.Id;
		a.EndDate = pbDate;
		return a;
	}
	
	@isTest
	public static void testOneAccountSeveralVolumes()
	{
		// this does not result in any errors.
		Date txnDate = Date.newInstance(2013, 1, 6);
		TestData a = createDataForOneAccountSeveralVolumes(txnDate);
		System.assertEquals(0, [SELECT Count() FROM Volume_Transaction_Summary__c]);
		System.assertEquals(0, VolumeTransactionSummarizeBatchSchedule.errCode);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent);
        Test.startTest();
        Database.executeBatch(new VolumeTransactionSummarizeBatchSchedule(txnDate));
        Test.stopTest();
		System.assertEquals(1, [SELECT Count() FROM Volume_Transaction_Summary__c]);
		System.assertEquals(0, VolumeTransactionSummarizeBatchSchedule.errCode);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent);
		Volume_Transaction_Summary__c vts = [SELECT Volume__c, Volume_YTD__c, Volume_PYTD__c, Total_Profit__c, Total_Profit_YTD__c, Total_Profit_PYTD__c
			FROM 
				Volume_Transaction_Summary__c 
			WHERE 
				Account__c = :a.AccountId AND Transaction_Week_Ending_Date__c = :a.EndDate];
		System.assertEquals(1500, vts.Volume__c);
		System.assertEquals(3000, vts.Volume_YTD__c);
		System.assertEquals(4500, vts.Volume_PYTD__c);
		System.assertEquals(6000, vts.Total_Profit__c);
		System.assertEquals(7500, vts.Total_Profit_YTD__c);
		System.assertEquals(9000, vts.Total_Profit_PYTD__c);
	}

// 2013-09-09, we decided for now we are not going to use the scheduler to schedule the batch job.  All scheduler related test code 
// are now commented out
/*
    @isTest 
    public static void scheduledBatchJobTest() 
    {
    	// First get today's last Sunday.  This is a test against scheduled job, so the transaction date MUST be the previous Sunday.
        Date todayDate = System.Today();
        Date lastSundayDate = todayDate.addDays(-1 * Math.abs(Math.mod(todayDate.daysBetween(Date.newInstance(1990, 1, 7)), 7)));
    	
		TestData a = createDataForOneAccountOneVolume(lastSundayDate);

    	// This is to test that the batch job is actually in the queue when the time hits Tuesday at 1am.
        final Id cronTriggerId;
 
        System.assertEquals(0, [SELECT COUNT()
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'VolumeTransactionSummarizeBatchSchedule') AND JobType = 'ScheduledApex']);
        System.assertEquals(0, [SELECT COUNT()
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'VolumeTransactionSummarizeBatchSchedule') AND JobType = 'BatchApex']);

        for (CronTrigger cronTrigger : [SELECT Id FROM CronTrigger]) System.abortJob(cronTrigger.Id);
        
        Test.startTest();
		// 1: second, 2: minute, 3: hour, 4: day of month, 5: month, 6: day of week, and optional 7: year
		// 0 0 1 ? * TUE means every Tuesday at 1am.  Second = 0, Minute = 0, Day of Month is not specified, Month is every month.
        cronTriggerId = System.schedule('VolumeTransactionSummarizeBatchSchedule', '0 0 1 ? * TUE', new VolumeTransactionSummarizeBatchSchedule());
        Test.stopTest();

        // This proves that the Scheduled Job has kicked off the Batch Job.
        System.assertEquals(1, [SELECT COUNT()
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'VolumeTransactionSummarizeBatchSchedule') AND JobType = 'ScheduledApex']);
        System.assertEquals(1, [SELECT COUNT()
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'VolumeTransactionSummarizeBatchSchedule') AND JobType = 'BatchApex']);
                                
        // Both jobs' status should be Queued
        System.assertEquals('Queued', [SELECT Status
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'VolumeTransactionSummarizeBatchSchedule') AND JobType = 'ScheduledApex'].Status);
        System.assertEquals('Queued', [SELECT Status
                                FROM AsyncApexJob
                                WHERE ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND Name = 'VolumeTransactionSummarizeBatchSchedule') AND JobType = 'BatchApex'].Status);
                                 
    }
*/
	
    @isTest 
    public static void scheduledBatchJobButNoWeeklyRecordTest() 
    {
    	// Create data for some Sunday, but not for the current week.
		Date txnDate = Date.newInstance(2013, 1, 6);
		TestData a = createDataForOneAccountButNoVolume(txnDate);

    	// First get today's last Sunday.  This is a test against scheduled job, so the transaction date MUST be the previous Sunday.
        Date todayDate = System.Today();
        Date lastSundayDate = todayDate.addDays(-1 * Math.abs(Math.mod(todayDate.daysBetween(Date.newInstance(1990, 1, 7)), 7)));
    	
    	// To simulate scheduled job but with no weekly data, we call the Database.executeBatch(new VolumeTransactionSummarizeBatchSchedule()) with no parameter.
		System.assertEquals(0, [SELECT Count() FROM Volume_Transaction__c WHERE Transaction_Week_Ending_Date__c = :lastSundayDate]);	
		System.assertEquals(0, [SELECT Count() FROM Volume_Transaction_Summary__c WHERE Transaction_Week_Ending_Date__c = :lastSundayDate]);
		//System.assertEquals(0, Limits.getEmailInvocations());
        VolumeTransactionSummarizeBatchSchedule vtsbs = new VolumeTransactionSummarizeBatchSchedule();
        Test.startTest();
        Database.executeBatch(vtsbs);
        Test.stopTest();
        system.debug(vtsbs.isEMailSent);
		//System.assertEquals(1, Limits.getEmailInvocations());
		System.assertEquals(0, [SELECT Count() FROM Volume_Transaction__c WHERE Transaction_Week_Ending_Date__c = :lastSundayDate]);	
		System.assertEquals(0, [SELECT Count() FROM Volume_Transaction_Summary__c WHERE Transaction_Week_Ending_Date__c = :lastSundayDate]);	
    }

	private static TestData createDataForOneAccountSeveralVolumesForThisWeek()
	{
		// This test checks for one account and 5 volume records.  At the end the summary
		// should only have one record for one account, and other data rolledup properly.
		// After 5 runs, volume should be 100 + 200 + 300 + 400 + 500 = 1500
		// YTD is 200 + 400 + 600 + 800 + 1000 = 3000
		// PYTD is 300 + 600 + 900 + 1200 + 1500 = 4500, and so on

		Cardlock_Settings__c csSettings = new Cardlock_Settings__c();
		csSettings.Num_Errors__c = 0;
		csSettings.Volume_Transaction_Scheduled_Job_Id__c = '0';
		insert csSettings;

		Account acct = new Account(Name = 'Account 1');
		insert acct;

    	// First get today's last Sunday.  This is a test against scheduled job, so the transaction date MUST be the previous Sunday.
        Date todayDate = System.Today();
        Date lastSundayDate = todayDate.addDays(-1 * Math.abs(Math.mod(todayDate.daysBetween(Date.newInstance(1990, 1, 7)), 7)));
		
		List<Volume_Transaction__c> vtrans = new List<Volume_Transaction__c>();
		for (Integer i = 1 ; i <= 5 ; i++)
		{
			Volume_Transaction__c vtran = new Volume_Transaction__c(
				Account__c = acct.Id,
				Card_Type__c = 'RC',
				Volume_Transaction__c = 100 * i,
				YTD_Volume__c = 200 * i,
				PYTD_Volume__c = 300 * i,
				Total_Profit__c = 400 * i,
				Total_YTD_Profit__c = 500 * i,
				Total_PYTD_Profit__c = 600 * i,
				Transaction_Week_Ending_Date__c = lastSundayDate
			);
			vtrans.add(vtran);	
		}
		insert vtrans;
		
		TestData a = new TestData();
		a.AccountId = acct.Id;
		a.EndDate = lastSundayDate;
		return a;
	}
	
	@isTest
	public static void testOneAccountSeveralVolumesForThisWeek()
	{
		// this does not result in any errors.
		// This is assuming scheduled job for this week.
		TestData a = createDataForOneAccountSeveralVolumesForThisWeek();
		System.assertEquals(0, [SELECT Count() FROM Volume_Transaction_Summary__c]);
		System.assertEquals(0, VolumeTransactionSummarizeBatchSchedule.errCode);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent);
        Test.startTest();
        Database.executeBatch(new VolumeTransactionSummarizeBatchSchedule());
        Test.stopTest();
		System.assertEquals(1, [SELECT Count() FROM Volume_Transaction_Summary__c]);
		System.assertEquals(0, VolumeTransactionSummarizeBatchSchedule.errCode);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent);
		System.assertEquals(false, VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent);

		Volume_Transaction_Summary__c vts = [SELECT Volume__c, Volume_YTD__c, Volume_PYTD__c, Total_Profit__c, Total_Profit_YTD__c, Total_Profit_PYTD__c
			FROM 
				Volume_Transaction_Summary__c 
			WHERE 
				Account__c = :a.AccountId AND Transaction_Week_Ending_Date__c = :a.EndDate];
		System.assertEquals(1500, vts.Volume__c);
		System.assertEquals(3000, vts.Volume_YTD__c);
		System.assertEquals(4500, vts.Volume_PYTD__c);
		System.assertEquals(6000, vts.Total_Profit__c);
		System.assertEquals(7500, vts.Total_Profit_YTD__c);
		System.assertEquals(9000, vts.Total_Profit_PYTD__c);
		Account acct = [SELECT Id, Name, NumberOfEmployees, Profit_Current_Week__c, Profit_PYTD__c, Profit_YTD__c, Volume_Current_Week__c, Volume_PYTD__c, Volume_YTD__c
			FROM
				Account
			WHERE
				Id = :a.AccountId];
		System.assertEquals(6000, acct.Profit_Current_Week__c);	
		System.assertEquals(9000, acct.Profit_PYTD__c);	
		System.assertEquals(7500, acct.Profit_YTD__c);	
		System.assertEquals(1500, acct.Volume_Current_Week__c);	
		System.assertEquals(4500, acct.Volume_PYTD__c);	
		System.assertEquals(3000, acct.Volume_YTD__c);	
	}
	

		/*
	@isTest(SeeAllData=true)
	public static void testRealData()
	{
		// Note that because the batch has already been run, this test does not run the batch.  It only makes sure the data between
		// volume weekly and volume summary is in sync
		List<Id> accts_list = new List<Id>();
		Set<Id> accts_set = new Set<Id>();
		for (List<Volume_Transaction__c> vtrans : [SELECT Account__c FROM Volume_Transaction__c])
		{
			for (Volume_Transaction__c vtran : vtrans)
			{
				accts_set.add(vtran.Account__c);
			}
		}
		accts_list.addAll(accts_set);
		// Because it is looking at real data, the summary table is not empty.
		System.assertEquals(accts_list.size(), [SELECT Count() FROM Volume_Transaction_Summary__c]);

		List<AggregateResult> vtrans = [SELECT Sum(Volume_Transaction__c) Volume,
			Sum(YTD_Volume__c) Volume_YTD,
			Sum(PYTD_Volume__c) Volume_PYTD,
			Sum(Total_Profit__c) Total_Profit,
			Sum(Total_YTD_Profit__c) Total_Profit_YTD,
			Sum(Total_PYTD_Profit__c) Total_Profit_PYTD
			FROM Volume_Transaction__c
			WHERE Account__c IN :accts_list
			GROUP BY Account__c
			ORDER BY Account__c];
		
		List<Volume_Transaction_Summary__c> vtsummaries = [SELECT Volume__c, Volume_YTD__c, Volume_PYTD__c, 
			Total_Profit__c, Total_Profit_YTD__c, Total_Profit_PYTD__c
			FROM Volume_Transaction_Summary__c
			WHERE Account__c IN :accts_list
			ORDER BY Account__c];

		// Both should have the same number of rows
		System.assertEquals(vtrans.size(), vtsummaries.size());
		
		for (Integer i=0; i < accts_list.size(); i++)
		{
			System.assertEquals((Decimal)vtrans[i].get('Volume'), vtsummaries[i].Volume__c);	
			System.assertEquals((Decimal)vtrans[i].get('Volume_YTD'), vtsummaries[i].Volume_YTD__c);	
			System.assertEquals((Decimal)vtrans[i].get('Volume_PYTD'), vtsummaries[i].Volume_PYTD__c);	
			System.assertEquals((Decimal)vtrans[i].get('Total_Profit'), vtsummaries[i].Total_Profit__c);	
			System.assertEquals((Decimal)vtrans[i].get('Total_Profit_YTD'), vtsummaries[i].Total_Profit_YTD__c);	
			System.assertEquals((Decimal)vtrans[i].get('Total_Profit_PYTD'), vtsummaries[i].Total_Profit_PYTD__c);	
		}
	}
	*/
}