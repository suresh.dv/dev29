@isTest
private class BillToRelatedListControllerTest {

    static testMethod void myUnitTest() 
    {                       
        Test.startTest();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ATS: Asphalt Product Category').getRecordTypeId();
        Opportunity opp = OpportunityTestData.createOpportunity(oppRecordTypeId);        
        ATS_Freight__c freightRecord = ATSTestData.createATSFreight(opp.Id);
        ApexPages.StandardController controller = new ApexPages.StandardController(freightRecord );
        BillToRelatedListController billto = new BillToRelatedListController(controller);
        billto.opp = opp;
        billto.editBillTo();
        billto.deleteBillTo();
        insert freightRecord;
        update freightRecord;
        Test.stopTest();
                  
    }
 }