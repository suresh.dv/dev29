/*
* Author: Kishore Chandolu
* Name: HCVC_Utility
* Purpose: This Class contains all Constants used in the HCVC related class's.
* Created Date: 25th November 2014
*/


  public class HCVC_Utility{
 
   //This Constant will declare the Community Site URL Path Prefix.
     
   public static final String communitySiteUrl=Site.getPathPrefix();
   // Get the IDs of all the record types for the asset types
   public static Id crudeRecordTypeId {get {if(crudeRecordTypeId==null)crudeRecordTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Crude Pipeline').getRecordTypeId(); return crudeRecordTypeId;}set;}
   public static Id overviewRecordTypeId {get {if(overviewRecordTypeId==null)overviewRecordTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Overview').getRecordTypeId(); return overviewRecordTypeId;}set;}
   public static Id refineryTypeId {get {if(refineryTypeId==null)refineryTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Refinery').getRecordTypeId(); return refineryTypeId;}set;}
   public static Id refineryUnitTypeId {get {if(refineryUnitTypeId==null)refineryUnitTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Refinery Unit').getRecordTypeId(); return refineryUnitTypeId;}set;}
   public static Id refineryRacksTypeId {get {if(refineryRacksTypeId==null)refineryRacksTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Refinery Racks').getRecordTypeId(); return refineryRacksTypeId;}set;}
   public static Id tankTypeId {get {if(tankTypeId==null)tankTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Tank').getRecordTypeId(); return tankTypeId;}set;}  
   public static Id geojsonTypeId {get {if(geojsonTypeId==null)geojsonTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Geojson').getRecordTypeId(); return geojsonTypeId;}set;}  
  }