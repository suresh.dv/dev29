public class PREP_BVS_Util
{
    
    public static void deleteBUSA(PREP_Business_Value_Submission__c BVS)
    {
        List<PREP_Business_Unit_Savings_Allocation__c> BUSAObjects = [SELECT Id FROM PREP_Business_Unit_Savings_Allocation__c WHERE Business_Value_Submission_Id__c = :BVS.Id];
        
        if(BUSAObjects != null || !BUSAObjects.isEmpty())
        {
            delete BUSAObjects;
        }
    
    }
    
    public static void createAtlanticBUSA(PREP_Business_Value_Submission__c BVS)
    {   
        PREP_Business_Unit_Savings_Allocation__c BUSA = new PREP_Business_Unit_Savings_Allocation__c();
                    
        BUSA.Allocation_Level__c = 'Atlantic Region - Operations';
        BUSA.Business_Unit__c = 'Atlantic Region';
        BUSA.Allocated_Savings_Percent__c = 100;
        BUSA.Business_Value_Submission_Id__c = BVS.Id;
                    
        insert BUSA;
    }
    
    public static void createWCPBUSA(PREP_Business_Value_Submission__c BVS)
    {
        PREP_Business_Unit_Savings_Allocation__c BUSA = new PREP_Business_Unit_Savings_Allocation__c();
                    
        BUSA.Allocation_Level__c = 'Operations & Projects';
        BUSA.Business_Unit__c = 'Western Canada Production';
        BUSA.Allocated_Savings_Percent__c = 20;
        BUSA.Business_Value_Submission_Id__c = BVS.Id;
           
        insert BUSA;
                    
        PREP_Business_Unit_Savings_Allocation__c BUSA2 = new PREP_Business_Unit_Savings_Allocation__c();
                    
        BUSA2.Allocation_Level__c = 'Lloydminster Production Operations';
        BUSA2.Business_Unit__c = 'Heavy Oil';
        BUSA2.Allocated_Savings_Percent__c = 75;
        BUSA2.Business_Value_Submission_Id__c = BVS.Id;
           
        insert BUSA2;
                    
        PREP_Business_Unit_Savings_Allocation__c BUSA3 = new PREP_Business_Unit_Savings_Allocation__c();
                    
        BUSA3.Allocation_Level__c = 'Canadian Downstream';
        BUSA3.Business_Unit__c = 'Downstream';
        BUSA3.Allocated_Savings_Percent__c = 5;
        BUSA3.Business_Value_Submission_Id__c = BVS.Id;
             
        insert BUSA3;
    
    }
    
    public static void createLimaBUSA(PREP_Business_Value_Submission__c BVS)
    {
         
         PREP_Business_Unit_Savings_Allocation__c BUSA = new PREP_Business_Unit_Savings_Allocation__c();
                    
         BUSA.Allocation_Level__c = 'U.S Refining';
         BUSA.Business_Unit__c = 'Downstream';
         BUSA.Allocated_Savings_Percent__c = 100;
         BUSA.Business_Value_Submission_Id__c = BVS.Id;
          
         insert BUSA;
    }



}