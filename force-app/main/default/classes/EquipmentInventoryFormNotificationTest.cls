/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EquipmentInventoryFormNotificationTest {

    static void createSampleData()
    {
        Business_Unit__c businessUnit = new Business_Unit__c(Name = 'Test Business Unit');
        insert businessUnit;

        Business_Department__c  businessDepartment = BusinessDepartmentTestData.createBusinessDepartment('Test Business Department');    
        insert businessDepartment;

        Operating_District__c operatingDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Field', businessDepartment.Id, businessUnit.Id);
        insert operatingDistrict;
        
        Id recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;
        
        Field__c field = FieldTestData.createField('Test Field Control Centre', operatingDistrict.Id, recordTypeId);
        insert field;

        Route__c route = RouteTestData.createRoute('999');
        insert route; 
        
        Location__c loc1 = LocationTestData.createLocation('Location 1', route.Id, field.Id);
        insert loc1;
        
        Equipment__c equip = EquipmentTestData.createEquipment(loc1);
        
        Location__c loc2 = LocationTestData.createLocation('Location 2', route.Id, field.Id);
        insert loc2;
        
        User user = UserTestData.createTestUser();
        insert user;
        //create bulk of "Open" Add Equipment form/Equipment Data Update form/Equipment Data Update form
        List<SObject> formList = new List<SObject>();
        for(integer i=0; i< 3; i++)
        {
            Equipment_Missing_Form__c missingForm = new Equipment_Missing_Form__c();
            missingForm.Location__c = loc1.Id;
            missingForm.Status__c = 'Open';
            missingForm.Equipment_Status__c = 'Missing';
            missingForm.Manufacturer__c = 'SAMSUNG';
            missingForm.Manufacturer_Serial_No__c = '46464';
            missingForm.Serial_plate_of_asset__c = '0024253647442';
            formList.add(missingForm);
        
            Equipment_Transfer_Form__c transferForm = new Equipment_Transfer_Form__c();
            transferForm.From_Location__c = loc1.Id;
            transferForm.To_Location__c = loc2.Id;
            transferForm.Status__c = 'Open';
            transferForm.Reason_for_Transfer_Additional_Details__c = 'NWC';
            transferForm.Date_of_Physical_Transfer__c = date.today();
            transferForm.Authorized_By__c = user.Id;
            
            formList.add(transferForm);
        
            Equipment_Correction_Form__c correctionForm = new Equipment_Correction_Form__c();
            correctionForm.Equipment__c = equip.Id;
            correctionForm.Status__c = 'Open';
            correctionForm.Description_new__c = 'Changing new Description';
           
            formList.add(correctionForm);
        }
        insert formList;
    }
    static void testNotification()
    {
          Test.startTest();
          createSampleData();
          // Schedule the test job
          String CRON_EXP = '0 0 0 3 9 ? 2022';
          String jobId = System.schedule('testEquipmentInventoryFormNotification',
                                   CRON_EXP, new EquipmentInventoryFormNotification());
    
          // Get the information from the CronTrigger API object
          CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                               FROM CronTrigger WHERE id = :jobId];
    
          // Verify the expressions are the same
          System.assertEquals(CRON_EXP, ct.CronExpression);
    
          // Verify the job has not run
          System.assertEquals(0, ct.TimesTriggered);
    
          // Verify the next time the job will run
          System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
          
         Test.stopTest();
    }
}