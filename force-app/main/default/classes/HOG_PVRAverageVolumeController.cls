/**
 * Author: Ganga Kaliyan
 * Description: Controller for the HOG_PVRAverageVolume page displayed on a Well Id page layout
 * History: ssd 17/08/2016 - Removed the calculation portion for updating PVR Average Volumes. That
 *                           will now be handled by the batch job HOG_PVRUpdateBatch. Still checking 
 *                           just in case
 *          mz 02/05/2018  - added vent gas fields into query
 **/
public with sharing class HOG_PVRAverageVolumeController 
{
  private List<Location__c> wellLocationList;
  
  public Location__c wellLocation {get;set;}
  public String recordId {get;set;}
  //public List<PieChartData> pieChartDataList {get;set;}
  //public List<BarChartData> barChartDataList {get;set;}
      
  public HOG_PVRAverageVolumeController(ApexPages.StandardController stdController)
  {
      recordId = ApexPages.currentPage().getParameters().get('id');
      //calculatePVRAverageVolume(recordId);
  }

  public void calculatePVRAverageVolume()
  {
      String wellLocRtId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Well ID').getRecordTypeId();
      wellLocationList = [Select Id, Name, RecordTypeId, PVR_AVGVOL_30D_COND__c, PVR_AVGVOL_30D_GAS__c, 
                                 PVR_AVGVOL_30D_OIL__c, PVR_AVGVOL_30D_SAND__c, PVR_AVGVOL_30D_WATER__c, 
                                 PVR_AVGVOL_5D_GAS__c, PVR_AVGVOL_5D_OIL__c, PVR_AVGVOL_5D_COND__c,
                                 PVR_AVGVOL_5D_SAND__c, PVR_AVGVOL_5D_WATER__c, PVR_UWI_RAW__c, PVR_AVGVOL_UPDATED__c,
                                 PVR_AVGVOL_5D_PROD__c, PVR_AVGVOL_30D_PROD__c,
                                 (Select  Id, Name, PVR_AVGVOL_30D_COND__c, PVR_AVGVOL_30D_GAS__c, PVR_AVGVOL_30D_OIL__c,
                                          PVR_AVGVOL_30D_SAND__c, PVR_AVGVOL_30D_WATER__c,PVR_AVGVOL_5D_GAS__c, 
                                          PVR_AVGVOL_5D_OIL__c, PVR_AVGVOL_5D_COND__c,PVR_AVGVOL_5D_SAND__c, 
                                          PVR_AVGVOL_5D_WATER__c, PVR_UWI_RAW__c, PVR_AVGVOL_UPDATED__c, 
                                          PVR_Hours_On_Prod__c, PVR_Month_To_Day_Generated__c,PVR_Produced_Cond__c, 
                                          PVR_Produced_Gas__c, PVR_Produced_Oil__c, PVR_Produced_Record_Count__c, 
                                          PVR_Produced_Sand__c, PVR_Produced_Water__c, Status__c, Product_Strategy__c,
                                          PVR_GOR_Factor__c, PVR_Fuel_Consumption__c, Measured_Vent_Rate__c, GOR_Test_Date__c,
                                          GOR_Effective_Date__c, Monthly_Trucked_Oil__c, Single_Well_Battery__c
                                  From Well_Events__r 
                                  ORDER BY Name DESC NULLS LAST)
                          From Location__c
                          Where Id =: recordId And RecordTypeId =: wellLocRtId];
      if(wellLocationList != null && !wellLocationList.isEmpty()) {
        wellLocation = wellLocationList[0];

        //Check if PVR Average Updated today (Should be by batch job)
        if(wellLocation.PVR_AVGVOL_UPDATED__c == null || wellLocation.PVR_AVGVOL_UPDATED__c.date() != Date.today()) {
          //then try an update of volume from Well Events once
          HOG_PVRAverageVolumeUtilities.UpdatePVRAverageVolume(new List<Location__c>{wellLocation});
          HOG_PVRAverageVolumeUtilities.commitToDB(new List<Location__c>{wellLocation});

          //Retrieve updated Locations 
          wellLocationList = [Select Id, Name, RecordTypeId, PVR_AVGVOL_30D_COND__c, PVR_AVGVOL_30D_GAS__c, 
                                     PVR_AVGVOL_30D_OIL__c, PVR_AVGVOL_30D_SAND__c, PVR_AVGVOL_30D_WATER__c, 
                                     PVR_AVGVOL_5D_GAS__c, PVR_AVGVOL_5D_OIL__c, PVR_AVGVOL_5D_COND__c,
                                     PVR_AVGVOL_5D_SAND__c, PVR_AVGVOL_5D_WATER__c, PVR_UWI_RAW__c, PVR_AVGVOL_UPDATED__c,
                                     PVR_AVGVOL_5D_PROD__c, PVR_AVGVOL_30D_PROD__c
                              From Location__c
                              Where Id =: recordId And RecordTypeId =: wellLocRtId];
          wellLocation = (wellLocationList != null && !wellLocationList.isEmpty()) ? wellLocationList[0] : wellLocation;
        }
      
        DateTime lastUpdatedDateTime = wellLocation.PVR_AVGVOL_UPDATED__c;
        if(lastUpdatedDateTime != null)
        {
          Date lastUpdatedDate = date.newInstance(lastUpdatedDateTime.year(), lastUpdatedDateTime.month(), lastUpdatedDateTime.day());
          if(lastUpdatedDate != Date.today())  
          {
              //add error message stating that well location does not have updated well events PVR information
              system.debug('inside if condition ');
              ApexPages.Message pvrNotUpdatedErrorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'PVR Average Production is not upto date. Please contact System Administrator ');
              ApexPages.addMessage(pvrNotUpdatedErrorMsg);
          }
        }
      }
  }
}