public class EstimateCloneController {
 
    // Constructor - this only really matters if the autoRun function doesn't work right
    private final Estimate__c o;
    
    public Estimate__c  ClonedEstimate {get; set;}
    
    public EstimateCloneController (ApexPages.StandardController stdController) {
        this.o = (Estimate__c )stdController.getRecord();
    }
     
    // Code we will invoke on page load.
    public PageReference autoRun() {
 
        String theId = ApexPages.currentPage().getParameters().get('id');
        String newId;
 
        if (theId == null) {
            // Display the Visualforce page's content if no Id is passed over
            return null;
        }
 
        for (Estimate__c oldEstimate :[select id, name,Description__c,
                                        Estimated_External_cost__c,
                                        Estimated_Internal_Cost__c,
                                        Is_Template__c,
                                        Opportunity__c,
                                        Project__c,
                                        Total_Estimated_Cost__c
                                        from Estimate__c where id =:theId]) {
            // Do all the dirty work we need the code to do
            Estimate__c  newEstimate = oldEstimate.clone(false, true);
            newEstimate.Is_Template__c = false;
            
            insert newEstimate;

            
            for(Estimate_Item__c oldEstimateItem :[select id, name,
                                        Category__c,
                                        Description__c,
                                        Estimate__c,
                                        Estimated_Cost__c,
                                        Internal_cost__c,
                                        Labour_Included__c,
                                        Quantity__c,
                                        Rate__c,
                                        Units__c  from Estimate_Item__c where Estimate__c=:theId]){
            Estimate_Item__c newEstimateItem = oldEstimateItem.clone(false,true);
            newEstimateItem.Estimate__c = newEstimate.id;
            insert newEstimateItem; 
            }
            newId = newEstimate.id;
            //update newEstimate;
            ClonedEstimate  = newEstimate;
        }

        // Redirect the user back to the original page
        PageReference pageRef = new PageReference('/' + newId + '/e?retURL=%2F' + newid );
        pageRef.setRedirect(true);
        return pageRef;
 
    }
 
}