/**
 * VTT_DynamicListController
 */
public with sharing virtual class VTT_DynamicListController {
    private ApexPages.StandardSetController controller;
    //private String baseQuery;
    public Transient String WhereClause {get; set;}
    public Transient String InnerWhereClause {get; set;}
    public String BaseQuery{get; set;}  
    
    //** constructor
    public VTT_DynamicListController(String baseQuery) {
        this.baseQuery = baseQuery;
        // query(); // lazy loading - don't query until the Search function is invoked 
    }

    //** query methods
    protected void query() {
        // construct the query string
        String queryString = getBaseQuery() + ' ' + getWhereClause() + ' ' + getSortClause() + ' limit 10000';
        System.debug('queryString: ' + queryString);

        // save pageSize
        Integer pageSize = this.pageSize;

        // reboot standard set controller
        controller = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));

        // reset pageSize
        controller.setPageSize(pageSize);
    }

    //** search methods
    public PageReference search() {
                
        try {
              query();
        } catch (Exception e) {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops!' + e));
        }
        // return to same page
        return null;
    }

	// Gets base query, if there is some inner where statement then adds it
    protected virtual String getBaseQuery() {
        if(InnerWhereClause!= null && InnerWhereClause.trim() != '') {
        	// FROM Work_Order_Activities__r will always be part of query since 
        	// As of 5.10.2017 we are displaying Work Orders and WOA as a child of them on
        	// VTT Actvity list view page
        	String bef = BaseQuery.split('FROM Work_Order_Activities__r')[0];
        	String aft = BaseQuery.split('FROM Work_Order_Activities__r')[1];
        	System.debug('bef ' + bef);
        	System.debug('aft ' + aft);

        	// On PageLoad there is possibility that there is no filtering if there is, we will retain querry
        	// On Other Hand if we have filtering use InnerWhereClause as WHERE filter
        	if(!aft.contains('WHERE')){
        		BaseQuery = bef + 'FROM Work_Order_Activities__r WHERE ' + InnerWhereClause + aft;
        		System.debug('BaseQuery HERE ' + BaseQuery);	
        	} else {
        		BaseQuery = bef + 'FROM Work_Order_Activities__r WHERE ' + InnerWhereClause + ') FROM HOG_Maintenance_Servicing_Form__c';
        		System.debug('BaseQuery THERE ' + BaseQuery);	
        	}

        	System.debug('INNER ' + InnerWhereClause);
        	System.debug('BASE AFTER' + BaseQuery);
        	return BaseQuery;
        }
     	
        else return BaseQuery;
    }

    // override to construct dynamic SOQL where clause 
    protected virtual String getWhereClause() {
        if(WhereClause!= null && WhereClause.trim() != '') {
       	 	return 'WHERE ' + WhereClause;
        }
        else return '';
    }

    //** sort methods
    public String sortColumn {
        get {
            if (sortColumn == null) sortColumn = '';
            return sortColumn;
        }
        set {
            if (sortColumn != value) sortAsc = false;
            sortColumn = value;
        }
    }

    public Boolean sortAsc {
        get {
            if (sortAsc == null) sortAsc = false;
            return sortAsc;
        } 
        set;
    }

    public virtual PageReference sort() {
        sortAsc = !sortAsc;
        query();
        reloadpage();
        // return to same page
        return null;
    }

    protected virtual String getSortClause() {
        if (sortColumn == '') return '';
        else return ' order by ' + sortColumn + (sortAsc ? ' asc ' : ' desc ') + ' nulls last';
    }


    //** pageable methods
    // get records on current page 
    protected List<SObject> getRecords() {
        if (controller != null) {
            return controller.getRecords();
        }
        else {
            return new List<SObject>();
        }
    }
    

    public virtual void reloadpage() {
    }


    public virtual void first() {
        controller.first();
    }

    public virtual void previous() {
        controller.previous();
    }

    public virtual void next() {
        controller.next();
    }

    public virtual void last() {
        controller.last();
    }

    public Boolean getHasPrevious() {
        if (controller != null)
            return controller.getHasPrevious();
        else
            return false;
    }

    public Boolean getHasNext() {
        if (controller != null)
            return controller.getHasNext();
        else
            return false;
    }

    public Integer getResultSize() {
        if (controller != null)
            return controller.getResultSize();
        else
            return 0;
    }

    public Integer getPageCount() {
        if (controller == null) {
            return 0;
        } else {
            Integer resultSize = getResultSize();
    
            Integer oddRecordCount = Math.mod(resultSize, pageSize);
            return ((resultSize - oddRecordCount) / pageSize) + (oddRecordCount > 0 ? 1 : 0);
        }
    }

    public Integer getPageNumber() {
        if (controller != null)
            return controller.getPageNumber();
        else
            return 0;
    }

    public void setPageNumber(Integer pageNumber) {
        controller.setPageNumber(pageNumber);
    }

    public Integer pageSize {
        get {
            if (pageSize != null)
                return pageSize;
            else if (controller != null) 
                pageSize = controller.getPageSize();
            else
                // default pagesize
                pageSize = 20; 

            return pageSize;
        }

        set {
            pageSize = value;
            
            if (controller != null)
                controller.setPageSize(pageSize);
        }
    }

    public Boolean getRenderResults() {
        return (getResultSize() > 0);
    }

    //** update methods
    public virtual PageReference save() {
        return controller.save();
    }

    public virtual PageReference cancel() {
        return controller.cancel();
    }

    //** pass reference to dynamic paginator component 
    public VTT_DynamicListController getController () {
        return this;
    }
}