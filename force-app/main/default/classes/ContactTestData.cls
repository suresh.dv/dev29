public class ContactTestData {

	public static Contact createContact(Id accountId, String contactFName, String contactLName) {
		Contact contact = new Contact();
		contact.AccountId = accountId;
		contact.FirstName = contactFName;
		contact.LastName = contactLName;
		
		return contact;
	}
}