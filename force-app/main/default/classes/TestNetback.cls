@isTest (seeAllData=true)
private class TestNetback
{
	static Opportunity createOpportunityTestData()
	{
		// Create Account
        Account acc = new Account(Name = 'Test Account');
        Database.saveResult accountResult = Database.insert(acc);
         
        // Make ATS Tender
        ATS_Parent_Opportunity__c atstender = new ATS_Parent_Opportunity__c(Sales_Type__c                = 'Tender',
                                                                            Sales_Status__c              = 'Active',
                                                                            Trade_Class__c               = 'Construction',
                                                                            Country__c                   = 'Canada',
                                                                            Stage__c                     = 'Bid Initiation - Marketer',
                                                                            Destination_City_Province__c = 'Destination',
                                                                            For_Season__c                = '2014',
                                                                            Marketer__c                  = UserInfo.getUserId(),
                                                                            Province_State__c            = 'British Columbia',
                                                                            Region__c                    = 'BC Kootenay',
                                                                            Closest_City__c              = 'Kamloops',
                                                                            Bid_Due_Date_Time__c         = Datetime.now(),
                                                                            Freight_Due_Date_Time__c     = Datetime.now(),
                                                                            Product_Category__c          = 'Asphalt',
                                                                            Bid_Description__c           = 'Description');
        Database.saveResult atstenderResult = Database.insert(atstender);
        
        // Get Opportunity record type
        Map<String, Schema.Recordtypeinfo> recTypesByName = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName();
        
        // Create Asphalt Opportunity
        Opportunity opp = new Opportunity(Name                                = 'Test Asphalt',
                                          StageName                           = 'Opportunity Initiation - Marketer',
                                          CloseDate                           = Date.newInstance(2014,1,1),
                                          RecordTypeId                        = recTypesByName.get('ATS: Asphalt Product Category').getRecordTypeId(),
                                          Opportunity_ATS_Product_Category__c = atstenderResult.getId(),
                                          AccountId                           = accountResult.getId(),
                                          Pay_out_Handling_Fees__c            = 'No');
        insert opp;
        
        return opp;
	}
	static PriceBookEntry createAsphaltPriceBook()
	{
		// Get Product2 record type
		Map<String, Schema.Recordtypeinfo> recTypesByName = Product2.SObjectType.getDescribe().getRecordTypeInfosByName();
        
		// Create Product
        Product2 prod = new Product2(Name         = 'Test Product',
                                     Density__c   = 2,
                                     RecordTypeId = recTypesByName.get('Asphalt').getRecordTypeId(),
                                     IsActive = true);
        Database.saveResult productResult = Database.insert(prod);
        
		 // Fetch price book entry
        return [SELECT Id
                    FROM PriceBookEntry
                    WHERE Product2Id      =: productResult.getId() AND
                          Pricebook2.Name =: 'Asphalt Price Book'
                    LIMIT 1];
                                    
	} 
    static testMethod void testAsphaltRefineryAndTerminalNetback_Destination()
    {
        Opportunity opp = createOpportunityTestData();
        PriceBookEntry pricebook = createAsphaltPriceBook();
        
        // Start test
        test.startTest();
         
        // Create Opportunity Line Item
        OpportunityLineItem oli = new OpportunityLineItem(Quantity         = 1,
                                                          Unit__c          = 'Tonne',
                                                          Currency__c      = 'CAD',
                                                          TotalPrice       = 10,
                                                          OpportunityId    = opp.Id,
                                                          PricebookEntryId = pricebook.Id);
        insert oli;
        
        // Because we've inserted an Asphalt opportunity line item, the trigger 'SetNetback' should have fired to calculate it's netbacks.
        // Everything is null so the netbacks should be too.
        // Fetch the oli from the database
        OpportunityLineItem newOli = [SELECT Id,
                                             Terminal_Netback_CAD__c,
                                             Refinery_Netback_CAD__c,
                                             Axle_7_Price__c,
                                             Handling_Fees_CAD__c,
                                             AC_Premium_CAD__c
                                      FROM OpportunityLineItem
                                      WHERE Id =: oli.Id];
        
        // Ensure netbacks are null.
        System.assertEquals(null, newOli.Terminal_Netback_CAD__c);
        System.assertEquals(null, newOli.Refinery_Netback_CAD__c);
        
        // Now we'll update a few key fields to fire the trigger again.
        newOli.Axle_7_Price__c      = 10;
        newOli.Handling_Fees_CAD__c = 2;
        newOli.AC_Premium_CAD__c    = 2;
        
        // Commit changes to database
        update newOli;
        
        // Re-fetch the Opportunity Line Item and check the netbacks.
        OpportunityLineItem newOli2 = [SELECT Id,
                                              Terminal_Netback_CAD__c,
                                              Refinery_Netback_CAD__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: oli.Id];
        
        // Should be 10 - 2 - 2
        System.assertEquals(6, newOli2.Terminal_Netback_CAD__c);
        // Should be 10 - 2 - 2
        System.assertEquals(6, newOli2.Refinery_Netback_CAD__c);
        
        // Now add the freight record.
        ATS_Freight__c freight = new ATS_Freight__c(Husky_Supplier_1_Selected__c = true,
                                                    Husky_Supplier_1__c          = 'Burnco--9796--9796',
                                                    Husky_Supplier_2__c          = 'Burnco--9796--9796',
                                                    Prices_F_O_B__c              = 'Destination',
                                                    ATS_Freight__c               = opp.Id,
                                                    Local_Freight_Via__c         = 'Rail',
                                                    Supplier_1_Unit__c           = 'Tonne',
                                                    Supplier_2_Unit__c           = 'Liter',
                                                    Emulsion_Rate8_Supplier1__c = 10,
                                                    Emulsion_Rate5_Supplier1__c= 20,
                                                    Emulsion_Rate6_Supplier_1__c = 30,
                                                    Emulsion_Rate7_Supplier1__c=40);
        Database.saveResult freightResult = Database.insert(freight);

        // The ATS Freight trigger 'SetOliNetbackFromFreight' should have fired, updating nothing since Prices FOB = 'Origin'
        // Make sure this is true.
        OpportunityLineItem newOli3 = [SELECT Id,
                                              Terminal_Netback_CAD__c,
                                              Refinery_Netback_CAD__c,
                                              Axle_8_Price__c,
                                              
                                              Additives_CAD__c,
                                              Terminal_Freight_CAD__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: oli.Id];
        
        // Should be 10 - 2 - 2
        System.assertEquals(6, newOli3.Terminal_Netback_CAD__c);
        // Should be 10 - 2 - 2
        System.assertEquals(6, newOli3.Refinery_Netback_CAD__c);
        
        // Now set all Opportunity Line Item fields. It will use Axle 8 Price and new fields.
        newOli3.Axle_8_Price__c         = 55;
        newOli3.Additives_CAD__c        = 1;
        newOli3.Terminal_Freight_CAD__c = 1;
        update newOli3;
        
        // Make sure that worked.
        OpportunityLineItem newOli4 = [SELECT Id,
                                              Terminal_Netback_CAD__c,
                                              Refinery_Netback_CAD__c,
                                              Axle_8_Price__c,
                                              Axle_5_Price__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: oli.Id];
        
        // Should be 55 - 10 - 2 - 2 - 1
        System.assertEquals(40, newOli4.Terminal_Netback_CAD__c);
        // Should be 55 - 10 - 2 - 2 - 1 - 1
        System.assertEquals(39, newOli4.Refinery_Netback_CAD__c);
        
        newOli4.Axle_8_Price__c         = null;
        newOli4.Axle_5_Price__c         = 44;
        update newOli4;
        
        OpportunityLineItem newOli5 = [SELECT Id,
                                              Terminal_Netback_CAD__c,
                                              Refinery_Netback_CAD__c,
                                              Axle_8_Price__c,
                                              Axle_5_Price__c,
                                              Axle_6_Price__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: oli.Id];
        // Should be 44 - 20 - 2 - 2 - 1                               
        System.assertEquals(19, newOli5.Terminal_Netback_CAD__c);
        // Should be 44 - 20 - 2 - 2 - 1 - 1
        System.assertEquals(18, newOli5.Refinery_Netback_CAD__c);
        
        
        newOli5.Axle_8_Price__c         = null;
        newOli5.Axle_5_Price__c         = null;
        newOli5.Axle_6_Price__c         = 33;
        update newOli5;
        
        OpportunityLineItem newOli6 = [SELECT Id,
                                              Terminal_Netback_CAD__c,
                                              Refinery_Netback_CAD__c,
                                              Axle_8_Price__c,
                                              Axle_5_Price__c,
                                              Axle_6_Price__c,
                                              Axle_7_Price__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: oli.Id];
        // Should be 33 - 30 - 2 - 2 - 1                               
        System.assertEquals(-2, newOli6.Terminal_Netback_CAD__c);
        // Should be 33 - 30 - 2 - 2 - 1 - 1
        System.assertEquals(-3, newOli6.Refinery_Netback_CAD__c);
        
        newOli6.Axle_8_Price__c         = null;
        newOli6.Axle_5_Price__c         = null;
        newOli6.Axle_6_Price__c         = null;
        newOli6.Axle_7_Price__c         = 33;
        update newOli6;
        
        OpportunityLineItem newOli7 = [SELECT Id,
                                              Terminal_Netback_CAD__c,
                                              Refinery_Netback_CAD__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: oli.Id];
        // Should be 33 - 40 - 2 - 2 - 1                               
        System.assertEquals(-12, newOli7.Terminal_Netback_CAD__c);
        // Should be 33 - 40 - 2 - 2 - 1 - 1
        System.assertEquals(-13, newOli7.Refinery_Netback_CAD__c);

        // Finish test
        test.stopTest();
    }
    
    static testMethod void testAsphaltRefineryAndTerminalNetback_Origin()
    {
        Opportunity opp = createOpportunityTestData();
        PriceBookEntry pricebook = createAsphaltPriceBook();
        
        // Create Opportunity Line Item
        OpportunityLineItem oli = new OpportunityLineItem(Quantity                = 1,
                                                          Unit__c                 = 'Tonne',
                                                          Currency__c             = 'CAD',
                                                          TotalPrice              = 10,
                                                          OpportunityId           = opp.Id,
                                                          PricebookEntryId        = pricebook.Id,
                                                          Axle_7_Price__c         = 10,
                                                          Axle_8_Price__c         = 55,
                                                          Axle_6_Price__c         = 44,
                                                          Axle_5_Price__c         = 33,
                                                          Additives_CAD__c        = 1,
                                                          Terminal_Freight_CAD__c = 1,
                                                          Handling_Fees_CAD__c    = 2,
                                                          AC_Premium_CAD__c       = 2);
        Database.saveResult opportunityLineItemResult = Database.insert(oli);
         
        // Now add the freight record.
        ATS_Freight__c freight = new ATS_Freight__c(Husky_Supplier_1_Selected__c = true,
                                                    Husky_Supplier_1__c          = 'Burnco--9796--9796',
                                                    Husky_Supplier_2__c          = 'Burnco--9796--9796',
                                                    Prices_F_O_B__c              = 'Origin',
                                                    ATS_Freight__c               = opp.Id,
                                                    Local_Freight_Via__c         = 'Rail',
                                                    Supplier_1_Unit__c           = 'Tonne',
                                                    Supplier_2_Unit__c           = 'Liter');
        Database.saveResult freightResult = Database.insert(freight);
        
        // Start test
        test.startTest();
        
        // Now we'll change the Freight to Destination. This will fire the 'SetOliNetbackFromFreight' Trigger.
        // Since the Axle Prices are null on the freight, nothing will change.
        ATS_Freight__c freight2 = new ATS_Freight__c(Id              = freightResult.getId(),
                                                     Prices_F_O_B__c = 'Destination');
        update freight2;
        
        // Make sure that worked.
        OpportunityLineItem newOli5 = [SELECT Id,
                                              Terminal_Netback_CAD__c,
                                              Refinery_Netback_CAD__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
        // Should be 55 - 2 - 2 - 1
        System.assertEquals(50.00, newOli5.Terminal_Netback_CAD__c);
        // Should be 55 - 2 - 2 - 1 - 1
        System.assertEquals(49.00, newOli5.Refinery_Netback_CAD__c);
        
        // Now set the freight axle values to something. The trigger will fire and update the oli.
        ATS_Freight__c freight3 = new ATS_Freight__c(Id                          = freightResult.getId(),
                                                     Emulsion_Rate8_Supplier1__c = 11,
                                                     Emulsion_Rate8_Supplier2__c = 9);
        update freight3;
        
        // Finish test
        test.stopTest();
        
    }
    
    
    static testMethod void testAsphaltRefineryAndTerminalNetback_Destination2()
    {
        Opportunity opp = createOpportunityTestData();
        PriceBookEntry pricebook = createAsphaltPriceBook();
        
        // Create Opportunity Line Item
        OpportunityLineItem oli = new OpportunityLineItem(Quantity                = 1,
                                                          Unit__c                 = 'Tonne',
                                                          Currency__c             = 'CAD',
                                                          TotalPrice              = 10,
                                                          OpportunityId           = opp.Id,
                                                          PricebookEntryId        = pricebook.Id,
                                                          Axle_7_Price__c         = 10,
                                                          Axle_8_Price__c         = 55,
                                                          Axle_6_Price__c         = 44,
                                                          Axle_5_Price__c         = 33,
                                                          Additives_CAD__c        = 1,
                                                          Terminal_Freight_CAD__c = 1,
                                                          Handling_Fees_CAD__c    = 2,
                                                          AC_Premium_CAD__c       = 2);
        Database.saveResult opportunityLineItemResult = Database.insert(oli);
        
        // Now add the freight record.
        ATS_Freight__c freight = new ATS_Freight__c(Husky_Supplier_1_Selected__c = true,
                                                    Husky_Supplier_1__c          = 'Burnco--9796--9796',
                                                    Husky_Supplier_2__c          = 'Burnco--9796--9796',
                                                    Prices_F_O_B__c              = 'Destination',
                                                    ATS_Freight__c               = opp.Id,
                                                    Local_Freight_Via__c         = 'Rail',
                                                    Supplier_1_Unit__c           = 'Tonne',
                                                    Supplier_2_Unit__c           = 'Liter');
        Database.saveResult freightResult = Database.insert(freight);
        
        // Start test
        test.startTest();
        
        // Make sure that worked.
        OpportunityLineItem newOli2 = [SELECT Id,
                                              Terminal_Netback_CAD__c,
                                              Refinery_Netback_CAD__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
        // Should be 55 - 11 - 2 - 2 - 1
        System.assertEquals(50, newOli2.Terminal_Netback_CAD__c);
        // Should be 55 - 11 - 2 - 2 - 1 - 1
        System.assertEquals(49, newOli2.Refinery_Netback_CAD__c);
        
        // Now switch the Freight supplier to #2. This will use a value of 9 in Liters.
        // Conversion from tonnes to liters (density = 2) is 1000/2
        ATS_Freight__c freight2 = new ATS_Freight__c(Id                           = freightResult.getId(),
                                                     Husky_Supplier_1_Selected__c = false,
                                                     Husky_Supplier_2_Selected__c = true,
                                                     Emulsion_Rate5_Supplier2__c = 10,
                                                     Emulsion_Rate6_Supplier2__c = 20,
                                                     Emulsion_Rate7_Supplier2__c=30,
                                                     Emulsion_Rate8_Supplier2__c=40);
        update freight2;
        
        // Make sure that worked.
        OpportunityLineItem newOli3 = [SELECT Id,
                                              Terminal_Netback_CAD__c,
                                              Refinery_Netback_CAD__c,
                                              Currency__c,
                                              Exchange_Rate_to_CAD__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
        // Should be 55 - (40 * 500) - 2 - 2 - 1
        System.assertEquals(-19950, newOli3.Terminal_Netback_CAD__c);
        // Should be 55 - (40 * 500) - 2 - 2 - 1 - 1
        System.assertEquals(-19951, newOli3.Refinery_Netback_CAD__c);
        
        
        // Now change the currency and exchange rate on the oli
        newOli3.Axle_8_Price__c = null;
        newOli3.Currency__c = 'US';
        newOli3.Exchange_Rate_to_CAD__c = 2;
        update newOli3;
        
        // The trigger should have fired... Make sure that worked.
        OpportunityLineItem newOli4 = [SELECT Id,
                                              Terminal_Netback_CAD__c,
                                              Refinery_Netback_CAD__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
        // Should be ((33 - (10 * 500)) * 2) - 2 - 2 - 1
        System.assertEquals(-9939, newOli4.Terminal_Netback_CAD__c);
        // Should be ((33 - (10 * 500)) * 2) - 2 - 2 - 1 - 1
        System.assertEquals(-9940, newOli4.Refinery_Netback_CAD__c);
        
        // Now change price so that Netback is calculated from Axle 6 price
        newOli4.Axle_5_Price__c = null;
        update newOli4;
        
        // The trigger should have fired... Make sure that worked.
        OpportunityLineItem newOli5 = [SELECT Id,
                                              Terminal_Netback_CAD__c,
                                              Refinery_Netback_CAD__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
        // Should be ((44 - (20 * 500)) * 2) - 2 - 2 - 1
        System.assertEquals(-19917, newOli5.Terminal_Netback_CAD__c);
        // Should be ((44 - (20 * 500)) * 2) - 2 - 2 - 1 - 1
        System.assertEquals(-19918, newOli5.Refinery_Netback_CAD__c);
        
        // Now change price so that Netback is calculated from Axle 7 price
        newOli5.Axle_6_Price__c = null;
        
        update newOli5;
        
        // The trigger should have fired... Make sure that worked.
        OpportunityLineItem newOli6 = [SELECT Id,
                                              Terminal_Netback_CAD__c,
                                              Refinery_Netback_CAD__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
        // Should be ((10 - (30 * 500)) * 2) - 2 - 2 - 1
        System.assertEquals(-29985, newOli6.Terminal_Netback_CAD__c);
        // Should be ((10 - (30 * 500)) * 2) - 2 - 2 - 1 - 1
        System.assertEquals(-29986, newOli6.Refinery_Netback_CAD__c);
        
        delete freight2;
        
        // Finish test
        test.stopTest();
    }
    static testMethod void testSetCompetitorNetback()
    {
        Opportunity opp = createOpportunityTestData();
        PriceBookEntry pricebook = createAsphaltPriceBook();
        
        // Start test
        test.startTest();
         // Now add the freight record.
        ATS_Freight__c freight = new ATS_Freight__c(Husky_Supplier_1_Selected__c = true,
                                                    Husky_Supplier_1__c          = 'Burnco--9796--9796',
                                                    Husky_Supplier_2__c          = 'Burnco--9796--9796',
                                                    Prices_F_O_B__c              = 'Origin',
                                                    ATS_Freight__c               = opp.Id,
                                                    Local_Freight_Via__c         = 'Rail',
                                                    Supplier_1_Unit__c           = 'Tonne',
                                                    Supplier_2_Unit__c           = 'Liter',
                                                    Emulsion_Rate8_Competitor1__c = 10,
                                                    Emulsion_Rate8_Competitor2__c = 20,
                                                    Emulsion_Axle8_Rate_Competitor3__c = 30,
                                                    Emulsion_Axle8_Rate_Competitor4__c = 40);
        Database.saveResult freightResult = Database.insert(freight);
        
        // Create Opportunity Line Item
        OpportunityLineItem oli = new OpportunityLineItem(Quantity         = 1,
                                                          Unit__c          = 'Tonne',
                                                          Currency__c      = 'CAD',
                                                          TotalPrice       = 10,
                                                          OpportunityId    = opp.Id,
                                                          PricebookEntryId = pricebook.Id);
        Database.saveResult opportunityLineItemResult = Database.insert(oli);
       
        oli.Competitor_1__c = 'Cenex-Mandan';
        oli.Competitor_2__c = 'Colasphalt-Acheson (COAC)';
        oli.Competitor_3__c = 'Chevron-Vancouver';
        oli.Competitor_4__c = 'Esso-Winnipeg (Exchange) (EX: IOEE)';
        oli.Competitor_1_Bid_Price__c = 100;
        oli.Competitor_2_Bid_Price__c = 200;
        oli.Competitor_3_Bid_Price__c = 300;
        oli.Competitor_4_Bid_Price__c = 400;
        update oli;
        
        OpportunityLineItem newOli5 = [SELECT Id,
                                              Competitor_1_Refinery_Netback_CAD__c,
                                              Competitor_2_Refinery_Netback_CAD__c,
                                              Competitor_3_Refinery_Netback_CAD__c,
                                              Competitor_4_Refinery_Netback_CAD__c,
                                              Competitor_1_Terminal_Netback_CAD__c,
                                              Competitor_2_Terminal_Netback_CAD__c,
                                              Competitor_3_Terminal_Netback_CAD__c,
                                              Competitor_4_Terminal_Netback_CAD__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: opportunityLineItemResult.getId()];
        
        
        //100 - ( 10 * 1)                               
        System.assertEquals(90, newOli5.Competitor_1_Refinery_Netback_CAD__c);
        //200 - (20 * 1)
        System.assertEquals(180, newOli5.Competitor_2_Refinery_Netback_CAD__c);
        //300 - 30
        System.assertEquals(270, newOli5.Competitor_3_Refinery_Netback_CAD__c);
        //400-40
        System.assertEquals(360, newOli5.Competitor_4_Refinery_Netback_CAD__c);      
        
        //100 - ( 10 * 1)                               
        System.assertEquals(90, newOli5.Competitor_1_Terminal_Netback_CAD__c);
        //200 - (20 * 1)
        System.assertEquals(180, newOli5.Competitor_2_Terminal_Netback_CAD__c);
        //300 - 30
        System.assertEquals(270, newOli5.Competitor_3_Terminal_Netback_CAD__c);
        //400-40
        System.assertEquals(360, newOli5.Competitor_4_Terminal_Netback_CAD__c);                          
        // Finish test
        test.stopTest();
    }
}