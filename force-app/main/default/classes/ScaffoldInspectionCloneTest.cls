@isTest(SeeAllData=false)
private class ScaffoldInspectionCloneTest {

    static TestMethod void createCloneTest() {
        Scaffold__c scaffold = ScaffoldTestData.createScaffold();
		Scaffold_Inspection__c scaffoldInspection = ScaffoldInspectionTestData.createScaffoldInspection(scaffold);
        
        PageReference ref = Page.ScaffoldInspectionClone;
        ref.getParameters().put('sId', scaffold.Id);
        Test.setCurrentPage(ref);
        
        ScaffoldInspectionClone cont = new ScaffoldInspectionClone();
        cont.createClone();
        
        scaffoldInspection = ScaffoldInspectionTestData.approveScaffoldInspection(scaffoldInspection);
        
        scaffoldInspection = [SELECT Id, isCertified__c, isLocked__c FROM Scaffold_Inspection__c WHERE Id =: scaffoldInspection.Id];
        System.assertNotEquals(scaffoldInspection.Id, null);
        System.assertEquals(scaffoldInspection.isCertified__c, 'Approved');
        System.assertEquals(scaffoldInspection.isLocked__c, True);
        
        scaffold = [SELECT Id, Last_Passed_Inspection__c, Days_Remaining__c, Inspection_Deadline__c, Last_Inspection__c FROM Scaffold__c WHERE Id =: scaffold.Id];
        System.assertNotEquals(scaffold.Id, Null);
        System.assertEquals(scaffold.Last_Passed_Inspection__c, Date.Today());
        System.assertEquals(scaffold.Last_Inspection__c, Date.Today());
        System.assertEquals(scaffold.Inspection_Deadline__c, Date.Today() + 21);
        System.assertEquals(scaffold.Days_Remaining__c, 21);
        
        ref = Page.ScaffoldInspectionClone;
        ref.getParameters().put('sId', scaffold.Id);
        Test.setCurrentPage(ref);
        
        cont = new ScaffoldInspectionClone();
        cont.createClone();
        
        scaffoldInspection = [SELECT Id, isCertified__c, isLocked__c FROM Scaffold_Inspection__c WHERE Scaffold__c =: scaffold.Id AND isLocked__c = False];
        System.assertNotEquals(scaffoldInspection.Id, null);
        System.assertEquals(scaffoldInspection.isCertified__c, 'Approved');
        System.assertEquals(scaffoldInspection.isLocked__c, False);
        
    }
}