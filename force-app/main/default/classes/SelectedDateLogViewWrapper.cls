public class SelectedDateLogViewWrapper {
    public Date selectedDate{get; set;}
    private Id teamAllocationRT;
    public List<Shift_Configuration__c> shiftsForSelectedDate{get; set;}

    public List<DOI__c> DOIs{get; set;}
    public List<Production_Loss_Event__c> productionEvents{get; set;}
    
    public List<Shift> shiftList{get; set;} 
    
    public Boolean hasDOIs {
        get {
            return (this.DOIs != null) && (this.DOIs.size() > 0);
        }
    }
    
    public Boolean hasProductionEvents {
        get {
            return (this.ProductionEvents != null) && (this.ProductionEvents.size() > 0);
        }
    }
    
    public SelectedDateLogViewWrapper(Date selectedDate, Id selectedRT) 
    {
        this.selectedDate = selectedDate;
        this.teamAllocationRT = selectedRT;
        // get Shift to Team Allocations Map    
        Map<Id, Simple_DTO__c> ShifttoTeamAllocationMap = lookupTeamAllocations();
        
        List<ShiftConfigWrapper> shiftConfigWrapperList = new List<ShiftConfigWrapper>();
        
        for(Shift_Configuration__c shiftConfig : lookupShifts(ShifttoTeamAllocationMap.KeySet())) {
            shiftConfigWrapperList.add(new ShiftConfigWrapper(shiftConfig));
        }
        shiftConfigWrapperList.sort();
        
        System.debug('shiftConfigWrapperList: ' + shiftConfigWrapperList);
        
        shiftsForSelectedDate = new List<Shift_Configuration__c>();
        
        for(ShiftConfigWrapper SCWrapper : shiftConfigWrapperList) {
            shiftsForSelectedDate.add(SCWrapper.shiftConfig);
        }
        System.debug('shiftsForSelectedDate: ' + shiftsForSelectedDate);
        
        // get Shift to Shift Operators Map
        Map<Id, List<Shift_Operator__c>> shiftIdToShiftOperatorsList = lookupShiftOperators();
        
        Map<Id, List<Simple_DTO__c>> shiftToOperationalEventsMap = lookupOperationalEvents();
        
        shiftList = new List<Shift>();
        
        System.debug('shiftsForSelectedDate: ' + shiftsForSelectedDate);
        
        for(Shift_Configuration__c shiftConfig : shiftsForSelectedDate) {
            if(ShifttoTeamAllocationMap.get(shiftConfig.Id) != Null) {
                Shift shift = new Shift(shiftConfig, ShifttoTeamAllocationMap.get(shiftConfig.Id)); 
                shift.shiftOperators = shiftIdToShiftOperatorsList.get(shiftConfig.Id);
                shift.operationalEvents = shiftToOperationalEventsMap.get(shiftConfig.Id);
                
                shiftList.add(shift);
            }
        }
        
        DOIs = lookupDOIS();
        productionEvents = lookupProductionEvents();
    }
    //YEN Sep 15, 2014: fix The Log View should display the selected date's Team Allocation record whether the shift is disabled or not
    public List<Shift_Configuration__c> lookupShifts(Set<Id> selectedDateShiftIds) {
        List<Shift_Configuration__c> shiftList = new List<Shift_Configuration__c>();
        
        for (Shift_Configuration__c sc : [SELECT 
                                            Id, 
                                            Name,
                                            Start_Time__c,
                                            End_Time__c, 
                                            Operating_District__c,
                                            Enabled__c,
                                            Sunday__c,
                                            Monday__c,
                                            Tuesday__c,
                                            Wednesday__c,
                                            Thursday__c,
                                            Friday__c,
                                            Saturday__c
                                        FROM Shift_Configuration__c
                                        WHERE Id in :selectedDateShiftIds]) {
            shiftList.add(sc);
        }
        
        return shiftList;
    }

   
    
    // Create Shift to Team Allocation map for the selected Date
    private Map<Id, Simple_DTO__c> lookupTeamAllocations() {
        
        Map<Id, Team_Allocation__c> ShifttoTeamAllocation = new Map<Id, Team_Allocation__c>();
        Map<Id, Simple_DTO__c> ShifttoTeamAllocationMap = new Map<Id, Simple_DTO__c>();
        
        for(Team_Allocation__c teamAllocation : [   SELECT 
                                                        Id,
                                                        Team_Allocation_Name__c, 
                                                        Shift__c,
                                                        Shift__r.Name,
                                                        Operating_District__c,
                                                        Operating_District__r.Name,
                                                        Date__c,
                                                        Status__c,
                                                        Team__r.Name,
                                                        Description__c,
                                                        Approver__r.FirstName, 
                                                        Approver__r.LastName,
                                                        Approve_Date__c
                                                    FROM 
                                                        Team_Allocation__c 
                                                    WHERE 
                                                        Date__c = :this.selectedDate
                                                        and RecordTypeId =: teamAllocationRT ]) {
            ShifttoTeamAllocation.put(teamAllocation.Shift__c, teamAllocation);                                                             
        }
        
        for(Id shiftId : ShifttoTeamAllocation.KeySet()){
            Simple_DTO__c result = new Simple_DTO__c();
            
            Team_Allocation__c selectedTA = ShifttoTeamAllocation.get(shiftId);
            
            Datetime taDatetime = Datetime.newInstance(selectedTa.Date__c.year(), selectedTa.Date__c.month(), selectedTa.Date__c.day());            
            
            String approveDate = selectedTa.Approve_Date__c == null ? '' : selectedTa.Approve_Date__c.format('dd-MM-yyyy hh:mm a');           
            String approver = selectedTa.Approver__r.FirstName == null ? '' : selectedTa.Approver__r.FirstName + ' ' + selectedTa.Approver__r.LastName;
                        
            // 1. Team Allocation
            result.Text_01__c = selectedTa.Team_Allocation_Name__c;
            result.Text_02__c = taDatetime.format('dd/MM/yyyy');
            result.Text_03__c = selectedTa.Shift__r.Name;
            result.Text_04__c = selectedTa.Status__c;
          
            result.Text_06__c = selectedTa.Team__r.Name;
         
            result.Text_08__c = selectedTa.Operating_District__r.Name;
   
            result.Text_10__c = approver;
            result.Text_11__c = approveDate;
            result.Text_Area_01__c = selectedTa.Description__c;
            
            ShifttoTeamAllocationMap.put(shiftId, result);
        }
        
        return ShifttoTeamAllocationMap;
    }
    
    private Map<Id, List<Shift_Operator__c>> lookupShiftOperators() {
        Map<Id, List<Shift_Operator__c>> shiftIdToShiftOperatorsList = new Map<Id, List<Shift_Operator__c>>();
        
        for(Shift_Operator__c shiftOperator : [ SELECT
                                                    Name,
                                                    Process_Area__r.Name,
                                                    Description__c,
                                                    Operator_Contact__r.Name,
                                                    Plant__r.Name,
                                                    Team_Allocation__r.Shift__c,
                                                    Role__r.Name
                                                FROM 
                                                    Shift_Operator__c
                                                WHERE 
                                                    Team_Allocation__r.Shift__c in :(new Map<Id, SObject>(shiftsForSelectedDate)).KeySet()
                                                    AND Team_Allocation__r.Date__c = :this.selectedDate
                                                ORDER BY 
                                                    Leader_Ind__c desc, Role__r.Name, Operator_Contact__r.Name]) {
            List<Shift_Operator__c> shiftOperatorList = shiftIdToShiftOperatorsList.get(shiftOperator.Team_Allocation__r.Shift__c);
            
            if(shiftOperatorList == Null) {
                shiftOperatorList = new List<Shift_Operator__c>();
            }
            
            shiftOperatorList.add(shiftOperator);
            
            shiftIdToShiftOperatorsList.put(shiftOperator.Team_Allocation__r.Shift__c, shiftOperatorList);                                      
        }
        
        return shiftIdToShiftOperatorsList;
    }
    
    private Map<Id, List<Simple_DTO__c>> lookupOperationalEvents() {
        Map<Id, List<Operational_Event__c>> shiftToOperationalEvents = new Map<Id, List<Operational_Event__c>>();
        Map<Id, List<Simple_DTO__c>> shiftToOperationalEventsMap = new Map<Id, List<Simple_DTO__c>>();
        
        for(Operational_Event__c opEvent : [SELECT
                                                Time__c,
                                                Unit__r.Name,
                                                Unit__r.Parent_Unit__r.Name,
                                                Area__r.Name,
                                                Operator_Contact__r.Name,
                                                Priority__c,
                                                Equipment__r.Name,
                                                Equipment__r.Tag_Number__c,
                                                Equipment__r.Description_of_Equipment__c,
                                                Description__c,
                                                Team_Allocation__r.Shift__c, Work_Order_Number__c
                                            FROM 
                                                Operational_Event__c
                                            WHERE 
                                                Team_Allocation__r.Shift__c in :(new Map<Id, SObject>(shiftsForSelectedDate)).KeySet()
                                                AND Team_Allocation__r.Date__c = :this.selectedDate            
                                            ORDER BY 
                                                Unit__r.Name, Time__c DESC, Operator_Contact__r.Name]) {
            
            List<Operational_Event__c> operationalEvents = shiftToOperationalEvents.get(opEvent.Team_Allocation__r.Shift__c);
            
            if(operationalEvents == null) {
                operationalEvents = new List<Operational_Event__c>();
            }
            operationalEvents.add(opEvent);
            shiftToOperationalEvents.put(opEvent.Team_Allocation__r.Shift__c, operationalEvents);
        }
        
        for(Id shiftId : shiftToOperationalEvents.KeySet()){
            // 1. Load and group all the events by area and then by unit
            // Map<AreaName, Map<UnitName, List<OperationalEvent>>>
            Map<String, Map<String, List<Operational_Event__c>>> eventsByAreaAndUnitsMap = new Map<String, Map<String, List<Operational_Event__c>>>();
            List<Simple_DTO__c> result = new List<Simple_DTO__c>();
            
            for(Operational_Event__c oEvent : shiftToOperationalEvents.get(shiftId)) {
                
                // Area group
                String curAreaName = oEvent.Area__r.Name;
                Map<String, List<Operational_Event__c>> curEventsByUnitsMap = eventsByAreaAndUnitsMap.get(curAreaName);
                if (curEventsByUnitsMap == null) {
                    curEventsByUnitsMap = new Map<String, List<Operational_Event__c>>();
                    eventsByAreaAndUnitsMap.put(curAreaName, curEventsByUnitsMap);
                }
                
                // Unit sub-group
                String curUnitName = oEvent.Unit__r.Name;
                String curUnitParentName = oEvent.Unit__r.Parent_Unit__r.Name;
                
                List<Operational_Event__c> curEventsList = curEventsByUnitsMap.get(curUnitName);
                if (curEventsList == null) {
                    curEventsList = new List<Operational_Event__c>();
                    curEventsByUnitsMap.put(curUnitName, curEventsList);
                }
                
                curEventsList.add(oEvent);
            }
            
            // 2. Extract and sort by area and then units
            List<SortableSunriseArea> sortedAreas = toSortedAreas(eventsByAreaAndUnitsMap.keySet());
            
            for (SortableSunriseArea curSortedArea : sortedAreas) {
                String curAreaName = curSortedArea.areaName;
                
                Map<String, List<Operational_Event__c>> curEventsByUnitMap = eventsByAreaAndUnitsMap.get(curAreaName);
                // Extract Unit for sorting.
                List<String> unitNames = new List<String>(curEventsByUnitMap.keySet());
                List<SortableSunriseUnit> sortableUnits = new List<SortableSunriseUnit>();
                Set<String> uniqueUnitsKeys = new Set<String>();
                for (String curUnitName : unitNames) {
                    List<Operational_Event__c> curEvents = curEventsByUnitMap.get(curUnitName);
                    String curUnitParentName;
                    for (Operational_Event__c oEvent : curEvents) {
                        curUnitParentName = oEvent.Unit__r.Parent_Unit__r.Name;
                        
                        String curUnitKey = curUnitName + ':' + curUnitParentName;
                        if (!uniqueUnitsKeys.contains(curUnitKey)) {
                            sortableUnits.add(new SortableSunriseUnit(curUnitName, curUnitParentName));
                            uniqueUnitsKeys.add(curUnitKey);
                        }
                        
                    }
                }
                
                // sort the units.
                sortableUnits.sort(); 
                 
                // 3. Load final list.
                for (SortableSunriseUnit curUnit : sortableUnits) {
                    List<Operational_Event__c> curEvents = curEventsByUnitMap.get(curUnit.unitName);
                    for (Operational_Event__c oEvent : curEvents) {
                        Simple_DTO__c curEventDto = new Simple_DTO__c();
                        if (oEvent.Unit__c != null)
                           curEventDto.Text_01__c = oEvent.Unit__r.Name.length() > 15 ? oEvent.Unit__r.Name.subString(0, 15) + '..' : oEvent.Unit__r.Name;
                        curEventDto.Text_02__c = oEvent.Time__c;
                        curEventDto.Text_03__c = oEvent.Operator_Contact__r.Name;
                        //curEventDto.Text_04__c = oEvent.Equipment__r.Name;
                        if (oEvent.Equipment__r != null)
                        {
                            String equipment = oEvent.Equipment__r.Tag_Number__c + ' ' + oEvent.Equipment__r.Description_of_Equipment__c;
                            curEventDto.Text_04__c = equipment.length() > 20 ? equipment.subString(0, 20) + '..' : equipment;
                        }
                        curEventDto.Text_05__c = oEvent.Priority__c;
                        curEventDto.Text_06__c = oEvent.Area__r.Name;
                        curEventDto.Text_Area_01__c = oEvent.Description__c;
                        curEventDto.Text_07__c = oEvent.Work_Order_Number__c;
                
                        result.add(curEventDto);    
                    }
                }
                
                shiftToOperationalEventsMap.put(shiftId, result);
            }
        }
        
        return shiftToOperationalEventsMap;
    }
    
    private List<SortableSunriseArea> toSortedAreas(Set<String> areas) {
        List<SortableSunriseArea> sortedAreas = new List<SortableSunriseArea>();
        
        for (String curArea : areas) {
            sortedAreas.add(new SortableSunriseArea(curArea));
        }
        
        sortedAreas.sort();
        
        return sortedAreas;
    }
    
    public List<DOI__c> lookupDOIS() {
        Map<Id, Schema.Recordtypeinfo> recTypesById = Team_Allocation__c.SObjectType.getDescribe().getRecordTypeInfosById();
        String teamAllocationRTName = recTypesById.get(teamAllocationRT).getName();
        Map<String, Schema.Recordtypeinfo> recTypesByName = DOI__c.SObjectType.getDescribe().getRecordTypeInfosByName();
        Id doiRTId = recTypesByName.get(teamAllocationRTName).getRecordTypeId();
        List<DOI__c> DOIs = [SELECT 
                                Id,
                                Name,
                                Date__c,
                                DOI_DetailPage_HLink__c,
                                Equipment__c,
                                Equipment__r.Tag_Number__c,
                                Equipment__r.Description_of_Equipment__c,
                                Plant__c,
                                Plant__r.Name,
                                Unit__c,
                                Unit__r.Name,
                                Instructions__c,
                                LastModifiedDate,
                                LastModifiedById,
                                LastModifiedBy.Name,
                                Completed__c,
                                Completion_Date__c,
                                Area__r.Name
                            FROM DOI__c 
                            WHERE Date__c <= :selectedDate AND (Completion_Date__c >= :selectedDate OR  Completion_Date__c = null)
                            /*WHERE (Date__c =: this.selectedDate or (Completed__c = false and Date__c <: selectedDate)) */
                                 and RecordTypeId =: doiRTId
                            ORDER BY Date__c,Plant__r.Name, LastModifiedDate DESC];
                            
        return DOIs;
    }
    
    public List<Production_Loss_Event__c> lookupProductionEvents() {
        DateTime selectedDateStartTime = DateTime.newInstance(selectedDate.year(), selectedDate.Month(), selectedDate.Day(), 0, 0, 0) ;
        DateTime selectedDateEndTime = DateTime.newInstance(selectedDate.year(), selectedDate.Month(), selectedDate.Day(), 23, 59, 59);
        
        List<Production_Loss_Event__c> productionEvents = [SELECT
                                                                Id,
                                                                Name,
                                                                Description__c,
                                                                Start_Date_Time__c,
                                                                End_Date_Time__c,
                                                                Production_Loss_Code__c,
                                                                Plant__r.Name,
                                                                Unit__r.Name,
                                                                Equipment__r.Name
                                                            FROM
                                                                Production_Loss_Event__c
                                                            WHERE
                                                                (Start_Date_Time__c <= :selectedDateEndTime AND (End_Date_Time__c >= :selectedDateStartTime OR End_Date_Time__c = Null))
                                                            ORDER BY Start_Date_Time__c
                                                            ];
        return productionEvents;
    }
}