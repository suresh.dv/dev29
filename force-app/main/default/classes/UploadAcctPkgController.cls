public class UploadAcctPkgController
{
	private Retail_Cluster__c msoRecord;
	private ApexPages.Standardcontroller controller;
	
	// controller gets the record into a private msoRecord variable.
	public UploadAcctPkgController(ApexPages.standardController std)
	{
		this.controller = std;
		msoRecord = (Retail_Cluster__c)std.getRecord();
	}
	
	// use a standard attachment object
	public Attachment attachment
	{
		get
		{
			if (attachment == null)
				attachment = new Attachment();
			return attachment;
		}
		set;
	}
	
	// set a new record pointing to the current MSO.  The attachment will then become a "Note and Attachment" record of this
	// new LOM_Accounting_Data_Upload record.  After upload, navigate back to the MSO detail page (which will then show a new
	// record in the "Accounting Package" related list for this MSO).
	public PageReference upload()
	{
		if ((attachment.name != null) && (attachment.name.right(4).toLowerCase() != '.csv'))
		{
			// 2013-11-27: setting the body to null is important because this way the blob is not included in
			// the view state.  Refer to
			// http://developer.force.com/cookbook/recipe/uploading-a-document-using-visualforce-and-a-custom-controller
			attachment.body = null;
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'The name of the file you are trying to upload must end with \'.csv\'.  Please select another file and try again.'));
			return null;
		}
		else
		{
			// first create a new accounting data upload object
			LOM_Accounting_Data_Upload__c adu = new LOM_Accounting_Data_Upload__c(
				Multi_Site_Operator__c = msoRecord.Id);
			insert adu;
			attachment.OwnerId = UserInfo.getUserId();
			attachment.parentId = adu.Id;
			
			try
			{
				insert attachment;
			} 
			catch (DMLException e)
			{
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error uploading attachment'));
				// if exception occurs, that means the attachment cannot be inserted.  Don't need to "delete" it then
				// as it doesn't exist.  However, do delete adu otherwise there will be an empty row in the related
				// list
				delete adu;
				return null;
			}
			finally
			{
				// 2013-10-16: setting the body to null is important because this way the blob is not included in
				// the view state.  Refer to
				// http://developer.force.com/cookbook/recipe/uploading-a-document-using-visualforce-and-a-custom-controller
				attachment.body = null;
				attachment = new Attachment();	
			}
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Attachment uploaded successfully'));	
			adu.Success__c = true;
			update adu;
			// to return the original page there seems to be two methods.  Get the cancel() PageReference or
			// simply navigate to the view page of the multi site operator.	
			//PageReference cancel = controller.cancel();
			//return cancel;  
			return new PageReference('/' + msoRecord.Id);
		}
	}
}