public class CaseCloseAndSaveController {
	@AuraEnabled
	public static void updateCase(string id) {
        Case currentCase = [SELECT id,status FROM Case WHERE Id=:id LIMIT 1];
        List<Case_data__mdt> caseMetaData = [SELECT Completed_Status__c FROM Case_data__mdt Limit 1];
    	currentCase.Status = caseMetaData[0].Completed_Status__c;
    	update currentCase;
    }
}