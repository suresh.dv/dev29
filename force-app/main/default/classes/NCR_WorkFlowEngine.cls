public class NCR_WorkFlowEngine{
    String recordId;
    ID userID;
  
    private final ApexPages.StandardController stdcontroller;
    private final sObject ncr_record;
    
    public Boolean ViewMode {get; private set;}
    public List<String> viewSelection {get; private set;}
    public Map<String,String> viewSelectionMap {get; private set;}
    
    
    public String ConfirmMessage {get; private set;}
    public String SaveButtonLabel {get; private set;}
    
    /* Button flags*/
    public Boolean btnEditEnabled {get; private set;}
    public Boolean btnChangeStateClicked   {get; private set;}   
    
    
    
    public String NCRObject {get;private set;} // object this workflow applied to
    public String NCRRecordType {get;private set;} //Record Type this workflow applied to    
    
    
    public String CurrentStateFieldName {get;set;}
    public String DefaultDetailViewSection {get;set;} 
    
    private String ActionDateFieldName {get; set;}
    
    
    public String CurrentState {
        private set; 
        get
        {
          return (String)ncr_record.get(CurrentStateFieldName);
        }
    
    
    }
    

    public String FieldSetName {
            get; 
            private set
            {
            
                FieldSetName = value;
                
                try
                {
                
                    EditSectionName = fieldSetLabelsMap.get(value);
                    /*
                    if(fieldSetLabelsMap.containsKey(value))
                    {
                        EditSectionName = fieldSetLabelsMap.get(value);
                    } 
                    else
                    {
                        FieldSetName = this.DefaultDetailViewSection;
                        NCR_Utils.logError('Unknown fieldset: ' + value);
                    
                    } 
                    */ 
                }
                catch(Exception ex)
                {
                    NCR_Utils.logError(ex);
                }
            }
    }
            
    public String EditSectionName {get; private set;} 
    public String EditSectionDescription {get; private set;}     
    

    private Map<String, NCR_State__c> NCRStateMap {get; set;}
    public NCR_State__c CurrentNCRState {get; private set;}
    public integer ActionCount {get; private set;}

    private String NewRecordState {get; set;}
    private String NewRecordOwner {get; set;}    
    
    
    private Boolean redirectToList {get; set;}
    
    
    public String ViewScreenDescription {get; private set;}
    

    public Boolean UserHasReadAccess {get; private set;}
    public Boolean UserHasEditAccess {get; private set;}    
    public Boolean UserHasDeleteAccess   {get; private set;}  
    public Boolean UserHasAllAccess   {get; private set;}    
    
    public boolean portalUser {get; private set;}  
   
     
    //public Map<String, Schema.FieldSet> fieldSetMap {get; private set;}
    public Map<String, string> fieldSetLabelsMap {get; private set;}
    
    public NCR_WorkFlowEngine(ApexPages.StandardController stdController) {
        recordId = stdController.getId();
        userID =   UserInfo.getUserid();
        portalUser = isPortalUser();
        
        //defaults
        this.SaveButtonLabel = 'Save';
        this.ViewScreenDescription  = '';
        
        fieldSetLabelsMap = new Map<string, String>();
        

       

        this.stdcontroller = stdController;
        this.ncr_record = (sObject)stdController.getRecord();  
        
        String type = this.ncr_record.getSObjectType().getDescribe().getName();


        Map<String, Schema.FieldSet> fieldSetMap ;

        if(type =='NCR_Case__c')
        {
            this.NCRObject = 'Case';
            String RecordTypeID = [select RecordTypeid from NCR_Case__c where id = :recordId LIMIT 1][0].RecordTypeID ;
            String RecordTypeName = [select Name from recordtype  where id = :RecordTypeID LIMIT 1][0].Name ;            
            this.NCRRecordType = RecordTypeName; 

            this.CurrentStateFieldName  =  'NCR_Case_State__c';
            this.DefaultDetailViewSection = 'NCR_Case_Detail';    
            
            fieldSetLabelsMap.put(this.DefaultDetailViewSection , 'Details'); //TODO

            fieldSetMap = SObjectType.NCR_Case__c.FieldSets.getMap();
            for (String fieldSetName : fieldSetMap.keySet()){
                fieldSetLabelsMap.put(fieldSetName, SObjectType.NCR_Case__c.FieldSets.getMap().get(fieldSetName).getLabel()) ;
            }            
                      
        }  

        if(type =='NCR_CAR__c')
        {
            this.NCRObject = 'Corrective Action';
            /*
            String RecordTypeID = [select RecordTypeid from NCR_CAR__c where id = :recordId LIMIT 1][0].RecordTypeID;
            String RecordTypeName = [select Name from recordtype  where id = :RecordTypeID LIMIT 1][0].Name ;            
            this.NCRRecordType = RecordTypeName; 
            */
            this.NCRRecordType = null;
            
            this.CurrentStateFieldName  =  'State__c';
            this.DefaultDetailViewSection = 'CAR_Details'; //TODO make sure we have this fieldset  
            
            fieldSetLabelsMap.put(this.DefaultDetailViewSection , 'Details'); //TODO

            fieldSetMap = SObjectType.NCR_CAR__c.FieldSets.getMap();
            for (String fieldSetName : fieldSetMap.keySet()){
                fieldSetLabelsMap.put(fieldSetName, SObjectType.NCR_CAR__c.FieldSets.getMap().get(fieldSetName).getLabel()) ;
            }              
                    
        }  
        ViewMode  = true;
        
        NewRecordState = null;
        NewRecordOwner = null;
        
        LoadStateMap();
        LoadCurrentStateActions();

        SetButtonFlags();       
        
        CheckRecordAccess();
        
        
        if(CurrentNCRState <> null)
        {
            if(this.UserHasEditAccess && CurrentNCRState.ViewScreen_Description__c <> null) 
            {
                this.ViewScreenDescription = CurrentNCRState.ViewScreen_Description__c;
            }
        }    
    }
    
    
    

    private void LoadStateMap()
    {
    
        Map<String, NCR_State__c> m = new Map<String, NCR_State__c>([
        select id, name, ActionCount__c, ViewScreen_Description__c, Screen_Sections__c, 
            (select id, name, Edit_Screen_Required__c, Edit_Section_Title__c,Edit_Section_Description__c,Confirmation_Message__c, Save_Button_Label__c, redirect__c, Change_Owner__c,
                FieldSet__c, NCR_New_State__r.Name, NCR_Action__r.Name,
                Date_FieldName__c 
                from NCR_StateActions__r order by Order__c ) 
        from NCR_State__c
        where NCR_Object__c = :this.NCRObject
        AND Record_Type__c = :this.NCRRecordType
        ]);
        NCRStateMap = new Map<String, NCR_State__c>();
        for(String strid: m.keySet())
        {
            NCR_State__c state = (NCR_State__c)m.get(strid);
            NCRStateMap.put((String)state.Name, (NCR_State__c)state);
        }    
    }

       

    
    private void CheckRecordAccess()
    {
       UserRecordAccess  recAccess = [SELECT 
                RecordId
                , HasReadAccess
                , HasEditAccess
                , HasDeleteAccess
                , MaxAccessLevel   
            FROM 
                UserRecordAccess 
            WHERE 
                UserId=:UserInfo.getUserId() 
                AND 
                RecordId =:recordId  LIMIT 1];    
                
       this.userHasReadAccess = recAccess.HasReadAccess;         
       this.userHasEditAccess = recAccess.HasEditAccess;       
       this.userHasDeleteAccess = recAccess.HasDeleteAccess;          
       
       //this.UserHasAllAccess = (recAccess.MaxAccessLevel == 'All');
       this.UserHasAllAccess = NCR_Utils.IsNCRAdminUser();
    //MaxAccessLevel  
       
    }
    
    private void LoadCurrentStateActions()
    {
    
        this.ActionCount = 0;
        this.CurrentNCRState = NCRStateMap.get(this.CurrentState);
        if(currentNCRState == null)
        {
        //throw error
            NCR_Utils.logError('Unknown State: '  + this.CurrentState);
            return;
        }
        this.ActionCount  = (Integer)currentNCRState.ActionCount__c;
        system.debug('Current State: ' + this.CurrentState);
    }
    
    
    
    public PageReference NCR_GenericAction(Integer actionNumber)
    {
        NewRecordOwner = null;
        NewRecordState = null;    

    
        NCR_StateAction__c stateAction = this.CurrentNCRState.NCR_StateActions__r[actionNumber];
        System.debug(stateAction.NCR_New_State__r.Name);
        if(stateAction.NCR_New_State__r.Name != null)
        {
          NewRecordState  = stateAction.NCR_New_State__r.Name;
        }
        
        if(stateAction.Change_Owner__c != null)
        {
          NewRecordOwner = stateAction.Change_Owner__c;
        }        
        

        //assign fieldset if needed
        if(stateAction.FieldSet__c != null && stateAction.FieldSet__c.trim()<>'') 
        {
        
            String fieldSetName = stateAction.FieldSet__c.trim();
            Map<String, Schema.FieldSet> fieldSetMap ;
            if(this.NCRObject == 'Case')
            {
                fieldSetMap = SObjectType.NCR_Case__c.FieldSets.getMap();
            }
            else if( this.NCRObject == 'Corrective Action')
            {   
                fieldSetMap = SObjectType.NCR_CAR__c.FieldSets.getMap();       
            }
                 
            if(fieldSetMap.containsKey(fieldSetName )) 
            {
               this.FieldSetName = fieldSetName ;
            }
            else
            {
               NCR_Utils.logerror('Unknown fieldset: ' + fieldSetName);
               return null;
            }         
          
        }          

        
        
        
        
        //assign section name if needed     
        if(stateAction.Edit_Section_Title__c!= null)
        {
          this.EditSectionName  =  stateAction.Edit_Section_Title__c;         
        }
        //assign section name if needed    
        this.EditSectionDescription = null; 
        if(stateAction.Edit_Section_Description__c!= null)
        {
          this.EditSectionDescription  =  stateAction.Edit_Section_Description__c;         
        }   
        
        //change Save button label if needed     
        if(stateAction.Save_Button_Label__c!= null)
        {
          this.SaveButtonLabel  =  stateAction.Save_Button_Label__c;         
        }             
        else
        {
            this.SaveButtonLabel = 'Save';
        }
        
        redirectToList = (stateAction.Redirect__c == 'List Screen'); 
       
        if(stateAction.Date_FieldName__c!= null)
        {
          this.ActionDateFieldName =  stateAction.Date_FieldName__c;         
        }   
        else
        {
          this.ActionDateFieldName  = null;
        }
        
        if(stateAction.Edit_Screen_Required__c)
        {
             ViewMode = false;
             btnChangeStateClicked = true;
        }
        else
        {
            NCR_Save(); //save record and change state
            
            if(redirectToList)
            {
                return Exit();
            }            
        }
         

                
         return null;    
    }
    
    public PageReference exit(){
        //change the Any_ObjectName__c with your Custom or Standard Object name.
        //Schema.DescribeSObjectResult anySObjectSchema = NCR_Case__c.SObjectType.getDescribe();
        //Schema.DescribeSObjectResult anySObjectSchema = ncr_record.SObjectType.getDescribe();
        
        Schema.SObjectType objObjectType = ncr_record.getSObjectType();
        DescribeSObjectResult anySObjectSchema = objObjectType.getDescribe();
        
        String objectIdPrefix = anySObjectSchema.getKeyPrefix();
        PageReference pageReference = new PageReference('/'+objectIdPrefix+'/o');
        pageReference.setRedirect(true);
        return pageReference;
    }    
    
    
    public PageReference NCR_Action1()
    {
        return NCR_GenericAction(0);
    }    
    public PageReference NCR_Action2()
    {
        return NCR_GenericAction(1);
    }
    public PageReference NCR_Action3()
    {
        return NCR_GenericAction(2);
    }
    public PageReference NCR_Action4()
    {
        return NCR_GenericAction(3);
    }    

    public PageReference NCR_Edit()
    {
        ViewMode = false;
        return null;
    }
    public PageReference NCR_Save()
    {
        PageReference pRef = null;
        System.debug('Save' + SaveButtonLabel);
        System.debug('NewRecordState: ' + NewRecordState);        
        System.debug('NewRecordOwner: ' + NewRecordOwner);            
        ViewMode = true;
        if(NewRecordState!= null) 
        {
        
            ncr_record.put(this.CurrentStateFieldName , NewRecordState);
            System.debug('New State:' + NewRecordState);
            
            
            if(this.ActionDateFieldName != null)
            {
                ncr_record.put(this.ActionDateFieldName , Date.TODAY());
            }
            
        }
        
        if(NewRecordOwner != null)
        {
        
            UpdateRecordOwner(NewRecordOwner);
        }          
        
        try{
            //return page reference back to VF page to reload the page            
            pRef = this.stdcontroller.save();
            //upsert this.ncr_record;
            //this.stdcontroller.save();
        } catch(Exception exp){
            System.debug('Exception : ' + exp);
        }                      

        LoadCurrentStateActions();
        
        SetButtonFlags();
        
        /*
        if(NewRecordOwner != null) //if owner was updated lest refresh sharing
        {
            SetManualShareRead(this.ncr_record.id, (ID)this.ncr_record.get('OwnerID'));
        }
        */
        SetManualShareRead(this.ncr_record.id, (ID)this.ncr_record.get('OwnerID'));        
        
        NewRecordOwner = null;
        NewRecordState = null;

        if(redirectToList)
        {
           return Exit();
        }  
        return pRef;
    }
    public PageReference NCR_CancelEdit()
    {
        ViewMode = true;
        SetButtonFlags();
        this.stdcontroller.cancel();
        return null;
    }

    private void UpdateRecordOwner(String recordOwnerType)
    {
        ID OwnerID;

        OwnerID = null;
    
        if(recordOwnerType =='Current User')
        {
            OwnerID  =  UserInfo.getUserId();
            //ncr_record.put('OwnerID', UserInfo.getUserId());
            //return;
        }
        else if(recordOwnerType =='Originator' && ncr_record.get('Originator__c') != null)
        {
            OwnerID  =  (ID)ncr_record.get('Originator__c');
            //ncr_record.put('OwnerID', ncr_record.get('Originator__c'));
            //return;
        }  
        else if(recordOwnerType =='QA Lead' && ncr_record.get('QA_Lead__c')!= null)
        {
            OwnerID  =  (ID)ncr_record.get('QA_Lead__c');
            //ncr_record.put('OwnerID',ncr_record.get('QA_Lead__c'));
            //return;
        }  
        else if(recordOwnerType =='Assignee' && ncr_record.get('Assignee__c') != null)
        {
            OwnerID  =   (ID)ncr_record.get('Assignee__c');
            //ncr_record.put('OwnerID', ncr_record.get('Assignee__c'));
            //return;
        }  
        
        if(OwnerID !=null )
        {
            ncr_record.put('OwnerID', OwnerID);
            //SetManualShareRead(ncr_record.id, OwnerID);
        }    
                          
    }


    private void SetButtonFlags()
    {
        btnEditEnabled = false;
        btnChangeStateClicked = false;
    }
    
    public List<String> getViewScreenStateList(){
        viewSelection = new List<String>();
        
        try{

            //viewSelection  = CurrentNCRState.Screen_Sections__c.split(','); 

            Map<String, Schema.FieldSet> fieldSetMap ;
            if(this.NCRObject == 'Case')
            {
                fieldSetMap = SObjectType.NCR_Case__c.FieldSets.getMap();
            }
            else if( this.NCRObject == 'Corrective Action')
            {   
                fieldSetMap = SObjectType.NCR_CAR__c.FieldSets.getMap();       
            } 

            
            //SMakarov - we need to validate that all fieldsets are acully exisist
            for(String f : CurrentNCRState.Screen_Sections__c.split(',')) {
            
               f = f.trim(); 
               if(fieldSetMap.containsKey(f)) 
               {
                   viewSelection.add(f);
               }
               else
               {
                   NCR_Utils.logerror('Unknown fieldset: ' + f);
               }            
            }
            
            
             
         }
         catch(Exception ex){
              viewSelection.add(this.DefaultDetailViewSection );
              NCR_Utils.logerror(ex);
         }
         
        return viewSelection;
    } 
    
    public Map<String,String> getViewScreenStateMap(){
        viewSelectionMap = new Map<String,String>();
         
        try{
        
            /*
            NCR_State__c viewScreenOptions  = [SELECT     Screen_Sections__c 
                                               FROM       NCR_State__c 
                                               WHERE      Name = :this.CurrentState]; 
            
            */
            
            Map<String, Schema.FieldSet> fieldSetMap ;
            if(this.NCRObject == 'Case')
            {
                fieldSetMap = SObjectType.NCR_Case__c.FieldSets.getMap();
            }
            else if( this.NCRObject == 'Corrective Action')
            {   
                fieldSetMap = SObjectType.NCR_CAR__c.FieldSets.getMap();       
            }             
            
            
            for(String f : CurrentNCRState.Screen_Sections__c.split(',')) {
            
               if(fieldSetMap.containsKey(f)) 
               {
                   viewSelectionMap.put(f,fieldSetMap.get(f).getLabel());
               }
               else
               {
                   NCR_Utils.logerror('Unknown fieldset: ' + f);
               }
            
               /* 
               if(this.NCRObject == 'Case') 
               {
                   viewSelectionMap.put(f,Schema.SObjectType.NCR_Case__c.fieldSets.getMap().get(f).getLabel()); //TODO Generic
               }
               if(this.NCRObject == 'Corrective Action') 
               {
                   viewSelectionMap.put(f,Schema.SObjectType.NCR_CAR__c.fieldSets.getMap().get(f).getLabel()); //TODO Generic
               }
               */

               
            }              
        }
        catch(Exception exp){
            viewSelectionMap.put(this.DefaultDetailViewSection ,
                                 Schema.SObjectType.NCR_Case__c.fieldSets.getMap().get(this.DefaultDetailViewSection).getLabel()); //TODO Generic
        }        
        
        return viewSelectionMap;
    }  
    
    private boolean SetManualShareRead(ID RecordID, ID OwnerID)
    {
       //if(this.NCRObject == 'Case' && userObj.AccountID != null ) 
       if(this.NCRObject == 'Case') 
       {
           //return NCR_Utils.manualNCRCaseShareRead(RecordID, OwnerID);
           return NCR_Utils.manualNCRCaseShareRead(RecordID);
       }
       else if(this.NCRObject == 'Corrective Action') 
       {
           return NCR_Utils.manualNCRCARShareRead(RecordID);
       }

    
       return false; 
    }
    
    public boolean isPortalUser(){
        String userType = [SELECT UserType FROM User WHERE Id =: userID][0].UserType;
        
        if(userType == 'PowerPartner'){
            return true;
        }
        
        return false;
    }

}