/*-------------------------------------------------------------------------------------------------
Author     : Shreyas Dhond
Company    : Husky Energy
Description: A Utility class for Work Order Activity Confirmation actions
Inputs     : N/A
Test Class : VTT_ConfirmationsUtilitiesTest
History    : 30.01.18 ssd Initial version
--------------------------------------------------------------------------------------------------*/ 
public with sharing class VTT_ConfirmationsUtilities {

	public final static String CONFIRMATION_STATUS_NEW = 'New';
	public final static String CONFIRMATION_STATUS_ERROR = 'Error';
	public final static String CONFIRMATION_STATUS_SENT = 'Sent'; 

	public final static String SAP_RESPONSE_ERROR_PREFIX = 'E -';

	public static void TriggerAfterInsertConfirmation(List<Work_Order_Activity_Confirmation__c> confirmationList) {
		Set<Id> confirmationIds = new Set<Id>();
		for(Work_Order_Activity_Confirmation__c confirmation : confirmationList) {
			confirmationIds.add(confirmation.Id);
		}
		SAP_CreateConfirmations(confirmationIds);
		
	}


	@future (callout=true)
	public static void SAP_CreateConfirmations(Set<Id> confirmationIds) {
		if(confirmationIds == null || confirmationIds.isEmpty()) {
			return;
		}

		//Create Payload
		List<Work_Order_Activity_Confirmation__c> confirmationList = [SELECT Id, Name, Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Number__c,
																		  Work_Order_Activity__r.Operation_Number__c, Work_Order_Activity__r.Description__c,
																		  Work_Order_Activity__r.System_Status__c, Work_Order_Activity__r.Work_Center__c,
																		  Actual_Work__c, Final_Confirmation__c, Work_Start__c, Work_Finish__c, 
																		  Confirmation_Text__c, Confirmation_Long_Text__c,
																		  Work_Order_Activity__r.Operating_Field_AMU__r.Maintenance_Plant__c, 
																		  Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Maintenance_Plant__c,
																		  Confirmation_Log__c
																   FROM Work_Order_Activity_Confirmation__c
																   Where Id In :confirmationIds];
		List<HOG_SAPConfirmationsService.DT_SFDC_Confirmation> sapConfirmationList = createSAPConfirmations(confirmationList);

		HOG_SAPConfirmationsService.HTTPS_Port confirmationService = new HOG_SAPConfirmationsService.HTTPS_Port();
		HOG_Settings__c settingsHOG = HOG_Settings__c.getInstance();
		confirmationService.inputHttpHeaders_x = new Map<String, String>();
		String authString = EncodingUtil.base64Encode(Blob.valueOf(settingsHOG.SAP_DPVR_Username__c + ':' + settingsHOG.SAP_DPVR_Password__c));
		confirmationService.inputHttpHeaders_x.put('Authorization', 'Basic ' + authString);

		try {
			HOG_SAPConfirmationsService.Confirmation_Resp_element[] response = confirmationService.SI_SFDC_Confirmation_Sync_OB(sapConfirmationList);
			System.debug('confirmation->Service callout response: ' + response);

			//Assuming response is in the same order
			for(Integer i=0; i < response.size(); i++) {
				System.debug('response: ' + response[i]);
				confirmationList[i].Confirmation_Number__c = response[i].Conf_No;
				confirmationList[i].Status__c = (response[i].Status.startsWith(SAP_RESPONSE_ERROR_PREFIX)) ? 
					CONFIRMATION_STATUS_ERROR : CONFIRMATION_STATUS_SENT;
				if(String.isBlank(confirmationList[i].Confirmation_Log__c))
					confirmationList[i].Confirmation_Log__c = String.format('{0} SAP Returned: ' + response[i].Status, new List<String>{String.valueof(Datetime.now())});
				else 
					addEntryToConfirmationLog(confirmationList[i], String.format('{0} SAP Returned: ' + response[i].Status, new List<String>{String.valueof(Datetime.now())}));
			}
		} catch (CalloutException ex) {
			for(Work_Order_Activity_Confirmation__c confirmation : confirmationList) {
				confirmation.Status__c = CONFIRMATION_STATUS_ERROR;
				if(String.isBlank(confirmation.Confirmation_Log__c))
					confirmation.Confirmation_Log__c = String.format('{0} SAP Callout Attempt Failed: ' + ex, new List<String>{String.valueof(Datetime.now())});
				else
					addEntryToConfirmationLog(confirmation, String.format('{0} SAP Callout Attempt Failed: ' + ex, new List<String>{String.valueof(Datetime.now())}));
			}
		}

		//Update confirmations in SF (using Data.update to not do action all or nothing)
		Database.SaveResult[] updateResults = Database.update(confirmationList, false);
	}

	public static List<HOG_SAPConfirmationsService.DT_SFDC_Confirmation> createSAPConfirmations(List<Work_Order_Activity_Confirmation__c> confirmationList){
	HOG_SAPConfirmationsService.DT_SFDC_Confirmation[] sapConfirmationList = new List<HOG_SAPConfirmationsService.DT_SFDC_Confirmation>();

		for(Work_Order_Activity_Confirmation__c confirmation: confirmationList){
			HOG_SAPConfirmationsService.DT_SFDC_Confirmation sapConfirmation = new HOG_SAPConfirmationsService.DT_SFDC_Confirmation();

			sapConfirmation.Order_Number = confirmation.Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Number__c.leftPad(12, '0');
			sapConfirmation.Operation_Number = confirmation.Work_Order_Activity__r.Operation_Number__c;
			sapConfirmation.Operation_Text = confirmation.Work_Order_Activity__r.Description__c;
			sapConfirmation.System_Status = confirmation.Work_Order_Activity__r.System_Status__c;
			sapConfirmation.Work_Center = confirmation.Work_Order_Activity__r.Work_Center__c;
			sapConfirmation.Plant = !String.isBlank(confirmation.Work_Order_Activity__r.Operating_Field_AMU__r.Maintenance_Plant__c) ?
				confirmation.Work_Order_Activity__r.Operating_Field_AMU__r.Maintenance_Plant__c :
				confirmation.Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Maintenance_Plant__c;
			sapConfirmation.Personal_Number = null;
			sapConfirmation.Actual_Work = String.valueOf(confirmation.Actual_Work__c*60);
			sapConfirmation.Actual_Duration = String.valueOf(confirmation.Actual_Work__c*60);
			sapConfirmation.Unit_of_Work = 'MIN';
			sapConfirmation.Activity_Type = null;
			sapConfirmation.Final_Confirmation = confirmation.Final_Confirmation__c ? 'X' : '';
			sapConfirmation.No_Remaining_Work = confirmation.Final_Confirmation__c ? 'X' : '';
			sapConfirmation.Work_Start_Date = confirmation.Work_Start__c.format('MM/dd/yyyy');
			sapConfirmation.Work_Start_Time = confirmation.Work_Start__c.format('HHmmss');
			sapConfirmation.Work_Finish_Date = confirmation.Work_Finish__c.format('MM/dd/yyyy');
			sapConfirmation.Work_Finish_Time = confirmation.Work_Finish__c.format('HHmmss');
			sapConfirmation.Confirmation_Text = confirmation.Confirmation_Text__c;
			sapConfirmation.Confirmation_Text_Long_Text = confirmation.Confirmation_Long_Text__c;

			sapConfirmationList.add(sapConfirmation);
		}

		return sapConfirmationList;
	}

	//Use this function to add response to log
	public static void addEntryToConfirmationLog(Work_Order_Activity_Confirmation__c confirmation,
		String entry) {
		String newLog = entry + '\n' + confirmation.Confirmation_Log__c;
		System.debug('newLog: ' + newLog);
		while(newLog.length() > Schema.SObjectType.Work_Order_Activity_Confirmation__c.fields.Confirmation_Log__c.getLength())
			newLog = newLog.substringBeforeLast('\n');
		confirmation.Confirmation_Log__c = newLog;
	}
}