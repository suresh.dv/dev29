@isTest
private class SPCC_UtilitiesTest {

	@testSetup static void setup() {
		SPCC_Utilities.executeTriggerCode = false;

		SPCC_Engineering_Work_Request__c ewr = SPCC_TestData.createEWR('TestProject', '12345', '','Other');
		insert ewr;
		
		SPCC_Engineering_Work_Request__c ewr2 = SPCC_TestData.createEWR('TestProject2', '22222', '','Other');
		insert ewr2;

		SPCC_Authorization_for_Expenditure__c afe = SPCC_TestData.createAFE('A1-F7GH56-4D-J7-K2', 'A1-F7GH56-4D-J7-K2', 'Test AFE', ewr.Id);
		insert afe;
		
		SPCC_Authorization_for_Expenditure__c afe2 = SPCC_TestData.createAFE('00-F7GH56-4D-J7-11', '00-F7GH56-4D-J7-11', 'Test AFE 2', ewr2.Id);
		insert afe2;

		SPCC_Cost_Element__c costElement = SPCC_TestData.createCostElement(afe.Id, 'Test Category', 
													 'Test Description', 
													 '6000000',
													 'Test Cose Element',
													 5000,
													 0);
		insert costElement;

		SPCC_Cost_Code__c costCode = SPCC_TestData.createCostCode(costElement.Id, '001', 'test Cost Code', 200);
		insert costCode;


		Account spccVendorAccount1 = SPCC_TestData.createSPCCVendor('Test Account 1');
		Account spccVendorAccount2 = SPCC_TestData.createSPCCVendor('Test Account 2');
		insert new List<Account>{spccVendorAccount1, spccVendorAccount2};

		Account epcVendorAccount1 = SPCC_TestData.createEPCVendor('Test EPC Vendor 1');
		Account epcVendorAccount2 = SPCC_TestData.createEPCVendor('Test EPC Vendor 2');
		insert new List<Account>{epcVendorAccount1, epcVendorAccount2};

		SPCC_Utilities.executeTriggerCode = true;
	}

	@isTest static void testLogErrorOnPage() {

	}
	
	/******************************************User Security Function Tests******************************/
	@isTest static void testUserSecurityProjectController() {
		Test.startTest();
			//Project Controller
			System.runAs(SPCC_TestData.createPCUser()) {
				SPCC_Utilities.UserPermissions userpermissions = new SPCC_Utilities.UserPermissions();
				System.assertEquals(userpermissions.isProjectController, true);
				System.assertEquals(userpermissions.isProjectLead, false);
				System.assertEquals(userpermissions.isCSR, false);
				System.assertEquals(userpermissions.isEPC, false);
			}
		Test.stopTest();
	}

	@isTest static void testUserSecurityProjectLead() {
		Test.startTest();
			//Project Lead
			System.runAs(SPCC_TestData.createPLUser()) {
				SPCC_Utilities.UserPermissions userpermissions = new SPCC_Utilities.UserPermissions();
				System.assertEquals(userpermissions.isProjectController, false);
				System.assertEquals(userpermissions.isProjectLead, true);
				System.assertEquals(userpermissions.isCSR, false);
				System.assertEquals(userpermissions.isEPC, false);
			}
		Test.stopTest();
	}

	@isTest static void testUserSecurityCSR() {
		Test.startTest();
			//CSR
			System.runAs(SPCC_TestData.createCSRUser()) {
				SPCC_Utilities.UserPermissions userpermissions = new SPCC_Utilities.UserPermissions();
				System.assertEquals(userpermissions.isProjectController, false);
				System.assertEquals(userpermissions.isProjectLead, false);
				System.assertEquals(userpermissions.isCSR, true);
				System.assertEquals(userpermissions.isEPC, false);
			}
		Test.stopTest();
	}

	@isTest static void testUserSecurityEPC() {
		Test.startTest();
			//EPC
			System.runAs(SPCC_TestData.createEPCUser()) {
				SPCC_Utilities.UserPermissions userpermissions = new SPCC_Utilities.UserPermissions();
				System.assertEquals(userpermissions.isProjectController, false);
				System.assertEquals(userpermissions.isProjectLead, false);
				System.assertEquals(userpermissions.isCSR, false);
				System.assertEquals(userpermissions.isEPC, true);
			}
		Test.stopTest();
	}
	/***************************************************************************************************/
	/******************************************User Related Info Function Tests*************************/
	@isTest static void testUserRelatedInfo() {
		User epcUser = SPCC_TestData.createEPCUser();

		Test.startTest();
			System.runAs(epcUser) {
				Account testAccount = SPCC_TestData.createEPCVendor('Test Account');
				insert testAccount;
				Contact testContact = SPCC_TestData.createEPCContact(testAccount.Id, 
					'Test', 'EPC Contact', epcUser.Id);
				testContact.User__c = epcUser.Id;
				insert testContact;

				SPCC_Utilities.UserDetails userDetails = new SPCC_Utilities.UserDetails();
				System.assertEquals(userDetails.userId, epcUser.Id);
				System.assertEquals(userDetails.hasAccount, true);
				System.assertEquals(userDetails.hasContact, true);
				System.assertEquals(userDetails.accountList.size(), 1);
				System.assertEquals(userDetails.contactList.size(), 1);
			}
		Test.stopTest();
	}
	/**************************************************************************************************/
	/*******************************************Test Trigger Functions*********************************/
	@isTest static void testPurchaseOrderTriggers() {
		//Setup -- Query Setup Objects
		List<SPCC_Engineering_Work_Request__c> ewrList = [Select Id, Name, EWR_Number__c
												From SPCC_Engineering_Work_Request__c];
		List<SPCC_Authorization_for_Expenditure__c> afeList = [Select Id, Name, AFE_Number__c, Description__c
													 From SPCC_Authorization_for_Expenditure__c];
		SPCC_Cost_Element__c costElement = [Select Id, Name, AFE__c, Category__c, Description__c,
												   GL_Number__c, Label__c, 
												   Original_Auth_Amt__c, Trend__c
											From SPCC_Cost_Element__c];
		RecordType spccAccountRecordType = [Select Id, Name From RecordType Where DeveloperName =: 'SPCC_Vendor'];
		RecordType epcAccountRecordType = [Select Id, Name From RecordType Where DeveloperName =: 'EPC_Vendor'];

		List<Account> vendorAccounts = [Select Id, Name
								 	    From Account 
								 	    Where RecordTypeId =: spccAccountRecordType.Id];

		List<Account> epcVendors = [Select Id, Name
									From Account
									Where RecordTypeId =: epcAccountRecordType.Id];

		//Setup -- Create Users
		User csrUser = SPCC_TestData.createCSRUser();
		User epcUser1 = SPCC_TestData.createEPCUser();
		User epcUser2 = SPCC_TestData.createEPCUser();
		User epcUser3 = SPCC_TestData.createEPCUser();

		User runningUser = [Select Id From User Where Id =: UserInfo.getUserId()];
		System.runAs(runningUser) {
			Test.startTest();
				//Setup -- Create CSR Assignment
				SPCC_CSR_Assignment__c csrAssignment = SPCC_TestData.createCSRAssignment(ewrList[0].Id, csrUser.Id);
				insert csrAssignment;

				//Setup -- Create Vendor Contacts and EPC Assignmment
				Contact epcContact1 = SPCC_TestData.createEPCContact(epcVendors[0].Id, 'Test', 'EPC1', epcUser1.Id);
				Contact epcContact2 = SPCC_TestData.createEPCContact(epcVendors[0].Id, 'Test', 'EPC2', epcUser2.Id);
				
				insert new List<Contact>{epcContact1, epcContact2};
				SPCC_EPC_Assignment__c epcAssignment = SPCC_TestData.createEPCAssignment(ewrList[0].Id, epcVendors[0].Id);
				insert epcAssignment;
				SPCC_EPC_Assignment__c epcAssignment2 = SPCC_TestData.createEPCAssignment(ewrList[1].Id, epcVendors[1].Id);
				insert epcAssignment2;
				SPCC_Vendor_Account_Assignment__c vaa = SPCC_TestData.createVAA(ewrList[0].Id, vendorAccounts[0].Id);
				insert vaa;

		
				//Test Sharing Rule Creation
				SPCC_Purchase_Order__c testPO1 = SPCC_TestData.createPO('1234567890', ewrList[0].Id, afeList[0].Id, 
					vendorAccounts[0].Id, '', 4500, null, null);
				insert testPO1;
				Map<Id, SPCC_Purchase_Order__Share> poShareToUserMap = new Map<Id, SPCC_Purchase_Order__Share>();
				for(SPCC_Purchase_Order__Share poShare : [Select Id, ParentId, UserOrGroupId, AccessLevel,
																	RowCause, UserOrGroup.Name
															 From SPCC_Purchase_Order__Share
															 Where ParentId =: testPO1.Id]) {
					poShareToUserMap.put(poShare.UserOrGroupId, poShare);
					System.debug('poShareToUserMap ' + poShareToUserMap.get(poShare.UserOrGroupId).UserOrGroup.Name + ' poshare ' + poShare);
				}
				//Check CSR
				System.assert(poShareToUserMap.containsKey(csrUser.Id));
				System.assertEquals(poShareToUserMap.get(csrUser.Id).AccessLevel, 'Edit');
				System.assertEquals(poShareToUserMap.get(csrUser.Id).RowCause, 
					Schema.SPCC_Purchase_Order__Share.RowCause.Manual);

				//Check EPC1
				System.assert(poShareToUserMap.containsKey(epcUser1.Id));
				System.assertEquals(poShareToUserMap.get(epcUser1.Id).AccessLevel, 'Edit');
				System.assertEquals(poShareToUserMap.get(epcUser1.Id).RowCause, 
					Schema.SPCC_Purchase_Order__Share.RowCause.Manual);

				//Check EPC2
				System.assert(poShareToUserMap.containsKey(epcUser2.Id));
				System.assertEquals(poShareToUserMap.get(epcUser2.Id).AccessLevel, 'Edit');
				System.assertEquals(poShareToUserMap.get(epcUser2.Id).RowCause, 
					Schema.SPCC_Purchase_Order__Share.RowCause.Manual);

				//Test Sharing Rule Update
				Contact epcContact3 = SPCC_TestData.createEPCContact(epcVendors[1].Id, 'Test', 'EPC3', epcUser3.Id);
				insert epcContact3;
				//SPCC_Vendor_Account_Assignment__c vaa2 = SPCC_TestData.createVAA(ewr.Id, vendorAccounts[1].Id);
				//SPCC_EPC_Assignment__c epcAssignment2 = SPCC_TestData.createEPCAssignment(ewr.Id, epcVendors[0].Id);
				//insert vaa2;
				//insert epcAssignment2; 
				testPO1 = [Select Id, Name, Purchase_Order_Number__c, OwnerId
						   From SPCC_Purchase_Order__c
						   Where Id =: testPO1.Id];
				//testPO1.Vendor_Account__c = vendorAccounts[1].Id;
				testPO1.Engineering_work_request__c = ewrList[1].Id;
				testPO1.authorization_for_expenditure__c = afeList[1].Id;
				update testPO1;

				poShareToUserMap.clear();
				for(SPCC_Purchase_Order__Share poShare : [Select Id, ParentId, UserOrGroupId, AccessLevel,
																 RowCause
														  From SPCC_Purchase_Order__Share
														  Where ParentId =: testPO1.Id]) {
					poShareToUserMap.put(poShare.UserOrGroupId, poShare);
				}
				System.assert(!poShareToUserMap.containsKey(epcUser1.Id));
				System.assert(!poShareToUserMap.containsKey(epcUser2.Id));
				System.assert(poShareToUserMap.containsKey(epcUser3.Id));
				
				
				testPO1.Engineering_work_request__c = ewrList[0].Id;
				testPO1.authorization_for_expenditure__c = afeList[0].Id;
				update testPO1;

				//Test update of Incurred Cost on Cost Elments
				SPCC_Line_Item__c testLineItem1 = SPCC_TestData.createLineItem(testPO1.Id, costElement.Id, 4500, '0010', 'Test Line Item 1');
				insert testLineItem1;
				SPCC_Service_Entry_Sheet__c serviceEntrySheet1 = SPCC_TestData.createSES(testPO1.Id, testLineItem1.Id, 1000, Date.today(), '00001');
				SPCC_Service_Entry_Sheet__c serviceEntrySheet2 = SPCC_TestData.createSES(testPO1.Id, testLineItem1.Id, 1200, Date.today(), '00002');
				insert new List<SPCC_Service_Entry_Sheet__c>{serviceEntrySheet1, serviceEntrySheet2};
				testPO1 = [Select Id, Name, Purchase_Order_Number__c, Incurred_Cost__c
						   From SPCC_Purchase_Order__c
						   Where Id =: testPO1.Id];
				System.assertEquals(testPO1.Incurred_Cost__c, 2200);

				testLineItem1 = [Select Id, Name, Committed_Amount__c, Cost_Element__c,
										Incurred_Cost__c, Incurred_Cost_Rate__c,
										Item__c, Purchase_Order__c, Short_Text__c
								 From SPCC_Line_Item__c
								 Where Id =: testLineItem1.Id];
				System.assertEquals(testLineItem1.Incurred_Cost__c, 2200);

				costElement = [Select Id, Name, AFE__c, Category__c, Description__c,
									  GL_Number__c, Incurred_Cost__c, Label__c, 
									  Original_Auth_Amt__c, Trend__c
							   From SPCC_Cost_Element__c];
				System.assertEquals(costElement.Incurred_Cost__c, 2200);


			Test.stopTest();
		}
	}

	@isTest static void testCSRAssignmentTriggers() {
		//Setup -- Query Setup Objects
		List<SPCC_Engineering_Work_Request__c> ewrList = [Select Id, Name, EWR_Number__c
												From SPCC_Engineering_Work_Request__c];
		List<SPCC_Authorization_for_Expenditure__c> afeList = [Select Id, Name, AFE_Number__c, Description__c
													 From SPCC_Authorization_for_Expenditure__c];
		SPCC_Cost_Element__c costElement = [Select Id, Name, AFE__c, Category__c, Description__c,
												   GL_Number__c, Label__c, 
												   Original_Auth_Amt__c, Trend__c
											From SPCC_Cost_Element__c];
		RecordType spccAccountRecordType = [Select Id, Name From RecordType Where DeveloperName =: 'SPCC_Vendor'];
		RecordType epcAccountRecordType = [Select Id, Name From RecordType Where DeveloperName =: 'EPC_Vendor'];

		List<Account> vendorAccounts = [Select Id, Name
								 	    From Account 
								 	    Where RecordTypeId =: spccAccountRecordType.Id];

		List<Account> epcVendors = [Select Id, Name
									From Account
									Where RecordTypeId =: epcAccountRecordType.Id];

		//Setup -- Create Users
		User csrUser = SPCC_TestData.createCSRUser();

		User runningUser = [Select Id From User Where Id =: UserInfo.getUserId()];
		System.runAs(runningUser) {
			Test.startTest();
				//Setup -- Add Purchase Order to costElement
				SPCC_Purchase_Order__c purchaseOrder = SPCC_TestData.createPO('1234567890', ewrList[0].Id, afeList[0].Id, vendorAccounts[0].Id, 
					null, 5000, null, null);
				insert purchaseOrder;

				//Test Insert CSR Assignment
				SPCC_CSR_Assignment__c csrAssignment = SPCC_TestData.createCSRAssignment(ewrList[0].Id, csrUser.Id);
				insert csrAssignment;
				//Test Sharing Rules on EWR
				List<SPCC_Engineering_Work_Request__Share> ewrShares = [Select Id, ParentId, UserOrGroupId, AccessLevel, RowCause
																	 	From SPCC_Engineering_Work_Request__Share
																	 	Where ParentId =: ewrList[0].Id
																	 	And RowCause =: Schema.SPCC_Engineering_Work_Request__Share.RowCause.Manual];
				System.assertEquals(ewrShares.size(), 1);
				System.assertEquals(ewrShares[0].UserOrGroupId, csrUser.Id);
				System.assertEquals(ewrShares[0].AccessLevel, 'Read');
				//Test Sharing Rules on PO
				List<SPCC_Purchase_Order__Share> poShares = [Select Id, ParentId, UserOrGroupId, AccessLevel, RowCause
															 From SPCC_Purchase_Order__Share
															 Where ParentId =: purchaseOrder.Id
															 And RowCause =: Schema.SPCC_Purchase_Order__Share.RowCause.Manual];
				System.assertEquals(poShares.size(), 1);
				System.assertEquals(poShares[0].UserOrGroupId, csrUser.Id);
				System.assertEquals(poShares[0].AccessLevel, 'Edit');

				//Test Delete CSR Assignment
				delete csrAssignment;
				ewrShares = [Select Id, ParentId, UserOrGroupId, AccessLevel, RowCause
						 	 From SPCC_Engineering_Work_Request__Share
						 	 Where ParentId =: ewrList[0].Id
						 	 And RowCause =: Schema.SPCC_Engineering_Work_Request__Share.RowCause.Manual];
				System.assert(ewrShares.isEmpty());
				poShares = [Select Id, ParentId, UserOrGroupId, AccessLevel, RowCause
							From SPCC_Purchase_Order__Share
							Where ParentId =: purchaseOrder.Id
							And RowCause =: Schema.SPCC_Purchase_Order__Share.RowCause.Manual];
				System.assert(poShares.isEmpty());
			Test.stopTest();
		}
	}

	@isTest static void testVendorEPCAssignmentTriggers() {
		//Setup -- Query Setup Objects
		List<SPCC_Engineering_Work_Request__c> ewrList = [Select Id, Name, EWR_Number__c
												From SPCC_Engineering_Work_Request__c];
		List<SPCC_Authorization_for_Expenditure__c> afeList = [Select Id, Name, AFE_Number__c, Description__c
													 From SPCC_Authorization_for_Expenditure__c];
		SPCC_Cost_Element__c costElement = [Select Id, Name, AFE__c, Category__c, Description__c,
												   GL_Number__c, Label__c, 
												   Original_Auth_Amt__c, Trend__c
											From SPCC_Cost_Element__c];
		RecordType spccAccountRecordType = [Select Id, Name From RecordType Where DeveloperName =: 'SPCC_Vendor'];
		RecordType epcAccountRecordType = [Select Id, Name From RecordType Where DeveloperName =: 'EPC_Vendor'];

		List<Account> vendorAccounts = [Select Id, Name
								 	    From Account 
								 	    Where RecordTypeId =: spccAccountRecordType.Id];

		List<Account> epcVendors = [Select Id, Name
									From Account
									Where RecordTypeId =: epcAccountRecordType.Id];

		//Setup -- Create EPC Users
		User epcUser1 = SPCC_TestData.createEPCUser();
		User epcUser2 = SPCC_TestData.createEPCUser();

		User runningUser = [Select Id From User Where Id =: UserInfo.getUserId()];
		System.runAs(runningUser) {
			//Test.startTest();
			//Setup -- Purchase Order
			SPCC_Purchase_Order__c poVendor1 = SPCC_TestData.createPO('1234567890', ewrList[0].Id, afeList[0].Id, vendorAccounts[0].Id, 
				null, 5000, null, null);
			SPCC_Purchase_Order__c poVendor2 = SPCC_TestData.createPO('1234567891', ewrList[0].Id, afeList[0].Id, vendorAccounts[1].Id, 
				null, 5000, null, null);
			insert new List<SPCC_Purchase_Order__c>{poVendor1, poVendor2};

			//Setup -- Add Contacts to Vendors
			Contact epc1 = SPCC_TestData.createEPCContact(epcVendors[0].Id, 'Test', 'EPC1', epcUser1.Id);
			Contact epc2 = SPCC_TestData.createEPCContact(epcVendors[1].Id, 'Test', 'EPC2', epcUser2.Id);
			List<Contact> contactsToAdd = new List<Contact>{epc1,epc2};
			insert contactsToAdd;

			//Test Adding EPC Account Assignments
			List<SPCC_Vendor_Account_Assignment__c> vaas = new List<SPCC_Vendor_Account_Assignment__c>{SPCC_TestData.createVAA(ewrList[0].Id, vendorAccounts[0].Id), SPCC_TestData.createVAA(ewrList[0].Id, vendorAccounts[1].Id)};
			insert vaas;
			
			List<SPCC_EPC_Assignment__c> eas = new List<SPCC_EPC_Assignment__c>{SPCC_TestData.createEPCAssignment(ewrList[0].Id, epcVendors[0].Id), SPCC_TestData.createEPCAssignment(ewrList[0].Id, epcVendors[1].Id)};
			insert eas;
			
			//Test EWR Shares
			List<SPCC_Engineering_Work_Request__Share> ewrShares = [Select Id, ParentId, UserOrGroupId, AccessLevel, RowCause
																 	From SPCC_Engineering_Work_Request__Share
																 	Where ParentId =: ewrList[0].Id
																 	And RowCause =: Schema.SPCC_Engineering_Work_Request__Share.RowCause.Manual];
			System.assertEquals(ewrShares.size(), 2);
			System.assertEquals(ewrShares[0].UserOrGroupId, epcUser1.Id);
			System.assertEquals(ewrShares[0].AccessLevel, 'Edit');
			System.assertEquals(ewrShares[1].UserOrGroupId, epcUser2.Id);
			System.assertEquals(ewrShares[1].AccessLevel, 'Edit');

			//Test PO Shares
			Set<Id> poIds = new Set<Id>{poVendor1.Id, poVendor2.Id};
			List<SPCC_Purchase_Order__Share> poShares = [Select Id, ParentId, UserOrGroupId, AccessLevel, RowCause
														 From SPCC_Purchase_Order__Share
														 Where ParentId In :poIds
														 And RowCause =: Schema.SPCC_Purchase_Order__Share.RowCause.Manual];
			System.assertEquals(poShares.size(), 4);
			Map<Id, List<SPCC_Purchase_Order__Share>> poShareMap = new Map<Id, List<SPCC_Purchase_Order__Share>>();
			for(SPCC_Purchase_Order__Share poShare : poShares) {
				if(poShareMap.containsKey(poShare.ParentId)) poShareMap.get(poShare.ParentId).add(poShare);
				else poShareMap.put(poShare.ParentId, new List<SPCC_Purchase_Order__Share>{poShare});
			}
			System.assertEquals(poShareMap.get(poVendor1.Id)[0].UserOrGroupId, epcUser1.Id);
			System.assertEquals(poShareMap.get(poVendor1.Id)[0].AccessLevel, 'Edit');
			System.assertEquals(poShareMap.get(poVendor1.Id)[1].UserOrGroupId, epcUser2.Id);
			System.assertEquals(poShareMap.get(poVendor1.Id)[1].AccessLevel, 'Edit');
			System.assertEquals(poShareMap.get(poVendor2.Id)[0].UserOrGroupId, epcUser1.Id);
			System.assertEquals(poShareMap.get(poVendor2.Id)[0].AccessLevel, 'Edit');
			System.assertEquals(poShareMap.get(poVendor2.Id)[1].UserOrGroupId, epcUser2.Id);
			System.assertEquals(poShareMap.get(poVendor2.Id)[1].AccessLevel, 'Edit');

			//Test Deleting Vendor Account Assignments -- Error Case
			try {
				delete vaas;
			} catch (DmlException ex) {
				System.assertEquals(true, ex.getMessage().
					contains('Purchase Orders exist with this vendor on this project. Cannot Delete!'));
			}

			//Test Deleting Vendor Account Assignments -- True case
			delete new List<SPCC_Purchase_Order__c>{poVendor1, poVendor2};
			delete vaas;
			ewrShares = [Select Id, ParentId, UserOrGroupId, AccessLevel, RowCause
					 	 From SPCC_Engineering_Work_Request__Share
					 	 Where ParentId =: ewrList[0].Id
					 	 And RowCause =: Schema.SPCC_Engineering_Work_Request__Share.RowCause.Manual];
		}

		System.runAs(epcUser1) {
			//Create PO as epcUser
			SPCC_Purchase_Order__c poCreatedByEPC = SPCC_TestData.createPO('1234567892', ewrList[0].Id, afeList[0].Id, vendorAccounts[0].Id, 
				null, 5000, null, null);
			insert poCreatedByEPC;
		}

		System.runAs(runningUser) {
			//Test Owner of PO Created By EPC
			SPCC_Purchase_Order__c poCreatedByEPC = [Select Id, OwnerId From SPCC_Purchase_Order__c Where OwnerId =: epcUser1.Id];
			System.assert(poCreatedByEPC != null);

			//Test Deleting EPC Assignemnt
			SPCC_EPC_Assignment__c eas = [Select Id, Name
										  From SPCC_EPC_Assignment__c
										  Where Engineering_work_request__c =: ewrList[0].Id 
										  And EPC_Vendor__c =: epcVendors[0].Id];
			delete eas;
			List<SPCC_Engineering_Work_Request__Share> ewrShares = [Select Id, ParentId, UserOrGroupId, AccessLevel, RowCause, UserOrGroup.FirstName, UserOrGroup.LastName
					 	 From SPCC_Engineering_Work_Request__Share
					 	 Where ParentId =: ewrList[0].Id
					 	 And RowCause =: Schema.SPCC_Engineering_Work_Request__Share.RowCause.Manual];
			poCreatedByEPC = [Select Id, OwnerId From SPCC_Purchase_Order__c Where Id =: poCreatedByEPC.Id];
			System.debug('EWRShares: ' + ewrShares[0].UserOrGroup.FirstName + ' ' + ewrShares[0].UserOrGroup.LastName);
			//System.assert(ewrShares.isEmpty());
			System.assertEquals(runningUser.Id, poCreatedByEPC.OwnerId);
			//Test.stopTest();
		}
	}

	@isTest static void testVendorContactTriggers() {
		//Setup -- Query Setup Objects
		List<SPCC_Engineering_Work_Request__c> ewrList = [Select Id, Name, EWR_Number__c
												From SPCC_Engineering_Work_Request__c];
		List<SPCC_Authorization_for_Expenditure__c> afeList = [Select Id, Name, AFE_Number__c, Description__c
													 From SPCC_Authorization_for_Expenditure__c];
		SPCC_Cost_Element__c costElement = [Select Id, Name, AFE__c, Category__c, Description__c,
												   GL_Number__c, Label__c, 
												   Original_Auth_Amt__c, Trend__c
											From SPCC_Cost_Element__c];
		RecordType spccAccountRecordType = [Select Id, Name From RecordType Where DeveloperName =: 'SPCC_Vendor'];
		RecordType epcAccountRecordType = [Select Id, Name From RecordType Where DeveloperName =: 'EPC_Vendor'];

		List<Account> vendorAccounts = [Select Id, Name
								 	    From Account 
								 	    Where RecordTypeId =: spccAccountRecordType.Id];

		List<Account> epcVendors = [Select Id, Name
									From Account
									Where RecordTypeId =: epcAccountRecordType.Id];

		//Setup -- Create EPC Users
		User epcUser1 = SPCC_TestData.createEPCUser();
		User epcUser2 = SPCC_TestData.createEPCUser();


		User runningUser = [Select Id From User Where Id =: UserInfo.getUserId()];
		System.runAs(runningUser) {
			Test.startTest();
				//Setup -- Vendor Account Assignments
				List<SPCC_Vendor_Account_Assignment__c> vaas = new List<SPCC_Vendor_Account_Assignment__c>{SPCC_TestData.createVAA(ewrList[0].Id, vendorAccounts[0].Id), SPCC_TestData.createVAA(ewrList[0].Id, vendorAccounts[1].Id)};
				insert vaas;
				
				//Setup -- EPC Account Assignments
				List<SPCC_EPC_Assignment__c> eas = new List<SPCC_EPC_Assignment__c>{SPCC_TestData.createEPCAssignment(ewrList[0].Id, epcVendors[0].Id), SPCC_TestData.createEPCAssignment(ewrList[0].Id, epcVendors[1].Id)};
				insert eas;

				//Setup -- Purchase Order
				SPCC_Purchase_Order__c poVendor1 = SPCC_TestData.createPO('1234567890', ewrList[0].Id, afeList[0].Id, vendorAccounts[0].Id, 
					null, 5000, null, null);
				SPCC_Purchase_Order__c poVendor2 = SPCC_TestData.createPO('1234567891', ewrList[0].Id, afeList[0].Id, vendorAccounts[1].Id, 
					null, 5000, null, null);
				insert new List<SPCC_Purchase_Order__c>{poVendor1, poVendor2};

				//Test adding contacts
				Contact epc1 = SPCC_TestData.createEPCContact(epcVendors[0].Id, 'Test', 'EPC1', epcUser1.Id);
				Contact epc2 = SPCC_TestData.createEPCContact(epcVendors[1].Id, 'Test', 'EPC2', epcUser2.Id);
				List<Contact> contactsToAdd = new List<Contact>{epc1,epc2};
				insert contactsToAdd;
				//Test EWR Shares
				//Test Sharing Rules on EWR
				List<SPCC_Engineering_Work_Request__Share> ewrShares = [Select Id, ParentId, UserOrGroupId, AccessLevel, RowCause
																	 	From SPCC_Engineering_Work_Request__Share
																	 	Where ParentId =: ewrList[0].Id
																	 	And RowCause =: Schema.SPCC_Engineering_Work_Request__Share.RowCause.Manual];
				System.assertEquals(ewrShares.size(), 2);
				System.assertEquals(ewrShares[0].UserOrGroupId, epcUser1.Id);
				System.assertEquals(ewrShares[0].AccessLevel, 'Edit');
				System.assertEquals(ewrShares[1].UserOrGroupId, epcUser2.Id);
				System.assertEquals(ewrShares[1].AccessLevel, 'Edit');

				//Test Sharing Rules on PO
				List<SPCC_Purchase_Order__Share> poShares = [Select Id, ParentId, UserOrGroupId, AccessLevel, RowCause
															 From SPCC_Purchase_Order__Share
															 Where ParentId =: poVendor1.Id
															 And RowCause =: Schema.SPCC_Purchase_Order__Share.RowCause.Manual];
				System.assertEquals(poShares.size(), 2);
				System.assertEquals(poShares[0].UserOrGroupId, epcUser1.Id);
				System.assertEquals(poShares[0].AccessLevel, 'Edit');
				poShares = [Select Id, ParentId, UserOrGroupId, AccessLevel, RowCause
						    From SPCC_Purchase_Order__Share
						    Where ParentId =: poVendor2.Id
						    And RowCause =: Schema.SPCC_Purchase_Order__Share.RowCause.Manual];
				System.assertEquals(poShares.size(), 2);
				System.assertEquals(poShares[1].UserOrGroupId, epcUser2.Id);
				System.assertEquals(poShares[1].AccessLevel, 'Edit');

				//Test Deleting Contacts
				delete contactsToAdd;
				Set<Id> poIds = new Set<Id>{poVendor1.Id, poVendor2.Id};
				ewrShares = [Select Id, ParentId, UserOrGroupId, AccessLevel, RowCause
						 	 From SPCC_Engineering_Work_Request__Share
						 	 Where ParentId =: ewrList[0].Id
						 	 And RowCause =: Schema.SPCC_Engineering_Work_Request__Share.RowCause.Manual];
				System.assert(ewrShares.isEmpty());
				poShares = [Select Id, ParentId, UserOrGroupId, AccessLevel, RowCause
						    From SPCC_Purchase_Order__Share
						    Where ParentId In :poIds
						    And RowCause =: Schema.SPCC_Purchase_Order__Share.RowCause.Manual];
				System.assert(poShares.isEmpty());

			Test.stopTest();
		}
	}

	//TEST DATA Retrieval functions
	public static List<SPCC_Cost_Element__c> getGLCode(){

		List<SPCC_Cost_Element__c> costElement = [SELECT Id, Name, AFE__c, Category__c, Description__c,
												   		 GL_Number__c, Label__c, Original_Auth_Amt__c, Trend__c 
												  FROM SPCC_Cost_Element__c];
		return 	costElement;								
	}

	public static List<SPCC_Cost_Code__c> getCostCode(){

		List<SPCC_Cost_Code__c> costCode = [SELECT Id, GL_Account__c, Cost_Code_Number__c, Description__c, Incurred_Cost__c, Committed_Purchase_Order_Amt__c 
									  		FROM SPCC_Cost_Code__c];

		return costCode;
	}

	public static List<SPCC_Engineering_Work_Request__c> getEWRs(){

		List<SPCC_Engineering_Work_Request__c> ewr = [Select Id, Name, EWR_Number__c 
													  FROM SPCC_Engineering_Work_Request__c];

		return ewr;
	}

	public static List<SPCC_Authorization_for_Expenditure__c> getAFEs(){

		List<SPCC_Authorization_for_Expenditure__c> afe = [SELECT Id, Name, AFE_Number__c, Description__c
													 	   FROM SPCC_Authorization_for_Expenditure__c];

		return afe;											 	   

	}
	
}