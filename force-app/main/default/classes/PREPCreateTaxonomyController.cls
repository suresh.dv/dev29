public with sharing class PREPCreateTaxonomyController {
    public Material_Service_Group_Clone__c matClone { get; set; }

    public PREPCreateTaxonomyController() {
        matClone = new Material_Service_Group_Clone__c();
    }
    
    public PageReference saveTaxonomy(){
        saveRecords();
        
        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect( true );
        return rageRef;
    }
    
    public PageReference saveAndNewTaxonomy(){
        saveRecords();
        
        PageReference rageRef = Page.PREP_Create_Taxonomy;
        rageRef.setRedirect( true );
        return rageRef;
    }
    
    public PageReference cancelTaxonomy(){
        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect( true );
        return rageRef;
    }
    
    private void saveRecords(){
        List<PREP_Material_Service_Group__c> taxonomyList = [ Select Id, Name, Type__c from PREP_Material_Service_Group__c
                                                            where Type__c =: matClone.Type__c order By Name DESC ];
        
        if( taxonomyList.size() > 0 ){
            String codeNumber = taxonomyList[0].Name;
            Integer codeVal = Integer.valueOf( codeNumber.subString( 1, codeNumber.length() ));
            codeVal++;
            
            matClone.Name = ( matClone.Type__c == 'Material' ? 'M' + codeVal : 'S' + codeVal );
        }
        
        insert matClone;
        
        PREP_Change_Header__c headerTax = new PREP_Change_Header__c( Reason__c = 'New Taxonomy Created', Change_Type__c = 'Taxonomy Created',
                                                                        Request_Created_Date__c = Date.today(),
                                                                        Request_Last_Updated_Date__c = Date.today(),
                                                                        Request_Title__c = 'Create Taxonomy', Request_Status__c = 'New',
                                                                        Approval_Status__c = 'New', Change_Status__c = 'New',
                                                                        Object_Name__c = 'PREP_Material_Service_Group__c' );
        insert headerTax;
        
        PREP_Change_Detail__c detailTax = new PREP_Change_Detail__c( PREP_Change_Header__c = headerTax.id, Field_Name__c = 'Taxonomy',
                                                                    Record_Id__c = matClone.Id,
                                                                    To_Field_value__c = matClone.Name,
                                                                    To_Field_Id__c = matClone.Id,
                                                                    Update_Record_Id__c = matClone.Id, Update_Record_Name__c = matClone.Name,
                                                                    Update_Record_Description__c = matClone.Material_Service_Group_Name__c );
        insert detailTax;
    }
}