public class PREP_updateRAS_Date{

    public static void updateRASDates(list<PREP_Mat_Serv_Group_Planned_Schedule__c> mspsalist) {
    ID matId =  Schema.getGlobalDescribe().get('PREP_Mat_Serv_Group_Planned_Schedule__c').getDescribe().getRecordTypeInfosByName().get('Mat Group Baseline Schedule').getRecordTypeId();
    ID servId =  Schema.getGlobalDescribe().get('PREP_Mat_Serv_Group_Planned_Schedule__c').getDescribe().getRecordTypeInfosByName().get('Serv Group Baseline Schedule').getRecordTypeId();
    //RecordType matACId = [SELECT Id From RecordType WHERE Name = 'Mat Acquisition Cycle'];
    //RecordType servACId = [SELECT Id From RecordType WHERE Name = 'Serv Acquisition Cycle'];
    //RecordType matId = [SELECT Id From RecordType WHERE Name = 'Mat Group Baseline Schedule'];
    //RecordType servId = [SELECT Id From RecordType WHERE Name = 'Serv Group Baseline Schedule'];
    set<Id>listId = new set<Id>();
    for (PREP_Mat_Serv_Group_Planned_Schedule__c MSschedule : mspsalist)
        {
          listId.add(MSschedule.Id);
          
        }
      System.debug('@@@@'+listId);
      //list<PREP_Mat_Serv_Group_Planned_Schedule__c> msgp = [select Id, R_A_S_Date__c,CWP_Completion__c,RecordTypeId from PREP_Mat_Serv_Group_Planned_Schedule__c where Id IN: listId];
      
      list<PREP_Mat_Serv_Group_Planned_Schedule__c> msgp = [select Id,Ship_Date__c,Strategy_Approval_Mat__c,X04_Sourcing_Options_Mat__c,CWP_Completion__c,RFQ_Issued__c,X04_Sourcing_Options_Serv__c,
      X03_Supplier_Market_Analysis_Mat__c,X02_Business_Requirements_Mat__c,X01_Segmentation_Team_Selection_Mat__c,ETA_Date__c,Commercial_Evaluations_Received_Mat__c,X03_Supplier_Market_Analysis_Serv__c,
      Drawings_Approved__c,P_O_Issued__c,Vendor_Initial_Drawing_Submittal__c, Bids_Received_Mat__c,R_A_S_Date__c,MRQ_Received__c,MRP_Received__c,CWP_Received__c,
      Decision_Summary_Issued_For_Approval_Mat__c,Decision_Summary_Approved_Mat__c,Bid_Tab_Finalized_Mat__c,Technical_Evaluations_Received_Mat__c,Bids_Received_Serv__c,X01_Segmentation_Team_Selection_Serv__c,
      Commercial_Evaluations_Received_Serv__c,Technical_Evaluations_Received_Serv__c,Bid_Tab_Finalized_Serv__c,Decision_Summary_Issued_For_Approval_Ser__c,Decision_Summary_Approved_Serv__c,
      Final_CWP_Received__c,Contract_Executed__c,Mobilzation_Construction_Start__c,Construction_Completion__c,Demob__c,X02_Business_Requirements_Serv__c,
      Strategy_Approval_Serv__c,RecordTypeId,Bid_Request_Issued_RFQ__c from PREP_Mat_Serv_Group_Planned_Schedule__c where Id IN: listId];
    
      list<PREP_Mat_Serv_Group_Update__c> mu =[select id,RecordTypeId, R_A_S_Date__c,CWP_Completion__c,Forecast_Strategy_Approval__c,Mat_Serv_Group_Planned_Schedule_Id__c from PREP_Mat_Serv_Group_Update__c where Mat_Serv_Group_Planned_Schedule_Id__c = :msgp[0].Id];
      list<PREP_Mat_Serv_Group_Update__c> msgu = new list<PREP_Mat_Serv_Group_Update__c>();
      for(PREP_Mat_Serv_Group_Planned_Schedule__c msp : msgp)
          {
           for(PREP_Mat_Serv_Group_Update__c msu :mu)
               {
               if(msp.RecordTypeId == matId)
                   {
                    msu.Planned_Bid_Request_Issued_RFQ__c = msp.Bid_Request_Issued_RFQ__c;
                    msu.Planned_MRQ_Received__c = msp.MRQ_Received__c;
                    msu.Planned_Bids_Received__c = msp.Bids_Received_Mat__c;
                    msu.Planned_Commercial_Evaluations_Received__c = msp.Commercial_Evaluations_Received_Mat__c;
                    msu.Planned_Technical_Evaluations_Received__c = msp.Technical_Evaluations_Received_Mat__c;
                    msu.Planned_Bid_Tab_Finalized__c = msp.Bid_Tab_Finalized_Mat__c;
                    msu.Planned_Decision_Sum_Issued_Approval__c = msp.Decision_Summary_Issued_For_Approval_Mat__c;
                    msu.Planned_Decision_Summary_Approved__c = msp.Decision_Summary_Approved_Mat__c;
                    msu.Planned_MRP_Received__c = msp.MRP_Received__c;
                    msu.Planned_P_O_Issued__c = msp.P_O_Issued__c;
                    msu.Planned_Vendor_Init_Drawing_Submittal__c = msp.Vendor_Initial_Drawing_Submittal__c;
                    msu.Planned_Drawings_Approved__c = msp.Drawings_Approved__c;
                    msu.Planned_Ship_Date__c = msp.Ship_Date__c;
                    msu.Planned_ETA__c = msp.ETA_Date__c;
                    msu.Planned_1_Segmentation_Team_Selection__c = msp.X01_Segmentation_Team_Selection_Mat__c;
                    msu.Planned_2_Business_Requirements__c = msp.X02_Business_Requirements_Mat__c;
                    msu.Planned_3_Supplier_Market_Analysis__c = msp.X03_Supplier_Market_Analysis_Mat__c;
                    msu.Planned_4_Sourcing_Options__c = msp.X04_Sourcing_Options_Mat__c;
                    msu.Planned_Strategy_Approval__c = msp.Strategy_Approval_Mat__c;
                    msu.Planned_Strategy_Approval__c = msp.Strategy_Approval_Mat__c;
                    msu.R_A_S_Date__c = msp.R_A_S_Date__c;
                       
                       msgu.add(msu);
                   }
              if(msp.RecordTypeId == servId)
                   {
          
                    msu.Planned_CWP_Received__c = msp.CWP_Received__c;
                    msu.Planned_RFQ_Issued__c = msp.RFQ_Issued__c;
                    msu.Planned_Bids_Received__c = msp.Bids_Received_Serv__c;
                    msu.Planned_Commercial_Evaluations_Received__c = msp.Commercial_Evaluations_Received_Serv__c;
                    msu.Planned_Technical_Evaluations_Received__c = msp.Technical_Evaluations_Received_Serv__c;
                    msu.Planned_Bid_Tab_Finalized__c = msp.Bid_Tab_Finalized_Serv__c;
                    msu.Planned_Decision_Sum_Issued_Approval__c = msp.Decision_Summary_Issued_For_Approval_Ser__c;
                    msu.Planned_Decision_Summary_Approved__c = msp.Decision_Summary_Approved_Serv__c;
                    msu.Planned_Final_CWP_Received__c = msp.Final_CWP_Received__c;
                    msu.Planned_Contract_Executed_LOA_Effective__c = msp.Contract_Executed__c;
                    msu.Planned_Mobilization_Construction_Start__c = msp.Mobilzation_Construction_Start__c;
                    msu.Planned_Construction_Completion__c = msp.Construction_Completion__c;
                    msu.Planned_Demob__c = msp.Demob__c;
                    msu.CWP_Completion__c = msp.CWP_Completion__c;
                    msu.Planned_1_Segmentation_Team_Selection__c = msp.X01_Segmentation_Team_Selection_Serv__c;
                    msu.Forecast_1_Segmentation_Team_Selection__c = msp.X01_Segmentation_Team_Selection_Serv__c;
                    msu.Planned_2_Business_Requirements__c = msp.X02_Business_Requirements_Serv__c;
                    msu.Planned_3_Supplier_Market_Analysis__c = msp.X03_Supplier_Market_Analysis_Serv__c;
                    msu.Planned_4_Sourcing_Options__c = msp.X04_Sourcing_Options_Serv__c;
                    msu.Planned_Strategy_Approval__c = msp.Strategy_Approval_Serv__c;
                        msgu.add(msu);
                   }
            }
        }
      update msgu;
      }

}