@isTest  
private class MetadataServiceTest
{   
    private class WebServiceMockImpl implements WebServiceMock 
    {
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType) 
        {
            if(request instanceof MetadataService.updateMetadata_element)
                response.put('response_x', new MetadataService.updateMetadataResponse_element());
                else if(request instanceof MetadataService.updateMetadata_element)
                response.put('response_x', new MetadataService.updateMetadataResponse_element());
           return;
        }
    }
    @IsTest
    private static void coverGeneratedCodeFileBasedOperations1()
    {       
        // Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        MetadataService metaDataService = new MetadataService();
        // Invoke operations    
        Test.startTest();     
        MetadataService.MetadataPort metaDataPort = new MetadataService.MetadataPort();
        metaDataPort.updateMetadata(null);
        Test.stopTest();
    }
    @IsTest
    private static void coverGeneratedCodeTypes()
    {              
        // Reference types
        Test.startTest();
        new MetadataService.CustomMetadata();
        new MetadataService.LookupFilter();
        new MetadataService.DebuggingInfo_element();
        new MetadataService.DebuggingHeader_element();
        new MetadataService.LogInfo();
        new MetadataService.CallOptions_element();
        new MetaDataService.AllOrNoneHeader_element();
        new MetadataService.FilterItem();
        new MetadataService.CustomField();
        new MetadataService.ValueSet();
        new MetadataService.updateMetadataResponse_element();
        new MetadataService.ValueSetValuesDefinition();
        new MetadataService.ValueSettings();
        new MetaDataService.CustomValue();
        new MetadataService.updateMetadata_element();
        new MetadataService.SessionHeader_element();
        new MetadataService.CustomMetadataValue();
        new MetadataService.SaveResult();
        new MetadataService.ExtendedErrorDetails();
        new MetadataService.Error();
        new MetadataService(); 
        new MetadataService.Picklist();
        new MetadataService.PicklistValue();
        new MetaDataService.PicklistEntry();
        new MetadataService.GlobalPicklistValue();
        new MetadataService.PicklistValue();
        Test.stopTest();
    }
}