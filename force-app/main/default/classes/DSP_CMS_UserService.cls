/*
Used by the Downstream Portal Calendar page to check is the user is a site admin or system admin.

See DSP_CMS_Calendar
*/
global with sharing class DSP_CMS_UserService implements cms.ServiceInterface
{

    private String action;
    private System.JSONGenerator response;

    public System.Type getType()
    {
        return DSP_CMS_UserService.class;
    }

    global String executeRequest(Map<String, String> p)
    {
        action = p.get('action');
        
        response = System.JSON.createGenerator(false);
        
        response.writeStartObject();
        
        List<PermissionSetAssignment> permissionSets = [SELECT PermissionSetId
                                                        FROM PermissionSetAssignment
                                                        WHERE AssigneeId= :UserInfo.getUserId() AND
                                                              (PermissionSet.Label = 'ocms OrchestraCMS Site Administrator HuskyDownstreamPortal' OR
                                                               PermissionSet.Label = 'ocms OrchestraCMS System Administrator HuskyDownstreamPortal')];
        if(permissionSets.size() == 0)
            response.writeNumberField('isSiteAdmin',0);
        else
            response.writeNumberField('isSiteAdmin',1);
        
        response.close();
        return (response.getAsString());
    }
}