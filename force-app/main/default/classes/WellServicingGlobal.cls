/*--------------------------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A Global Utility class for Well Servicing
Inputs     : N/A
Test Class : WellServicingGlobalTest
History    :
			01.08.16	rbo Modifications due to Refactoring
							replaced all HOG_Rigless_Servicing_Form__c with HOG_Maintenance_Servicing_Form__c
							modified reference to Notification Type to come directly from the Notification Type Master	
            03.03.16 rbo modified startDate and stopDate to be startDateTime and stopDateTime                   										
----------------------------------------------------------------------------------------------------------------------*/ 
global with sharing class WellServicingGlobal 
{

    @TestVisible private static Boolean testErrorCondition = false;

    /********************************************************************************************************************
    * Calls the SAP TECO web service, sets the TECO if successful, and returns the work order number or an error message
    * workOrderId - The id of the Work Order Record
    ********************************************************************************************************************/    
    webservice static String updateTECO(Id workOrderId, Boolean validateStatus)
    {
        System.debug('\n**********************************************************\n'
            + 'METHOD: WellServicingGlobal.updateALTConfirmed()'
            + '\nworkOrderId: ' + workOrderId
            + '\n*****************************************************************\n');

        String returnMessage = 'Service is down, please try again later...';

		// Retrieve 1 record or nothing
		List<HOG_Maintenance_Servicing_Form__c> workOrderRecord =
            [Select
                Id,
                Work_Order_Number__c,
                Notification_Number__c,
                TECO__c,
                TECO_Date_Completed__c,
                Service_Status__c,
                Service_Booked_On__c,
                Service_Completed__c,
		    	//HOG_Service_Request_Notification_Form__r.HOG_Work_Order_Type__r.HOG_Notification_Type__r.Order_Type__c
		    	HOG_Notification_Type__r.Order_Type__c
            From HOG_Maintenance_Servicing_Form__c
            Where Id = :workOrderId For Update];  
		                    
        If (workOrderRecord != null && workOrderRecord.size() > 0)
        {
            if (workOrderRecord[0].TECO__c)
                returnMessage = 'Unable to "TECO" work order ' + workOrderRecord[0].Work_Order_Number__c + ' because it has already been "TECOed"';
            else
            {                    
                if (workOrderRecord[0].Work_Order_Number__c == null)
                    returnMessage = 'You need a work order number to "TECO".';
                else {
                        if (validateStatus && workOrderRecord[0].Service_Status__c != 'Complete' && workOrderRecord[0].Service_Status__c != 'Cancelled')
                            returnMessage = 'Service Status must be set to "Complete" or "Cancelled" in order to "TECO".';
                        else {

                            DateTime startDateTime = workOrderRecord[0].Service_Booked_On__c;
                            DateTime stopDateTime = System.now();
                            
                            if (workOrderRecord[0].Service_Completed__c != null)
                            {
	                            stopDateTime = workOrderRecord[0].Service_Completed__c;
                            }
                            // rbo 03.03.16
                            
                            // after passing all validations, call web service to ALT_Confirm work order
                            SAPHOGWorkOrderServices.WorkOrderPort workOrder = new SAPHOGWorkOrderServices.WorkOrderPort();
                            //SAPHOGWorkOrderServices.ATLConfirmWorkOrderResponse response;
                            SAPHOGWorkOrderServices.ATLConfirmWorkOrderResponse response = new SAPHOGWorkOrderServices.ATLConfirmWorkOrderResponse();
                            
                            try
                            {
					            if (Test.isRunningTest())
					   				response.type_x = True;
					            else
	                                response = 
	                                    workOrder.ATLConfirmWorkOrder
	                                    (
	                                        workOrderRecord[0].Work_Order_Number__c,
	                                        workOrderRecord[0].HOG_Notification_Type__r.Order_Type__c, 
	                                        startDateTime, 
	                                        stopDateTime
	                                    );
                            }
                            catch(Exception e)
                            {
                                response = null;                                
                                System.debug('An unexpected error has occurred: ' + e.getMessage());
                                returnMessage = 'Service is not available because of the following error: ' + e.getMessage();
                            }
                                                            
                            if (response != null)
                            {        
                                System.debug('\n**********************************************************\n'
                                    + 'METHOD: WellServicingGlobal.updateALTConfirmed()'
                                    + '\nresponse: ' + response
                                    + '\nresponse.WorkOrderNumber: ' + response.WorkOrderNumber
                                    + '\nresponse.Message: ' + response.Message
                                    + '\nresponse.type_x: ' + response.type_x                
                                    + '\n*****************************************************************\n');   
                                                
                                if (response.type_x)
                                {
                                    workOrderRecord[0].TECO__c = true;
                                    workOrderRecord[0].TECO_Date_Completed__c = stopDateTime;                                                                                                
                                    
	                                String notificationCloseMessage = CloseNotification(workOrderRecord[0].Notification_Number__c)
	                                        ? '' : ' Notification number ' 
	                                        + workOrderRecord[0].Notification_Number__c 
	                                        + ' has not been closed, please contact Admin.';                                    

                                    // do not execute the trigger
                                    WellServicingUtilities.executeTriggerCode = false;
									MaintenanceServicingUtilities.executeTriggerCode = false;

                                    if(Test.isRunningTest() && testErrorCondition) {
                                        System.debug('Testing error condition');
                                        workOrderRecord[0].Unit_Number__c = '';
                                    }
                                    
                                    try 
                                    {                                
                                        update workOrderRecord;
                                        returnMessage = response.WorkOrderNumber + ' has been successfully "TECOed".' + notificationCloseMessage;
                                    }    
                                    catch(DmlException e)
                                    {                     
                                        System.debug('An unexpected error has occurred: ' + e.getMessage());
                                        returnMessage = response.WorkOrderNumber
                                            + ' has been successfully "TECOed" in SAP, but not in Salesforce because of the following error: ' 
                                            + e.getMessage();                   
                                    }
                                }    
                                else
                                    returnMessage = response.Message;                                                                      
                            }
                                                                
                        }                                
                }                                                   
            }
        }
        else
            returnMessage = 'Unable to "TECO", this work order does not exist!';
                                
        return returnMessage;
    }


    private static Boolean CloseNotification(String notificationNumber)    
    {
        Boolean returnCloseNotification = false;
        
        if (notificationNumber != null)
        {
            SAPHOGNotificationServices.NotificationPort notification = new SAPHOGNotificationServices.NotificationPort();
                    
            try
            {
                SAPHOGNotificationServices.CloseNotificationResponse closeNotificationResponse = notification.CloseNotification(notificationNumber);
                if (closeNotificationResponse != null)
                    returnCloseNotification = closeNotificationResponse.type_x;                
            }
            catch(Exception e)
            {
                System.debug('An unexpected error has occurred: ' + e.getMessage());
            }    
        }
        else        
            returnCloseNotification = true;
    
        return returnCloseNotification;
    }    
}