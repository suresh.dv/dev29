public class NotestAttsRelatedListController {
    public list<AttachmentWrapper> atts {get;set;}
    public String aid {get; set;}
    public String Atstr {get; set;}
    public string subject {get; set;}
    public ATSWrapperUtilities.ATSQuoteWrapper quote{get; set;}

    public NotestAttsRelatedListController( ApexPages.StandardController controller ) {

        atts = new list<AttachmentWrapper>();
        List<Attachment> attachments = [Select a.Id,a.ContentType,a.ParentId,a.Parent.Type,a.Parent.Name,a.OwnerId,a.Owner.Name, a.Name, a.LastModifiedDate, a.BodyLength From Attachment a 
        where a.ParentId=:ApexPages.currentPage().getParameters().get('id')]; 

        Set<Id> attachmentIds = new Set<Id>();
        for( Attachment att : attachments )
        {
            attachmentIds.add( att.Id );
        }
        
        Map<Id, String> attachmentToSentDateMap = new Map<Id, String>();
        for( ATS_Customer_Attachment_Junction__c ats : [ Select Id, Attachment_Sent_Date__c, Attachment_Id__c from ATS_Customer_Attachment_Junction__c where Attachment_Id__c IN :attachmentIds ])
        {
            if( ats.Attachment_Sent_Date__c != null )
            {
                attachmentToSentDateMap.put( ats.Attachment_Id__c, ats.Attachment_Sent_Date__c.format() );
            }
        }

        for( Attachment att : attachments )
        {
            String dateStr = ( attachmentToSentDateMap.containsKey( att.Id ) ? attachmentToSentDateMap.get( att.Id ) : null );
            atts.add( new AttachmentWrapper( att, dateStr ));
        }
    }

    public pagereference SendAttachment() {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        Attachment r= [select Owner.Email,Name,Parent.Name, Owner.Name from Attachment where Id= :aid];
        
        ATS_Parent_Opportunity__c At = [select Owner.Email,Name, Bid_Description__c, Marketer__r.Name, Marketer__r.Email from ATS_Parent_Opportunity__c where Id= :ApexPages.currentPage().getParameters().get('id')];
        
        List<ATS_Customer_Attachment_Junction__c> junctions = [ Select Id, ATS_Customer__c, ATS_Customer__r.Email, Attachment_Sent_Date__c from ATS_Customer_Attachment_Junction__c where Attachment_Id__c =: aid ];

        String[] toAddresses = new String[]{}; 
        if( junctions.size() > 0 )
        {
            ATS_Customer_Attachment_Junction__c junc = junctions[0];
            toAddresses.add( junc.ATS_Customer__r.Email );
            
            junc.Attachment_Sent_Date__c = DateTime.now();
            update junc;
        }
        else
        {
            toAddresses.add( AT.Owner.Email );
        }
    
        String[] ccAddresses = new String[]{ At.Marketer__r.Email }; 
        mail.setToAddresses(toAddresses );
        mail.setCcAddresses( ccAddresses );
        String body = 'Dear Madam/Sir' +',' + '</br></br>'; 
        body += 'Please find attached our quote for: ' + '<b>' + r.Parent.Name + '</b>' + ',' + '</br>';
        body += 'Project: '+ At.Bid_Description__c  + '</br></br>';
        body += 'Best Regards' + ',' + '</br>';
        body += '<b>' + At.Marketer__r.Name + '</b>' + '</br>'; 
        body += 'Asphalt & Emulsions Sales Representative  '  ;
        
        mail.setHtmlBody(body);
        mail.setBccSender(true);
        mail.setUseSignature(true);
        
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        for( Attachment a : [ select Name, Body, BodyLength from Attachment where Id= :aid ]){
            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName( a.name );
            mail.setSubject( r.Parent.Name + ' ' + a.name );
            efa.setBody( a.Body );
            fileAttachments.add( efa );
        }
        
        mail.setFileAttachments(fileAttachments);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    
        for( AttachmentWrapper att : atts )
        {
            if( att.attach.Id == aid )
            {
                DateTime attachmentTime = DateTime.now();
                att.attachmentDateTime = attachmentTime.format();
            }
        }
        
        return null;
    }

    public class AttachmentWrapper
    {
        public String attachmentDateTime { get; set; }
        public Attachment attach { get; set; }
        
        public AttachmentWrapper( Attachment attach, String attachmentDateTime )
        {
            this.attach = attach;
            this.attachmentDateTime = attachmentDateTime;
        }
    }
}