/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Service_Priority__c
-------------------------------------------------------------------------------------------------*/
@isTest        
public class ServicePriorityTestData
{
    public static HOG_Service_Priority__c createServicePriority
        (String priorityDescription, Integer priorityNumber, String priorityCode)
    {                
        HOG_Service_Priority__c results = new HOG_Service_Priority__c
            (
                Priority_Description__c = priorityDescription,
                Priority_Number__c = priorityNumber,
                Priority_Code__c = priorityCode                        
            );
                
        return results;
    }
}