//
// Custom controller for lookup example popup page
//
public with sharing class NCR_ProjectPopupController
{
    public String query {get; set;}
    public List<Account> accounts {get; set;}

    public String searchByProjectName{get;set;}
    public String searchByBUName{get;set;}
    
    public list<Milestone1_Project__c> listOfProjects {get;set;}
    
    public NCR_ProjectPopupController()
    {

        getListOfProjects();
    }

   public void getListOfProjects(){
        try{ 
            listOfProjects  = new List<Milestone1_Project__c>();
            String query = 'SELECT Name, Business_Unit__c, Business_Unit__r.Name FROM Milestone1_Project__c order by Business_Unit__r.Name, Name';
            listOfProjects   = Database.query(query);
        }catch(Exception ex){
             apexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Parameter is not defined, Please contact your Administrator!'));
        }       
    }

    /*Search event*/
    public void onSearch(){
        //phoneNumberPassToTextBox = '';
        //phoneNamePassToHdn  = '';
        //isAllSelectCheckBox = false;
        listOfProjects  = new List<Milestone1_Project__c>();
        String byProjectName = '\'%'+String.escapeSingleQuotes(searchByProjectName) + '%\'';
        String byBUName = '\'%'+String.escapeSingleQuotes(searchByBUName) + '%\'';

        try{
            String query = 'SELECT Name, Business_Unit__c, Business_Unit__r.Name FROM Milestone1_Project__c WHERE name Like '+byProjectName +' AND Business_Unit__r.Name Like '+byBUName + ' order by Business_Unit__r.Name, Name' ;
            listOfProjects   = Database.query(query);
        }catch(Exception ex){
            apexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Illegal Search parameter!'));
        }       
    }

}