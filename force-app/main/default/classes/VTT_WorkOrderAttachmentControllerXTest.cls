@IsTest
private class VTT_WorkOrderAttachmentControllerXTest {
	
	@IsTest static void TestWithNoAttachments() {
		User runningUser = VTT_TestData.createVTTAdminUser();
		
		System.runAs(runningUser) {	
			MaintenanceServicingUtilities.executeTriggerCode = false;		
			HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest();
			
			RecordType rt = [SELECT Id
							FROM RecordType 
							WHERE SobjectType = 'HOG_Maintenance_Servicing_Form__c' 
								AND DeveloperName = 'Maintenance_Servicing'];    
        	
        	 
        	HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrderWithRecordType(
        		serviceRequest.Id, VTT_TestData.notificationType.Id, rt.Id);

			VTT_WorkOrderAttachmentControllerX controller = new VTT_WorkOrderAttachmentControllerX(
            	new ApexPages.StandardController(workOrder));

			System.assertEquals(controller.getAttachments().size(), 0);
			
			for(ApexPages.Message msg:ApexPages.getMessages()){
				System.assertEquals(VTT_WorkOrderAttachmentControllerX.NO_ATTACHMENTS_AND_NOTE_MESSAGE, msg.getSummary());

			}

		}
	}

	@IsTest static void TestWithAttachments() {
		User runningUser = VTT_TestData.createVTTAdminUser();

		System.runAs(runningUser) {	
			MaintenanceServicingUtilities.executeTriggerCode = false;		
			HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest();
			RecordType rt = [SELECT Id
							FROM RecordType 
							WHERE SobjectType = 'HOG_Maintenance_Servicing_Form__c' 
								AND DeveloperName = 'Maintenance_Servicing'];    
        	
        	Account vendor = VTT_TestData.createVendorAccount('Vendor');
            Contact tradesman1 = VTT_TestData.createTradesmanContact('Tom', 'Cruise',  vendor.Id, runningUser.Id);
            
        	HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrderWithRecordType(
        		serviceRequest.Id, VTT_TestData.notificationType.Id, rt.Id);

        	List<Work_Order_Activity__c> activity =  VTT_TestData.createWorkOrderActivitiesWithAssignments(
        		workOrder.Id, tradesman1.Id, 2);
			
        	// Insert Attachment
        	Attachment attach = new Attachment();   	
	    	attach.Name = 'SomeFileForTest';
	    	Blob bodyBlob = Blob.valueOf('SomeFileForTest Body');
	    	attach.Body = bodyBlob;
	        attach.ParentId = activity[0].Id;
	        insert attach;
	    	

			VTT_WorkOrderAttachmentControllerX controller = new VTT_WorkOrderAttachmentControllerX(
            	new ApexPages.StandardController(workOrder));

			System.assertEquals(controller.getAttachments().size(), 1);
		}
	}

	@IsTest static void TestDeleteAttachments() {
		User runningUser = VTT_TestData.createVTTAdminUser();
		
		System.runAs(runningUser) {	
			MaintenanceServicingUtilities.executeTriggerCode = false;		
			HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest();
			RecordType rt = [SELECT Id
							FROM RecordType 
							WHERE SobjectType = 'HOG_Maintenance_Servicing_Form__c' 
								AND DeveloperName = 'Maintenance_Servicing'];    
        	
        	Account vendor = VTT_TestData.createVendorAccount('Vendor');
            Contact tradesman1 = VTT_TestData.createTradesmanContact('Tom', 'Cruise',  vendor.Id, runningUser.Id);
            
        	HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrderWithRecordType(
        		serviceRequest.Id, VTT_TestData.notificationType.Id, rt.Id);

        	List<Work_Order_Activity__c> activity =  VTT_TestData.createWorkOrderActivitiesWithAssignments(
        		workOrder.Id, tradesman1.Id, 2);
			
        	// Insert Attachment
        	Attachment attach = new Attachment();   	
	    	attach.Name = 'SomeFileForTest';
	    	Blob bodyBlob = Blob.valueOf('SomeFileForTest Body');
	    	attach.Body = bodyBlob;
	        attach.ParentId = activity[0].Id;
	        insert attach;

	        // Insert Attachment
        	Note note = new Note();   	
	    	note.Title = 'SomeNote';
	    	note.ParentId = activity[0].Id;
	        insert note;
	    	

			VTT_WorkOrderAttachmentControllerX controller = new VTT_WorkOrderAttachmentControllerX(
            	new ApexPages.StandardController(workOrder));

			controller.clickedType = VTT_WorkOrderAttachmentControllerX.ATTACHMENT_TYPE;
			controller.clickedId = attach.Id;
			controller.deleteAttachment();

			controller.clickedType = VTT_WorkOrderAttachmentControllerX.NOTE_TYPE;
			controller.clickedId = note.Id;
			controller.deleteAttachment();


			System.assertEquals(controller.getAttachments().size(), 0);
		}
	}
}