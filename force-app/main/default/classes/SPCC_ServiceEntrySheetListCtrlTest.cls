@isTest
private class SPCC_ServiceEntrySheetListCtrlTest {

	@testSetup static void setup() {
		SPCC_Utilities.executeTriggerCode = false;

		//Setup Multiple Project Hierarchies
		List<SPCC_Engineering_Work_Request__c> ewrList = new List<SPCC_Engineering_Work_Request__c>();
		List<SPCC_Authorization_for_Expenditure__c> afeList = new List<SPCC_Authorization_for_Expenditure__c>();
		List<SPCC_Cost_Element__c> costElementList = new List<SPCC_Cost_Element__c>();
		List<SPCC_Purchase_Order__c> purchaseOrderList = new List<SPCC_Purchase_Order__c>();
		List<SPCC_Line_Item__c> lineItemList = new List<SPCC_Line_Item__c>();
		List<SPCC_Service_Entry_Sheet__c> serviceEntrySheetList = new List<SPCC_Service_Entry_Sheet__c>();
		List<Account> vendorAccountList = new List<Account>();
		List<SPCC_Vendor_Account_Assignment__c> vendorAccountAssignmentList = new List<SPCC_Vendor_Account_Assignment__c>();

		for(Integer n=1; n <= 5; n++) {
			//Create EWR
			SPCC_Engineering_Work_Request__c ewr = SPCC_TestData.createEWR('Test Project ' + n, '1234' + n, '','Other');
			ewrList.add(ewr);
		}
		insert ewrList;

		Integer i = 1;
		for(SPCC_Engineering_Work_Request__c ewr : ewrList) {
			//Create AFE
			SPCC_Authorization_for_Expenditure__c afe = SPCC_TestData.createAFE('A1-F7GH56-4D-J7-K' + i, 'A1-F7GH56-4D-J7-K' + i, null, ewr.Id);
			afeList.add(afe);
			i++;
		}
		insert afeList;

		i = 1;
		for(SPCC_Authorization_for_Expenditure__c afe :afeList) {
			//Create Cost Element
			SPCC_Cost_Element__c costElement = SPCC_TestData.createCostElement(afe.Id, null, null, '600000' + i, 'Test Cost Element' + i, 2000, 0);
			costElementList.add(costElement);
			i++;
		}
		insert costElementList;

		for(Integer n=1; n <= 5; n++) {
			//Create Vendor Account
			Account vendorAccount = SPCC_TestData.createSPCCVendor('Test Account' + n);
			vendorAccountList.add(vendorAccount);
		}
		insert vendorAccountList;

		i=0;
		for(Account vendorAccount : vendorAccountList) {
			SPCC_Vendor_Account_Assignment__c vendorAccountAssignment = SPCC_TestData.createVAA(ewrList[i].Id, vendorAccount.Id);
			vendorAccountAssignmentList.add(vendorAccountAssignment);
			i++;
		}
		insert vendorAccountAssignmentList;

		for(Integer n=0; n < 5; n++) {
			//Create Purchase Order
			SPCC_Purchase_Order__c purchaseOrder = SPCC_TestData.createPO('123456789' + n, ewrList[n].Id, afeList[n].Id, vendorAccountList[n].Id, 
				null, 800, null, null);
			purchaseOrderList.add(purchaseOrder);
		}
		insert purchaseOrderList;

		for(Integer n=0; n < 5; n++) {
			SPCC_Line_Item__c lineItem = SPCC_TestData.createLineItem(purchaseOrderList[n].Id, costElementList[n].Id, 100, '0010', 'Test Line Item');
			lineItemList.add(lineItem);
		}
		insert lineItemList;

		for(Integer j=0; j < purchaseOrderList.Size(); j++) {
			//Create Service Entry Sheets
			SPCC_Service_Entry_Sheet__c ses = SPCC_TestData.createSES(purchaseOrderList[j].Id, lineItemList[j].Id, 100, Date.today(), '1234' + j);
			serviceEntrySheetList.add(ses);
		}
		insert serviceEntrySheetList;

		SPCC_Utilities.executeTriggerCode = true;
	}
	
	@isTest static void testController() {
		//Setup -- Query Setup Objects
		Map<Id, SPCC_Engineering_Work_Request__c> ewrMap = new Map<Id, SPCC_Engineering_Work_Request__c>([Select Id, Name
														  												  From SPCC_Engineering_Work_Request__c]);
		Map<Id, SPCC_Authorization_for_Expenditure__c> afeMap = new Map<Id, SPCC_Authorization_for_Expenditure__c>();
		for(SPCC_Authorization_for_Expenditure__c afe :	[Select Id, Name, Engineering_Work_Request__c
														 From SPCC_Authorization_for_Expenditure__c]) {
			afeMap.put(afe.Engineering_Work_Request__c, afe);
		}
		Map<Id, SPCC_Cost_Element__c> costElementMap = new Map<Id, SPCC_Cost_Element__c>();
		for(SPCC_Cost_Element__c costElement : [Select Id, Name, AFE__c, GL_Number__c
												From SPCC_Cost_Element__c]) {
			costElementMap.put(costElement.AFE__c, costElement);
		}
		Map<String, String> purchaseOrderNumberMap = new Map<String, String>();
		for(SPCC_Line_Item__c lineItem : [Select Id, Name, Purchase_Order__r.Purchase_Order_Number__c, Cost_Element__r.GL_Number__c 
										 From SPCC_Line_Item__c]) {
			purchaseOrderNumberMap.put(lineItem.Cost_Element__r.GL_Number__c, lineItem.Purchase_Order__r.Purchase_Order_Number__c);
		}



		Test.startTest();
			//Set Page
			PageReference pref = Page.SPCC_ServiceEntrySheetList;
			Test.setCurrentPage(pref);

			//Create Controller
			SPCC_ServiceEntrySheetListCtrl customListCtrl = new SPCC_ServiceEntrySheetListCtrl();

			//Test Options
			System.assertEquals(ewrMap.size()+1, customListCtrl.ewrOptions.size());
			customListCtrl.pageFilter.ewrId = ewrMap.values()[0].Id;
			System.assertEquals(customListCtrl.afeOptions.size(), 2);
			System.assertEquals(customListCtrl.vendorOptions.size(), 2);
			System.assertEquals(customListCtrl.glOptions.size(), new List<SPCC_GL_Code_Entry__mdt>([Select DeveloperName, MasterLabel, Category__c, 
                                                                                  GL_Number__c
                                                                           From SPCC_GL_Code_Entry__mdt
                                                                           Where Category__c <> :SPCC_Utilities.GL_CODE_CATEGORY_OTHER]).size()+1);
			System.assertEquals(customListCtrl.costCodeOptions.size(), 1);

			//Test Initial View
			List<SPCC_Service_Entry_Sheet__c> allServiceEntrySheets = [Select Id, Name, Amount__c, Date_Entered__c
																	   From SPCC_Service_Entry_Sheet__c];
			System.debug('customListCtrl.currentPageServiceEntrySheets: ' + customListCtrl.currentPageServiceEntrySheets);
			System.debug('allServiceEntrySheets.size(): ' + allServiceEntrySheets.size());
			System.assertEquals(customListCtrl.currentPageServiceEntrySheets.size(), allServiceEntrySheets.size());

			//Test Filtered View
			customListCtrl.pageFilter.ewrId = ewrMap.values()[0].Id;
			customListCtrl.pageFilter.afeId = afeMap.get(customListCtrl.pageFilter.ewrId).Id;
			customListCtrl.pageFilter.glNumber = costElementMap.get(customListCtrl.pageFilter.afeId).GL_Number__c;
			customListCtrl.pageFilter.poNumber = purchaseOrderNumberMap.get(customListCtrl.pageFilter.glNumber);
			customListCtrl.refreshData();
			System.assertEquals(customListCtrl.currentPageServiceEntrySheets.size(), 1);

			//Cover Pagination functions
			customListCtrl.populateDependentOptionsAction();
			customListCtrl.first();
			customListCtrl.previous();
			customListCtrl.next();
			customListCtrl.last();
			System.assertEquals(customListCtrl.currentPageServiceEntrySheets.size(), 1);

			//Clear Filter
			customListCtrl.clearFilter();
			customListCtrl.refreshData();
			System.assertEquals(customListCtrl.currentPageServiceEntrySheets.size(), allServiceEntrySheets.size());

		Test.stopTest();
	}
	
}