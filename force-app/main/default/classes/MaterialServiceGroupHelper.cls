public with sharing class MaterialServiceGroupHelper {
    public static Boolean flag = true;
    public static List<PREP_Material_Service_Group__c> newMaterialSeriveGroupList = new List<PREP_Material_Service_Group__c>();
    public static List<PREP_Material_Service_Group__c> oldMaterialSeriveGroupList = new List<PREP_Material_Service_Group__c>();
    public static Map<Id, PREP_Material_Service_Group__c> newMaterialSeriveGroupMap = new Map<Id, PREP_Material_Service_Group__c>();
    public static Map<Id, PREP_Material_Service_Group__c> oldMaterialSeriveGroupMap = new Map<Id, PREP_Material_Service_Group__c>();

    public static void updateInitiatives(){
        List<PREP_Mat_Serv_Group_Baseline_Allocation__c> baselineAllocations = [ Select Id, Baseline_Id__c,
                                                        Baseline_Id__r.Initiative_Id__c, Material_Service_Group__c from
                                                        PREP_Mat_Serv_Group_Baseline_Allocation__c where
                                                        Material_Service_Group__c IN :trigger.new
                                                        AND Baseline_Id__c != null
                                                        AND Baseline_Id__r.Initiative_Id__c != null ]; 
            
        Map<Id, Set<Id>> taxonomyToInitiativesId = new Map<Id, Set<Id>>();
        for( PREP_Mat_Serv_Group_Baseline_Allocation__c bas : baselineAllocations ){
            Set<Id> iniIds = new Set<Id>();
            if( taxonomyToInitiativesId.containsKey( bas.Material_Service_Group__c )){
                iniIds = taxonomyToInitiativesId.get( bas.Material_Service_Group__c );
            }
            iniIds.add( bas.Baseline_Id__r.Initiative_Id__c );
            taxonomyToInitiativesId.put( bas.Material_Service_Group__c, iniIds );
        }
        
        List<PREP_Initiative__c> initiativesToBeUpdated = new List<PREP_Initiative__c>();
        for( PREP_Material_Service_Group__c mat : newMaterialSeriveGroupList ){
            if( taxonomyToInitiativesId.containsKey( mat.Id )){
                for( Id ini : taxonomyToInitiativesId.get( mat.Id )){
                    Boolean flag = false;
                    PREP_Initiative__c init = new PREP_Initiative__c( Id = ini );
                    if( mat.Category_Link__c != oldMaterialSeriveGroupMap.get( mat.Id ).Category_Link__c
                        || mat.Sub_Category__c != oldMaterialSeriveGroupMap.get( mat.Id ).Sub_Category__c ){                
                        init.Category__c = mat.Category_Link__c;
                        init.Sub_Category__c = mat.Sub_Category__c;
                        flag = true;
                    }
                    if( mat.Discipline_Link__c != oldMaterialSeriveGroupMap.get( mat.Id ).Discipline_Link__c ){
                        init.Discipline__c = mat.Discipline_Link__c;
                        flag = true;
                    }
                    if( flag )
                        initiativesToBeUpdated.add( init );
                }
            }
        }
        
        if( initiativesToBeUpdated.size() > 0 )
            update initiativesToBeUpdated;
    }
}