@isTest
global class RaM_OpenCaseMockTest implements WebServiceMock {
    global void doInvoke(
    Object stub, Object request, Map < String, Object > response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
    
        RaM_salesforceComPm.DT_SFDC_TicketQuery response_x=new RaM_salesforceComPm.DT_SFDC_TicketQuery();
        // start - specify the response you want to send
        RaM_salesforceComPm.CASE_element  response_x1 = new RaM_salesforceComPm.CASE_element ();
        response_x1.CaseNumber = 'test';
        response_x1.LocationNumber_xc = 'test';
        List<RaM_salesforceComPm.CASE_element > response_x1Lst= new list<RaM_salesforceComPm.CASE_element >();
        
        // end
        response_x1Lst.add(response_x1);
        response_x.CASE_x=response_x1Lst;
        response.put('response_x', response_x);
    }
}