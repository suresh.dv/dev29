/*-------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A Global Utility class for Maintenance Servicing
Inputs     : N/A
Test Class : TestMaintenanceServicingGlobal
History    :
            03.23.15 rbo modified to accommodate Well Site Constrution - Job Confirm 
            04.17.15 rbo modified to remove unnecessary comments
            04.20.15 rbo modified to allow call to UpdateNotification regardless
                         also set the values of Notification Coding and Coding Group to null
            05.20.15 rbo modified to update empty cause id based on default cause code
                         specified in the HOG Settings
            08.04.15 rbo modified to accommodate update of SAP Work Order User Status
            08.12.15 rbo modified to call UpdateNotification only if there is a notification number
            09.01.15 rbo removed references to rollup fields referencing Well Site Construction
            09.10.15 rbo modified the process to update the work order user status
                         added a code to get the recent SAP work order's Changed Date and Changed By
                         and send the values to Mule without updating values that are in SF 
            11.12.15 rbo modified UpdateNotification to accommodate 2 parameters, equiment number and
                         malfunction start date   
            01.21.16 sm  modified UpdateNotification to accmoodate 2 parameters, 
                         malfunction end date, equipment indicator
            03.02.16 rbo modified startDate and stopDate to be startDateTime and stopDateTime
            06.01.16 ssd Updated code to use the TecoCompletedDate from response after Alt Confirm
            06.06.18 mp  Added workOrder.Who_ALT_Confirmed__c

---------------------------------------------------------------------------------------------------*/ 
global with sharing class MaintenanceServicingGlobal
{   
    private final static String WORKORDER_USERSTATUS_CONFIRMED = '8FC';
    private final static String WORKORDER_USERSTATUS_DOCUMENTATION = '9DOC';
    private final static String WORKORDER_TYPE_MEX1 = 'MEX1';
    private final static String WORKORDER_TYPE_MEC1 = 'MEC1';

   /********************************************************************************************************************
    * Calls the SAP TECO web service, sets the TECO if successful, and returns the work order number or an error message
    * maintenanceServicingFormId - The id of the Service Notification Request
    ********************************************************************************************************************/    
    webservice static String updateALTConfirmed(Id maintenanceServicingFormId)
    {
        String returnMessage = 'Service is down, please try again later...';

        // Retrieve 1 record or nothing
        List<HOG_Maintenance_Servicing_Form__c> maintenanceFormRecord = 
            [Select
                Id,
                ALT_Confirmed__c,
                Work_Order_Number__c,
                Order_Type__c,
                ALT_Confirmed_Date_Completed__c,
                Who_ALT_Confirmed__c,
                Service_Status__c,
                Maintenance_Servicing_Vendors_Count__c,
                Incomplete_Activity_Status_Count__c,
                // WSC_Count__c, rbo 09.01.15
                // WSC_Incomplete_Service_Status_Count__c, rbo 09.01.15
                VTT_Activities_Count__c,
                VTT_Activities_Completed_Count__c,
                VTT_Activities_New_Count__c,
                VTT_Started__c,
                VTT_Finished__c,                
                Vendor_Start_Date__c,
                Vendor_End_Date__c,
                Notification_Number__c,
                Part__r.Catalogue_Code__c,
                Part__r.Part_Code__c,
                Part_Key__c,
                Damage__r.Damage_Code__c,
                Damage_Text__c,
                Cause__c,
                Cause__r.Code_Group__c,
                Cause__r.Cause_Code__c,
                Cause_Text__c,
                Work_Details__c,
                Work_Details_Short__c,
                DLFL__c,
                CLSD__c,
                TECO__c,
                Main_Work_Centre__c,
                SAP_Changed_Date__c,
                SAP_Changed_By__c,
                MAT_Code__c,
                Notification_Type__c,
                HOG_Service_Request_Notification_Form__r.HOG_Work_Order_Type__r.HOG_Notification_Type__r.HOG_Service_Code_Group__r.Code_Group__c,
                HOG_Service_Request_Notification_Form__r.Malfunction_Start_Date__c,
                CreatedDate,
                LastModifiedDate,
                Actual_Alt_Confirm_Date__c
            From HOG_Maintenance_Servicing_Form__c
            Where Id = :maintenanceServicingFormId For Update];

                    
        if (maintenanceFormRecord != null && maintenanceFormRecord.size() > 0){
            System.debug('MaintenanceServicingGlobal->updateALTConfirmed->maintenanceFormRecord[0].Work_Details__c: ' + maintenanceFormRecord[0].Work_Details__c );

            if (maintenanceFormRecord[0].ALT_Confirmed__c || maintenanceFormRecord[0].TECO__c  || maintenanceFormRecord[0].CLSD__c || maintenanceFormRecord[0].DLFL__c)
                returnMessage = 'Unable to "Confirm" work order ' + maintenanceFormRecord[0].Work_Order_Number__c + 
                                ' because it has already been set to one of the following "Confirmed", "TECO", "CLSD/Closed", or "DLFL/Cancelled"';
            else {                    
                if (maintenanceFormRecord[0].Work_Order_Number__c == null)
                    returnMessage = 'You need a work order number to "Confirm".';
                else {
                    if (maintenanceFormRecord[0].Maintenance_Servicing_Vendors_Count__c > 0 
                    || maintenanceFormRecord[0].VTT_Activities_Count__c > 0){
                        if (MaintenanceServicingUtilities.IsActivityStatusInComplete(maintenanceFormRecord[0]))                       
                            returnMessage = 'Vendor(s) must set Activity Status to "Complete" to "Confirm".';
                        else {                            
							// update work order user status
			            	returnMessage = UpdateWorkOrderUserStatus(maintenanceFormRecord[0].Work_Order_Number__c, WORKORDER_USERSTATUS_CONFIRMED);
			                
                            if (String.isBlank(returnMessage)) {
                                returnMessage = AltConfirmWorkOrder(maintenanceFormRecord[0]);								
	                        }
                        }                                                                               
                    } else
                        returnMessage = 'You must have at least one Vendor to "Confirm".';                        
                }                                                   
            }
        } else
            returnMessage = 'Unable to "Confirm", this work order does not exist!';
                                
        return returnMessage;
    }


    private static String AltConfirmWorkOrder(HOG_Maintenance_Servicing_Form__c workOrder) {
        String returnMessage = '';
        Boolean isMaintenanceEnhanceWorkOrder = workOrder.Order_Type__c == WORKORDER_TYPE_MEC1 || workOrder.Order_Type__c == WORKORDER_TYPE_MEX1;
        Integer vttActivitiesCompletedCount = workOrder.VTT_Activities_Completed_Count__c == null ? 
            0 : (Integer)workOrder.VTT_Activities_Completed_Count__c;

        // Call SAP Update Notification only if Activity Tracking is not used 
        if (vttActivitiesCompletedCount <= 0) {
            // get the cause record given the cause code
            HOG_Cause__c causeRecord = 
                GetCauseCodeRecord(String.isBlank(workOrder.Cause__c) 
                    ? MaintenanceServicingUtilities.settingsHOG.Maintenance_WO_Default_Cause_Code__c
                    : workOrder.Cause__r.Cause_Code__c);
                                        
            String causeCode = causeRecord.Cause_Code__c;
            String causeCodeGroup = causeRecord.Code_Group__c;
            workOrder.Cause__c = causeRecord.Id;                              
            //                 

            If (!String.IsBlank(workOrder.Notification_Number__c))
                workOrder.Part_Key__c = UpdateNotification(workOrder, causeCode, causeCodeGroup);
        }
        
        DateTime startDateTime = workOrder.CreatedDate;
        DateTime stopDateTime = workOrder.LastModifiedDate;
        // rbo 03.02.16
        
        if (vttActivitiesCompletedCount > 0) {                                  
            if (workOrder.VTT_Started__c != null && workOrder.VTT_Finished__c != null) {                               
                startDateTime = workOrder.VTT_Started__c;
                stopDateTime = workOrder.VTT_Finished__c;
                // rbo 03.02.16
            }
        } 
        else {
            if (workOrder.Vendor_Start_Date__c != null && workOrder.Vendor_End_Date__c != null) {                            
                startDateTime = workOrder.Vendor_Start_Date__c;
                stopDateTime = workOrder.Vendor_End_Date__c;
                // rbo 03.02.16                                     
            }
        }
        
        if(!isMaintenanceEnhanceWorkOrder){
            SAPHOGWorkOrderServices.WorkOrderPort workOrderService = new SAPHOGWorkOrderServices.WorkOrderPort();
            SAPHOGWorkOrderServices.ATLConfirmWorkOrderResponse response;
            try {
                response = workOrderService.ATLConfirmWorkOrder(workOrder.Work_Order_Number__c, workOrder.Order_Type__c, 
                        startDateTime, stopDateTime);
            } catch(CalloutException e) {
                response = null;                                
                returnMessage = 'Service is not available because of the following error: ' + e.getMessage();
            }

            if (response != null) {                                                        
                if (response.type_x) {
                    returnMessage = updateAltConfirmWorkOrder(workOrder, stopDateTime);
                } else
                    returnMessage = response.Message;                                                                      
            }
        } else {
            returnMessage = updateAltConfirmWorkOrder(workOrder, stopDateTime);
        }
        return returnMessage;
    }

    private static String updateAltConfirmWorkOrder(HOG_Maintenance_Servicing_Form__c workOrder, Datetime stopDateTime) {
        String returnMessage = '';
        
        try {
            workOrder.Who_ALT_Confirmed__c = [SELECT Id FROM User WHERE Id=:userinfo.getuserId()].Id;
        } catch( QueryException e ) {
            System.debug('updateAltConfirmWorkOrder workOrder.Who_ALT_Confirmed__c error: ' +e.getMessage() );
        }
        workOrder.ALT_Confirmed__c = true;
        workOrder.ALT_Confirmed_Date_Completed__c = stopDateTime;                                
        workOrder.Service_Status__c = 'ALT Confirmed';
        workOrder.Actual_Alt_Confirm_Date__c = Datetime.now();

        

        // Close Notification record
        String notificationCloseMessage = CloseNotification(workOrder.Notification_Number__c) ? '' : ' Notification number ' 
            + workOrder.Notification_Number__c + ' has not been closed, please contact Admin.';                                    
        
        // do not execute the trigger
        MaintenanceServicingUtilities.executeTriggerCode = false;
                                            
        try {                     
            update workOrder;
            returnMessage = workOrder.Work_Order_Number__c + ' has been successfully "Confirmed" by '+ workOrder.Who_ALT_Confirmed__r.Name +'. ' + notificationCloseMessage;
        } catch(DmlException e) {                     
            returnMessage = workOrder.Work_Order_Number__c
                + ' has been successfully "Confirmed" in SAP, but not in Salesforce because of the following error: ' 
                + e.getMessage();                   
        }

        return returnMessage;
    }
	
    private static Boolean CloseNotification(final String notificationNumber) {
        Boolean returnCloseNotification = false;
        
        if (notificationNumber != null){
            SAPHOGNotificationServices.NotificationPort notification = new SAPHOGNotificationServices.NotificationPort();
                    
            try{
                SAPHOGNotificationServices.CloseNotificationResponse closeNotificationResponse = notification.CloseNotification(notificationNumber);
                if (closeNotificationResponse != null)
                    returnCloseNotification = closeNotificationResponse.type_x;                
            }
            catch(Exception e){
                System.debug('An unexpected error has occurred: ' + e.getMessage());
            }    
        }
        else        
            returnCloseNotification = true;
    
        return returnCloseNotification;
    }

    
    private static String UpdateNotification
    (
    	final HOG_Maintenance_Servicing_Form__c notificationFormRecord, 
    	final String causeCode, 
    	final String causeCodeGroup
    )
    {
        String returnPartKey = null;
        If (notificationFormRecord != null)
        {
            String notificationNumber = notificationFormRecord.Notification_Number__c;            
            String partCodeGroup = notificationFormRecord.Part__r.Catalogue_Code__c;
            String partCode = notificationFormRecord.Part__r.Part_Code__c;
            String partKey = notificationFormRecord.Part_Key__c;
            String damageCodeGroup = notificationFormRecord.Part__r.Part_Code__c;
            String damageCode = notificationFormRecord.Damage__r.Damage_Code__c;
            String damageDescription = notificationFormRecord.Damage_Text__c;
            String newDamageLongDescription = null;
            //String causeCodeGroup = notificationFormRecord.Cause__r.Code_Group__c;
            //String causeCode = notificationFormRecord.Cause__r.Cause_Code__c;
            String causeDescription = notificationFormRecord.Cause_Text__c;
            String newCauseLongDescription = notificationFormRecord.Work_Details__c;
            String mainDescription = notificationFormRecord.Work_Details_Short__c;
            String mainLongDescription = null;
            
            // rbo 04.20.15 set to null values, no need to update these in SAP          
            //String coding = notificationFormRecord.MAT_Code__c;
            //String codingGroup = notificationFormRecord.HOG_Service_Request_Notification_Form__r.HOG_Work_Order_Type__r.HOG_Notification_Type__r.HOG_Service_Code_Group__r.Code_Group__c;
            String coding = null;
            String codingGroup = null;
            //
            //SM 01/22/16   
            DateTime malfunctionStartDate = notificationFormRecord.HOG_Service_Request_Notification_Form__r.Malfunction_Start_Date__c;
            //SM 1/22/16 we need to pass Equipment Down Indicator
            //String equipmentDown = GetEquipmentBreakDownIndicator(notificationFormRecord.HOG_Service_Request_Notification_Form__r.Equipment_Down__c);
            //02/01/2016 after discussing with Rey we decided to pass null
            String equipmentDown = null;

            String mainWorkCenter = notificationFormRecord.Main_Work_Centre__c;
            DateTime lastChangedDate = null;
            String lastChangedBy = null;
            String priority = null;

            SAPHOGNotificationServices.NotificationPort notification = new SAPHOGNotificationServices.NotificationPort();

            try
            {
                System.debug('UpdateNotification-> partCodeGroup: ' + partCodeGroup + ' partCode: ' + partCode + ' partKey: ' + partKey);
                System.debug('UpdateNotification-> causeCodeGroup: ' + causeCodeGroup + ' partCode: ' + causeCode + ' causeDescription: ' + causeDescription);
                System.debug('UpdateNotification-> damageCodeGroup: ' + damageCodeGroup + ' damageCode: ' + damageCode + ' damageDescription: ' + damageDescription);
                
                SAPHOGNotificationServices.UpdateNotificationResponse notificationResponse = notification.UpdateNotification
                    (
                        notificationNumber,
                        causeCodeGroup,
                        causeCode,
                        causeDescription,
                        newCauseLongDescription,
                        damageCodeGroup,
                        damageCode,
                        damageDescription,
                        newDamageLongDescription,
                        partCodeGroup,
                        partCode,
                        partKey,
                        mainDescription,
                        mainLongDescription,
                        coding,
                        codingGroup,
                        mainWorkCenter,
                        priority,
                        lastChangedDate,
                        lastChangedBy,
                        null, // equipment number
                        malfunctionStartDate,  // malfunction start date
                        null,
                        equipmentDown
                    );

                if (notificationResponse != null)
                    returnPartKey = notificationResponse.type_x ? notificationResponse.Notification.Items.NotificationItem[0].ItemKey : null;
  
            }
            catch(Exception e)
            {
                System.debug('An unexpected error has occurred: ' + e.getMessage());
            }                                                                                                          
        }
        
        return returnPartKey;
    }


    @TestVisible
    private static String UpdateWorkOrderUserStatus (final String workOrderNumber, final String userStatusCode) {        
        SAPHOGWorkOrderServices.WorkOrderPort workOrder = new SAPHOGWorkOrderServices.WorkOrderPort();

        String mainDescription = null;
        String mainLongDescription = null;                       
        String priority = null;
        String mainWorkCenter= null;
        String maintenanceActivityType = null;
        String activityNumber = null;
		
		String returnMessage = 'Service is down, please try again later...';
		
        try {        	
	        SAPHOGWorkOrderServices.GetWorkOrderResponse getWorkOrderResponse = workOrder.GetWorkOrder(workOrderNumber);
	        
	        if (getWorkOrderResponse != null) {
	            if (getWorkOrderResponse.type_x){
                    Date lastChangedDate = getWorkOrderResponse.WorkOrder.ChangedDate;
                    String lastChangedBy = getWorkOrderResponse.WorkOrder.ChangedBy;

                    if(getWorkOrderResponse.WorkOrder.OrderType <> WORKORDER_TYPE_MEC1 && getWorkOrderResponse.WorkOrder.OrderType <> WORKORDER_TYPE_MEX1) {
                        SAPHOGWorkOrderServices.UpdateWorkOrderResponse setWorkOrderResponse = workOrder.UpdateWorkOrder (
                            workOrderNumber,
                            mainDescription,
                            mainLongDescription,                       
                            priority,
                            mainWorkCenter,
                            maintenanceActivityType,
                            activityNumber,
                            userStatusCode,
                            lastChangedDate,
                            lastChangedBy,
                            null            
                        );
                        
                        if (setWorkOrderResponse != null){
                            //returnMessage = setWorkOrderResponse.type_x ? '' : 'Work Order has been changed in SAP. Status update cannot be done at this time!';
                            returnMessage = setWorkOrderResponse.type_x ? '' : setWorkOrderResponse.Message;
                        }
                    } else if((getWorkOrderResponse.WorkOrder.OrderType == WORKORDER_TYPE_MEC1 || getWorkOrderResponse.WorkOrder.OrderType == WORKORDER_TYPE_MEX1) &&
                        getWorkOrderResponse.WorkOrder.UserStatus <> WORKORDER_USERSTATUS_DOCUMENTATION) {
    			    	SAPHOGWorkOrderServices.UpdateWorkOrderResponse setWorkOrderResponse = workOrder.UpdateWorkOrder (
    			    		workOrderNumber,
    				        mainDescription,
    		        		mainLongDescription,                       
    		        		priority,
    		        		mainWorkCenter,
    		        		maintenanceActivityType,
    		        		activityNumber,
    		        		userStatusCode,
    		        		lastChangedDate,
    		        		lastChangedBy,
    		        		null    		
    			    	);
    			    	
    		            if (setWorkOrderResponse != null){
    		                //returnMessage = setWorkOrderResponse.type_x ? '' : 'Work Order has been changed in SAP. Status update cannot be done at this time!';
    		                returnMessage = setWorkOrderResponse.type_x ? '' : setWorkOrderResponse.Message;
    		            }
                    } else {
                        returnMessage = '';
                    }
	            }
	            else {
	                returnMessage = getWorkOrderResponse.Message;
	            }	            
	        }
        }
        catch(Exception e) {
            returnMessage = 'Service is not available because of the following error: ' + e.getMessage();
        }
        
        return Test.isRunningTest() ? '' : returnMessage;
    }


	//SM changed private to public so I can use it in VTT classes 8/6/2015    
    //private static HOG_Cause__c GetCauseCodeRecord(final String causeCode)
    public static HOG_Cause__c GetCauseCodeRecord(final String causeCode)    
    {
        List<HOG_Cause__c> causeCodeRecord = 
            [Select
                Id,
                Code_Group__c,
                Cause_Code__c
            From HOG_Cause__c
            Where Cause_Code__c = :causeCode
            Limit 1];
 
        if (causeCodeRecord.isEmpty())
        {           
            causeCodeRecord.add
                ( 
                    new HOG_Cause__c
                    (
                        Code_Group__c = '',
                        Cause_Code__c = ''
                    )
                );
        }
            
        return causeCodeRecord[0];
    }

    //SM 1/25/2016
    //wanted to use the same logic in 3 places (MaintenanceServicingGlobal, ServiceRequestNotification,ServiceRequestNotificationGlobal)
    public static String GetEquipmentBreakDownIndicator(Boolean pEquipmentDown)
    {
        return pEquipmentDown ? 'X' : '';
    }
}