@isTest
private class PREP_Reports_Test {
    static void createSampleTestData()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard SEP User (Platform)']; 
        
        User u = new User(Alias = 'standt', Email='PREP@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='PREP@testorg.com');
            
        insert u;
        
        PREP_Discipline__c disp = new PREP_Discipline__c();
        disp.Name = 'Disp 1';
        disp.Active__c = true;
        disp.SCM_Manager__c = u.Id;
        
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c();
        cat.Name = 'Category';
        cat.Active__c = true;
        cat.Discipline__c = disp.Id;
        
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c();
        sub.Name = 'SubCat';
        sub.Category__c = cat.Id;
        sub.Category_Manager__c = u.Id;
        sub.Category_Specialist__c = u.Id;
        sub.Active__c = true;
        
        insert sub;
        
        createProjectInitiative(u, disp, cat, sub);
        createCategoryInitiative(u, disp, cat, sub);
    }
    static void createProjectInitiative(User u, PREP_Discipline__c disp, PREP_Category__c cat, PREP_Sub_Category__c  sub)
    {
        //create Project Initative

        PREP_Initiative__c ini = new PREP_Initiative__c();
        ini.Initiative_Name__c = 'Test ini';
        ini.Discipline__c = disp.Id;
        ini.TIC__c = 10000;
        ini.PDE_Phase__c = '1';
        ini.Phase_4_Start_Date__c = Date.newInstance(2015, 9, 10);
        ini.Phase_4_Completion_Date__c = Date.newInstance(2015, 9, 10);
        ini.Status__c = 'Active';
        ini.Business_Unit__c = 'Atlantic Region';
        ini.Allocation_Level__c = 'Atlantic Region - Operations';
        ini.Project_Complexity__c = 'High';
        ini.X01_Segmentation_Team_Selection__c = Date.newInstance(2015, 5, 23);
        ini.X02_Business_Requirements__c = Date.newInstance(2015, 5, 24);
        ini.X03_Supplier_Market_Analysis__c = Date.newInstance(2015, 5, 25);
        ini.X04_Sourcing_Options__c = Date.newInstance(2015, 5, 26);
        ini.Strategy_Approved__c = Date.newInstance(2015, 5, 27); 
        ini.Project_End_Date__c = Date.newInstance(2015, 5, 27);
        ini.SCM_Manager__c = u.Id;
        ini.SCM_Team__c = 'HOG';
        ini.RecordTypeId = Schema.SObjectType.PREP_Initiative__c.getRecordTypeInfosByName().get('Projects Initiative').getRecordTypeId();
        
        insert ini; 
        
        
        PREP_Baseline__c baseline = new PREP_Baseline__c();
        baseline.P_Baseline_Spend_Dollar_To_Be_Awarded__c = 10000;
        baseline.MSA_Baseline_Spend_Dollar__c = 10000;
        baseline.Local_Baseline_Spend_Percent__c = 10;
        baseline.Local_Baseline_Savings_Percent__c = 10;
        baseline.Global_Baseline_Savings_Percent__c = 10;
        baseline.X05_Going_To_Market__c = Date.newInstance(2015, 5, 27);
        baseline.XCAC2_CAC_Award_Approval__c = Date.newInstance(2015, 5, 27);
        baseline.X07_Contract_Start_Up__c = Date.newInstance(2015, 5, 28);
        baseline.RecordTypeId = Schema.SObjectType.PREP_Baseline__c.getRecordTypeInfosByName().get('Projects Baseline').getRecordTypeId();
        baseline.Initiative_Id__c = ini.Id;
        insert baseline;
        
        PREP_Material_Service_Group__c MS = new PREP_Material_Service_Group__c();
        MS.Name = 'MS Name';
        MS.Material_Service_Group_Name__c = 'MS';
        MS.Sub_Category__c = sub.Id;
        MS.Category_Manager__c = u.Id;
        MS.SAP_Short_Text_Name__c = 'MS';
        MS.Active__c = true;
        MS.Type__c = 'Material';
        MS.Category_Specialist__c = u.Id;
        MS.Includes__c = 'MS';
        MS.Does_Not_Include__c = 'MS';
        insert MS;
    
        PREP_Mat_Serv_Group_Baseline_Allocation__c MSGBA = new PREP_Mat_Serv_Group_Baseline_Allocation__c();
        MSGBA.Material_Service_Group__c = MS.Id;
        MSGBA.Global_Sourcing__c = 'Yes';
        MSGBA.Planned_Spend_Allocation_Dollar__c = 10000;
       
        MSGBA.Baseline_Id__c = baseline.Id;
        MSGBA.Contract_Type__c = 'MSA';
        MSGBA.RecordTypeId = Schema.SObjectType.PREP_Mat_Serv_Group_Baseline_Allocation__c.getRecordTypeInfosByName().get('Projects Mat Group Baseline Allocation').getRecordTypeId();
        
        insert MSGBA;
        
        PREP_Business_Value_Submission__c BVS = new PREP_Business_Value_Submission__c();
        BVS.Mat_Serv_Group_Baseline_Allocation_Id__c = MSGBA.Id;
        BVS.Category__c = cat.Id;
        BVS.Discipline__c = disp.Id;
        BVS.Sub_Category__c = sub.Id;
        BVS.Initiative_Id__c = ini.Id;
        BVS.SCM_Manager__c = u.Id;
        BVS.Specialist_Rep__c = u.Id;
        BVS.Contract_Purchase_Approve_Date__c = Date.newInstance(2015, 4, 10);
        //BVS.Local_Sourcing__c = 10;
        //BVS.Global_Sourcing__c = 90;
        BVS.Short_Term_Interests_Rate__c = 1.50;
        BVS.RecordTypeId = Schema.SObjectType.PREP_Business_Value_Submission__c.getRecordTypeInfosByName().get('Cat-Project Business Value Submission').getRecordTypeId();
        BVS.Contract_Start_Date__c = Date.newInstance(2015, 4, 10);
        BVS.Contract_End_Date__c = Date.newInstance(2015, 12, 10);
        BVS.Awarded_Spend__c = 100000000;
        BVS.Rate_Reduction_Increase__c = 100000000;
       
        BVS.Status__c = 'Approved';
        insert BVS;
        
        List<PREP_Mat_Serv_Group_Planned_Schedule__c> msguaList = [SELECT Id FROM PREP_Mat_Serv_Group_Planned_Schedule__c WHERE Mat_Serv_Group_Baseline_Allocation_Id__c = :MSGBA.Id]; 
  
        PREP_Mat_Serv_Group_Planned_Schedule__c sche = msguaList[0];
        sche.Acquisition_Cycle_Type__c = 'Standard 12';
        sche.R_A_S_Date__c = Date.newInstance(2015, 9, 9);
        sche.Critical_Vendor_Data_Receipt_1_Weeks__c = 1;
        sche.Critical_Vendor_Data_Receipt_2_Weeks__c = 1;
        sche.MFG_Lead_Time_Weeks__c = 1;
        sche.RecordTypeId = Schema.SObjectType.PREP_Mat_Serv_Group_Planned_Schedule__c.getRecordTypeInfosByName().get('Mat Group Baseline Schedule').getRecordTypeId();
        
        update sche;
        
        PREP_Mat_Serv_Group_Baseline_Allocation__c MSGBAServ = new PREP_Mat_Serv_Group_Baseline_Allocation__c();
        MSGBAServ.Material_Service_Group__c = MS.Id;
        MSGBAServ.Global_Sourcing__c = 'Yes';
        MSGBAServ.Planned_Spend_Allocation_Dollar__c = 10000;
        MSGBAServ.Planned_Savings_Allocation_Dollar__c = 10000;
        MSGBAServ.Baseline_Id__c = baseline.Id;
        MSGBAServ.RecordTypeId = Schema.SObjectType.PREP_Mat_Serv_Group_Baseline_Allocation__c.getRecordTypeInfosByName().get('Projects Serv Group Baseline Allocation').getRecordTypeId();
        MSGBAServ.Contract_Type__c = 'One Off';
        insert MSGBAServ;
        
        msguaList = [SELECT Id FROM PREP_Mat_Serv_Group_Planned_Schedule__c WHERE Mat_Serv_Group_Baseline_Allocation_Id__c = :MSGBAServ.Id];
        PREP_Mat_Serv_Group_Planned_Schedule__c scheServ = msguaList[0];
        scheServ.Acquisition_Cycle_Type__c = 'Standard 12';
        scheServ.R_A_S_Date__c = Date.newInstance(2015, 9, 9);
        scheServ.Critical_Vendor_Data_Receipt_1_Weeks__c = 1;
        scheServ.Critical_Vendor_Data_Receipt_2_Weeks__c = 1;
        scheServ.MFG_Lead_Time_Weeks__c = 1;
        scheServ.RecordTypeId = Schema.SObjectType.PREP_Mat_Serv_Group_Planned_Schedule__c.getRecordTypeInfosByName().get('Serv Group Baseline Schedule').getRecordTypeId();
        
        update scheServ;
      
        
        PREP_Mat_Serv_Group_Update__c matUpdate = new PREP_Mat_Serv_Group_Update__c();
        matUpdate.RecordTypeId = Schema.SObjectType.PREP_Mat_Serv_Group_Update__c.getRecordTypeInfosByName().get('Mat Group Update').getRecordTypeId();
        matUpdate.Planned_1_Segmentation_Team_Selection__c = Date.newInstance(2015, 2, 3);
        matUpdate.Forecast_1_Segmentation_Team_Selection__c = Date.newInstance(2015, 2, 3);

        matUpdate.Planned_2_Business_Requirements__c = Date.newInstance(2015, 2, 3);
        matUpdate.Planned_3_Supplier_Market_Analysis__c = Date.newInstance(2015, 2, 3);
        matUpdate.Planned_4_Sourcing_Options__c = Date.newInstance(2015, 2, 3);
        matUpdate.Planned_Strategy_Approval__c = Date.newInstance(2015, 2, 3);

        matUpdate.Planned_MRQ_Received__c = Date.newInstance(2015, 2, 3);
        matUpdate.Planned_Bid_Request_Issued_RFQ__c = Date.newInstance(2015, 2, 6);
        matUpdate.Planned_Bids_Received__c = Date.newInstance(2015, 3, 6);
        matUpdate.Planned_Commercial_Evaluations_Received__c = Date.newInstance(2015, 3, 27);
        matUpdate.Planned_Technical_Evaluations_Received__c = Date.newInstance(2015, 3, 27);
        matUpdate.Planned_Bid_Tab_Finalized__c = Date.newInstance(2015, 3, 29);
        
        
        matUpdate.Planned_Decision_Sum_Issued_Approval__c = Date.newInstance(2015, 4, 5);
        matUpdate.Planned_Decision_Summary_Approved__c = Date.newInstance(2015, 4, 12);
        matUpdate.Planned_MRP_Received__c = Date.newInstance(2015, 4, 19);
        matUpdate.Planned_P_O_Issued__c = Date.newInstance(2015, 4, 28);
        matUpdate.Planned_Vendor_Init_Drawing_Submittal__c = Date.newInstance(2015, 5, 5);
        matUpdate.Planned_Drawings_Approved__c = Date.newInstance(2015, 5, 12);
        matUpdate.Planned_Ship_Date__c = Date.newInstance(2015, 5, 19);
        matUpdate.Planned_ETA__c = Date.newInstance(2015, 5, 26);
        matUpdate.R_A_S_Date__c = Date.newInstance(2015, 9, 9);

        matUpdate.Forecast_Business_Requirements__c = 1;
        matUpdate.Forecast_Supplier_Market_Analysis__c = 1;
        matUpdate.Forecast_Sourcing_Options__c = 1;
        matUpdate.Forecast_Strategy_Approval_Cycle__c = 1;
        matUpdate.Forecast_MRQ_To_Be_Received__c = 1;
        matUpdate.Forecast_RFQ_Prep__c = 1;
        matUpdate.Forecast_Bid_Period__c = 1;
        matUpdate.Forecast_Bid_Evaluation_1__c = 1;
        matUpdate.Forecast_Bid_Evaluation_2__c = 1;
        matUpdate.Forecast_Bid_Evaluation_3__c = 1;
        matUpdate.Forecast_Bid_Evaluation_4__c = 1;
        
        matUpdate.Forecast_Husky_Approval__c = 1;
        matUpdate.Forecast_MRP_Prep__c = 1;
        matUpdate.Forecast_PO_Prep_Issued__c = 1;
        matUpdate.Forecast_Critical_Vendor_Data_Receipt1_W__c = 1;
        matUpdate.Forecast_Critical_Vendor_Data_Receipt2_W__c = 1;
        matUpdate.Forecast_MFG_Lead_Time_Weeks__c = 1;
        matUpdate.Forecast_Shipping_Duration__c = 1;
        
        matUpdate.Actual_1_Segmentation_Team_Selection__c = null;
        matUpdate.Actual_2_Business_Requirements__c = null;
        matUpdate.Actual_3_Supplier_Market_Analysis__c = null;
        matUpdate.Actual_4_Sourcing_Options__c = null;
        matUpdate.Actual_Strategy_Approval__c = null;
        matUpdate.Actual_MRQ_Received__c = null;
        matUpdate.Completed__c = true;
        matUpdate.Mat_Serv_Group_Planned_Schedule_Id__c = sche.Id;
        insert matUpdate; 
     
        PREP_Mat_Serv_Group_Update__c servUpdate = new PREP_Mat_Serv_Group_Update__c();
        servUpdate.RecordTypeId = Schema.SObjectType.PREP_Mat_Serv_Group_Update__c.getRecordTypeInfosByName().get('Serv Group Update').getRecordTypeId();
        servUpdate.Planned_1_Segmentation_Team_Selection__c = Date.newInstance(2015, 2, 3);
        servUpdate.Forecast_1_Segmentation_Team_Selection__c = Date.newInstance(2015, 2, 3);

        servUpdate.Planned_2_Business_Requirements__c = Date.newInstance(2015, 2, 3);
        servUpdate.Planned_3_Supplier_Market_Analysis__c = Date.newInstance(2015, 2, 3);
        servUpdate.Planned_4_Sourcing_Options__c = Date.newInstance(2015, 2, 3);
        servUpdate.Planned_Strategy_Approval__c = Date.newInstance(2015, 2, 3);

        servUpdate.Planned_MRQ_Received__c = Date.newInstance(2015, 2, 3);
        servUpdate.Planned_Bid_Request_Issued_RFQ__c = Date.newInstance(2015, 2, 6);
        servUpdate.Planned_Bids_Received__c = Date.newInstance(2015, 3, 6);
        servUpdate.Planned_Commercial_Evaluations_Received__c = Date.newInstance(2015, 3, 27);
        servUpdate.Planned_Technical_Evaluations_Received__c = Date.newInstance(2015, 3, 27);
        servUpdate.Planned_Bid_Tab_Finalized__c = Date.newInstance(2015, 3, 29);
        
        
        servUpdate.Planned_Decision_Sum_Issued_Approval__c = Date.newInstance(2015, 4, 5);
        servUpdate.Planned_Decision_Summary_Approved__c = Date.newInstance(2015, 4, 12);
        servUpdate.Planned_MRP_Received__c = Date.newInstance(2015, 4, 19);
        servUpdate.Planned_P_O_Issued__c = Date.newInstance(2015, 4, 28);
        servUpdate.Planned_Vendor_Init_Drawing_Submittal__c = Date.newInstance(2015, 5, 5);
        servUpdate.Planned_Drawings_Approved__c = Date.newInstance(2015, 5, 12);
        servUpdate.Planned_Ship_Date__c = Date.newInstance(2015, 5, 19);
        servUpdate.Planned_ETA__c = Date.newInstance(2015, 5, 26);
        servUpdate.R_A_S_Date__c = Date.newInstance(2015, 9, 9);

        servUpdate.Forecast_Business_Requirements__c = 1;
        servUpdate.Forecast_Supplier_Market_Analysis__c = 1;
        servUpdate.Forecast_Sourcing_Options__c = 1;
        servUpdate.Forecast_Strategy_Approval_Cycle__c = 1;
        servUpdate.Forecast_MRQ_To_Be_Received__c = 1;
        servUpdate.Forecast_RFQ_Prep__c = 1;
        servUpdate.Forecast_Bid_Period__c = 1;
        servUpdate.Forecast_Bid_Evaluation_1__c = 1;
        servUpdate.Forecast_Bid_Evaluation_2__c = 1;
        servUpdate.Forecast_Bid_Evaluation_3__c = 1;
        servUpdate.Forecast_Bid_Evaluation_4__c = 1;
        
        servUpdate.Forecast_Husky_Approval__c = 1;
        servUpdate.Forecast_MRP_Prep__c = 1;
        servUpdate.Forecast_PO_Prep_Issued__c = 1;
        servUpdate.Forecast_Critical_Vendor_Data_Receipt1_W__c = 1;
        servUpdate.Forecast_Critical_Vendor_Data_Receipt2_W__c = 1;
        servUpdate.Forecast_MFG_Lead_Time_Weeks__c = 1;
        servUpdate.Forecast_Shipping_Duration__c = 1;
        
        servUpdate.Actual_1_Segmentation_Team_Selection__c = null;
        servUpdate.Actual_2_Business_Requirements__c = null;
        servUpdate.Actual_3_Supplier_Market_Analysis__c = null;
        servUpdate.Actual_4_Sourcing_Options__c = null;
        servUpdate.Actual_Strategy_Approval__c = null;
        servUpdate.Actual_MRQ_Received__c = null;
        servUpdate.Completed__c = true;
        servUpdate.Mat_Serv_Group_Planned_Schedule_Id__c = scheServ.Id;
        insert servUpdate; 
        
    }
    static void createCategoryInitiative(User u, PREP_Discipline__c disp, PREP_Category__c cat, PREP_Sub_Category__c  sub)
    {
        //create Category Initative
        
        PREP_Initiative__c iniCat = new PREP_Initiative__c();
        iniCat.Initiative_Name__c = 'Test Category ini';
        iniCat.Discipline__c = disp.Id;
        iniCat.Category__c = cat.Id;
        iniCat.Sub_Category__c = sub.Id;
        iniCat.Status__c = 'Active';
        iniCat.Type__c = 'Category';
        iniCat.Risk_Level__c = 'High';
        iniCat.HSSM_Package__c = 'Full';
        iniCat.Workbook__c = 'Yes';
        iniCat.SRPM_Package__c = 'As Needed';
        iniCat.Aboriginal__c = 'No';
        iniCat.Global_Sourcing__c = 'Yes';
        iniCat.OEM__c = 'Yes';
        iniCat.Rate_Validation__c = 'Yes';
        iniCat.Reverse_Auction__c = 'No';
        iniCat.Rebate__c = 'Multiple Area Award';
        iniCat.Rebate_Frequency__c = 'Annually';
        iniCat.SCM_Manager__c = u.Id;
        iniCat.Category_Manager__c = u.Id;
        iniCat.Category_Specialist__c = u.Id;
        iniCat.RecordTypeId = Schema.SObjectType.PREP_Initiative__c.getRecordTypeInfosByName().get('Category Initiative').getRecordTypeId();
        insert iniCat; 
        
        
        PREP_Business_Value_Submission__c BVS = new PREP_Business_Value_Submission__c();
        BVS.Category__c = cat.Id;
        BVS.Discipline__c = disp.Id;
        BVS.Sub_Category__c = sub.Id;
        BVS.Initiative_Id__c = iniCat.Id;
        BVS.SCM_Manager__c = u.Id;
        BVS.Specialist_Rep__c = u.Id;
        BVS.Contract_Purchase_Approve_Date__c = Date.newInstance(2015, 4, 10);
        //BVS.Local_Sourcing__c = 10;
        //BVS.Global_Sourcing__c = 90;
        BVS.Short_Term_Interests_Rate__c = 1.50;
        BVS.RecordTypeId = Schema.SObjectType.PREP_Business_Value_Submission__c.getRecordTypeInfosByName().get('Cat-Project Business Value Submission').getRecordTypeId();
        BVS.Contract_Start_Date__c = Date.newInstance(2015, 4, 10);
        BVS.Contract_End_Date__c = Date.newInstance(2015, 12, 10);
        BVS.Awarded_Spend__c = 100000000;
        BVS.Rate_Reduction_Increase__c = 100000000;
        BVS.Status__c = 'Approved';
        insert BVS;
        

        
        PREP_Baseline__c baselineCat = new PREP_Baseline__c();
        baselineCat.Baseline_Spend_Dollar_Target__c = 10000;
        baselineCat.Local_Baseline_Spend_Percent__c = 0.10;
        baselineCat.Local_Baseline_Savings_Percent__c = 0.10;
        baselineCat.Global_Baseline_Savings_Percent__c = 0.10;
        baselineCat.X05_Going_To_Market__c = Date.newInstance(2015, 5, 5);
        baselineCat.XCAC2_CAC_Award_Approval__c = Date.newInstance(2015, 5, 10);
        baselineCat.RecordTypeId = Schema.SObjectType.PREP_Baseline__c.getRecordTypeInfosByName().get('Global Baseline').getRecordTypeId();
        baselineCat.Initiative_Id__c = iniCat.Id;
        insert baselineCat;
        
        
        PREP_Initiative_Status_Update__c ISU = new PREP_Initiative_Status_Update__c();
        ISU.Local_Forecast_Spend_Percent__c = 50;
        ISU.Local_Forecast_Savings_Percent__c = 50;
        ISU.Global_Forecast_Savings_Percent__c = 50;
        ISU.Forecast_Spend_Dollar_Total__c = 100;
        ISU.Baseline_id__c = baselineCat.Id;
        ISU.RecordTypeId = Schema.SObjectType.PREP_Initiative_Status_Update__c.getRecordTypeInfosByName().get('Global Initiative Status Update').getRecordTypeId();
        ISU.Complete__c = true;    
        insert ISU;
    }
    static testMethod void testDeliveryPlanReport()
    {
        createSampleTestData();
        PREPDeliveryPlanController deliveryPlanCtrl = new PREPDeliveryPlanController();
        deliveryPlanCtrl.iniMonth = '5';
        deliveryPlanCtrl.iniYear = 2015;
        deliveryPlanCtrl.selectedDisc = 'All';
        System.assertEquals(deliveryPlanCtrl.getDisciplineOptions().size(), 2);
        deliveryPlanCtrl.runSearch();
        List<PREPDeliveryPlanController.DeliveryPlan> plans =  deliveryPlanCtrl.plans;
        System.assertEquals(plans.size(), 0);
         
        deliveryPlanCtrl.selectedDisc = 'Disp 1';
        deliveryPlanCtrl.runSearch();
        plans =  deliveryPlanCtrl.plans;
        System.assertEquals(plans.size(), 2); 
    }
    static testMethod void TestProjectPackageDetailsReport()
    {
        createSampleTestData();
        PREP_Initiative__c ini = [select Id, Name from PREP_Initiative__c where RecordType.Name = 'Projects Initiative' limit 1];
        PREPProjectPackageDetailsCtrl reportCtrl = new PREPProjectPackageDetailsCtrl();
        reportCtrl.initID = ini.Name;
        System.assert(reportCtrl.getMSGBAColumnHeaders().size() > 0);
        System.assert(reportCtrl.getMSGBAFieldsName().size() > 0);
        System.assert(reportCtrl.getMSGUColumnHeaders().size() > 0);
        System.assert(reportCtrl.getMSGUFieldsName().size() > 0);
        System.assert(reportCtrl.getUnitHeaders().size() > 0);
        System.assert(reportCtrl.getPlannedFieldsName().size() > 0);
        System.assert(reportCtrl.getForecastFieldsName().size() > 0);
        System.assert(reportCtrl.getActualFieldsName().size() > 0);
        System.assert(reportCtrl.getStatusFieldsName().size() > 0);
        reportCtrl.runSearch();
        List<PREP_Mat_Serv_Group_Update__c> packages = reportCtrl.packages;
        System.assert(packages.size() > 0);
        reportCtrl.gotoPDF();
        reportCtrl.ExportDetail();
        
        reportCtrl.initID = ini.Name;       
        reportCtrl.packageType = 'Serv';
        System.assert(reportCtrl.getMSGBAColumnHeaders().size() > 0);
        System.assert(reportCtrl.getMSGBAFieldsName().size() > 0);
        System.assert(reportCtrl.getMSGUColumnHeaders().size() > 0);
        System.assert(reportCtrl.getMSGUFieldsName().size() > 0);
        System.assert(reportCtrl.getUnitHeaders().size() > 0);
        System.assert(reportCtrl.getPlannedFieldsName().size() > 0);
        System.assert(reportCtrl.getForecastFieldsName().size() > 0);
        System.assert(reportCtrl.getActualFieldsName().size() > 0);
        System.assert(reportCtrl.getStatusFieldsName().size() > 0);
        reportCtrl.runSearch();
        packages = reportCtrl.packages;
        System.debug('******** test packages =' + packages);
        System.assert(packages.size() > 0);
        
    }
    static testMethod void testProjectMonthlyInitiativeReport()
    {
        createSampleTestData();
        PREP_Initiative__c ini = [select Id, Name from PREP_Initiative__c where RecordType.Name = 'Projects Initiative' limit 1];
        PREP_ProjectMonthlyInitiativeUpdateCtrl ctrl = new PREP_ProjectMonthlyInitiativeUpdateCtrl();
        ctrl.searchTerm = 'Test init';
        List<PREP_Initiative__c> projects = PREP_ProjectMonthlyInitiativeUpdateCtrl.searchProject('Test ini');
        System.assertEquals(projects.size(), 1);
        ctrl.projId = ini.Id;
        ctrl.loadAwardYears();
        List<SelectOption> awardYears = ctrl.awardYears;
        System.assertEquals(awardYears.size(), 2);
        ctrl.selectedYear = '2015';
        ctrl.runSearch();
    
        List<PREP_ChartData> result = PREP_ProjectMonthlyInitiativeUpdateCtrl.getSpendToMarketChartData(ini.Id, '2015', '4/2015');  
        List<PREP_ChartData> result1 = PREP_ProjectMonthlyInitiativeUpdateCtrl.getSavingsChartData(ini.Id, '2015', '4/2015');
        List<PREP_ChartData> result2 = PREP_ProjectMonthlyInitiativeUpdateCtrl.getAwardChartData(ini.Id, '2015', '4/2015');
        
        //test PREP_ChartData
        PREP_ChartData chartData = new PREP_ChartData('4/2015',4, 2015, 2000);
    }
}