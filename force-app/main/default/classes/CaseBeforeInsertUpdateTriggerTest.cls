// ====================================================================
// Test all the use cases involved with BeforeInsert, Before Update
// trigger for case
@isTest
private class CaseBeforeInsertUpdateTriggerTest {
  
  // ------------------------------------------------------------------
  // User Story: Auto-populate Account on Retail Location assignment
  //   
  // Given I have a case
  // And I am not a portal user
  // When I assign a retail location to the case
  // Then the retail location's account should be assigned to the case
  static testMethod void 
      populateAccountAndContactOnRetailLocationAssignment() {
  	
  	Account testAccount = 
  	    new Account(Name = 'Test');
  	    
  	Database.Saveresult accountResult = Database.insert(testAccount);   	
  	
  	
  	// Primary contact for retail location
  	Contact testContact = new Contact(
  	    FirstName = 'Tester', 
  	    LastName='Blah2');
  	    
  	
  	Database.Saveresult contactResult = Database.insert(testContact);
  	
  	Retail_Location__c location = 
  	   new Retail_Location__c(
  	       Account__c = accountResult.getId(),
  	       Primary_Case_Contact__c = contactResult.getId());
  	
  	Database.Saveresult locationResult = Database.insert(location);
  	
  	System.debug(location);
  	System.debug(locationResult.getId());
  	
  	Case testCase = new Case(Retail_Location__c = 
  	                         locationResult.getId());
	  	
	  Database.Saveresult caseResult = Database.insert(testCase);
	  	
	  Case updatedCase = 
        [SELECT Id, AccountId, ContactId 
                FROM Case 
 	              WHERE Id = :caseResult.getId()];
  	
    System.assertEquals(updatedCase.AccountId, accountResult.getId());
    System.assertEquals(updatedCase.ContactId, contactResult.getId());
  	
  	
  }
  
  // ------------------------------------------------------------------
  // User Story: Create case comment when the user enters closed 
  //             comments
  //
  // Given I have a case
  // When I enter closed comment
  // Then a new case comment for the case should be created
  //      with closed comment as the comment and public checkbox
  //      checked
  static testMethod void createCaseCommentForClosedComment() {
  	
  	Id testCaseId = TestUtility.getFakeId(Case.SobjectType, 1);
  	
  	Case testCase = new Case(Id = testCaseId, 
  	                         Closed_Comments__c = 'Test');
  	
  	//insert testCase;
  	List<CaseComment> testComment =
  	    CaseTriggerHandler.onClosedComment(new List<Case>{testCase});
  	    
  	System.assertEquals(testComment[0].CommentBody, 'Test');
  	System.assert(testComment[0].isPublished);
  	System.assertEquals(testComment[0].ParentId, testCaseId);
  	
  }
  
  
  static testMethod void createCase() {
  	try {
  		Case testCase = new Case(Closed_Comments__c = 'Test');
  	  insert testCase;
  	}
  	catch (Exception ex) {
  		
  	}
  }
  
}