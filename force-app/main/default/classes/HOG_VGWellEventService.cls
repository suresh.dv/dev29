/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Endpoint for recieving Vent Gas data for well events.
Test Class:		HOG_VGWellEventServiceTest
				HOG_VGWellEventServiceTestData -> data
History:        jschn 04.12.2018 - Created.
**************************************************************************************************/
global class HOG_VGWellEventService {

	private static List<Well_Event__c> events;

	global class APIVGWellEvent {
		webservice String RAW_PVR_UWI			{get;set;}
		webservice String PVR_GOR_Factor 		{get;set;}
		webservice String GOR_Test_Date 		{get;set;} 
		webservice String Measured_Vent_Rate 	{get;set;} 
		webservice String PVR_Fuel_Consumption 	{get;set;} 
		webservice String Single_Well_Battery 	{get;set;} 
		webservice String Monthly_Trucked_Oil 	{get;set;}
		webservice String GOR_Effective_Date    {get;set;}
	}

	private static void initialize(){ 
		events = new List<Well_Event__c>();
	}

	webservice static HOG_VGApiUtils.Response pushRecords(List<APIVGWellEvent> records){
		HOG_VGApiUtils.Response response = new HOG_VGApiUtils.Response();
		initialize();
		response.addResults(parseRecords(records));
		response.addResults(commitRecords());
		return response;
	}

	private static List<HOG_VGApiUtils.Result> parseRecords(List<APIVGWellEvent> records) {
		List<HOG_VGApiUtils.Result> results = new List<HOG_VGApiUtils.Result>();

		for(APIVGWellEvent record : records)
			try{
				Well_Event__c event = parseRecord(record);
				if (event != null)
					events.add(event);
				else results.add(addParsingError(record, 'Missing required field(s).'));
			} catch(Exception ex) {
				results.add(addParsingError(record, ex.getMessage()));
			}
		
		return results;
	}

	private static Well_Event__c parseRecord(APIVGWellEvent record) {
		Well_Event__c eventRecord;
		if(String.isNotBlank(record.RAW_PVR_UWI)
		&& String.isNotBlank(record.PVR_GOR_Factor)
		&& String.isNotBlank(record.Measured_Vent_Rate)
		&& String.isNotBlank(record.PVR_Fuel_Consumption)
		&& String.isNotBlank(record.Monthly_Trucked_Oil)
		&& String.isNotBlank(record.GOR_Test_Date)
		&& String.isNotBlank(record.GOR_Effective_Date)
		&& String.isNotBlank(record.Single_Well_Battery)){
			eventRecord = new Well_Event__c();
			eventRecord.PVR_UWI_RAW__c = record.RAW_PVR_UWI;
			eventRecord.PVR_GOR_Factor__c = Decimal.valueOf(record.PVR_GOR_Factor);
			eventRecord.Measured_Vent_Rate__c = Decimal.valueOf(record.Measured_Vent_Rate);
			eventRecord.PVR_Fuel_Consumption__c = Decimal.valueOf(record.PVR_Fuel_Consumption);
			eventRecord.Monthly_Trucked_Oil__c = Decimal.valueOf(record.Monthly_Trucked_Oil);
			eventRecord.GOR_Test_Date__c = Date.parse(record.GOR_Test_Date);//FORMAT MM/DD/YYYY
			eventRecord.GOR_Effective_Date__c = Date.parse(record.GOR_Effective_Date);//FORMAT MM/DD/YYYY
			eventRecord.Single_Well_Battery__c = Boolean.valueOf(record.Single_Well_Battery.toLowerCase());
		}
		return eventRecord;
	}

	private static HOG_VGApiUtils.Result addParsingError(APIVGWellEvent record, String msg) {
		HOG_VGApiUtils.Result result = new HOG_VGApiUtils.Result(record.RAW_PVR_UWI, false, HOG_VGApiUtils.STATUS_ERROR_PARSE);
		result.addError(new HOG_VGApiUtils.Error(new List<String>(), msg, StatusCode.INVALID_INPUT));
		return result;
	}

	private static List<HOG_VGApiUtils.Result> commitRecords() {
		List<HOG_VGApiUtils.Result> results = new List<HOG_VGApiUtils.Result>();
		List<Database.UpsertResult> updateResults = new List<Database.UpsertResult>();

		updateResults.addAll(Database.upsert(events, Well_Event__c.fields.PVR_UWI_RAW__c, false));

		for(Integer i=0; i < updateResults.size(); i++)
			results.add(addUpdateResult(updateResults, i));
		
		return results;
	}

	private static HOG_VGApiUtils.Result addUpdateResult(List<Database.UpsertResult> updateResults, Integer i) {
		Database.UpsertResult updateResult = updateResults[i];
		String statusCode = updateResult.isSuccess() ? HOG_VGApiUtils.STATUS_SUCCESS_UPSERT : HOG_VGApiUtils.STATUS_ERROR_UPSERT;
		String idString = events[i].PVR_UWI_RAW__c;
		HOG_VGApiUtils.Result result = new HOG_VGApiUtils.Result(idString, updateResult.isSuccess(), statusCode);
		for(Database.Error err : updateResult.getErrors())
			result.addError(new HOG_VGApiUtils.Error(err.getFields(), err.getMessage(), err.getStatusCode()));
		return result;
	}

}