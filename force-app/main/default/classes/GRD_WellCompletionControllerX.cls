public class GRD_WellCompletionControllerX{
    public List<GRD_Well_Test__c> wellTestList {get ; private set;}
    public List<InlineStats> stats {get; private set;}   
    public Decimal grandTotal {get; private set;}
    public Decimal flareTotal {get; private set;}    
    public String reportDate {get; set;}
    private Set<String> formationSet {private  get; private set;}
    private Map<String,Integer> formCount {private get; private set;}
    private Map<String,Decimal> formSum {private get; private set;} 
    
    public Date reportDate2 {get; set;}
   
        
    public GRD_WellCompletionControllerX() {
        reportDate = Date.today().format();
        
        runReport();      
    }
    
    public void runReport(){
        stats = new List<InlineStats>(); 
        formationSet = new Set<String>();   
        grandTotal = 0.0;
        flareTotal = 0.0;            
        
        
        try{
            wellTestList = [SELECT Id,Name,Well__c,GRD_Borehole__c,Target_Formation__c,Stages__c,Choke__c,Casing_Pressure__c,Tubing_Pressure__c,
                            Gas_Rate__c,X24_hr_Cum_Gas__c,Liquid_Rate__c,X24_hr_Cum_Liquid__c,X24_hr_Cum_BOE__c,UWI_Location__c,GRD_Borehole__r.Name,
                            Inline_Flare__c,X24hr_Cum_Water__c,LLTR__c,Vendor__c,Comments__c,Status__c,Completion_Date__c
                        FROM GRD_Well_Test__c                        
                        WHERE Completion_Date__c =: /*reportDate2*/ Date.parse(reportDate)
                        ORDER BY Well__c ];
                        
            //get the grand total and total of flare
            for(GRD_Well_Test__c w : wellTestList){
                formationSet.add(w.Target_Formation__c);
                grandTotal += w.X24_hr_Cum_BOE__c == null ? 0.0 : w.X24_hr_Cum_BOE__c;
                flareTotal += w.Inline_Flare__c != null && w.Inline_Flare__c == 'Flare' && w.X24_hr_Cum_BOE__c != null ?  w.X24_hr_Cum_BOE__c : 0.0;
                
            }  
                
            grandTotal = grandTotal - flareTotal;                                    
                        
            //get the counts of initiated tests
            AggregateResult[] formationCounts = [SELECT Target_Formation__c, COUNT(id)
                                FROM GRD_Well_Test__c
                                WHERE Completion_Date__c =: Date.parse(reportDate)
                                    AND Inline_Flare__c = 'Inline'
                                GROUP BY Target_Formation__c];
                                
            //get the sum of formations of initiated tests
            AggregateResult[] formationSums = [SELECT Target_Formation__c, SUM(X24_hr_Cum_BOE__c)
                                FROM GRD_Well_Test__c
                                WHERE Completion_Date__c =: Date.parse(reportDate)
                                    AND Inline_Flare__c != 'Flare'
                                GROUP BY Target_Formation__c];    

            formCount = new Map<String,Integer>();
            formSum = new Map<String,Decimal>();  
            
            for(AggregateResult ar : formationCounts){
                formCount.put((String)ar.get('Target_Formation__c'),(Integer)ar.get('expr0'));
            }  
            
            for(AggregateResult ar : formationSums){
                formSum.put((String)ar.get('Target_Formation__c'),(Decimal)ar.get('expr0'));
            } 
            
            //Iterate the Set and populate InlineStats object
            for(String f : formationSet){
                InlineStats s = new InlineStats();
                s.formation = (f == null ? '--Empty Formation--' : f) + ' ( ' + (formCount.get(f) == null ? '0' : formCount.get(f).format()) + (formCount.get(f) == 1 ? ' well )' : ' wells )');
                s.total = formSum.get(f)== null ? '0.0' :  formSum.get(f).format();
                s.total += ' boe';
                stats.add(s);
            }                                                       
                                
        } catch(Exception exp) {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.severity.ERROR,'Error while generating report. ' + exp);
            ApexPages.addMessage(errorMsg);            
        }    
    }
    
    //submit all well test completions for approval
    public void submitForApproval(){
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        System.debug('wellTestList' + wellTestList);
        for(GRD_Well_Test__c comletion : wellTestList) {
            req1.setComments('Submitting well completions for approval.');
            req1.setObjectId(comletion.id);
            req1.setProcessDefinitionNameOrId('GRD_Manager_Approval');
            req1.setSkipEntryCriteria(true);  
            
            Approval.ProcessResult result = Approval.process(req1);
        }      
    }
    
    //Inner class to keep a track of different formation and it's sums
    public class InlineStats {
        public String formation {get; protected set;}
        public String total {get; protected set;}
    }  
    
}