public class FM_OperatorOnCallController
{
	// *************************************************************************************** //
	// ** THIS CONTROLLER HANDLES LOGIC RELATED TO THE OPERATOR ON CALL FOR OPERATOR ROUTES ** //
	// *************************************************************************************** //

	private static final String ROUTE_STRING_SPLITTER = '==_==';

	///////////////////////////
	// * PERMISSION FIELDS * //
	///////////////////////////

	private Boolean bIsOperator;
	private Boolean bIsControlRoomOperator;
	private Boolean bInEditMode;

	///////////////////////////////
	// * COLUMN SORTING FIELDS * //
	///////////////////////////////

	public String sSearchField {get; set;}
	public String sPrevField {get; private set;}
	public Boolean bSortAscending {get; set;}

	////////////////////////////
	// * USER-SPECIFIC DATA * //
	////////////////////////////

	private String sCurrentUserId;
	private String sCurrentUserName;

	///////////////////////////////
	// * ROUTE & RUNSHEET DATA * //
	///////////////////////////////

	private List<OperatorOnCallWrapper> lFullRouteData;
	private String sRouteNum;
	private String sPreviousRouteNum;

	private Map<String, Id> mapRoutes;
	private Map<String, OperatorOnCallWrapper> mapRouteWrappers;

	//////////////////////////
	// * MAIN CONSTRUCTOR * //
	//////////////////////////

	public FM_OperatorOnCallController()
	{
		InitializeData();
		BuildRouteList();
	}

	/////////////////////////////////////
	// * INITIALIZE CONTROLLER DATA * ///
	/////////////////////////////////////

	private void InitializeData()
	{
		this.mapRoutes = new Map<String, Id>();
		this.mapRouteWrappers = new Map<String, OperatorOnCallWrapper>();

		this.sCurrentUserId = UserInfo.getUserId();
		this.sCurrentUserName = UserInfo.getName();

		this.sRouteNum = '';

		this.bIsOperator = CheckUserOperatorPrivileges(this.sCurrentUserId);
		this.bIsControlRoomOperator = CheckUserControlRoomPrivileges(this.sCurrentUserId);
		this.bInEditMode = false;
	}

	///////////////////////////////////////////////////
	// * SET THE DEFAULT SORT FIELD AND DIRECTION * ///
	///////////////////////////////////////////////////

	private void SetDefaultSearchParameters()
	{
		this.sSearchField = 'Name';
		this.sPrevField = 'Name';
		this.bSortAscending = true;
	}

	/////////////////////////////////////////////////////////////////
	// * BUILD THE LIST OF ROUTES WITH CURRENT OPERATOR ON CALL * ///
	/////////////////////////////////////////////////////////////////

	private void BuildRouteList()
	{
		this.lFullRouteData = new List<OperatorOnCallWrapper>();

		if(this.sSearchField == null)
		{
			SetDefaultSearchParameters();
		}

		List<Route__c> lFullRouteList = Database.query('SELECT Id, Name, Field_Senior__c, Operator_On_Call__r.CreatedDate, Operator_On_Call__r.CreatedBy.Id, Operator_On_Call__r.CreatedBy.Name, Operator_On_Call__r.Operator__r.Id, Operator_On_Call__r.Operator__r.Name, Operator_On_Call__r.Operator__r.Phone, Operator_On_Call__r.Operator__r.MobilePhone, Operator_On_Call__r.Operator__r.CreatedBy.Id, Operator_On_Call__r.Operator__r.CreatedBy.Name FROM Route__c WHERE Fluid_Management__c = true ORDER BY ' + this.sSearchField + ' ' + ((this.bSortAscending == false) ? 'DESC NULLS LAST' : 'ASC NULLS LAST'));
		OperatorOnCallWrapper oOneWrapper;

		for(Route__c oOneRoute : lFullRouteList)
		{
			oOneWrapper = new OperatorOnCallWrapper(oOneRoute, this.sCurrentUserId, this.lOperatorList);
			this.lFullRouteData.add(oOneWrapper);

			mapRoutes.put(oOneRoute.Name, oOneRoute.Id);
			mapRouteWrappers.put(oOneRoute.Name, oOneWrapper);
		}
	}

	/////////////////////////////////////////////////////
	// * ADJUST THE SORTING VALUES FOR PAGE DISPLAY * ///
	/////////////////////////////////////////////////////

	public PageReference ColumnSortToggle()
	{
		if(this.sSearchField == this.sPrevField)
		{
			this.bSortAscending = (this.bSortAscending == true) ? false : true;
		}
		else
		{
			this.sPrevField = this.sSearchField;
			this.bSortAscending = true;
		}

		BuildRouteList();
		return null;
	}

	/////////////////////////////////////////////////////////
	// * INITIATE OR CANCEL EDIT MODE FOR A GIVEN ROUTE * ///
	/////////////////////////////////////////////////////////

	public PageReference EditMode()
	{
		String sActualRoute = this.sRouteNum.substringBefore(ROUTE_STRING_SPLITTER);
		this.sPreviousRouteNum = (sActualRoute != '') ? sActualRoute : this.sPreviousRouteNum;

		OperatorOnCallWrapper oRouteWrapper = this.mapRouteWrappers.get(this.sPreviousRouteNum);

		// Cancelling Edit --> reset selected Operator
		if(this.bInEditMode == true)
		{
			if(oRouteWrapper != null)
			{
				oRouteWrapper.ResetSelectedOperator();
			}

			this.bInEditMode = false;
		}
		else
		{
			this.bInEditMode = true;
		}

		return null;
	}

	/////////////////////////////////////////////////////////////////////////////////////////
	// * CHANGE THE CURRENT OPERATOR ON CALL FOR THE SUPPLIED ROUTE -> INSERT NEW ENTRY * ///
	/////////////////////////////////////////////////////////////////////////////////////////

	public PageReference ChangeOperator()
	{
		Operator_On_Call__c oNewOperator = new Operator_On_Call__c();
		Route__c oUpdateRoute = new Route__c();

		// Get Actual Route Number (NON-OPERATOR Save)
		String sActualRoute = this.sRouteNum.substringBefore(ROUTE_STRING_SPLITTER);
		OperatorOnCallWrapper oRouteWrapper = this.mapRouteWrappers.get(sActualRoute);

		// Double Check Permissions
		this.bIsOperator = CheckUserOperatorPrivileges(this.sCurrentUserId);
		this.bIsControlRoomOperator = CheckUserControlRoomPrivileges(this.sCurrentUserId);

		Boolean bOperatorSave = (this.sRouteNum.contains(ROUTE_STRING_SPLITTER) == true) ? false : true;
		String sActualOperatorId = (bOperatorSave == true) ? this.sCurrentUserId : this.sRouteNum.substringAfter(ROUTE_STRING_SPLITTER).substringBefore(oRouteWrapper.getWrapperSplitterString());

		// Set Savepoint
		Savepoint oSave = Database.setSavepoint();

		// Operator is attempting to save -> invalid permissions OR missing data?
		if(bOperatorSave == true && (this.bIsOperator == false || this.sRouteNum == null))
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'You do not have permission to change Operator on Call for route ' + sActualRoute));
			return null;
		}
		// Control Room Operator OR Field Senior is attempting to save -> invalid permissions OR missing data?
		else if(bOperatorSave == false && ((this.bIsControlRoomOperator == false && oRouteWrapper.bCurrentUserIsFieldSenior == false) || this.sRouteNum == null))
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'You do not have permission to change Operator on Call for route ' + sActualRoute));
			return null;
		}
		else
		{
			// Validation Successful --> Insert New Entry

			try
			{
				// New Operator does NOT match current Operator?
				if(sActualOperatorId != oRouteWrapper.sOperatorId)
				{
					oNewOperator.Operator__c = sActualOperatorId;
					oNewOperator.Operator_Route__c = mapRoutes.get(sActualRoute);
					insert oNewOperator;

					oUpdateRoute.Id = mapRoutes.get(sActualRoute);
					oUpdateRoute.Operator_On_Call__c = oNewOperator.Id;
					update oUpdateRoute;
				}

				return FM_Utilities.FluidRedirect('/apex/FM_OperatorOnCall', true);
			}
			catch(Exception ex)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'An error has occured for Route ' + sActualRoute + ': ' + ex.getMessage()));
				Database.rollback(oSave);

				return null;
			}
			finally
			{
				// Clear Edit Mode
				this.sRouteNum = '';
				this.bInEditMode = false;
			}
		}
	}

	///////////////////////////////////////////////////////////
	// * DETERMINE IF USER HAS FIELD OPERATOR PERMISSIONS * ///
	///////////////////////////////////////////////////////////

	private static Boolean CheckUserOperatorPrivileges(String sUserId)
	{
		return FM_Utilities.VerifyPermissions(sUserId, FM_Utilities.FLUID_PERMISSION_SET_OPERATOR);
	}

	//////////////////////////////////////////////////////////////////
	// * DETERMINE IF USER HAS CONTROL ROOM OPERATOR PERMISSIONS * ///
	//////////////////////////////////////////////////////////////////

	private static Boolean CheckUserControlRoomPrivileges(String sUserId)
	{
		return FM_Utilities.VerifyPermissions(sUserId, FM_Utilities.HOG_PERMISSION_SET_CONTROL_ROOM_OPERATOR);
	}

	/////////////////////////////////////////////
	// * WRAPPER CLASS FOR OPERATOR ON CALL * ///
	/////////////////////////////////////////////

	public class OperatorOnCallWrapper
	{
		private final String DATE_FORMAT = 'MMMMM dd yyyy h:mm a';
		private final String STRING_SPLITTER = '##_##';

		private List<SelectOption> lOperatorAssignment;
		public Route__c oRouteObject {get; private set;}
		public String sSelectedOperator {get; set;}

		public String sRouteId {get; private set;}
		public String sRouteName {get; private set;}
		public String sOperatorId {get; private set;}
		public String sOperatorName {get; private set;}
		public String sOperatorPhone {get; private set;}
		public String sAssignedById {get; private set;}
		public String sAssignedByName {get; private set;}

		public DateTime dAssignedDate {get; private set;}
		public String sFormattedDate {get; private set;}

		public Boolean bCurrentUserIsOperatorOnCall {get; private set;}
		public Boolean bCurrentUserIsFieldSenior {get; private set;}

		public OperatorOnCallWrapper(Route__c oRouteData, String sCurrentUserId, List<User> lFullOperatorList)
		{
			this.lOperatorAssignment = new List<SelectOption>();
			this.oRouteObject = oRouteData;

			this.sRouteId = oRouteData.Id;
			this.sRouteName = oRouteData.Name;
			this.sOperatorId = oRouteData.Operator_On_Call__r.Operator__r.Id;
			this.sOperatorName = oRouteData.Operator_On_Call__r.Operator__r.Name;
			this.sOperatorPhone = (oRouteData.Operator_On_Call__r.Operator__r.MobilePhone == null) ? oRouteData.Operator_On_Call__r.Operator__r.Phone : oRouteData.Operator_On_Call__r.Operator__r.MobilePhone;
			this.sAssignedById = oRouteData.Operator_On_Call__r.CreatedBy.Id;
			this.sAssignedByName = oRouteData.Operator_On_Call__r.CreatedBy.Name;

			this.dAssignedDate = oRouteData.Operator_On_Call__r.CreatedDate;
			this.sFormattedDate = FormatDate(this.dAssignedDate, DATE_FORMAT);

			this.bCurrentUserIsOperatorOnCall = (this.sOperatorId == sCurrentUserId) ? true : false;
			this.bCurrentUserIsFieldSenior = (oRouteData.Field_Senior__c == sCurrentUserId) ? true : false;

			ResetSelectedOperator();
			SelectOption oOneOption;

			for(User oOneUser : lFullOperatorList)
			{
				oOneOption = new SelectOption(oOneUser.Id + STRING_SPLITTER + oOneUser.Name, oOneUser.Name);
				this.lOperatorAssignment.add(oOneOption);
			}
		}

		private String FormatDate(DateTime dOneDate, String sOneFormat)
		{
			return (dOneDate != null) ? dOneDate.format(DATE_FORMAT) : 'NA';
		}

		public void ResetSelectedOperator()
		{
			this.sSelectedOperator = this.sOperatorId + STRING_SPLITTER + this.sOperatorName;
		}

		public List<SelectOption> getOperatorAssignmentList()
		{
			return this.lOperatorAssignment;
		}

		public String getSelectedOperator()
		{
			return this.sSelectedOperator.substringAfter(STRING_SPLITTER);
		}

		public String getWrapperSplitterString()
		{
			return STRING_SPLITTER;
		}
	}

	////////////////////////////
	// * LIST OF OPERATORS * ///
	////////////////////////////

	private List<User> lOperatorList
	{
		get
		{
			if(lOperatorList == null)
			{
				lOperatorList = [SELECT Id, Name, Phone, MobilePhone FROM User WHERE IsActive = true AND Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = :FM_Utilities.FLUID_PERMISSION_SET_OPERATOR) ORDER BY Name];
			}

			return lOperatorList;
		}

		private set;
	}

	/////////////////////////////////
	// * GETTER + SETTER METHODS * //
	/////////////////////////////////

	public List<OperatorOnCallWrapper> getRouteList()
	{
		return this.lFullRouteData;
	}

	public static String getSplitterString()
	{
		return ROUTE_STRING_SPLITTER;
	}

	public String getRouteNum()
	{
		return this.sRouteNum;
	}

	public void setRouteNum(String sNewString)
	{
		this.sRouteNum = sNewString;
	}

	public Boolean getIsOperator()
	{
		return this.bIsOperator;
	}

	public Boolean getIsControlRoomOperator()
	{
		return this.bIsControlRoomOperator;
	}

	public Boolean getInEditMode()
	{
		return this.bInEditMode;
	}
}