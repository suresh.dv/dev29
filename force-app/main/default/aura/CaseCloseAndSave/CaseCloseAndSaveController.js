({
	doInit : function(component, event, helper) {
        var action = component.get("c.updateCase");
        action.setParams({
            'id' : component.get("v.recordId")
		});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                $A.get("e.force:closeQuickAction").fire();
				$A.get('e.force:refreshView').fire();
            }
            else{
                var error = response.getError();
                console.log(error);
                $A.get("e.force:closeQuickAction").fire();
				$A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action); 
	}
})