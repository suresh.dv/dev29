//this trigger will update Tender Workflow on Tender when Opp Stage is updated
trigger Opportunity_UpdateTenderWorkflow on Opportunity (after insert, after update) {
    Set<Id> tenderIds = new Set<Id>();
    
    for(Opportunity opp : trigger.new)
    {
        if (opp.Opportunity_ATS_Product_Category__c != null)
            tenderIds.add(opp.Opportunity_ATS_Product_Category__c);
        
    }
    Map<Id, ATS_Parent_Opportunity__c> tenderMap = new Map<Id, ATS_Parent_Opportunity__c>(
                            [select Id, Stage__c from ATS_Parent_Opportunity__c where Id in: tenderIds]);
    
    List<Opportunity> relatedOppList = [select Id, StageName, Opportunity_ATS_Product_Category__c 
                                        from Opportunity 
                                        where Opportunity_ATS_Product_Category__c in: tenderIds
                                        order by Opportunity_ATS_Product_Category__c, StageName];
    Map<Id, List<Opportunity>> oppListByTender = new Map<Id, List<Opportunity>>();
    
    for(Opportunity opp : relatedOppList)
    {
        if (oppListByTender.containsKey(opp.Opportunity_ATS_Product_Category__c))
        {
            List<Opportunity> oppList = oppListByTender.get(opp.Opportunity_ATS_Product_Category__c);
            oppList.add(opp);
        }
        else
        {
            List<Opportunity> oppList = new List<Opportunity>();
            oppList.add(opp);
            oppListByTender.put(opp.Opportunity_ATS_Product_Category__c, oppList);
        }
    }  
    
    for(String key : oppListByTender.keySet())
    {
        List<Opportunity> oppList = oppListByTender.get(key);
        ATS_Parent_Opportunity__c tender = tenderMap.get(key);
        if (oppList != null && oppList.size() > 0)
        {
            tender.Stage__c = oppList[0].StageName;
            System.debug('previousStage =' + tender.Stage__c);
            if (oppList.size() > 1)
            {
                for(integer i=1; i < oppList.size(); i++)
                {
                    Opportunity opp = oppList[i];
                    System.debug('opp =' + opp);
                    if (tender.Stage__c != opp.StageName && (opp.StageName == 'Won' || tender.Stage__c == 'Won'))
                    {
                        tender.Stage__c = 'Bid Won - Partial';
                        break;
                    }
                    else if (tender.Stage__c == opp.StageName && opp.StageName == 'Won')
                    {
                        tender.Stage__c = 'Won';
                    }
                    else if (tender.Stage__c != opp.StageName && opp.StageName == 'Lost' && tender.Stage__c != 'Won' )
                        tender.Stage__c = tender.Stage__c;
                    else if (tender.Stage__c != opp.StageName && tender.Stage__c == 'Lost' && opp.StageName != 'Won')
                        tender.Stage__c = opp.StageName;
                    else if (tender.Stage__c != 'Lost' && tender.Stage__c != 'Won' && opp.StageName != 'Lost' && opp.StageName != 'Won')
                        tender.Stage__c = tender.Stage__c;
                    
                   System.debug('---- updated ' + tender.Stage__c);
                }
               
            }
            
            
            if (tender.Stage__c == 'Opportunity Initiation - Marketer')   
               tender.Stage__c = 'Bid Initiation - Marketer';
            else if (tender.Stage__c == 'Freight Details Analysis - Logistics')   
               tender.Stage__c = 'Freight Details Analysis - Logistics';
            else if (tender.Stage__c == 'Perform Bid Analysis - Marketer')   
               tender.Stage__c = 'Perform Bid Analysis - Marketer';
            else if (tender.Stage__c == 'Sales Contract Gen - Contract Analyst')   
               tender.Stage__c = 'Sales Contract Generation - Contract Analyst';      
            else if (tender.Stage__c == 'Won')
                tender.Stage__c = 'Bid Won - Full';     
        }
           
    } 
    update tenderMap.values();                 
}