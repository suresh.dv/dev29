trigger VTT_ActivityRescheduleRequest on Work_Order_Activity_Reschedule_Request__c (after insert) {
	if(VTT_Utilities.executeRescheduleRequestTriggerCode) {
		if(Trigger.isAfter) {
			if(Trigger.isInsert){
				VTT_Utilities.TriggerAfterInsertRescheduleRequest(Trigger.new);
			}
		}
	}
}