trigger ATSFreight_UpdateOpportunity on ATS_Freight__c (after delete, after insert, after undelete) 
{
    Set<Id> oppIds = new Set<Id>();
    List<ATS_Freight__c> triggerValues = new List<ATS_Freight__c>();
    if (trigger.isInsert || trigger.isUnDelete)
    {
        triggerValues = trigger.new;
        
    }
    else if (trigger.isDelete)
    {
        triggerValues = trigger.old;
    }

    for(ATS_Freight__c f : triggerValues)
    {
        if (f.ATS_Freight__c != null)
            oppIds.add(f.ATS_Freight__c);
    }
    List<Opportunity> oppList = [select Id, ATS_Freight__c, (select Id from Freight_ATS__r order by CreatedDate) from Opportunity where Id in :oppIds];
    
    
    for(Opportunity opp : oppList)
    {
        if (opp.Freight_ATS__r != null && opp.Freight_ATS__r.size() > 0)
        {
            opp.ATS_Freight__c = opp.Freight_ATS__r[0].Id;
        }
        else
            opp.ATS_Freight__c = null;
    }
    
    update oppList;
}