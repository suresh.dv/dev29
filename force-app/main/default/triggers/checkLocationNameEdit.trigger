trigger checkLocationNameEdit on Location__c (before update) {
  for (Location__c updatedLocation: Trigger.new) {
    if (updatedLocation.Name != Trigger.oldMap.get(updatedLocation.Id).Name) {
      updatedLocation.Name.addError('You cannot change the name of the location');
    }
  }
}