trigger OpportunityProduct_SetAxlePrices on OpportunityLineItem (after update)
{
    // All Opportunity Products to update the axle prices of
    Map<Id, OpportunityLineItem> olis = new Map<Id, OpportunityLineItem>();
    
    Set<Id> opportunitiesOfOlis = new Set<Id>();
    Set<Id> productsOfOlis      = new Set<Id>();
    
    // The set stores all Opportunity Line Items whose Emulsion_Cost_CAD__c or Emulsion_Margin_CAD__c changed.
    // These line items will overwrite all axle prices.
    // Otherwise, only those axle prices not flagged as manually set can be overwritten.
    Set<Id> olisWhoAreNowCalculated = new Set<Id>();
    
    // If any of these axles changed, we'll assume it's a manual change. So we'll mark them as manually set
    // and not overwrite their values.
    Set<Id> axle5Set = new Set<Id>();
    Set<Id> axle6Set = new Set<Id>();
    Set<Id> axle7Set = new Set<Id>();
    Set<Id> axle8Set = new Set<Id>();
    
    if(Trigger.isUpdate)
    {
        for(Id oliId : Trigger.newMap.keySet())
        {
            OpportunityLineItem newOli = Trigger.newMap.get(oliId);
            OpportunityLineItem oldOli = Trigger.oldMap.get(oliId);

            // Check if it needs an update
            if(newOli.Emulsion_Cost_CAD__c    != oldOli.Emulsion_Cost_CAD__c    ||
               newOli.Emulsion_Margin_CAD__c  != oldOli.Emulsion_Margin_CAD__c  ||
               newOli.OpportunityId           != oldOli.OpportunityId           ||
               newOli.Currency__c             != oldOli.Currency__c             ||
               newOli.Exchange_Rate_to_CAD__c != oldOli.Exchange_Rate_to_CAD__c ||
               newOli.Unit__c                 != oldOli.Unit__c)
            {
                olis.put(oliId, newOli);
                opportunitiesOfOlis.add(newOli.OpportunityId);
                productsOfOlis.add(newOli.Product2Id);
            }
            if(newOli.Emulsion_Cost_CAD__c    != oldOli.Emulsion_Cost_CAD__c   ||
               newOli.Emulsion_Margin_CAD__c  != oldOli.Emulsion_Margin_CAD__c)
            {
                olisWhoAreNowCalculated.add(newOli.Id);
            }
            if(newOli.Axle_5_Price__c != oldOli.Axle_5_Price__c) axle5Set.add(newOli.Id);
            if(newOli.Axle_6_Price__c != oldOli.Axle_6_Price__c) axle6Set.add(newOli.Id);
            if(newOli.Axle_7_Price__c != oldOli.Axle_7_Price__c) axle7Set.add(newOli.Id);
            if(newOli.Axle_8_Price__c != oldOli.Axle_8_Price__c) axle8Set.add(newOli.Id);
        }

    }
    if(Trigger.isInsert)
    {
        for(Id oliId : Trigger.newMap.keySet())
        {
            OpportunityLineItem newOli = Trigger.newMap.get(oliId);
            
            olis.put(oliId, newOli);
            opportunitiesOfOlis.add(newOli.OpportunityId);
            productsOfOlis.add(newOli.Product2Id);
        }
        // We don't fill olisWhoAreNowCalculated here, since it'll be set by the VF page.
    }
    if(opportunitiesOfOlis.size() == 0)
        return;
    
    // Get the opp record type we need
    RecordType emulsion = [SELECT Id
                           FROM RecordType
                           WHERE DeveloperName =: 'ATS_Emulsion_Product_Category'];
    
    // Get all freights for these olis
    Map<Id, Opportunity> opps = new Map<Id, Opportunity>([SELECT Id,
                                                                 ATS_Freight__c,
                                                                 ATS_Freight__r.Id,
                                                                 ATS_Freight__r.Husky_Supplier_1_Selected__c,
                                                                 ATS_Freight__r.Husky_Supplier_2_Selected__c,
                                                                 ATS_Freight__r.Prices_F_O_B__c,
                                                                 ATS_Freight__r.Emulsion_Rate8_Supplier1__c,
                                                                 ATS_Freight__r.Emulsion_Rate8_Supplier2__c,
                                                                 ATS_Freight__r.Emulsion_Rate7_Supplier1__c,
                                                                 ATS_Freight__r.Emulsion_Rate7_Supplier2__c,
                                                                 ATS_Freight__r.Emulsion_Rate6_Supplier_1__c,
                                                                 ATS_Freight__r.Emulsion_Rate6_Supplier2__c,
                                                                 ATS_Freight__r.Emulsion_Rate5_Supplier1__c,
                                                                 ATS_Freight__r.Emulsion_Rate5_Supplier2__c,
                                                                 ATS_Freight__r.Supplier_1_Unit__c,
                                                                 ATS_Freight__r.Supplier_2_Unit__c
                                                          FROM Opportunity
                                                          WHERE Id IN: opportunitiesOfOlis AND
                                                                RecordTypeId =: emulsion.Id]);
     // Get all products for these olis
     Map<Id, Product2> products = new Map<Id, Product2>([SELECT Id,
                                                                Density__c
                                                         FROM Product2
                                                         WHERE Id IN: productsOfOlis]);
     
     // These are the olis with the updated axle prices
     List<OpportunityLineItem> olisToCommit = new List<OpportunityLineItem>();
     
     // For each oli we need to update, recalc all axle prices
     for(OpportunityLineItem oli : olis.values())
     {
         // If we don't have the opportunity, it's because it's not an emulsion type. Skip it.
         if(!opps.containsKey(oli.OpportunityId))
             continue;
         
         // Build freight object
        
         ATS_Freight__c f = new ATS_Freight__c(Id                           = opps.get(oli.OpportunityId).ATS_Freight__r.Id,
                                               Husky_Supplier_1_Selected__c = opps.get(oli.OpportunityId).ATS_Freight__r.Husky_Supplier_1_Selected__c,
                                               Husky_Supplier_2_Selected__c = opps.get(oli.OpportunityId).ATS_Freight__r.Husky_Supplier_2_Selected__c,
                                               Prices_F_O_B__c              = opps.get(oli.OpportunityId).ATS_Freight__r.Prices_F_O_B__c,
                                               Emulsion_Rate8_Supplier1__c  = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate8_Supplier1__c,
                                               Emulsion_Rate8_Supplier2__c  = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate8_Supplier2__c,
                                               Emulsion_Rate7_Supplier1__c  = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate7_Supplier1__c,
                                               Emulsion_Rate7_Supplier2__c  = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate7_Supplier2__c,
                                               Emulsion_Rate6_Supplier_1__c = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate6_Supplier_1__c,
                                               Emulsion_Rate6_Supplier2__c  = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate6_Supplier2__c,
                                               Emulsion_Rate5_Supplier1__c  = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate5_Supplier1__c,
                                               Emulsion_Rate5_Supplier2__c  = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate5_Supplier2__c,
                                               Supplier_1_Unit__c           = opps.get(oli.OpportunityId).ATS_Freight__r.Supplier_1_Unit__c,
                                               Supplier_2_Unit__c           = opps.get(oli.OpportunityId).ATS_Freight__r.Supplier_2_Unit__c);
         
         // Build product object
         
         Product2 p = null;
         if(products.containsKey(oli.Product2Id))
             p = new Product2(Id         = products.get(oli.Product2Id).Id,
                              Density__c = products.get(oli.Product2Id).Density__c);
         
         Map<Integer,Decimal> axlePrices = calculateNetback.calculateEmulsionAxlePrices(oli, f, p);
         
         OpportunityLineItem oliToCommit = new OpportunityLineItem(Id                           = oli.Id,
                                                                   Axle_5_Price__c              = null,
                                                                   Axle_6_Price__c              = null,
                                                                   Axle_7_Price__c              = null,
                                                                   Axle_8_Price__c              = null,
                                                                   Axle_5_Price_Set_Manually__c = oli.Axle_5_Price_Set_Manually__c,
                                                                   Axle_6_Price_Set_Manually__c = oli.Axle_6_Price_Set_Manually__c,
                                                                   Axle_7_Price_Set_Manually__c = oli.Axle_7_Price_Set_Manually__c,
                                                                   Axle_8_Price_Set_Manually__c = oli.Axle_8_Price_Set_Manually__c);
//         if(axlePrices.containsKey(5) && (olisWhoAreNowCalculated.contains(oliToCommit.Id) || oli.Axle_5_Price_Set_Manually__c == false))
//         if(axlePrices.containsKey(5) && olisWhoAreNowCalculated.contains(oliToCommit.Id))
//         {
             oliToCommit.Axle_5_Price__c = axlePrices.get(5);
             if(oliToCommit.Axle_5_Price__c != null)
//KK 2014-11-21 need to set the rounded value back to the variable             
//                  oliToCommit.Axle_5_Price__c.setScale(4, RoundingMode.HALF_UP);
                  oliToCommit.Axle_5_Price__c = oliToCommit.Axle_5_Price__c.setScale(4, RoundingMode.HALF_UP);
             oliToCommit.Axle_5_Price_Set_Manually__c = false;
//         }
//         if(axlePrices.containsKey(6) && (olisWhoAreNowCalculated.contains(oliToCommit.Id) || oli.Axle_6_Price_Set_Manually__c == false))
//         if(axlePrices.containsKey(6) && olisWhoAreNowCalculated.contains(oliToCommit.Id))
//         {
             oliToCommit.Axle_6_Price__c = axlePrices.get(6);
             if(oliToCommit.Axle_6_Price__c != null)
//KK 2014-11-21 need to set the rounded value back to the variable             
//                  oliToCommit.Axle_6_Price__c.setScale(4, RoundingMode.HALF_UP);
                  oliToCommit.Axle_6_Price__c = oliToCommit.Axle_6_Price__c.setScale(4, RoundingMode.HALF_UP);
             oliToCommit.Axle_6_Price_Set_Manually__c = false;
//         }
//         if(axlePrices.containsKey(7) && (olisWhoAreNowCalculated.contains(oliToCommit.Id) || oli.Axle_7_Price_Set_Manually__c == false))
//         if(axlePrices.containsKey(7) && olisWhoAreNowCalculated.contains(oliToCommit.Id))
//         {
             oliToCommit.Axle_7_Price__c = axlePrices.get(7);
             if(oliToCommit.Axle_7_Price__c != null)
                  oliToCommit.Axle_7_Price__c = oliToCommit.Axle_7_Price__c.setScale(4, RoundingMode.HALF_UP);
             oliToCommit.Axle_7_Price_Set_Manually__c = false;
//         }
//         if(axlePrices.containsKey(8) && (olisWhoAreNowCalculated.contains(oliToCommit.Id) || oli.Axle_8_Price_Set_Manually__c == false))
//         if(axlePrices.containsKey(8) && olisWhoAreNowCalculated.contains(oliToCommit.Id))
//         {
             oliToCommit.Axle_8_Price__c = axlePrices.get(8);
             if(oliToCommit.Axle_8_Price__c != null)
                 oliToCommit.Axle_8_Price__c = oliToCommit.Axle_8_Price__c.setScale(4, RoundingMode.HALF_UP);
             oliToCommit.Axle_8_Price_Set_Manually__c = false;
//         }
         
         olisToCommit.add(oliToCommit);
     }
     update olisToCommit;
}