trigger trgForOrderComponents on jbrvrm__Subscription_Order__c (before update) {
    
    Set<ID> setOrderIDs = new Set<ID>();
    Map<Id, Date> mapTitleDate = new Map<ID, Date>();
    Map<ID, Date> mapComponentDate = new Map<ID, Date>();
    Map<Id, Date> mapTitleStartDate = new Map<ID, Date>();
    Map<ID, Date> mapComponentStartDate = new Map<ID, Date>();
    for(jbrvrm__Subscription_Order__c order : Trigger.new) {
        if((order.jbrvrm__Renewal_Status__c == 'Complete') && (Trigger.oldMap.get(order.Id).jbrvrm__Renewal_Status__c == null ||
             order.jbrvrm__Renewal_Status__c != Trigger.oldMap.get(order.Id).jbrvrm__Renewal_Status__c )) {
            setOrderIDs.add(order.Id);
            mapTitleDate.put(order.jbrvrm__Subscription_Title__c, order.jbrvrm__End_Date__c);
            mapTitleStartDate.put(order.jbrvrm__Subscription_Title__c, order.jbrvrm__Start_Date__c);
        }
    }
    System.debug('#### setOrderIDs = '+setOrderIDs);
    for(jbrvrm__Subscription_Order_Component__c  component : [Select id, name, jbrvrm__Vendor_Subscription_Title__c, jbrvrm__End_Date__c, 
        jbrvrm__Subscription_Order__r.jbrvrm__End_Date__c, jbrvrm__Subscription_Order__r.jbrvrm__Start_Date__c from jbrvrm__Subscription_Order_Component__c where jbrvrm__Subscription_Order__c in : setOrderIDs ]) {
        //mapComponentDate.put(component.jbrvrm__Vendor_Subscription_Title__c, component.jbrvrm__End_Date__c);
        mapComponentDate.put(component.jbrvrm__Vendor_Subscription_Title__c, component.jbrvrm__Subscription_Order__r.jbrvrm__End_Date__c);
        mapComponentStartDate.put(component.jbrvrm__Vendor_Subscription_Title__c, component.jbrvrm__Subscription_Order__r.jbrvrm__Start_Date__c);
        
        
    }
    System.debug('#### mapComponentDate= '+mapComponentDate);
    List<jbrvrm__Vendor_Subscription_Title__c> listTitles = new List<jbrvrm__Vendor_Subscription_Title__c>();
    listTitles = [Select id, name, jbrvrm__Contract_End_Date__c,jbrvrm__Renewal_Start_Date__c from jbrvrm__Vendor_Subscription_Title__c where (ID in: mapComponentDate.keyset() OR ID in: mapTitleDate.keyset())];
    System.debug('#### listTitles = '+listTitles );
    for(jbrvrm__Vendor_Subscription_Title__c title : listTitles ) {
        if(mapTitleDate.containsKey(title .ID)) {
            if(title.jbrvrm__Contract_End_Date__c == null)    title.jbrvrm__Contract_End_Date__c = mapTitleDate.get(title.ID);
            if(title.jbrvrm__Renewal_Start_Date__c== null)    title.jbrvrm__Renewal_Start_Date__c= mapTitleStartDate .get(title.ID);
        } else if(mapComponentDate.containsKey(title .ID)) {
        
            if(title.jbrvrm__Contract_End_Date__c == null)    title.jbrvrm__Contract_End_Date__c = mapComponentDate.get(title.ID);
            if(title.jbrvrm__Renewal_Start_Date__c== null)    title.jbrvrm__Renewal_Start_Date__c= mapComponentStartDate .get(title.ID);
        }
        
        System.debug('#### title.jbrvrm__Contract_End_Date__c = '+title.jbrvrm__Contract_End_Date__c);
    }
    System.debug('#### listTitles2 = '+listTitles );
    update listTitles;
}