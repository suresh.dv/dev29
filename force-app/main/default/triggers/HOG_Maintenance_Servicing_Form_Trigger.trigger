trigger HOG_Maintenance_Servicing_Form_Trigger on HOG_Maintenance_Servicing_Form__c (before update, before insert, before delete, after insert, after update) 
{    
    if (MaintenanceServicingUtilities.executeTriggerCode)
    {
        System.debug('\n*****************************************\n'
            + 'METHOD: HOG_Maintenance_Servicing_Form_Trigger()'
            + '\nTrigger: executed'
            + '\n************************************************\n');

        //Filter Maintenance and Well Servicing Work Orders
        List<HOG_Maintenance_Servicing_Form__c> maintenanceServicingWorkOrdersNew = 
            (Trigger.isInsert || Trigger.isUpdate) ? 
            MaintenanceServicingUtilities.GetMaintenanceWorkOrders(Trigger.new) : null;
        List<HOG_Maintenance_Servicing_Form__c> maintenanceServicingWorkOrdersOld =
            (Trigger.isDelete || Trigger.isUpdate) ?
            MaintenanceServicingUtilities.GetMaintenanceWorkOrders(Trigger.old) : null;
        List<HOG_Maintenance_Servicing_Form__c> wellServicingWorkOrdersNew =
            (Trigger.isInsert || Trigger.isUpdate) ? 
            MaintenanceServicingUtilities.GetWellServicingWorkOrders(Trigger.new) : null;
        List<HOG_Maintenance_Servicing_Form__c> wellServicingWorkOrdersOld =
            (Trigger.isDelete || Trigger.isUpdate) ?
            MaintenanceServicingUtilities.GetWellServicingWorkOrders(Trigger.old) : null;

        if (Trigger.isDelete && 
            maintenanceServicingWorkOrdersOld != null && !maintenanceServicingWorkOrdersOld.isEmpty()) 
            MaintenanceServicingUtilities.TriggerBeforeDeleteWorkOrder(maintenanceServicingWorkOrdersOld);
                        
        if (Trigger.isUpdate)
        {
            if (Trigger.isBefore) {       
                if(maintenanceServicingWorkOrdersNew != null && !maintenanceServicingWorkOrdersNew.isEmpty())
                    MaintenanceServicingUtilities.TriggerBeforeUpdateWorkOrder(maintenanceServicingWorkOrdersNew, Trigger.oldMap);
                
                if(wellServicingWorkOrdersNew != null && !wellServicingWorkOrdersNew.isEmpty()) 
                    WellServicingUtilities.TriggerBeforeUpdateWellServicingForm(wellServicingWorkOrdersNew, Trigger.oldMap);
            } else {
                if(maintenanceServicingWorkOrdersNew != null && !maintenanceServicingWorkOrdersNew.isEmpty())
                    MaintenanceServicingUtilities.TriggerAfterUpdateWorkOrder(maintenanceServicingWorkOrdersNew, Trigger.oldMap);
            }
        }
                
        if (Trigger.isInsert &&
            maintenanceServicingWorkOrdersNew != null && !maintenanceServicingWorkOrdersNew.isEmpty())
        {
            if (Trigger.isBefore)
                MaintenanceServicingUtilities.TriggerBeforeInsertWorkOrder(maintenanceServicingWorkOrdersNew);
            else
                MaintenanceServicingUtilities.TriggerAfterInsertWorkOrder(maintenanceServicingWorkOrdersNew);
        }        
    }    
}