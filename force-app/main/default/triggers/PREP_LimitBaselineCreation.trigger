trigger PREP_LimitBaselineCreation on PREP_Baseline__c (before insert) 
{
    
    List< PREP_Baseline__c > baselineRecords = new List< PREP_Baseline__c >();
    
    RecordType projId = [SELECT Id From RecordType WHERE Name = 'Projects Baseline'];
    
    for(PREP_Baseline__c b : Trigger.New)
    {   
        if(b.RecordTypeId != projId.Id)
        {
            baselineRecords = [SELECT Id FROM PREP_Baseline__c WHERE Initiative_Id__c = :b.Initiative_Id__c];
            
            if(baselineRecords.size() > 0)
            {
                b.addError('ERROR: Unable to create a second baseline record on an Initiative');
            }
        }
    }
}