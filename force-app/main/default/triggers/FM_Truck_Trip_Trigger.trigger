trigger FM_Truck_Trip_Trigger on FM_Truck_Trip__c (before update, after update) {
   System.debug('\n*****************************************\n'
        + 'METHOD: FM_Truck_Trip_Trigger()'
        + '\nTrigger: executed'
        + '\n************************************************\n');

    if (FM_Utilities.executeTriggerCode)
    {
        if (Trigger.isBefore && Trigger.isInsert) {
            FM_Utilities.TriggerBeforeInsertTruckTrip(Trigger.new);
        } else if (Trigger.isBefore && Trigger.isUpdate) {
            FM_Utilities.TriggerBeforeUpdateTruckTrip(Trigger.new, Trigger.oldMap);
        } else if (Trigger.isAfter && Trigger.isUpdate) {
            FM_Utilities.TriggerAfterUpdateTruckTrip(Trigger.new, Trigger.oldMap);
        }
    }
}