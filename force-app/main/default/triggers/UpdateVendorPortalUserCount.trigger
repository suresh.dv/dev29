trigger UpdateVendorPortalUserCount on Contact (after insert, after delete,after undelete,after update) {
	if(VendorPortalUtility.portalExecuteTriggerCode == true){
		if(Trigger.isUpdate){
			VendorPortalUtility.UpdateUserCountOnAccount(Trigger.New);
		}
	}
}