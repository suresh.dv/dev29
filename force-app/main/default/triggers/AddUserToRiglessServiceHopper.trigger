// ====================================================================
// Automatically add users with certain permission sets to the 
// rigless service hopper queue
trigger AddUserToRiglessServiceHopper on User (after update) {
  // List of the permission sets that belong to 
  Set<String> validPermissionSets = 
     new Set<String>{'HOG_Well_Site_Supervisor'};
     
  // Get the permission set Ids
  Set<Id> permissionSetIds = new Set<Id>();
  
  for (PermissionSet currentPermission : 
       [SELECT Id FROM PermissionSet 
                  WHERE Name IN :validPermissionSets]) {
    permissionSetIds.add(currentPermission.Id);
  }
  
  System.debug(permissionSetIds);
  
  // The rigless service hopper queue
  Set<Id> riglessQueueMembers = new Set<Id>();
  Id riglessQueueId = null;
  for (GroupMember existingGroupMember :
       [SELECT GroupId, UserOrGroupId 
               FROM GroupMember
               WHERE Group.Name = 'Rigless Service Hopper']) {
    if (riglessQueueId == null) {
      riglessQueueId = existingGroupMember.GroupId;
    }
    riglessQueueMembers.add(existingGroupMember.UserOrGroupId);
  }    
  
  System.debug(riglessQueueMembers);         
 
  // List of the users who should no longer be in 
  // rigless service hopper queue.
  Set<Id>userToBeRemoved = new Set<Id>();
  
  // List of the users to be added to the queue.
  List<GroupMember>userToBeAdded = new List<GroupMember>();
  
  // Ids of the users who have been added
  Set<Id>userAdded = new Set<Id>();

  // If the user has valid permission set and is not already in 
  // the rigless service hopper queue, add the user to the queue.
  for (PermissionSetAssignment assignment: 
       [SELECT AssigneeId, PermissionSetId 
               FROM PermissionSetAssignment 
               WHERE AssigneeId IN :Trigger.newMap.keySet()]) {
    
    //Boolean hasValidPermission = false; 
         
    // Add the user to queue if he has the valid permission set   
    if (permissionSetIds.contains(assignment.PermissionSetId)&& 
        !riglessQueueMembers.contains(assignment.AssigneeId) &&
        !userAdded.contains(assignment.AssigneeId)) {
        //hasValidPermission = true;
        userAdded.add(assignment.AssigneeId);
        userToBeAdded.add(new GroupMember(
                              GroupId = riglessQueueId,
                              UserOrGroupId = assignment.AssigneeId));
        System.debug(assignment.AssigneeId);
    }
    
    /*if (!hasValidPermission) {
      userToBeRemoved.add(currUser.Id);
    }*/
  }
  
  // Insert and remove users from the queue.
  insert userToBeAdded;
  
  /*List<GroupMember>removedUser = 
    [SELECT Id FROM GroupMember WHERE GroupId = :riglessQueueId AND 
                                      UserOrGroupId In :userToBeRemoved];
  
  delete removedUser;
  */
}