trigger PREP_Lock_RTs on PREP_Baseline__c (after insert, after update) 
{

    List<PREP_Business_Unit_Baseline_Allocation__c> buaRecord = new List<PREP_Business_Unit_Baseline_Allocation__c>();
    List<PREP_Mat_Serv_Group_Baseline_Allocation__c> msgRecord = new List<PREP_Mat_Serv_Group_Baseline_Allocation__c>();
    

    for(PREP_Baseline__c B : Trigger.New)
    {   
        
        RecordType recordName = [SELECT DeveloperName From RecordType WHERE Id = :B.RecordTypeId];
        
        if(recordName.DeveloperName == 'Locked_Global_Baseline' || recordName.DeveloperName == 'Locked_Local_Baseline' && B.Status__c == 'Approved')
        {
            
           buaRecord = [SELECT Id, RecordTypeId FROM PREP_Business_Unit_Baseline_Allocation__c WHERE Baseline_Id__c = :B.Id];
            
           RecordType msgId = [SELECT Id From RecordType WHERE DeveloperName = 'Locked_Material_Service_Group_Baseline_Allocation'];
           RecordType buaId = [SELECT Id From RecordType WHERE DeveloperName = 'Locked_Business_Unit_Baseline_Allocation'];


           for(Integer i = 0; i < buaRecord.size(); i++)
           {
           
               buaRecord[i].RecordTypeId = buaId.Id;
               update buaRecord[i];
           }
            
           msgRecord = [SELECT Id, RecordTypeId FROM PREP_Mat_Serv_Group_Baseline_Allocation__c WHERE Baseline_Id__c = :B.Id];

           for(Integer i = 0; i < msgRecord.size(); i++)
           {
               msgRecord[i].RecordTypeId = msgId.Id;
               update msgRecord[i];
           }
        }   
    }
}