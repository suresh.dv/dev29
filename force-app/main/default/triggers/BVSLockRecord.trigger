trigger BVSLockRecord on PREP_Business_Value_Submission__c (before update) {
    if (Trigger.isBefore) {
        if(Trigger.isUpdate){
            Boolean isAllowedToEdit = false;
            
            for(PREP_Business_Value_Submission__c b : Trigger.New){
                if(b.Status__c == 'Approved'){
                    List<PermissionSetAssignment> permissionSetList = [SELECT AssigneeId, PermissionSet.Name FROM PermissionSetAssignment where AssigneeId = :UserInfo.getUserId() ];
                    
                    for(PermissionSetAssignment a : permissionSetList){
                        if(a.PermissionSet.Name == 'PREP_Initiative_Admin'){
                            isAllowedToEdit = true;
                        }
                    }         
                }
            }
            
            
            if(isAllowedToEdit){
                trigger.new[0].name.addError('Sorry no permission to edit this record');
            } 
        }
    }
}