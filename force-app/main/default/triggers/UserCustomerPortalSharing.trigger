trigger UserCustomerPortalSharing on User (after insert) {
	
	Map<Id, Id> userToAccountMap = new Map<Id, Id>();
	
	// This get's the Parent Account Id and also creates a set of child account ids
    Map<Id, Set<Id>> parentAccountToChildAccountMap = new Map<Id, Set<Id>> ();
    
    // This will hold details to create the AccountShare records which will be created in the FutureMethod
    List<Id> userIdList = new List<Id>();
    List<Id> accountIdList = new List<Id>();
    
    // Create User ID to Account ID Map
    for(User user : Trigger.New) {
    	System.debug('User IsPortalEnabled Flag : ' + user.IsPortalEnabled);
    	System.debug('User AccountId : ' + user.AccountId);
    		
    	if(Trigger.isInsert && user.AccountId != Null) {
    		userToAccountMap.put(user.Id, user.AccountId);
    	}
    }
    System.debug('userToAccountMap : ' + userToAccountMap);
    
    // Create Parent Account ID to Child Account ID set Map
    if(userToAccountMap.size() > 0) {
	    for(Account account : [SELECT Id, ParentId FROM Account WHERE ParentId in :userToAccountMap.values()]) {
	    	Set<Id> childAccountIds = parentAccountToChildAccountMap.get(account.ParentId);
	            
	        if(childAccountIds == Null)
	            childAccountIds = new Set<Id> ();
	        
	        childAccountIds.add(account.Id);
	            
	        parentAccountToChildAccountMap.put(account.ParentId, childAccountIds);
	    }
	    System.debug('parentAccountToChildAccountMap : ' + parentAccountToChildAccountMap);
	    
	    if(parentAccountToChildAccountMap.size() > 0) {
		    for(Id userId : userToAccountMap.KeySet()) {
		    	for(Id childAccountId : parentAccountToChildAccountMap.get(userToAccountMap.get(userId))) {
		    		userIdList.add(userId);
		        	accountIdList.add(childAccountId);
		    	}    	
		    }
		    
		    System.debug('Future Method to create AccountShare records is called');
		    FutureMethodUtility.createAccountShare(userIdList, accountIdList);
	    }
    }
}