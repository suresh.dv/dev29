// ====================================================================
// Actions to be triggered before insert or update of Case
// The actions include
// 1) If closed comments have been entered, create a case comment record
//    with matching data
trigger CaseAfterInsertUpdate on Case (after insert, after update) {
  List<Case>retailLocationChanged = new List<Case>();
  
  List<Case>closedCommentsEntered = new List<Case>();
  
  
  for (Case triggered: Trigger.new) {
    
    // Closed comments entered
    if ((Trigger.isInsert && triggered.Closed_Comments__c != null) ||
        (Trigger.isUpdate && 
         triggered.Closed_Comments__c != null &&
         triggered.Closed_Comments__c != 
         Trigger.oldMap.get(triggered.Id).Closed_Comments__c)) {
       closedCommentsEntered.add(triggered);    
    }
  }
  
  List<CaseComment> newComments = 
      CaseTriggerHandler.onClosedComment(closedCommentsEntered);
  insert newComments;
}