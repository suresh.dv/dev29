/**********************************************************************************
  CreatedBy : Subhashini Katta
  Organization:Accenture
  Purpose   :  Trigger For AutoAssignment of Status, Ticket Creator, Entitlement, Role
  Handler Class: RaM_CaseTriggerHandler 
  Test Class:  RaM_CaseTriggerHandler_Test
  Version:1.0.0.1
**********************************************************************************/
trigger RaM_AutoAssignP1Ticket on Case (before insert,before update, after update) {
   
  
   if(Trigger.isBefore && Trigger.isInsert){
        system.debug('------Trigger.isBefore------'+Trigger.isBefore);
        system.debug('------Trigger.isInsert------'+Trigger.isInsert);
      //To update status,Origin,entitlment,vendor name and Assign fields in Ticket creation process
      RaM_CaseTriggerHandler.CaseAssignmentProcess(Trigger.New,new Map<id,Case>(),true); 
      //To Change case origin 
      RaM_CaseTriggerHandler.ChangeCaseOrgin(Trigger.New);    
   }  
   
   
   if(Trigger.isBefore && Trigger.isUpdate){
        system.debug('------Trigger.isBefore------'+Trigger.isBefore);
        system.debug('------Trigger.isUpdate------'+Trigger.isUpdate);
      //To update Origin,entitlment,vendor name and Assign fields in Ticket updation process  
      RaM_CaseTriggerHandler.CaseAssignmentProcess(Trigger.New,Trigger.oldMap,false);
  }   
      
    // Update case milestone when status changed to Vendor Complete: 12/1/2017 Added by Akanksha
    if(Trigger.isAfter && Trigger.isUpdate) 
       RaM_CaseTriggerHandler.completeCaseMilestone(Trigger.New,Trigger.oldMap);  
 }