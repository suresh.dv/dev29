trigger FM_LoadConfirmationTrigger on FM_Load_Confirmation__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {


	if(FM_Utilities.executeTriggerCode) {
		if (Trigger.isBefore) {
			
		} else if (Trigger.isAfter) {
	    	if(Trigger.isInsert) {
	    		FM_LoadRequest_Utilities.TriggerAfterInsertLoadConfirmation(Trigger.new);
	    	} else if (Trigger.isUpdate) {
	    		FM_LoadRequest_Utilities.TriggerAfterUpdateLoadConfirmation(Trigger.new, Trigger.oldMap);
	    	}
		}
	}
}