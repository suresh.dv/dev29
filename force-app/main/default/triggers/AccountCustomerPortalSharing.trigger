trigger AccountCustomerPortalSharing on Account (after update, after insert) {
    Set<Id> userIds = new Set<Id>();
    
    Set<Id> accountIds2 = new Set<Id>();

    Set<Id> accountIds = new Set<Id>();   
    
    Map<Id, Boolean> accountToOldIsCustomerPortalValueMap = new Map<Id, Boolean>();
    //Map<Id, AccountShare> UserIdToAccountShareMap = new Map<Id, AccountShare>();
    
    // Used for Re-parenting - delete account share records 
    Map<Id, Id> OldParentIdToAccountIdMap = new Map<Id, Id>();
    Map<Id, Id> contactToAccountIdMap = new Map<Id, Id>();
    Map<Id, Id> userToAccountIdMap = new Map<Id, Id>();
    
    // Used for Re-parenting - create new AccountShare records
    Map<Id, Set<Id>> parentAcctToChildAcctsMap = new Map<Id, Set<Id>> ();
    List<AccountShare> accountShareToCreate = new List<AccountShare> ();
    
    List<AccountShare> accountShareToDelete = new List<AccountShare>(); 
    
    // Loop through Trigger.New and generate a Map of Account Id to Old value of IsCutomerPortal
    for(Account account : Trigger.New) {
        if(Trigger.isUpdate) {
            accountToOldIsCustomerPortalValueMap.put(account.Id, Trigger.OldMap.get(account.Id).IsCustomerPortal);
            
            // If the ParentID of the account changed, generate OldParentIDToAccountIdMap
            if(account.ParentId != Trigger.OldMap.get(account.Id).ParentId && Trigger.OldMap.get(account.Id).ParentId != Null) {
                OldParentIdToAccountIdMap.put(Trigger.OldMap.get(account.Id).ParentId, account.Id);
            }
        }
        
        if((Trigger.isUpdate && Trigger.OldMap.get(account.Id).ParentId == Null && account.ParentId != Null) || (Trigger.isInsert && account.ParentId != Null)) {
            accountIds2.add(account.Id);
        }
    }
    
    if(Trigger.isUpdate) {
        // 
        for(Account account : [SELECT Id, ParentId, Parent.IsCustomerPortal FROM Account WHERE ParentId in : accountToOldIsCustomerPortalValueMap.KeySet()]) {
            if(accountToOldIsCustomerPortalValueMap.get(account.ParentId) == True && account.Parent.IsCustomerPortal == False) {
                accountIds.add(account.ParentId);
            }
        }
        
        //
        for(User user : [SELECT Id, AccountId FROM User WHERE AccountId in : accountIds]) {
            userIds.add(user.Id);   
        }
        
        accountShareToDelete = [SELECT Id, AccountId, UserOrGroupId FROM AccountShare WHERE UserOrGroupId in : userIds];
        
        // Re-parenting - delete account share records
        System.debug('OldParentIdToAccountIdMap : ' + OldParentIdToAccountIdMap);
        
        //
        for(User user : [SELECT Id, AccountId FROM User WHERE AccountId =:OldParentIdToAccountIdMap.KeySet()]) {
            userToAccountIdMap.put(user.Id, OldParentIdToAccountIdMap.get(user.AccountId));  
        }
        System.debug('userToAccountIdMap : ' + userToAccountIdMap);
        
        // Query AccountShare object with the UserID and AccountId and add it to accountShareToDelete list.
        for(AccountShare accountShare : [SELECT Id, UserOrGroupId, AccountId, RowCause FROM AccountShare WHERE AccountId in :userToAccountIdMap.values()]) {
            if(accountShare.AccountId == userToAccountIdMap.get(accountShare.UserOrGroupId)) {
                accountShareToDelete.add(accountShare);
            }
        }
        System.debug('accountShareToDelete : ' + accountShareToDelete);
        
        // Delete the AccountShare records
        if(accountShareToDelete.size() > 0) {
            delete accountShareToDelete; 
        }
    }
    
    // Re-parenting - create new AccountShare records
    
    
    // If the account and it's parent account are enabled for Customer Portal then add it to parentAcctToChildAcctsMap
    for(Account account : [SELECT Id, IsCustomerPortal, ParentId, Parent.IsCustomerPortal FROM Account WHERE Id in : OldParentIdToAccountIdMap.values() OR Id in : accountIds2]) {
        if(account.ParentId != Null) {
            Set<Id> childAccountIds = parentAcctToChildAcctsMap.get(account.ParentId);
            
            if(childAccountIds == Null)
                childAccountIds = new Set<Id> ();
            
            childAccountIds.add(account.Id);
            
            parentAcctToChildAcctsMap.put(account.ParentId, childAccountIds);
        }
    }
    
    System.debug('parentAcctToChildAcctsMap : ' + parentAcctToChildAcctsMap);
    // Query the Customer Portal users that are associated to the new Parent Account record and create AccountShare records for those users to the child account.
    for(User user : [SELECT Id, ContactId, AccountId FROM User WHERE AccountId in :parentAcctToChildAcctsMap.KeySet() AND IsPortalEnabled = True AND IsActive = True]) {
        Set<Id> childAccountIds = parentAcctToChildAcctsMap.get(user.AccountId);
        
        for(Id childAccountId : childAccountIds) {
        
            AccountShare accountShare = new AccountShare();
            accountShare.AccountId = childAccountId;
            accountShare.UserOrGroupId = user.Id;
            accountShare.AccountAccessLevel = 'Read';
            accountShare.OpportunityAccessLevel = 'Read';
            accountShare.CaseAccessLevel = 'Edit';
            accountShareToCreate.add(accountShare);
        }
    }
    
    System.debug('accountShareToCreate.size() : ' + accountShareToCreate.size());
    System.debug('accountShareToCreate : ' + accountShareToCreate);
    
    if(accountShareToCreate.size() > 0) {
        insert accountShareToCreate;
    }
}