trigger VendorPersonnelRecord on Vendor_Personnel__c (before insert, before delete, before update) {

    List<Vendor_Personnel__c> vendorPersonnelOld = Trigger.old;
    List<Vendor_Personnel__c> vendorPersonnelNew = Trigger.new;

    String riglessServicingFormID;
    
    if (Trigger.isDelete)
        riglessServicingFormID = vendorPersonnelOld[0].Rigless_Servicing_Form__c;
    else
        riglessServicingFormID = vendorPersonnelNew[0].Rigless_Servicing_Form__c;
    
    dateTime minValueOffLocation;

    // Get the lowest recorded value of Vendor Off Location
    List<Vendor_Personnel__c> vendorPersonnelOffMin =
          [Select Off_Location_Time__c From Vendor_Personnel__c Where Rigless_Servicing_Form__c =: riglessServicingFormID Order By Off_Location_Time__c Asc Limit 1];

    if (vendorPersonnelOffMin.size() > 0)
        minValueOffLocation = vendorPersonnelOffMin[0].Off_Location_Time__c;
    else
        minValueOffLocation = null;
            
    dateTime maxValueOffLocation;
    
    // Get the highest recorded value of Vendor Off Location
    List<Vendor_Personnel__c> vendorPersonnelOffMax =
      [Select Off_Location_Time__c From Vendor_Personnel__c Where Rigless_Servicing_Form__c =: riglessServicingFormID And Off_Location_Time__c != Null Order By Off_Location_Time__c Desc Limit 1];

    if (vendorPersonnelOffMax.size() > 0)
        maxValueOffLocation = vendorPersonnelOffMax[0].Off_Location_Time__c;
    else
        maxValueOffLocation = null;

    // Get the Service Status of the Rigless Service
    String serviceStatus =
        [Select Service_Status__c From Rigless_Servicing_Form__c Where Id =: riglessServicingFormID][0].Service_Status__c;

    if (Trigger.isDelete) 
    {              
        if (serviceStatus == 'Complete')
            vendorPersonnelOld[0].addError('You can not delete any record of Vendor Invoicing & Personnel, you must first set the Rigless Service Status to a status other than "Complete".');
        
        /*    
        else if ((vendorPersonnelOld[0].Off_Location_Time__c != Null && vendorPersonnelOld[0].Off_Location_Time__c != maxValueOffLocation) || (vendorPersonnelOld[0].Off_Location_Time__c == maxValueOffLocation && minValueOffLocation == Null))
            vendorPersonnelOld[0].addError('You can not delete this record, you must first delete newer record(s) of Vendor Invoicing & Personnel.');
        */
    }
    
    if (Trigger.isUpdate) 
    {              
        if (serviceStatus == 'Complete')
            vendorPersonnelNew[0].addError('You can not edit any record of Vendor Invoicing & Personnel, you must first set the Rigless Service Status to a status other than "Complete".');
        /*    
        else if (vendorPersonnelOld[0].Off_Location_Time__c == Null || (vendorPersonnelOld[0].Off_Location_Time__c == maxValueOffLocation && minValueOffLocation != Null))
        {
            dateTime maxValueOnLocation = maxValueOffLocation;

            // Get the previous record value of Vendor Off Location
            List<Vendor_Personnel__c> vendorPersonnelOnMax =
              [Select Off_Location_Time__c From Vendor_Personnel__c Where Rigless_Servicing_Form__c =: riglessServicingFormID And Id <>: vendorPersonnelOld[0].Id And Off_Location_Time__c != Null Order By Off_Location_Time__c Desc Limit 1];
        
            if (vendorPersonnelOnMax.size() > 0)
                maxValueOnLocation = vendorPersonnelOnMax[0].Off_Location_Time__c;
            else
                maxValueOnLocation = null;

            if (maxValueOnLocation > vendorPersonnelNew[0].On_Location_Time__c)
                vendorPersonnelNew[0].addError('On Location must be greater than the value of the previous Vendor Invoicing & Personnel Vendor Off Location record.');
        }
        else if (vendorPersonnelOld[0].On_Location_Time__c != vendorPersonnelNew[0].On_Location_Time__c || vendorPersonnelOld[0].Off_Location_Time__c != vendorPersonnelNew[0].Off_Location_Time__c)
            vendorPersonnelNew[0].addError('Vendor Invoicing & Personnel record is locked, delete newer record(s) to unlock.');
       */
    }    
    
    if (Trigger.isInsert)
    {
        if (serviceStatus == 'Complete')
            vendorPersonnelNew[0].addError('You can not create a record of Vendor Invoicing & Personnel, you must first set the Rigless Service Status to a status other than "Complete".');
        /*    
        else if (minValueOffLocation == Null && vendorPersonnelOffMin.size() > 0)
            vendorPersonnelNew[0].addError('The value of the previous Vendor Invoicing & Personnel Vendor Off Location must not be empty');    
        else if (maxValueOffLocation > vendorPersonnelNew[0].On_Location_Time__c)
            vendorPersonnelNew[0].addError('On Location must be greater than the value of the previous Vendor Invoicing & Personnel Vendor Off Location record');    
        */
    }
}