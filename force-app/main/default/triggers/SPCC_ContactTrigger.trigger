trigger SPCC_ContactTrigger on Contact (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

	if(SPCC_Utilities.executeTriggerCode) {
		if (Trigger.isBefore) {
	    	//call your handler.before method
	    
		} else if (Trigger.isAfter) {
	    	if(Trigger.isInsert) {
	    		SPCC_Utilities.TriggerAfterInsertContact(Trigger.new);
	    	} else if (Trigger.isDelete) {
	    		SPCC_Utilities.TriggerAfterDeleteContact(Trigger.old);
	    	}
		}
	}
}