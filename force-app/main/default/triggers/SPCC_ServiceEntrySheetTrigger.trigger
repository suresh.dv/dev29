trigger SPCC_ServiceEntrySheetTrigger on SPCC_Service_Entry_Sheet__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

		if (Trigger.isBefore) {
	    	//call your handler.before method
	    
		} else if (Trigger.isAfter) {
	    	if(Trigger.isInsert) {
	    		SPCC_Utilities.TriggerAfterInsertServiceEntrySheet(Trigger.new);
	    	} else if (Trigger.isUpdate) {
	    		SPCC_Utilities.TriggerAfterUpdateServiceEntrySheet(Trigger.new, Trigger.oldMap);
	    	} else if (Trigger.isDelete) {
	    		SPCC_Utilities.TriggerAfterDeleteServiceEntrySheet(Trigger.old);
	    	}
	    
		}
}