// ====================================================================
// Set Day 1 Rig On Location time of the rigless service form and
// Set the Day value of the Service time.
trigger SetDayOfTheServiceAndDay1RigOnLocation on Service_Time__c (Before Insert, Before Update) {
  // The map of the new service times
  Map<Id, Service_Time__c> serviceTimeProcessed = new Map<Id, Service_Time__c>();

  // Populate the new service times map
  for (Service_Time__c currServiceTime: Trigger.new) {
    serviceTimeProcessed.put(currServiceTime.Rigless_Servicing_Form__c, currServiceTime);
  }
  
  // Rigless Service that is modified by given service time
  List <Rigless_Servicing_Form__c> serviceToBeUpdated = new List<Rigless_Servicing_Form__c>();
  
  // Update Day 1 rig on location time of the rigless service form
  // and the day value of the service time.
  for (Rigless_Servicing_Form__c serviceLogged : 
       [SELECT Id, Day_1_Rig_On_Location__c, 
              (SELECT Day__c, Rig_On_Location__c FROM Service_Times__r)
               FROM Rigless_Servicing_Form__c 
               WHERE Id IN :serviceTimeProcessed.keySet()]) {
    
     // This is the first service time created for this rigless service form
     if (serviceLogged.Service_Times__r.size() == 0) {
       serviceLogged.Day_1_Rig_On_Location__c = 
         serviceTimeProcessed.get(serviceLogged.Id).Rig_On_Location__c;
       serviceToBeUpdated.add(serviceLogged);
       serviceTimeProcessed.get(serviceLogged.Id).Day__c = 'Day 1';
     }
     
     // Set the Day 1 Rig on Location time
     else if (serviceTimeProcessed.get(serviceLogged.Id).Day__c == 'Day 1') {
       serviceLogged.Day_1_Rig_On_Location__c = 
         serviceTimeProcessed.get(serviceLogged.Id).Rig_On_Location__c;
       serviceToBeUpdated.add(serviceLogged);
     } 
     
     // Set the Day value
     else if (serviceTimeProcessed.get(serviceLogged.Id).Day__c == null) {
       serviceTimeProcessed.get(serviceLogged.Id).Day__c = 
         'Day ' + String.valueOf(serviceLogged.Service_Times__r.size() + 1);
     }
  } // for (Rigless_Servicing_Form__c serviceLogged: ...
  
  
  // Update the rigless service form.
  update serviceToBeUpdated;
   
}