/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Trigger for HOG_Vent_Gas_Alert_Task__c. 
				Validations mostly.
Test class:		HOG_VentGasTaskCtrlTest
History:        jschn 05.18.2018 - Created.
**************************************************************************************************/
trigger HOG_VentGasAlertTaskTrigger on HOG_Vent_Gas_Alert_Task__c (before insert, before update) {
	if (Trigger.isBefore) {
		if(Trigger.isInsert)
    		HOG_VentGas_Utilities.ventGasTaskTriggerBeforeInsert(Trigger.new);
		if(Trigger.isUpdate)
			HOG_VentGas_Utilities.ventGasTaskTriggerBeforeUpdate(Trigger.new, Trigger.oldMap);
	} 
}