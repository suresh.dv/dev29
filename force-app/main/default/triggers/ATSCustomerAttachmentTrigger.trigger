trigger ATSCustomerAttachmentTrigger on ATS_Customer_Attachment_Junction__c( before insert, before update, after insert, after update ){
    ATSCustomerAttachmentTriggerHelper.newAtsCustomerAtachmentList = trigger.new;
    ATSCustomerAttachmentTriggerHelper.oldAtsCustomerAtachmentList = trigger.old;
    ATSCustomerAttachmentTriggerHelper.newAtsCustomerAtachmentMap = trigger.newMap;
    ATSCustomerAttachmentTriggerHelper.oldAtsCustomerAtachmentMap = trigger.oldMap;
    
    if( ATSCustomerAttachmentTriggerHelper.runTrigger ){
        if( trigger.isBefore ){
            if( trigger.isInsert ){
                ATSCustomerAttachmentTriggerHelper.setAttachmentName();
            }
        }
    }
}