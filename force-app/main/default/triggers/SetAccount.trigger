trigger SetAccount on Bill_To__c (before insert, before update) 
{    
    Set<String> customerNumbers = new Set<String>();
    List<String> customerNumbersList = new List<String>(); 
    Map<String, Id> custNumberAccountId = new Map<String, Id>();    
    for(Bill_To__c  billTo : Trigger.new)
    {
        if(billTo.Customer__c != null)
        {
            customerNumbers.add(billTo.Customer__c);        
        }
    }
    customerNumbersList.addAll(customerNumbers);
    List<Account> accountsCustNumber = [SELECT Id, Customer_Number__c,AccountNumber FROM Account WHERE ( Customer_Number__c!=NULL AND Customer_Number__c IN :customerNumbersList) ];
    List<Account> accountAcctNumber = [SELECT Id, Customer_Number__c,AccountNumber FROM Account WHERE ( AccountNumber!=NULL AND AccountNumber IN :customerNumbersList) ];
    for(Account account : accountsCustNumber )
    {
        custNumberAccountId.put(account.Customer_Number__c, account.Id);
    }
    for(Account account : accountAcctNumber )
    {
        custNumberAccountId.put(account.AccountNumber, account.Id);
    }
    for(Bill_To__c  billToRecord : Trigger.new)
    {
        if( billToRecord.Customer__c!=null && custNumberAccountId.containsKey(billToRecord.Customer__c ) )
        {
            billToRecord.Account__c = custNumberAccountId.get(billToRecord.Customer__c);
        }
    }
}