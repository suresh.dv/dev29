//KK 13/Mar/2014: need to accommodate after update and after delete as well, as admin level people can 
// update the carseal status directly
// ****************************************************************************
// Update current status of carseal when new status is inserted
//trigger UpdateCurrentStatus on Carseal_Status__c (after insert) {
trigger CarsealStatus_UpdateCurrentStatus on Carseal_Status__c (after insert, after update, after delete, after undelete) {
  
  // List of updated valves
  List<Carseal__c>updatedCarseals = new List<Carseal__c>();
  
  // Map of updated status and corresponding valve
  Map<Id, Carseal_Status__c> updatedStatuses = new Map<Id, Carseal_Status__c>();
  
  if (Trigger.isDelete)
  {
  for (Carseal_Status__c newStatus: Trigger.old) {
    updatedStatuses.put(newStatus.Carseal__c, newStatus);
  }
  	
  }
  else
  {
  for (Carseal_Status__c newStatus: Trigger.new) {
    updatedStatuses.put(newStatus.Carseal__c, newStatus);
  }
  	
  }
  
  for (Carseal__c updatedCarseal: [SELECT Id, Carseal_Status_Lookup__r.Status_Lookup__c,
  	(SELECT Date__c, Status_Lookup__c FROM Carseal_Statuses__r ORDER BY Date__c DESC) 
  	FROM Carseal__c 
  	WHERE Id IN :updatedStatuses.keySet()])
  	{
  		if (updatedStatuses.get(updatedCarseal.Id).Status_Lookup__c != updatedCarseal.Carseal_Status_Lookup__r.Status_Lookup__c)
  		{
  			if (Trigger.isInsert)
  			{
  				updatedCarseal.Carseal_Status_Lookup__c = updatedStatuses.get(updatedCarseal.Id).Id;
  			}
  			else
  			{
  				if (updatedCarseal.Carseal_Statuses__r.size() > 0)
  				{
	  				updatedCarseal.Carseal_Status_Lookup__c = updatedCarseal.Carseal_Statuses__r[0].Id;
  				}
  				else
  				{
  					updatedCarseal.Carseal_Status_Lookup__c = null;
  				}
  			}
  			updatedCarseals.add(updatedCarseal);  
  		}
  	}
  update updatedCarseals;
}