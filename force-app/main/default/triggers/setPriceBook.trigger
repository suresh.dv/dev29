trigger setPriceBook on Opportunity (before insert){
    string PRICEBOOK_ASPHALT = 'Asphalt Price Book';
    string PRICEBOOK_EMULSION = 'Emulsion Price Book';
    string PRICEBOOK_RESIDUAL = 'Residual Price Book';   
    
    string OPPORTUNITY_DEVELOPERNAME_ASPHALT = 'ATS_Asphalt_Product_Category';
    string OPPORTUNITY_DEVELOPERNAME_EMULSION = 'ATS_Emulsion_Product_Category';
    string OPPORTUNITY_DEVELOPERNAME_RESIDUAL = 'ATS_Residual_Product_Category';
    
    List <RecordType> asphaltRecordTypeList = [Select Id From RecordType  Where SobjectType = 'Opportunity' and DeveloperName = :OPPORTUNITY_DEVELOPERNAME_ASPHALT];
    List <RecordType> emulsionRecordTypeList = [Select Id From RecordType  Where SobjectType = 'Opportunity' and DeveloperName = :OPPORTUNITY_DEVELOPERNAME_EMULSION];
    List <RecordType> residualRecordTypeList = [Select Id From RecordType  Where SobjectType = 'Opportunity' and DeveloperName = :OPPORTUNITY_DEVELOPERNAME_RESIDUAL];
    
    List<Pricebook2> priceBooks = [select name, id, isactive from Pricebook2 where name = :PRICEBOOK_ASPHALT or name = :PRICEBOOK_EMULSION or name = :PRICEBOOK_RESIDUAL order by Name];

    if(priceBooks.size() == 0)
        return;              
        
    for( Opportunity oppty : trigger.new ) {        
        if(oppty.RecordTypeId == asphaltRecordTypeList[0].Id){
            oppty.Pricebook2Id = priceBooks[0].Id;
        }
        else if(oppty.RecordTypeId == emulsionRecordTypeList[0].Id){
            oppty.Pricebook2Id = priceBooks[1].Id;
        }
        else if(oppty.RecordTypeId == residualRecordTypeList[0].Id){
            oppty.Pricebook2Id = priceBooks[2].Id;
        }
    }
}