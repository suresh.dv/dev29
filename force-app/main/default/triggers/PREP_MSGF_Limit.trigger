trigger PREP_MSGF_Limit on PREP_Mat_Serv_Group_Forecast_Allocation__c (before insert) {
    
    List< PREP_Mat_Serv_Group_Forecast_Allocation__c > MSGFRecord = new List< PREP_Mat_Serv_Group_Forecast_Allocation__c >();
    
    for(PREP_Mat_Serv_Group_Forecast_Allocation__c MSGF : Trigger.New)
    {   
    
        MSGFRecord = [SELECT Id, Material_Group__c FROM PREP_Mat_Serv_Group_Forecast_Allocation__c WHERE Initiative_Status_Update_Id__c = :MSGF.Initiative_Status_Update_Id__c];
        
        for(Integer i = 0; i < MSGFRecord.size(); i++)
        {
            if(MSGF.Material_Group__c == MSGFRecord[i].Material_Group__c)
            {
               MSGF.addError('ERROR: You are unable to create a Material / Service Group Forecast Allocation with the same Material / Service Group');
            }
        }
        
    }
}