trigger Customer_PopuplateWonCustomerToATSTender on ATS_Tender_Customers__c (after delete, after insert, after update, after undelete) 
{
    Set<Id> tenderIDs = new Set<Id>();
    if (trigger.isInsert || trigger.isUnDelete)
    {
        for(ATS_Tender_Customers__c c : trigger.new)
        {
            tenderIDs.add(c.ATS_Parent_Opportunity__c);
        }
    }
    else if (trigger.isUpdate)
    {
        for(ATS_Tender_Customers__c c : trigger.new)
        {
            ATS_Tender_Customers__c beforeUpdate = trigger.oldMap.get(c.Id);
            if (c.Won__c != beforeUpdate.Won__c && c.Won__c)
                tenderIDs.add(c.ATS_Parent_Opportunity__c);
        }
    }
    else if (trigger.isDelete)
    {
        for(ATS_Tender_Customers__c c : trigger.old)
        {
            tenderIDs.add(c.ATS_Parent_Opportunity__c);
        }
    }
    List<ATS_Parent_Opportunity__c> tenderList = [select Id, selected_Customer__c, selected_Customer_Name__c,
                (select Id, Name, Account__r.Name from Customers__r where Won__c = true) 
            from ATS_Parent_Opportunity__c where Id in: tenderIds];
    
    for(ATS_Parent_Opportunity__c tender : tenderList)
    {
        if (tender.Customers__r != null && tender.Customers__r.size() > 0)
        {
           tender.Selected_Customer__c = tender.Customers__r[0].Name;
           tender.Selected_Customer_Name__c = tender.Customers__r[0].Account__r.Name;
        }
        else
        {
           tender.Selected_Customer__c = '';  
           tender.Selected_Customer_Name__c = '';  
        } 
    }        
    update tenderList;
}