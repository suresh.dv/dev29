trigger FM_StuckTruckTrigger on FM_Stuck_Truck__c (before update, before insert) {
	if(FM_Utilities.executeTriggerCode) {
		if(Trigger.isBefore) {
			if(Trigger.isInsert) {
				FM_Utilities.TriggerBeforeInsertStuckTruck(Trigger.new);
			} else if (Trigger.isUpdate) {
				FM_Utilities.TriggerBeforeUpdateStuckTruck(Trigger.new, Trigger.oldMap);
			}
		}
	}
}