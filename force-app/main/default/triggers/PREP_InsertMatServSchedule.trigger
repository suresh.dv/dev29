trigger PREP_InsertMatServSchedule on PREP_Mat_Serv_Group_Baseline_Allocation__c (after insert) 
{
    RecordType matId = [SELECT Id From RecordType WHERE Name = 'Projects Mat Group Baseline Allocation'];
    RecordType servId = [SELECT Id From RecordType WHERE Name = 'Projects Serv Group Baseline Allocation'];
    RecordType matScheId = [SELECT Id From RecordType WHERE Name = 'Mat Group Baseline Schedule'];
    RecordType servScheId = [SELECT Id From RecordType WHERE Name = 'Serv Group Baseline Schedule'];
    List<PREP_Mat_Serv_Group_Planned_Schedule__c> newScheduleList = new List<PREP_Mat_Serv_Group_Planned_Schedule__c>();
    for(PREP_Mat_Serv_Group_Baseline_Allocation__c MSGB : [select Id, Baseline_Id__r.Initiative_Id__r.Phase_4_Completion_Date__c, RecordTypeId 
    													from PREP_Mat_Serv_Group_Baseline_Allocation__c
    													where Id in: trigger.newMap.keySet()])
    {
         
         PREP_Mat_Serv_Group_Planned_Schedule__c MSGBSchedule = new PREP_Mat_Serv_Group_Planned_Schedule__c();
         MSGBSchedule.Mat_Serv_Group_Baseline_Allocation_Id__c = MSGB.Id;
         MSGBSchedule.Phase_4_Completion_Date__c = MSGB.Baseline_Id__r.Initiative_Id__r.Phase_4_Completion_Date__c; 
         If(MSGB.RecordTypeId == matId.Id)
         {
             MSGBSchedule.RecordTypeId = matScheId.Id;
		 }
         else if(MSGB.RecordTypeId == servId.Id)
         {
             MSGBSchedule.RecordTypeId = servScheId.Id;
             
		 }   
		  newScheduleList.add(MSGBSchedule);  
        
    }
    insert newScheduleList;
}