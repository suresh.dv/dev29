trigger PREP_BUAF_Limit on PREP_Business_Unit_Forecast_Allocation__c (before insert) {
    
    List<PREP_Business_Unit_Forecast_Allocation__c> BUAFRecord = new List<PREP_Business_Unit_Forecast_Allocation__c>();
    
    for(PREP_Business_Unit_Forecast_Allocation__c BUAF : Trigger.New)
    {   
        
        BUAFRecord = [SELECT Id, Allocation_Level__c, Business_Unit__c FROM PREP_Business_Unit_Forecast_Allocation__c WHERE Initiative_Status_Update_Id__c = :BUAF.Initiative_Status_Update_Id__c];
        
        for(Integer i = 0; i < BUAFRecord.size(); i++)
        {
            if(BUAF.Allocation_Level__c == BUAFRecord[i].Allocation_Level__c && BUAF.Business_Unit__c == BUAFRecord[i].Business_Unit__c)
            {
               BUAF.addError('ERROR: The Business Unit/Allocation Level Combination you selected is already in use');
            }
        }
    }
}