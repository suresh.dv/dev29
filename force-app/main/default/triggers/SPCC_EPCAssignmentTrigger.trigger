trigger SPCC_EPCAssignmentTrigger on SPCC_EPC_Assignment__c (
    before insert, 
    before update, 
    before delete, 
    after insert, 
    after update, 
    after delete, 
    after undelete) {

    if(SPCC_Utilities.executeTriggerCode) {
        if (Trigger.isBefore) {

            if (SPCC_Utilities.checkForDuplicate == true){
            
                if(Trigger.isInsert) {
                    SPCC_Utilities.TriggerBeforeInsertEPCAssignment(Trigger.new);
                }
            }    
        
        } else if (Trigger.isAfter) {

            if(Trigger.isInsert) {
                SPCC_Utilities.TriggerAfterInsertEPCAssignment(Trigger.new);
            }
            
            else if(Trigger.isDelete){
                SPCC_Utilities.TriggerAfterDeleteEPCAssignment(Trigger.old);
            }
        }
    }
}