// ====================================================================
// Actions to be triggered before insert or update of Case
// The actions include
// 1) If retail location assignment changes, populate retail location's
//    account to the case
trigger CaseBeforeInsertUpdate on Case (before insert, before update) {
  List<Case>retailLocationChanged = new List<Case>();
  
  
  for (Case triggered: Trigger.new) {
    
    System.debug(triggered);
    
    // Retail location assignment.
    if (triggered.Retail_Location__c != null) {
        retailLocationChanged.add(triggered);
    }
    
  }
  
  CaseTriggerHandler.onRetailLocationAssignment(
      retailLocationChanged, Trigger.oldMap, Trigger.isInsert);
}