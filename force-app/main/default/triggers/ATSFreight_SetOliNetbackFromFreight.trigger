trigger ATSFreight_SetOliNetbackFromFreight on ATS_Freight__c (after update, after delete)
{
    // Get the opp record type we need
    RecordType asphalt = [SELECT Id
                          FROM RecordType
                          WHERE DeveloperName =: 'ATS_Asphalt_Product_Category'];
    
    // Stores all freights that have changed
    Set<Id> freightsAffected = new Set<Id>();
    
    // If this was an update to a Freight record, check if the relevant fields have changed.
    if(Trigger.isUpdate)
    {
        for(Id freightId : Trigger.newMap.keyset())
        {
            ATS_Freight__c newFreight = Trigger.newMap.get(freightId);
            ATS_Freight__c oldFreight = Trigger.oldMap.get(freightId);
            
            if(newFreight.Husky_Supplier_1_Selected__c != oldFreight.Husky_Supplier_1_Selected__c ||
               newFreight.Husky_Supplier_2_Selected__c != oldFreight.Husky_Supplier_2_Selected__c ||
               newFreight.Prices_F_O_B__c              != oldFreight.Prices_F_O_B__c              ||
               newFreight.Emulsion_Rate8_Supplier1__c  != oldFreight.Emulsion_Rate8_Supplier1__c  ||
               newFreight.Emulsion_Rate8_Supplier2__c  != oldFreight.Emulsion_Rate8_Supplier2__c  ||
               newFreight.Emulsion_Rate7_Supplier1__c  != oldFreight.Emulsion_Rate7_Supplier1__c  ||
               newFreight.Emulsion_Rate7_Supplier2__c  != oldFreight.Emulsion_Rate7_Supplier2__c  ||
               newFreight.Emulsion_Rate6_Supplier_1__c != oldFreight.Emulsion_Rate6_Supplier_1__c ||
               newFreight.Emulsion_Rate6_Supplier2__c  != oldFreight.Emulsion_Rate6_Supplier2__c  ||
               newFreight.Emulsion_Rate5_Supplier1__c  != oldFreight.Emulsion_Rate5_Supplier1__c  ||
               newFreight.Emulsion_Rate5_Supplier2__c  != oldFreight.Emulsion_Rate5_Supplier2__c  ||
               newFreight.Supplier_1_Unit__c           != oldFreight.Supplier_1_Unit__c           ||
               newFreight.Supplier_2_Unit__c           != oldFreight.Supplier_2_Unit__c)
            {
                freightsAffected.add(freightId);
            }
        }
    }
    // If this was a delete, no need to check if relevant fields have changed.
    if(Trigger.isDelete)
    {
        freightsAffected = Trigger.oldMap.keySet();
    }
    
    if(freightsAffected.size() == 0)
        return;
    
    // Now get all olis using these freights
    List<OpportunityLineItem> olis = [SELECT Id,
                                             Axle_8_Price__c,
                                             Axle_7_Price__c,
                                             Axle_6_Price__c,
                                             Axle_5_Price__c,
                                             Unit__c,
                                             Handling_Fees_CAD__c,
                                             Additives_CAD__c,
                                             Terminal_Freight_CAD__c,
                                             AC_Premium_CAD__c,
                                             Currency__c,
                                             Exchange_Rate_to_CAD__c,
                                             
                                             // Get the associated Opportunity
                                             
                                             OpportunityId,
                                             Opportunity.ATS_Freight__c,
                                             
                                             // Get the associated Product2
                                             
                                             Product2Id,
                                             Product2.Density__c,
                                             
                                             // Get the associated Freight
                                             
                                             Opportunity.ATS_Freight__r.Id,
                                             Opportunity.ATS_Freight__r.Husky_Supplier_1_Selected__c,
                                             Opportunity.ATS_Freight__r.Husky_Supplier_2_Selected__c,
                                             Opportunity.ATS_Freight__r.Prices_F_O_B__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier_1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Supplier_1_Unit__c,
                                             Opportunity.ATS_Freight__r.Supplier_2_Unit__c
                                      FROM OpportunityLineItem
                                      WHERE Opportunity.ATS_Freight__r.Id IN: freightsAffected AND
                                            Opportunity.RecordTypeId =: asphalt.Id];
    
    // These are the olis we'll be updating to the db
    List<OpportunityLineItem> olisToUpdate = new List<OpportunityLineItem>();
    
    for(OpportunityLineItem oli : olis)
    {
        // Check if an Opportunity exists
        
        if(oli.OpportunityId == null)
        {
            olisToUpdate.add(new OpportunityLineItem(Id                      = oli.Id,
                                                     Refinery_Netback_CAD__c = null,
                                                     Terminal_Netback_CAD__c = null));
            continue;
        }
        
        // Build freight object
        
        if(oli.Opportunity.ATS_Freight__c == null)
        {
            olisToUpdate.add(new OpportunityLineItem(Id                      = oli.Id,
                                                     Refinery_Netback_CAD__c = null,
                                                     Terminal_Netback_CAD__c = null));
            continue;
        }
        
        ATS_Freight__c f = new ATS_Freight__c(Id                           = oli.Opportunity.ATS_Freight__r.Id,
                                              Husky_Supplier_1_Selected__c = oli.Opportunity.ATS_Freight__r.Husky_Supplier_1_Selected__c,
                                              Husky_Supplier_2_Selected__c = oli.Opportunity.ATS_Freight__r.Husky_Supplier_2_Selected__c,
                                              Prices_F_O_B__c              = oli.Opportunity.ATS_Freight__r.Prices_F_O_B__c,
                                              Emulsion_Rate8_Supplier1__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier1__c,
                                              Emulsion_Rate8_Supplier2__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier2__c,
                                              Emulsion_Rate7_Supplier1__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier1__c,
                                              Emulsion_Rate7_Supplier2__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier2__c,
                                              Emulsion_Rate6_Supplier_1__c = oli.Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier_1__c,
                                              Emulsion_Rate6_Supplier2__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier2__c,
                                              Emulsion_Rate5_Supplier1__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier1__c,
                                              Emulsion_Rate5_Supplier2__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier2__c,
                                              Supplier_1_Unit__c           = oli.Opportunity.ATS_Freight__r.Supplier_1_Unit__c,
                                              Supplier_2_Unit__c           = oli.Opportunity.ATS_Freight__r.Supplier_2_Unit__c);
        
        // Build Product2 object
        
        if(oli.Product2Id == null)
        {
            olisToUpdate.add(new OpportunityLineItem(Id                      = oli.Id,
                                                     Refinery_Netback_CAD__c = null,
                                                     Terminal_Netback_CAD__c = null));
            continue;
        }
        
        Product2 p = new Product2(Id         = oli.Product2Id,
                                  Density__c = oli.Product2.Density__c);
        
        // Calculate netback amounts
        
        Decimal refineryNetback = calculateNetback.calculateAsphaltRefineryNetback(oli, f, p);
        if(refineryNetback != null) refineryNetback.setScale(4, RoundingMode.HALF_UP);
        Decimal terminalNetback = calculateNetback.calculateAsphaltTerminalNetback(oli, f, p);
        if(terminalNetback != null) terminalNetback.setScale(4, RoundingMode.HALF_UP);
        
        olisToUpdate.add(new OpportunityLineItem(Id                      = oli.Id,
                                                 Refinery_Netback_CAD__c = refineryNetback,
                                                 Terminal_Netback_CAD__c = terminalNetback));
    }
    
    update olisToUpdate;
}