trigger OpportunityProduct_SetCompetitorMargin on OpportunityLineItem (after update)
{
    // Stores all freights that have changed
    Set<Id> OPsAffected = new Set<Id>();
    
    // If this was an update to a Freight record, check if the relevant fields have changed.
    if(Trigger.isUpdate)
    {
        for(Id OPId : Trigger.newMap.keyset())
        {
            OpportunityLineItem newOli = Trigger.newMap.get(OPId);
            OpportunityLineItem oldOli = Trigger.oldMap.get(OPId);
            
            if(newOli.Axle_8_Price__c                   != oldOli.Axle_8_Price__c                   ||
               newOli.Axle_7_Price__c                   != oldOli.Axle_7_Price__c                   ||
               newOli.Axle_6_Price__c                   != oldOli.Axle_6_Price__c                   ||
               newOli.Axle_5_Price__c                   != oldOli.Axle_5_Price__c                   ||
               newOli.Unit__c                           != oldOli.Unit__c                           ||
               newOli.Handling_Fees_CAD__c              != oldOli.Handling_Fees_CAD__c              ||
               newOli.Additives_CAD__c                  != oldOli.Additives_CAD__c                  ||
               newOli.Terminal_Freight_CAD__c           != oldOli.Terminal_Freight_CAD__c           ||
               newOli.AC_Premium_CAD__c                 != oldOli.AC_Premium_CAD__c                 ||
               newOli.OpportunityId                     != oldOli.OpportunityId                     ||
               newOli.Currency__c                       != oldOli.Currency__c                       ||
               newOli.Exchange_Rate_to_CAD__c           != oldOli.Exchange_Rate_to_CAD__c           ||
               newOli.Product2Id                        != oldOli.Product2Id                        ||
               newOli.Competitor_1_Bid_Price__c         != oldOli.Competitor_1_Bid_Price__c         ||
               newOli.Competitor_1_Emulsion_Cost_CAD__c != oldOli.Competitor_1_Emulsion_Cost_CAD__c ||
               newOli.Competitor_2_Bid_Price__c         != oldOli.Competitor_2_Bid_Price__c         ||
               newOli.Competitor_2_Emulsion_Cost_CAD__c != oldOli.Competitor_2_Emulsion_Cost_CAD__c ||
               newOli.Competitor_3_Bid_Price__c         != oldOli.Competitor_3_Bid_Price__c         ||
               newOli.Competitor_3_Emulsion_Cost_CAD__c != oldOli.Competitor_3_Emulsion_Cost_CAD__c ||
               newOli.Competitor_4_Bid_Price__c         != oldOli.Competitor_4_Bid_Price__c         ||
               newOli.Competitor_4_Emulsion_Cost_CAD__c != oldOli.Competitor_4_Emulsion_Cost_CAD__c)
            {
                OPsAffected.add(OPId);
            }
        }
    }
    // If this was a delete, no need to check if relevant fields have changed.
    if(Trigger.isDelete)
    {
        OPsAffected = Trigger.oldMap.keySet();
    }
    System.debug('OpportunityProduct_SetCompetitorMargin OPsAffected =' + OPsAffected);
    if(OPsAffected.size() == 0) return;
    
    // Get the opp record type we need
    RecordType emulsion = [SELECT Id
                           FROM RecordType
                           WHERE DeveloperName =: 'ATS_Emulsion_Product_Category'];

    
    // Now get all olis using these freights
    List<OpportunityLineItem> olis = [SELECT Id,
                                             Competitor_1_Emulsion_Margin_CAD__c,
                                             Competitor_1_Bid_Price__c,
                                             Competitor_1_Emulsion_Cost_CAD__c,
                                             Competitor_2_Emulsion_Margin_CAD__c,
                                             Competitor_2_Bid_Price__c,
                                             Competitor_2_Emulsion_Cost_CAD__c,
                                             Competitor_3_Emulsion_Margin_CAD__c,
                                             Competitor_3_Bid_Price__c,
                                             Competitor_3_Emulsion_Cost_CAD__c,
                                             Competitor_4_Emulsion_Margin_CAD__c,
                                             Competitor_4_Bid_Price__c,
                                             Competitor_4_Emulsion_Cost_CAD__c,
                                             Unit__c,
                                             Currency__c,
                                             Exchange_Rate_to_CAD__c,
                                             AC_Premium_CAD__c,
                                             Terminal_Freight_CAD__c,
                                             Handling_Fees_CAD__c,
                                             Additives_CAD__c,
                                             
                                             // Get the associated Opportunity
                                             
                                             OpportunityId,
                                             Opportunity.RecordTypeId,
                                             Opportunity.ATS_Freight__c,
                                             
                                             // Get the associated Product2
                                             
                                             Product2Id,
                                             Product2.Density__c,
                                             
                                             // Get the associated Freight
                                             
                                             Opportunity.ATS_Freight__r.Id,
                                             Opportunity.ATS_Freight__r.Husky_Supplier_1_Selected__c,
                                             Opportunity.ATS_Freight__r.Husky_Supplier_2_Selected__c,
                                             Opportunity.ATS_Freight__r.Prices_F_O_B__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier_1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Supplier_1_Unit__c,
                                             Opportunity.ATS_Freight__r.Supplier_2_Unit__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate5_Competitor1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate5_Competitor2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Axle5_Rate_Competitor3__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Axle5_Rate_Competitor4__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate6_Competitor1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate6_Competitor2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Axle6_Rate_Competitor3__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Axle6_Rate_Competitor4__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor3__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor4__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate8_Competitor1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate8_Competitor2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Axle8_Rate_Competitor3__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Axle8_Rate_Competitor4__c
                                      FROM OpportunityLineItem
                                      WHERE Id IN: OPsAffected AND
                                            Opportunity.RecordTypeId =: emulsion.Id];
    
    // These are the olis we'll be updating to the db
    List<OpportunityLineItem> olisToUpdate = new List<OpportunityLineItem>();
    System.debug('OpportunityProduct_SetCompetitorMargin olis.size =' + olis.size());
    for(OpportunityLineItem oli : olis)
    {
        // Build freight object
        
        ATS_Freight__c f = new ATS_Freight__c(Id                                 = oli.Opportunity.ATS_Freight__r.Id,
                                              Husky_Supplier_1_Selected__c       = oli.Opportunity.ATS_Freight__r.Husky_Supplier_1_Selected__c,
                                              Husky_Supplier_2_Selected__c       = oli.Opportunity.ATS_Freight__r.Husky_Supplier_2_Selected__c,
                                              Prices_F_O_B__c                    = oli.Opportunity.ATS_Freight__r.Prices_F_O_B__c,
                                              Emulsion_Rate8_Supplier1__c        = oli.Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier1__c,
                                              Emulsion_Rate8_Supplier2__c        = oli.Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier2__c,
                                              Emulsion_Rate7_Supplier1__c        = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier1__c,
                                              Emulsion_Rate7_Supplier2__c        = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier2__c,
                                              Emulsion_Rate6_Supplier_1__c       = oli.Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier_1__c,
                                              Emulsion_Rate6_Supplier2__c        = oli.Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier2__c,
                                              Emulsion_Rate5_Supplier1__c        = oli.Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier1__c,
                                              Emulsion_Rate5_Supplier2__c        = oli.Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier2__c,
                                              Supplier_1_Unit__c                 = oli.Opportunity.ATS_Freight__r.Supplier_1_Unit__c,
                                              Supplier_2_Unit__c                 = oli.Opportunity.ATS_Freight__r.Supplier_2_Unit__c,
                                              Emulsion_Rate5_Competitor1__c      = oli.Opportunity.ATS_Freight__r.Emulsion_Rate5_Competitor1__c,
                                              Emulsion_Rate5_Competitor2__c      = oli.Opportunity.ATS_Freight__r.Emulsion_Rate5_Competitor2__c,
                                              Emulsion_Axle5_Rate_Competitor3__c = oli.Opportunity.ATS_Freight__r.Emulsion_Axle5_Rate_Competitor3__c,
                                              Emulsion_Axle5_Rate_Competitor4__c = oli.Opportunity.ATS_Freight__r.Emulsion_Axle5_Rate_Competitor4__c,
                                              Emulsion_Rate6_Competitor1__c      = oli.Opportunity.ATS_Freight__r.Emulsion_Rate6_Competitor1__c,
                                              Emulsion_Rate6_Competitor2__c      = oli.Opportunity.ATS_Freight__r.Emulsion_Rate6_Competitor2__c,
                                              Emulsion_Axle6_Rate_Competitor3__c = oli.Opportunity.ATS_Freight__r.Emulsion_Axle6_Rate_Competitor3__c,
                                              Emulsion_Axle6_Rate_Competitor4__c = oli.Opportunity.ATS_Freight__r.Emulsion_Axle6_Rate_Competitor4__c,
                                              Emulsion_Rate7_Competitor1__c      = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor1__c,
                                              Emulsion_Rate7_Competitor2__c      = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor2__c,
                                              Emulsion_Rate7_Competitor3__c      = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor3__c,
                                              Emulsion_Rate7_Competitor4__c      = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor4__c,
                                              Emulsion_Rate8_Competitor1__c      = oli.Opportunity.ATS_Freight__r.Emulsion_Rate8_Competitor1__c,
                                              Emulsion_Rate8_Competitor2__c      = oli.Opportunity.ATS_Freight__r.Emulsion_Rate8_Competitor2__c,
                                              Emulsion_Axle8_Rate_Competitor3__c = oli.Opportunity.ATS_Freight__r.Emulsion_Axle8_Rate_Competitor3__c,
                                              Emulsion_Axle8_Rate_Competitor4__c = oli.Opportunity.ATS_Freight__r.Emulsion_Axle8_Rate_Competitor4__c);
        
        // Build Product2 object
        
        Product2 p = new Product2(Id         = oli.Product2Id,
                                  Density__c = oli.Product2.Density__c);
        
        // Calculate axle prices
        
        Decimal competitor1Margin = calculateNetback.calculateCompetitorMargin(1, oli, f);
        Decimal competitor2Margin = calculateNetback.calculateCompetitorMargin(2, oli, f);
        Decimal competitor3Margin = calculateNetback.calculateCompetitorMargin(3, oli, f);
        Decimal competitor4Margin = calculateNetback.calculateCompetitorMargin(4, oli, f);
        System.debug('competitor1Margin = ' + competitor1Margin);
        OpportunityLineItem oliToCommit = new OpportunityLineItem(Id                          = oli.Id,
                                                                  Competitor_1_Emulsion_Margin_CAD__c = null,
                                                                  Competitor_2_Emulsion_Margin_CAD__c = null,
                                                                  Competitor_3_Emulsion_Margin_CAD__c = null,
                                                                  Competitor_4_Emulsion_Margin_CAD__c = null);
        
        if(competitor1Margin != null)
        {
            oliToCommit.Competitor_1_Emulsion_Margin_CAD__c = competitor1Margin;
            oliToCommit.Competitor_1_Emulsion_Margin_CAD__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(competitor2Margin != null)
        {
            oliToCommit.Competitor_2_Emulsion_Margin_CAD__c = competitor2Margin;
            oliToCommit.Competitor_2_Emulsion_Margin_CAD__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(competitor3Margin != null)
        {
            oliToCommit.Competitor_3_Emulsion_Margin_CAD__c = competitor3Margin;
            oliToCommit.Competitor_3_Emulsion_Margin_CAD__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(competitor4Margin != null)
        {
            oliToCommit.Competitor_4_Emulsion_Margin_CAD__c = competitor4Margin;
            oliToCommit.Competitor_4_Emulsion_Margin_CAD__c.setScale(4, RoundingMode.HALF_UP);
        }
        
        olisToUpdate.add(oliToCommit);
    }
    
    update olisToUpdate;
}