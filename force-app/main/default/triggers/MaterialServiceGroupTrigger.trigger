trigger MaterialServiceGroupTrigger on PREP_Material_Service_Group__c( after update ){
    MaterialServiceGroupHelper.newMaterialSeriveGroupList = trigger.new;
    MaterialServiceGroupHelper.oldMaterialSeriveGroupList = trigger.old;
    MaterialServiceGroupHelper.newMaterialSeriveGroupMap = trigger.newMap;
    MaterialServiceGroupHelper.oldMaterialSeriveGroupMap = trigger.oldMap;
    
    if( MaterialServiceGroupHelper.flag ){
        if( trigger.isAfter ){
            if( trigger.isUpdate ){
                MaterialServiceGroupHelper.updateInitiatives();
            }
        }
    }
}