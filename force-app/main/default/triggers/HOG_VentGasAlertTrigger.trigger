/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Trigger for HOG_Vent_Gas_Alert__c
Test class:		HOG_VentGasAlertTriggerTest
History:        jschn 04.19.2018 - Created.
**************************************************************************************************/
trigger HOG_VentGasAlertTrigger on HOG_Vent_Gas_Alert__c (before update) {
	if(Trigger.isBefore)
		if(Trigger.isUpdate)
			HOG_VentGas_Utilities.ventGasTriggerBeforeUpdate(Trigger.new, Trigger.oldMap);
}