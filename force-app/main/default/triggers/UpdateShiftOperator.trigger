trigger UpdateShiftOperator on Shift_Operator__c (before insert, before update) 
{
    String insertErrorMsg = 'Cannot create new shift operator for a team allocation that is not active.';
    String updateErrorMsg = 'Cannot update shift operator for a team allocation that is not active.';
    Map<String, Schema.Recordtypeinfo> recTypesByName = Team_Allocation__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    // Sanchivan's Change Start
    if (trigger.isBefore && (trigger.isInsert || trigger.isUpdate)) 
    {
        Set<Id> teamAllocationIds = new Set<Id>();
        
        for (Shift_Operator__c curOp : trigger.new) 
        {
            if (curOp.Team_Allocation__c != null)
                teamAllocationIds.add(curOp.Team_Allocation__c);
            
        }
        Map<Id, Team_Allocation__c> teamAllocationMap = new Map<Id, Team_Allocation__c> 
                  ([SELECT Id, Status__c, Operating_District__c, RecordTypeId FROM Team_Allocation__c WHERE Id in :teamAllocationIds]);
            
                       
        for (Shift_Operator__c curOp : trigger.new) 
        {
            if (curOp.Team_Allocation__c == null) continue;
            Team_Allocation__c ta = teamAllocationMap.get(curOp.Team_Allocation__c);
            // Don't allow edits to in-active items.
            //Allow Maintenance team update Shift Operators on Inactive Team Allocation
            if (ta.Status__c == TeamAllocationUtilities.INACTIVE_STATUS_NAME && ta.RecordTypeId == recTypesByName.get('Operations').getRecordTypeId()) {
                String errorMsg = (trigger.isInsert) ? insertErrorMsg : updateErrorMsg;
                curOp.addError(errorMsg);
            }
            
            // Make sure operating districts match
            if (curOp.Operating_District__c == null) {
                curOp.Operating_District__c = ta.Operating_District__c;
            }
             
            if (curOp.Operating_District__c != ta.Operating_District__c) {
                curOp.addError('Operating district must match parent team allocation\'s value.');
            }
        }
    }
    
    // Sanchivan's Change End
    /*
    if (trigger.isBefore && (trigger.isInsert || trigger.isUpdate)) {
        // Don't allow edits to in-active items.
        for (Shift_Operator__c curOp : trigger.new) {
            Team_Allocation__c ta = TeamAllocationUtilities.getTeamAllocation(curOp.Team_Allocation__c);
            if (ta.Status__c == TeamAllocationUtilities.INACTIVE_STATUS_NAME) {
                String errorMsg = (trigger.isInsert) ? insertErrorMsg : updateErrorMsg;
                curOp.addError(errorMsg);
            }
            
             // Make sure operating districts match
             if (curOp.Operating_District__c == null) {
                curOp.Operating_District__c = ta.Operating_District__c;
             }
             
             if (curOp.Operating_District__c != ta.Operating_District__c) {
                curOp.addError('Operating district must match parent team allocation\'s value.');
             }
        }
    }*/
}