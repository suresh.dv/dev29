trigger Update_Team_Allocation_Approve_Date on Team_Allocation__c (before insert, before update) {

    List<Team_Allocation__c> teamAllocationOld = Trigger.old;
    List<Team_Allocation__c> teamAllocationNew = Trigger.new;

 // set the Approve Date
    if(trigger.isUpdate)
    {
        if (teamAllocationOld[0].Approver__c != teamAllocationNew[0].Approver__c)
            teamAllocationNew[0].Approve_Date__c = (teamAllocationNew[0].Approver__c == null) ? null : System.now();
    }
    
    if(trigger.isInsert)
        teamAllocationNew[0].Approve_Date__c = (teamAllocationNew[0].Approver__c == null) ? null : System.now();
}