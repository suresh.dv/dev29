// ====================================================================
// Actions to be triggered before insert or update of Task
// The actions include
// 1) If the task is closed, set the Last Activity Date of the 
//    parent case
trigger TaskAfterInsertUpdate on Task (after insert, after update) {
  List<Task>lastActivities = new List<Task>();
  
  for (Task triggered : Trigger.new) {
    if ((Trigger.isInsert && triggered.isClosed) || 
        (Trigger.isUpdate && triggered.isClosed && 
         !Trigger.oldMap.get(triggered.Id).isClosed)) {
        lastActivities.add(triggered);
    }
  }
  
  TaskTriggerHandler.onClosedTask(lastActivities);
}