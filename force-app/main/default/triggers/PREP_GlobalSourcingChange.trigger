trigger PREP_GlobalSourcingChange on PREP_Initiative__c (before update) 
{
    
    Id devRecordTypeId = Schema.SObjectType.PREP_Initiative__c.getRecordTypeInfosByName().get('Category Initiative').getRecordTypeId();
    Id gloRecordTypeId = Schema.SObjectType.PREP_Baseline__c.getRecordTypeInfosByName().get('Global Baseline').getRecordTypeId();
    Id locRecordTypeId = Schema.SObjectType.PREP_Baseline__c.getRecordTypeInfosByName().get('Local Baseline').getRecordTypeId();
    PREP_Baseline__c baseline;

    for(PREP_Initiative__c ini : Trigger.New)
    {
        if(devRecordTypeId == ini.RecordTypeId)
        {
            if(trigger.isUpdate)
            {
                PREP_Initiative__c beforeUpdate = trigger.oldMap.get(ini.Id);
                
                List<PREP_Baseline__c> baselineList = [SELECT Id, RecordTypeId, Local_Baseline_Spend_Percent__c, Global_Baseline_Savings_Percent__c, Validation_Rule_Override__c FROM PREP_Baseline__c WHERE Initiative_Id__c = :ini.Id];


                if(ini.Global_Sourcing__c == 'Yes' && beforeUpdate.Global_Sourcing__c == 'No' && baselineList.size() != 0)
                {

                    baseline = baselineList[0];

                    if(baseline.RecordTypeId == gloRecordTypeId || baseline.RecordTypeId == locRecordTypeId)
                    {
                        baseline.RecordTypeId = gloRecordTypeId;
                        baseline.Validation_Rule_Override__c = true;

                        update baseline;
                        
                                                
                        baseline.Validation_Rule_Override__c = false;
                        update baseline;
                    }

                }

                else if(ini.Global_Sourcing__c == 'No' && beforeUpdate.Global_Sourcing__c == 'Yes' && baselineList.size() != 0)
                {
                    
                    baseline = baselineList[0];

                    if(baseline.RecordTypeId == gloRecordTypeId || baseline.RecordTypeId == locRecordTypeId)
                    {
                        baseline.RecordTypeId = locRecordTypeId;
                        baseline.Local_Baseline_Spend_Percent__c = 100;
                        baseline.Global_Baseline_Savings_Percent__c = 0;
                        baseline.Validation_Rule_Override__c = false;

                        update baseline;

                    }
                }
            }
        }
    }
}