/*------------------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: Service Time Trigger
Test Class : TestNewHOGTriggers
History    :
			01.08.16	rbo Modifications due to Refactoring
							replaced all HOG_Rigless_Servicing_Form__c with HOG_Maintenance_Servicing_Form__c				
							replaced all Rigless_Servicing_Form__c with Work_Order__c
-------------------------------------------------------------------------------------------------------------*/
trigger HOG_Service_Time_Trigger on HOG_Service_Time__c (before insert, before delete, before update) 
{
    If (ServiceRequestNotificationUtilities.executeTriggerCode)
    {
        List<HOG_Service_Time__c> serviceTimeOld = Trigger.old;
        List<HOG_Service_Time__c> serviceTimeNew = Trigger.new;
    
        String riglessServicingFormID;
        
        if (Trigger.isDelete)
            riglessServicingFormID = serviceTimeOld[0].Work_Order__c;
        else
            riglessServicingFormID = serviceTimeNew[0].Work_Order__c;
    
        if (riglessServicingFormID != null)
        {
            dateTime minValueRigOffLocation;
        
            // Get the lowest recorded value of Rig Off Location
            List<HOG_Service_Time__c> serviceTimeRigOffMin =
              [Select Rig_Off_Location__c From HOG_Service_Time__c Where Work_Order__c =: riglessServicingFormID Order By Rig_Off_Location__c Asc Limit 1];
        
            if (serviceTimeRigOffMin.size() > 0)
                minValueRigOffLocation = serviceTimeRigOffMin[0].Rig_Off_Location__c;
            else
                minValueRigOffLocation = null;
                    
            dateTime maxValueRigOffLocation;
            
            // Get the highest recorded value of Rig Off Location
            List<HOG_Service_Time__c> serviceTimeRigOffMax =
              [Select Rig_Off_Location__c From HOG_Service_Time__c Where Work_Order__c =: riglessServicingFormID And Rig_Off_Location__c != Null Order By Rig_Off_Location__c Desc Limit 1];
        
            if (serviceTimeRigOffMax.size() > 0)
                maxValueRigOffLocation = serviceTimeRigOffMax[0].Rig_Off_Location__c;
            else
                maxValueRigOffLocation = null;
        
            // Get the Service Status of the Rigless Service
            String serviceStatus =
                [Select Service_Status__c From HOG_Maintenance_Servicing_Form__c Where Id =: riglessServicingFormID][0].Service_Status__c;
            
            if (Trigger.isDelete) 
            {              
                if (serviceStatus == 'Complete')
                    serviceTimeOld[0].addError('You can not delete any record of Service Time Log, you must first set the Rigless Service Status to a status other than "Complete".');            
                else if ((serviceTimeOld[0].Rig_Off_Location__c != Null && serviceTimeOld[0].Rig_Off_Location__c != maxValueRigOffLocation) || (serviceTimeOld[0].Rig_Off_Location__c == maxValueRigOffLocation && minValueRigOffLocation == Null))
                    serviceTimeOld[0].addError('You can not delete the ' + serviceTimeOld[0].Day__c + ' record, you must first delete newer record(s) of Service Time Log.');
            }
            
            if (Trigger.isUpdate) 
            {              
                if (serviceStatus == 'Complete')
                    serviceTimeNew[0].addError('You can not edit any record of Service Time Log, you must first set the Rigless Service Status to a status other than "Complete".');
                else if (serviceTimeOld[0].Rig_Off_Location__c == Null || (serviceTimeOld[0].Rig_Off_Location__c == maxValueRigOffLocation && minValueRigOffLocation != Null))
                {
                    dateTime maxValueRigOnLocation = maxValueRigOffLocation;
        
                    // Get the previous record value of Rig Off Location
                    List<HOG_Service_Time__c> serviceTimeRigOnMax =
                      [Select Rig_Off_Location__c From HOG_Service_Time__c Where Work_Order__c =: riglessServicingFormID And Id <>: serviceTimeOld[0].Id And Rig_Off_Location__c != Null Order By Rig_Off_Location__c Desc Limit 1];
                
                    if (serviceTimeRigOnMax.size() > 0)
                        maxValueRigOnLocation = serviceTimeRigOnMax[0].Rig_Off_Location__c;
                    else
                        maxValueRigOnLocation = null;
        
                    if (maxValueRigOnLocation > serviceTimeNew[0].Rig_On_Location__c)
                        serviceTimeNew[0].addError('Rig On Location must be greater than the value of the previous Service Time Log Rig Off Location record.');
                }
                else if (serviceTimeOld[0].Rig_On_Location__c != serviceTimeNew[0].Rig_On_Location__c || serviceTimeOld[0].Rig_Off_Location__c != serviceTimeNew[0].Rig_Off_Location__c)
                    serviceTimeNew[0].addError('Service Time Log record is locked, delete newer record(s) to unlock.');
            }    
            
            if (Trigger.isInsert)
            {
                if (serviceStatus == 'Complete')
                {    
                    serviceTimeNew[0].addError('You can not create a record of Service Time Log, you must first set the Rigless Service Status to a status other than "Complete".');
                }   
                else if (minValueRigOffLocation == Null && serviceTimeRigOffMin.size() > 0)
                    serviceTimeNew[0].addError('The value of the previous Service Time Log Rig Off Location must not be empty');    
                else if (maxValueRigOffLocation > serviceTimeNew[0].Rig_On_Location__c)
                    serviceTimeNew[0].addError('Rig On Location must be greater than the value of the previous Service Time Log Rig Off Location record');    
            }
        }
        
        if (!Trigger.isDelete) 
        {              
            // The map of the new service times
            Map<Id, HOG_Service_Time__c> serviceTimeProcessed = new Map<Id, HOG_Service_Time__c>();
        
            // Populate the new service times map
            for (HOG_Service_Time__c currServiceTime: Trigger.new) 
                serviceTimeProcessed.put(currServiceTime.Work_Order__c, currServiceTime);
        
            // Rigless Service that is modified by given service time
            List <HOG_Maintenance_Servicing_Form__c> serviceToBeUpdated = new List<HOG_Maintenance_Servicing_Form__c>();
        
            // Update Day 1 rig on location time of the rigless service form
            // and the day value of the service time.
            for (HOG_Maintenance_Servicing_Form__c serviceLogged : 
                [Select Id, Day_1_Rig_On_Location__c, 
                    (Select Day__c, Rig_On_Location__c From Service_Times__r)
                    From HOG_Maintenance_Servicing_Form__c 
                    Where Id In :serviceTimeProcessed.keySet()]) 
            {
            
                // This is the first service time created for this rigless service form
                if (serviceLogged.Service_Times__r.size() == 0) 
                {
                    serviceLogged.Day_1_Rig_On_Location__c = serviceTimeProcessed.get(serviceLogged.Id).Rig_On_Location__c;
                    serviceToBeUpdated.add(serviceLogged);
                    serviceTimeProcessed.get(serviceLogged.Id).Day__c = 'Day 1';
                }        
                // Set the Day 1 Rig on Location time
                else if (serviceTimeProcessed.get(serviceLogged.Id).Day__c == 'Day 1') 
                {
                    serviceLogged.Day_1_Rig_On_Location__c = serviceTimeProcessed.get(serviceLogged.Id).Rig_On_Location__c;
                    serviceToBeUpdated.add(serviceLogged);
                } 
                // Set the Day value
                else if (serviceTimeProcessed.get(serviceLogged.Id).Day__c == null) 
                    serviceTimeProcessed.get(serviceLogged.Id).Day__c = 'Day ' + String.valueOf(serviceLogged.Service_Times__r.size() + 1);
            } // for (Work_Order__c serviceLogged: ...
        
            // Update the rigless service form.
            update serviceToBeUpdated;
        }
    }    
}