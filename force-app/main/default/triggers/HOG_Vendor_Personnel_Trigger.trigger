/*------------------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: Vendor Personnel Trigger
Test Class : TestNewHOGTriggers
History    :
			01.08.16	rbo Modifications due to Refactoring
							replaced all HOG_Rigless_Servicing_Form__c with HOG_Maintenance_Servicing_Form__c				
							replaced all Rigless_Servicing_Form__c with Work_Order__c
            12.04.17    ssd Refactored logic out of trigger into utility class
-------------------------------------------------------------------------------------------------------------*/
trigger HOG_Vendor_Personnel_Trigger on HOG_Vendor_Personnel__c (before insert, before delete, before update,
    after insert, after update, after delete) 
{
    if (ServiceRequestNotificationUtilities.executeTriggerCode)
    {
        if(Trigger.isBefore) {
            if (Trigger.isDelete) {              
                ServiceRequestNotificationUtilities.TriggerBeforeDeleteVendorPersonnel(Trigger.old);
            } else if(Trigger.isUpdate) {              
                ServiceRequestNotificationUtilities.TriggerBeforeUpdateVendorPersonnel(Trigger.new);
            } else if (Trigger.isInsert) {
                ServiceRequestNotificationUtilities.TriggerBeforeInsertVendorPersonnel(Trigger.new);
            }
        } else {//if after
            if(Trigger.isInsert) {
                ServiceRequestNotificationUtilities.TriggerAfterInsertVendorPersonnel(Trigger.new);
            } else if (Trigger.isUpdate) {
                ServiceRequestNotificationUtilities.TriggerAfterUpdateVendorPersonnel(Trigger.new);
            } else if (Trigger.isDelete) {
                ServiceRequestNotificationUtilities.TriggerAfterDeleteVendorPersonnel(Trigger.old);
            }
        }   
    }       
}