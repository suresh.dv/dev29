/**********************************************************************************
  CreatedBy : Accenture
  Organization:Accenture
  Purpose   :  Trigger For update new value of Equipment Type and Equipment Class 
  Handler Class: RaM_EquipmentTriggerHandler 
  Test Class:  RaM_EquipmentTriggerHandler_Test
  Version:1.0.0.1
**********************************************************************************/
trigger RaM_updateEquipmentNewValue on RaM_Equipment__c (after insert, before insert, after update, before update) {
   
   
   //if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate) ){
   if(Trigger.isAfter && Trigger.isInsert){
      //To get new value of Equipment Type and Equipment Class
      RaM_EquipmentTriggerHandler.equipmentTypeClassNewValueCheck(Trigger.New,new Map<id,RaM_Equipment__c >(),true);     
   }  
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
      //To update only functional location in Insert and Update trigger
      RaM_EquipmentTriggerHandler.updateLocationNumberLookup(Trigger.New);     
   } 
 }