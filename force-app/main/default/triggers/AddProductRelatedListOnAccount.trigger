trigger AddProductRelatedListOnAccount on Opportunity (after update) {    
    
    for( Opportunity opp : Trigger.New){
    	// if statement for opportunity stageName = Offer Issued – Won   
        if( opp.StageName == 'Offer Issued – Won'){
            
            // retrieve opportunity related account
            Account acct = [SELECT Id, Name
                                  FROM Account 
                                  WHERE Id IN (SELECT AccountId 
                                               FROM Opportunity 
                                               WHERE Id IN :Trigger.New) LIMIT 1];
                        
            // retrieve opportunity product names
            Product2[] productName = [SELECT Name
                                      FROM Product2 
                                      WHERE Id IN (SELECT Product2Id 
                                                   FROM OpportunityLineItem 
                                                   WHERE OpportunityId IN :Trigger.New)];
                    
            Set<String> apaProductName = new Set<String>();
            
            //create set of product names retrieved from Account related Products
            for( cpm_AccountProductAssociation__c apa : [SELECT Product_Name__c 
                                                      FROM cpm_AccountProductAssociation__c 
                                                      WHERE cpm_Account__c = :acct.Id]){
            
            	apaProductName.add(apa.Product_Name__c);
            }
            
            List<cpm_AccountProductAssociation__c> apaNew = new List<cpm_AccountProductAssociation__c>();
            
            // add new account related products to the list to be inserted into the Account Product Association Object           
            for( Product2 pn : productName){
                // dont add product duplicate
                if( !apaProductName.contains(pn.Name)){
                 	apaNew.add( new cpm_AccountProductAssociation__c(cpm_Account__c = acct.Id, Product_Name__c = pn.Name));   
                }               
            }
            
            insert apaNew;
        }
    }   
}