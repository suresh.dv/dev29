trigger ServiceTimeRecord on Service_Time__c (before insert, before delete, before update) {

    List<Service_Time__c> serviceTimeOld = Trigger.old;
    List<Service_Time__c> serviceTimeNew = Trigger.new;

    String riglessServicingFormID;
    
    if (Trigger.isDelete)
        riglessServicingFormID = serviceTimeOld[0].Rigless_Servicing_Form__c;
    else
        riglessServicingFormID = serviceTimeNew[0].Rigless_Servicing_Form__c;

    if (riglessServicingFormID != null)
    {
        dateTime minValueRigOffLocation;
    
        // Get the lowest recorded value of Rig Off Location
        List<Service_Time__c> serviceTimeRigOffMin =
              [Select Rig_Off_Location__c From Service_Time__c Where Rigless_Servicing_Form__c =: riglessServicingFormID Order By Rig_Off_Location__c Asc Limit 1];
    
        if (serviceTimeRigOffMin.size() > 0)
            minValueRigOffLocation = serviceTimeRigOffMin[0].Rig_Off_Location__c;
        else
            minValueRigOffLocation = null;
                
        dateTime maxValueRigOffLocation;
        
        // Get the highest recorded value of Rig Off Location
        List<Service_Time__c> serviceTimeRigOffMax =
          [Select Rig_Off_Location__c From Service_Time__c Where Rigless_Servicing_Form__c =: riglessServicingFormID And Rig_Off_Location__c != Null Order By Rig_Off_Location__c Desc Limit 1];
    
        if (serviceTimeRigOffMax.size() > 0)
            maxValueRigOffLocation = serviceTimeRigOffMax[0].Rig_Off_Location__c;
        else
            maxValueRigOffLocation = null;
    
        // Get the Service Status of the Rigless Service
        String serviceStatus =
            [Select Service_Status__c From Rigless_Servicing_Form__c Where Id =: riglessServicingFormID][0].Service_Status__c;

    /*
        List<Rigless_Servicing_Form__c> riglessServicingFormServiceStatus = 
            [Select Service_Status__c From Rigless_Servicing_Form__c Where Id =: riglessServicingFormID];
    
        String serviceStatus;
        
        if (riglessServicingFormServiceStatus.size() > 0)
            serviceStatus = riglessServicingFormServiceStatus[0].Service_Status__c;
        else
        {    
            if (Trigger.isInsert)
                serviceTimeNew[0].addError('Rigless Service has not been created. Unable to create new Service Time record!');
            else
                serviceTimeOld[0].addError('Rigless Service has already been deleted. Unable to delete/update the Service Time record!');
        }   
    */
        
        if (Trigger.isDelete) 
        {              
            if (serviceStatus == 'Complete')
                serviceTimeOld[0].addError('You can not delete any record of Service Time Log, you must first set the Rigless Service Status to a status other than "Complete".');            
            else if ((serviceTimeOld[0].Rig_Off_Location__c != Null && serviceTimeOld[0].Rig_Off_Location__c != maxValueRigOffLocation) || (serviceTimeOld[0].Rig_Off_Location__c == maxValueRigOffLocation && minValueRigOffLocation == Null))
                serviceTimeOld[0].addError('You can not delete the ' + serviceTimeOld[0].Day__c + ' record, you must first delete newer record(s) of Service Time Log.');
        }
        
        if (Trigger.isUpdate) 
        {              
            if (serviceStatus == 'Complete')
                serviceTimeNew[0].addError('You can not edit any record of Service Time Log, you must first set the Rigless Service Status to a status other than "Complete".');
            else if (serviceTimeOld[0].Rig_Off_Location__c == Null || (serviceTimeOld[0].Rig_Off_Location__c == maxValueRigOffLocation && minValueRigOffLocation != Null))
            {
                dateTime maxValueRigOnLocation = maxValueRigOffLocation;
    
                // Get the previous record value of Rig Off Location
                List<Service_Time__c> serviceTimeRigOnMax =
                  [Select Rig_Off_Location__c From Service_Time__c Where Rigless_Servicing_Form__c =: riglessServicingFormID And Id <>: serviceTimeOld[0].Id And Rig_Off_Location__c != Null Order By Rig_Off_Location__c Desc Limit 1];
            
                if (serviceTimeRigOnMax.size() > 0)
                    maxValueRigOnLocation = serviceTimeRigOnMax[0].Rig_Off_Location__c;
                else
                    maxValueRigOnLocation = null;
    
                if (maxValueRigOnLocation > serviceTimeNew[0].Rig_On_Location__c)
                    serviceTimeNew[0].addError('Rig On Location must be greater than the value of the previous Service Time Log Rig Off Location record.');
            }
            else if (serviceTimeOld[0].Rig_On_Location__c != serviceTimeNew[0].Rig_On_Location__c || serviceTimeOld[0].Rig_Off_Location__c != serviceTimeNew[0].Rig_Off_Location__c)
                serviceTimeNew[0].addError('Service Time Log record is locked, delete newer record(s) to unlock.');
        }    
        
        if (Trigger.isInsert)
        {
            if (serviceStatus == 'Complete')
                serviceTimeNew[0].addError('You can not create a record of Service Time Log, you must first set the Rigless Service Status to a status other than "Complete".');
            else if (minValueRigOffLocation == Null && serviceTimeRigOffMin.size() > 0)
                serviceTimeNew[0].addError('The value of the previous Service Time Log Rig Off Location must not be empty');    
            else if (maxValueRigOffLocation > serviceTimeNew[0].Rig_On_Location__c)
                serviceTimeNew[0].addError('Rig On Location must be greater than the value of the previous Service Time Log Rig Off Location record');    
        }
    }
}