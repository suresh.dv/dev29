trigger CarSealDuplicateId on Carseal__c(before insert, before update) {
 Set < String > carNames = new Set < String > ();
 List < Carseal__c > cSealLst;

 if (Trigger.isBefore) {
  if (Trigger.isInsert) {

   for (Carseal__c csnew: Trigger.new) {
    carNames.add(csnew.Name);
   }
   System.debug('carNames in Insert###' + carNames);
   if (carNames != null && carNames.size() > 0) {
    cSealLst = new List < Carseal__c > ([SELECT Id, Name FROM Carseal__c WHERE Name IN: carNames]);
   }
   System.debug('cSealLst in Insert###' + cSealLst);
   if (cSealLst != null && !cSealLst.isEmpty()) {
    for (Carseal__c csnew: Trigger.new) {
     for (Carseal__c cSQ: cSealLst) {
      if (cSQ.Name == csnew.Name) {
       csnew.addError('Duplicate value on Carseal Id, Record cannot be saved.');
      }
     }
    }
   }
  }
  if (Trigger.isUpdate) {   
   System.debug('old Trigger in Update###' + Trigger.old);
   System.debug('new Trigger in Update###' + Trigger.new);
      
   for (Carseal__c csnew: Trigger.new) {
    for (Carseal__c csold: Trigger.old) {
     if (csnew.Id == csold.Id && csnew.Name != csOld.Name) {
      carNames.add(csnew.Name);
     }
    }
   }
   System.debug('carNames in Update###' + carNames);
   if (carNames != null && carNames.size() > 0) {
    cSealLst = new List < Carseal__c > ([SELECT Id, Name FROM Carseal__c WHERE Name IN: carNames]);
   }
   System.debug('cSealLst in Update###' + cSealLst);
   if (cSealLst != null && !cSealLst.isEmpty()) {
    for (Carseal__c csnew: Trigger.new) {
     for (Carseal__c cSQ: cSealLst) {
      if (cSQ.Name == csnew.Name) {
       csnew.addError('Duplicate value on Carseal Id, Record cannot be saved.');
      }
     }
    }
   }
  }
 }
    
}