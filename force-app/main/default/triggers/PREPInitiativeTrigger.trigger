trigger PREPInitiativeTrigger on PREP_Initiative__c( after insert ){
    PREPInitiativeTriggerHelper.runTrigger = true;
    PREPInitiativeTriggerHelper.newInitiativeList = trigger.new;
    PREPInitiativeTriggerHelper.oldInitiativeList = trigger.old;
    PREPInitiativeTriggerHelper.newInitiativeMap = trigger.newMap;
    PREPInitiativeTriggerHelper.oldInitiativeMap = trigger.oldMap;
    
    if( trigger.isAfter && trigger.isinsert ){
        PREPInitiativeTriggerHelper.createAcquisition();
    } 
}