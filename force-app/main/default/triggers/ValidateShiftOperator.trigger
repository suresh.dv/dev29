trigger ValidateShiftOperator on Shift_Operator__c (before insert, before update) 
{
    Set<Id> shiftOperatorIds = new Set<Id>();
    Set<Id> roleIds = new Set<Id>();
    Set<Id> teamAllocationIds = new Set<Id>();
    Set<Id> teamIds = new Set<Id>();
    if (trigger.isUpdate)
        shiftOperatorIds = trigger.newMap.keySet();
    
    for (Shift_Operator__c curOp : trigger.new) 
    {
        if (curOp.Team_Allocation__c != null)
            teamAllocationIds.add(curOp.Team_Allocation__c);
        if (curOp.Role__c != null)
            roleIds.add(curOp.Role__c);
        if (curOp.Team__c != null)
            teamIds.add(curOp.Team__c);        
    }    
    Map<Id, Team_Role__c> roleMap = new Map<Id, Team_Role__c>([select Id, Name, Max_Occurence__c from Team_Role__c where Id =: roleIds]);
    
    //this map is used to store number of existing ShiftOperator with key = Team_AllocationId/TeamId + RoleId            
    Map<String, integer> numShiftOperatorMap = new Map<String, integer>();    
    if (teamAllocationIds != null)
    {
	    List<AggregateResult> countShiftByRole = 
	                  [SELECT count(Id) Sum, Team_Allocation__c, Role__c FROM Shift_Operator__c 
	                     WHERE Team_Allocation__c in: teamAllocationIds and Role__c in: roleIds
	                     and Id not in: shiftOperatorIds
	                     GROUP By Team_Allocation__c, Role__c];
	                
	                 
	    for(AggregateResult ar : countShiftByRole)   
	    {
	    	System.debug(ar);
	    	integer sum = integer.valueOf(ar.get('Sum'));
	    	String taId = String.valueOf(ar.get('Team_Allocation__c'));
	    	String roleId = String.valueOf(ar.get('Role__c'));
	    	String key = taId + '_' + roleId;
	    	numShiftOperatorMap.put(key, sum);
	    }              
    }
    if (teamIds != null)
    {
    	List<AggregateResult> countShiftByRole = 
                      [SELECT count(Id) Sum, Team__c, Role__c FROM Shift_Operator__c 
                         WHERE Team__c in: teamIds and Role__c in: roleIds
                         and Id not in: shiftOperatorIds
                         GROUP By Team__c, Role__c];
                    
                     
        for(AggregateResult ar : countShiftByRole)   
        {
            integer sum = integer.valueOf(ar.get('Sum'));
            String teamId = String.valueOf(ar.get('Team__c'));
            String roleId = String.valueOf(ar.get('Role__c'));
            String key = teamId + '_' + roleId;
            numShiftOperatorMap.put(key, sum);
        }    
    }
    for (Shift_Operator__c curOp : trigger.new) 
    {
    	String key = '';
    	if (curOp.Team_Allocation__c != null)
            key = curOp.Team_Allocation__c;
        
        if (curOp.Team__c != null)
            key = curOp.Team__c;
        
        if (curOp.Role__c != null)
            key += '_' + curOp.Role__c;     
        
        if (numShiftOperatorMap.containsKey(key))
        {
        	integer num = integer.valueOf(numShiftOperatorMap.get(key));
        	Team_Role__c role = roleMap.get(curOp.Role__c);
        	if (role != null && role.Max_Occurence__c > -1 && num >= role.Max_Occurence__c)
        	   curOp.addError('There can only be ' + role.Max_Occurence__c + ' ' + role.Name);
        }     
    }
}