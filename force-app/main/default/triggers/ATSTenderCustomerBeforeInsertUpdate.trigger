trigger ATSTenderCustomerBeforeInsertUpdate on ATS_Tender_Customers__c (before insert, before update) {
	Set<Id> accountIds = new Set<Id>();
    Set<Id> tenderIds = new Set<Id>();
    
    Map<Id, Account> accountMap = new Map<Id, Account>();
    Map<Id, ATS_Parent_Opportunity__c> tenderMap = new Map<Id, ATS_Parent_Opportunity__c>();
    
    for(ATS_Tender_Customers__c customer : Trigger.New) {
    	accountIds.add(customer.Account__c);
        tenderIds.add(customer.ATS_Parent_Opportunity__c);
    }
    
    for(Account acct : [SELECT Id, Name FROM Account WHERE Id in : accountIds]) {
		accountMap.put(acct.Id, acct);   
    }

    for(ATS_Parent_Opportunity__c tender : [SELECT Id, Name FROM ATS_Parent_Opportunity__c WHERE Id in : tenderIds]) {
    	tenderMap.put(tender.Id, tender);
    }
    
    for(ATS_Tender_Customers__c customer : Trigger.New) {
    	customer.Name = tenderMap.get(customer.ATS_Parent_Opportunity__c).Name + ' - ' + accountMap.get(customer.Account__c).Name;
    }                   
}