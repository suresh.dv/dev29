trigger VTT_ConfirmationTrigger on Work_Order_Activity_Confirmation__c (before insert, after insert) {
	if(VTT_Utilities.executeTriggerCode) {
		if(Trigger.isAfter) {
			if(Trigger.isInsert) {
				VTT_ConfirmationsUtilities.TriggerAfterInsertConfirmation(Trigger.new);
			}
		}
	}
}