trigger Product2_PopulateProductToPriceBook on Product2 (after insert) {
    
    List<Pricebook2> activePricebooks = [Select id, Name from Pricebook2 where isActive = true];
    Id stdPricebook;
    Id asphaltPricebook;
    Id emulsionPricebook;
    Id residualPricebook;
    for(Pricebook2 pb : activePricebooks)
    {
        if (pb.Name == 'Standard Price Book')
            stdPricebook = pb.Id;
        else if (pb.Name == 'Asphalt Price Book')    
            asphaltPricebook = pb.Id;
        else if (pb.Name == 'Residual Price Book')    
            residualPricebook = pb.Id;
        else if (pb.Name == 'Emulsion Price Book')    
            emulsionPricebook = pb.Id;
        
    }
    Map<String, Schema.Recordtypeinfo> recTypesByName = Product2.SObjectType.getDescribe().getRecordTypeInfosByName();
    Set<Id> prodIds = new Set<Id>();
    List<PricebookEntry> insertList = new List<PricebookEntry>();
    
    for(Product2 prod : trigger.new)
    {
        if (recTypesByName.get('Asphalt').getRecordTypeId() == prod.RecordTypeId 
        || recTypesByName.get('Emulsion').getRecordTypeId() == prod.RecordTypeId 
        || recTypesByName.get('Residual').getRecordTypeId() == prod.RecordTypeId )
        {
            PricebookEntry stdPriceBookEntry = new PricebookEntry( IsActive = prod.IsActive, Product2Id = prod.id, UnitPrice = 0, Pricebook2Id = stdPricebook);
            insertList.add(stdPriceBookEntry);
            
            PricebookEntry entry;
            if (recTypesByName.get('Asphalt').getRecordTypeId() == prod.RecordTypeId
               && asphaltPricebook != null)
            {
                entry = new PricebookEntry( IsActive = prod.IsActive, Product2Id = prod.id, UnitPrice = 0, Pricebook2Id = asphaltPricebook);
            }
            else if (recTypesByName.get('Emulsion').getRecordTypeId() == prod.RecordTypeId
               && emulsionPricebook != null)
            {
                entry = new PricebookEntry( IsActive = prod.IsActive, Product2Id = prod.id, UnitPrice = 0, Pricebook2Id = emulsionPricebook);
            }   
            else if (recTypesByName.get('Residual').getRecordTypeId() == prod.RecordTypeId
               && residualPricebook != null)
            {
                entry = new PricebookEntry( IsActive = prod.IsActive, Product2Id = prod.id, UnitPrice = 0, Pricebook2Id = residualPricebook);
            }
            if (entry != null)
               insertList.add(entry);
        }
    }
    if (insertList.size() > 0)
        insert insertList;
    
}