trigger setTenderInitialInfo on ATS_Parent_Opportunity__c (before insert) {    
                        
    for (ATS_Parent_Opportunity__c tender : trigger.new){     
        if(tender.Price_Valid_To__c == null){
            //set the Price Valid To field                       
            Date currentDate = date.today();
            integer december = 12;
            integer lastDay = 31;                
                    		
            tender.Price_Valid_To__c = date.newInstance(currentDate.year(), december, lastDay);
        }
        
        if(tender.Acceptance_Deadline__c == null){
            //add 2 weeks to the acceptance deadline from the bid due date
            DateTime newAcceptanceDate = tender.Bid_Due_Date_Time__c.addDays(14);
            tender.Acceptance_Deadline__c = date.newInstance(newAcceptanceDate.year(), newAcceptanceDate.month(), newAcceptanceDate.day());
        }
        
        if(tender.Price_Valid_From__c == null){
            //Price Valid From date should match Bid Due Date
         	tender.Price_Valid_From__c = date.newinstance(tender.Bid_Due_Date_Time__c.year(), tender.Bid_Due_Date_Time__c.month(), tender.Bid_Due_Date_Time__c.day());   
        }
    }
         
}