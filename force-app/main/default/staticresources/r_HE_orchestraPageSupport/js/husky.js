﻿/*
	husky.js
*/

$(document).ready(function () {
	
	function prepareHomeCarousel() {
		if ($('.homeCarousel').length) {
			OCMS.consoleMsg('prepareHomeCarousel');
			
			$('.homeCarousel > span').addClass('ocmsCarousel');
			$('.ocmsCarousel > span').wrapAll('<ul class="carouselList"/>');
			$('.carouselList > span').wrap('<li class="ocmsCarouselItem"/>');
            var $car = $('.homeCarousel .ocmsCarousel ul'),
                $desc = $('.small-block-content', $car),
                mainCarousel;

            $car.after('<div class="carDescriptions">');
            $('.homeCarousel .carDescriptions').append($desc);

            if ($desc.length > 1) {
                $desc.hide().eq(0).show();

                mainCarousel = $car.ocmsCarousel({
                    plugin: 'slideshow',
                    auto: {
                        autoStart: true,
                        frequency: 20000
                    },
                    visibleItems: 1,
                    slideWidth: 960
                }).ocmsCarouselInstance();


                // toggle filmstrip sliding by double-clicking the descriptive text
                $('.homeCarousel .carDescriptions').dblclick(function () {
                    mainCarousel.toggle();
                    $(this)
                        .animate({top: '-=1px'}, 100)
                        .animate({top: '+=2px'}, 100)
                        .animate({top: '-=1px'}, 100);
                });
                
                // descriptive text doesn't slide with image, and
                // update the selected bullet for each slide transition
                $('.homeCarousel ul').bind('ocmsCarouselBeforeTransition', function(evt, params) {
                    $desc.eq(params.from).hide();
                    $desc.eq(params.to).fadeIn();
                });
            }
		}
	};	// prepareHomeCarousel

	// --- begin $(document).ready() ---

	try {
		OCMS.consoleMsg('♢♢ husky.js - $doc.ready ♢♢', Date());

		if (!OCMS.inPageEditor) {	
			
			if ($('.pg').length) {
				prepareHomeCarousel();
			}
			
			$('.tableList .class-styled-list').livequery(function() {
				$(this).find('.header_cell').each(function() {
					var str = $(this).text();
					$(this).html(str.substring(str.indexOf(">") + 1));
				});
				
				$(this).find('.header_cell.column_4').text('');
				$(this).find('tbody tr').each(function() {
						var email = $(this).find('.column_4').text();
						var subject = $(this).find('.column_10').text();
						var successfulBidder = $(this).find('.column_9').html();
						
						var successfulBidderText = $(this).find('.column_9').text();
						
						var getEOICloseDate = $(this).find('.column_2').text()
						
						//var datearray = getEOICloseDate.split("/");
						//var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
						
						var eoiCloseDate = Date.parse(getEOICloseDate);
						
    					// replace contents
    					if (email != '' && email != null && (eoiCloseDate - (new Date()) >= 0)) {
							emailLink = '<a class="submitBid" href="mailto:' + email + '?subject=' + encodeURIComponent(subject) + '">Submit EOI/Prequal</a>';
							$(this).find('.column_4').html(
								emailLink
							);
    					} else {
    						$(this).find('.column_4').text('');
    					}
    					
    					if (successfulBidderText != '' && successfulBidderText != null) {
    						var sbhtml = '<a href="#" class="successfulBidLink">View</a>';
    						sbhtml += '<div class="successfulBidText">'
    						sbhtml += '<div class="close-icon" rel="close-panel">';
							sbhtml += '<a href="#">x</a></div>';
    						sbhtml += successfulBidder + '</div>';
    						$(this).find('.column_9').html(sbhtml);
    					}
    					
				});
				$(this).find('.column_10').remove();
				
				$(this).find('tbody .column_9').find('.successfulBidLink').click(function(e) {
        			e.preventDefault();
        			$('.successfulBidText').hide();
        			$(this).siblings('.successfulBidText').fadeIn(300);
        			position = $(this).position();
        		    $('.successfulBidText').css('top', position.top + 17);
                });
                
                $('.close-icon').click(function(e) {
                	e.preventDefault();
        			$('.qualifiedBidText, .successfulBidText').fadeOut(300);
                });
    			
				$(this).find('tfoot td').attr('colspan', '10');
				var colCount = 0;
    			$('.row-0 td').each(function() {
        			colCount++;
    			});
    			var html = '<tr class="last-row">';
    			for (var i = 0; i < colCount; i++) {
    				html += '<td class="column-' + i + '"></td>';
				}
				html += '</tr>';
				$(this).find('tbody').append(html);
			});
			
			if ($('.mainNav').length) {
				var numitems =  $('.mainNav li.root-level-item').length;
    			var widthitem = Math.floor(960 / numitems );
    			$('.mainNav li.root-level-item').css('width', (widthitem - 1));
			}
		}

		OCMS.consoleMsg('♦♦ husky.js / $doc.ready ♦♦', Date());
	} catch (ex) {
		OCMS.consoleMsg('▶▶ husky.ready()', ex);
        if (ex.type) {
            console.log('\t' + ex.type);
        }
        if (ex.arguments && ex.arguments.length) {
            for (var nIt = 0; nIt < ex.arguments.length; ++nIt) {
                console.log('\t' + ex.arguments[nIt]);
            }
        }
	}
});	// $(document).ready