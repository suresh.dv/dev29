<apex:page controller="GRDFilterController">
    <style>
        .sortArrow {
            vertical-align:middle;
            width:11px;
            height:11px;
            background: url('/img/alohaSkin/sortArrows_sprite.png') no-repeat scroll 0px top transparent;
            display:none;
        }
    </style>
    <apex:form id="wellform">
        <apex:pageBlock title="{!fieldLabel}: {!fieldValue}">
            <!-- tabel of wells -->
            <apex:outputPanel id="wellsView">
                <apex:outputPanel layout="block" style="text-align:center;width:100%" rendered="{!wells.size > 0}" id="wellsInfo">
                    <apex:outputText rendered="{!mySetController.hasNext}" style="padding-right: 8px;">{!(((mySetController.pageNumber - 1) * mySetController.pageSize) + 1 )} - {!(mySetController.pageNumber * mySetController.pageSize)}</apex:outputText>
                    <apex:outputText rendered="{!(!mySetController.hasNext)}" style="padding-right: 8px;">{!(((mySetController.pageNumber - 1) * mySetController.pageSize) + 1 )} - {!mySetController.resultSize}</apex:outputText> (Total Wells: {!mySetController.resultSize})
                </apex:outputPanel>
                <apex:outputPanel rendered="{!wells.size > 0}">
                    <apex:pageBlockTable value="{!wells}" var="well" id="wellTable">
                        <apex:column >
                            <apex:facet name="header">
                                <div onclick="sort('Name');">Well Name<div class="sortArrow" style="{!IF(sortField='Name','display:inline-block;','')}{!IF(sortOrder='DESC','background-position:0px -16px;','')}"></div></div>
                            </apex:facet>
                            <a href="{!URLFOR($Page.GRDDashboard)}?id={!well.id}">{!well.name}</a>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">
                                <div onclick="sort('License Number');">License Number<div class="sortArrow" style="{!IF(sortField='License Number','display:inline-block;','')}{!IF(sortOrder='DESC','background-position:0px -16px;','')}"></div></div>
                            </apex:facet>
                            <apex:outputText value="{!well.Well_License_No__c}"/>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">
                                <div onclick="sort('Surface Location');">Surface Location<div class="sortArrow" style="{!IF(sortField='Surface Location','display:inline-block;','')}{!IF(sortOrder='DESC','background-position:0px -16px;','')}"></div></div>
                            </apex:facet>
                            <apex:outputLink value="/{!well.Well__r.Surface_Location__c}">{!well.Well__r.Surface_Location__r.Name}</apex:outputLink>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">
                                <div onclick="sort('Target Formation');">Target Formation<div class="sortArrow" style="{!IF(sortField='Target Formation','display:inline-block;','')}{!IF(sortOrder='DESC','background-position:0px -16px;','')}"></div></div>
                            </apex:facet>
                            <apex:outputLink value="/apex/GRDFilter?field=Target_Formation__c&value={!well.Target_Formation__c}">{!well.Target_Formation__c}</apex:outputLink>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">
                                <div onclick="sort('Well Class');">Well Class<div class="sortArrow" style="{!IF(sortField='Well Class','display:inline-block;','')}{!IF(sortOrder='DESC','background-position:0px -16px;','')}"></div></div>
                            </apex:facet>
                            <apex:outputLink value="/apex/GRDFilter?field=Well_Class__c&value={!well.Well_Class__c}">{!well.Well_Class__c}</apex:outputLink>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">
                                <div onclick="sort('Well Orientation');">Well Orientation<div class="sortArrow" style="{!IF(sortField='Well Orientation','display:inline-block;','')}{!IF(sortOrder='DESC','background-position:0px -16px;','')}"></div></div>
                            </apex:facet>
                            <apex:outputLink value="/apex/GRDFilter?field=Well_Orientation__c&value={!well.Well_Orientation__c}">{!well.Well_Orientation__c}</apex:outputLink>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">
                                <div onclick="sort('Budget Year');">Budget Year<div class="sortArrow" style="{!IF(sortField='Budget Year','display:inline-block;','')}{!IF(sortOrder='DESC','background-position:0px -16px;','')}"></div></div>
                            </apex:facet>
                            <apex:outputPanel rendered="{!NOT(ISBLANK(well.Budget_Year_Drilling__c))}"><apex:outputLink value="/apex/GRDFilter?field=Budget_Year&value={!well.Budget_Year_Drilling__c}">{!well.Budget_Year_Drilling__c}</apex:outputLink></apex:outputPanel>
                            <apex:outputPanel rendered="{!AND(NOT(ISBLANK(well.Budget_Year_Completion__c)), well.Budget_Year_Drilling__c != well.Budget_Year_Completion__c)}"><apex:outputPanel rendered="{!NOT(ISBLANK(well.Budget_Year_Drilling__c))}">,&nbsp;</apex:outputPanel><apex:outputLink value="/apex/GRDFilter?field=Budget_Year&value={!well.Budget_Year_Completion__c}">{!well.Budget_Year_Completion__c}</apex:outputLink></apex:outputPanel>
                            <apex:outputPanel rendered="{!AND(NOT(ISBLANK(well.Budget_Year_Tie_In__c)), well.Budget_Year_Drilling__c != well.Budget_Year_Tie_In__c, well.Budget_Year_Completion__c != well.Budget_Year_Tie_In__c)}"><apex:outputPanel rendered="{!AND(NOT(ISBLANK(well.Budget_Year_Completion__c)), well.Budget_Year_Drilling__c != well.Budget_Year_Completion__c)}">,&nbsp;</apex:outputPanel><apex:outputLink value="/apex/GRDFilter?field=Budget_Year&value={!well.Budget_Year_Tie_In__c}">{!well.Budget_Year_Tie_In__c}</apex:outputLink></apex:outputPanel>
                            <apex:outputPanel rendered="{!AND(NOT(ISBLANK(well.Budget_Year_Facility_Equipment__c)), well.Budget_Year_Drilling__c != well.Budget_Year_Facility_Equipment__c, well.Budget_Year_Completion__c != well.Budget_Year_Facility_Equipment__c, well.Budget_Year_Tie_In__c != well.Budget_Year_Facility_Equipment__c)}"><apex:outputPanel rendered="{!AND(NOT(ISBLANK(well.Budget_Year_Tie_In__c)), well.Budget_Year_Drilling__c != well.Budget_Year_Tie_In__c, well.Budget_Year_Completion__c != well.Budget_Year_Tie_In__c)}">,&nbsp;</apex:outputPanel><apex:outputLink value="/apex/GRDFilter?field=Budget_Year&value={!well.Budget_Year_Facility_Equipment__c}">{!well.Budget_Year_Facility_Equipment__c}</apex:outputLink></apex:outputPanel>
                        </apex:column>
                    </apex:pageBlockTable>
                    <apex:outputPanel style="width:100%;text-align:center" layout="block">
                        <apex:outputText rendered="{!(!mySetController.hasPrevious)}" style="padding-right: 8px; color: #999999;" value="First" />
                        <apex:commandLink action="{!mySetController.first}" rendered="{!mySetController.hasPrevious}" style="padding-right: 8px;" rerender="wellform">First</apex:commandlink>
                        <apex:outputText rendered="{!(!mySetController.hasPrevious)}" style="padding-right: 8px; color: #999999;" value="Previous" />
                        <apex:commandLink action="{!mySetController.previous}" rendered="{!mySetController.hasPrevious}" style="padding-right: 8px;" rerender="wellform">Previous</apex:commandlink>
                        <apex:outputText rendered="{!mySetController.hasNext}" style="padding-right: 8px;">{!(((mySetController.pageNumber - 1) * mySetController.pageSize) + 1 )} - {!(mySetController.pageNumber * mySetController.pageSize)}</apex:outputText>
                        <apex:outputText rendered="{!(!mySetController.hasNext)}" style="padding-right: 8px;">{!(((mySetController.pageNumber - 1) * mySetController.pageSize) + 1 )} - {!mySetController.resultSize}</apex:outputText>
                        <apex:outputText rendered="{!(!mySetController.hasNext)}" style="padding-right: 8px; color: #999999;" value="Next" />
                        <apex:commandLink action="{!mySetController.next}" rendered="{!mySetController.hasNext}" style="padding-right: 8px;" rerender="wellform">Next</apex:commandlink>
                        <apex:outputText style="padding-right: 8px; color: #999999;" rendered="{!(!mySetController.hasNext)}" value="Last" />
                        <apex:commandLink action="{!mySetController.last}" style="padding-right: 8px;" rendered="{!mySetController.hasNext}" rerender="wellform">Last</apex:commandlink>
                    </apex:outputPanel>
                </apex:outputPanel>
                <apex:outputPanel rendered="{!wells.size == 0}">
                    No wells to display.
                </apex:outputPanel>
            </apex:outputPanel>
            <apex:actionFunction name="sort" action="{!sortWells}" reRender="wellTable">
                <apex:param name="sortField" value="" assignTo="{!sortField}"/>
            </apex:actionFunction>
        </apex:pageBlock>
    </apex:form>
</apex:page>