<apex:page id="fmfc_page" controller="FM_FacilitySummaryCapacityController" title="Update Facility Capacity" tabstyle="Facility_Summary__tab" docType="html-5.0">

	<apex:pageMessages id="messages" escape="false" />

	<apex:form id="fmform">
		<apex:pageBlock id="pb" title="Update Facility Capacity">

			<apex:pageBlockButtons id="pbButtons" location="bottom">
				<apex:actionStatus id="saveStatus">

					<apex:facet name="stop">
						<apex:outputPanel >
							<apex:commandButton action="{!SaveRecordChanges}" value="Save" rendered="{!sSelectedFacility != ''}" status="ajaxStatus" disabled="false" rerender="pb, messages" />
							<apex:commandButton action="{!CloseButton}" value="Close" immediate="true" status="saveStatus" disabled="false" rerender="pb, messages" />
						</apex:outputPanel>
					</apex:facet>

					<apex:facet name="start">
						<apex:outputPanel >
							<apex:commandButton value="Save" rendered="{!sSelectedFacility != ''}" disabled="true" />
							<apex:commandButton value="Close" disabled="true" />
						</apex:outputPanel>
					</apex:facet>

				</apex:actionStatus>
			</apex:pageBlockButtons>

			<apex:actionstatus id="ajaxStatus">
				<apex:facet name="start">

					<div class="waitingSearchDiv" id="el_loading" style="background-color: #dcd6d6; height: 100%; opacity:0.65; width: 100%;">
						<div class="waitingHolder" style="top: 150px; width: 200px; height: 40px; border: 1px solid black; background-color: white;">
							<p>
								<img class="waitingImage" src="/img/loading.gif" title="Please Wait..." />
								<span class="waitingDescription">Please Wait...</span>
							</p>
						</div>
					</div>

				</apex:facet>
			</apex:actionstatus>

			<apex:pageBlockSection title="Select a Facility" collapsible="false" id="pbsSelect" columns="1">

				<apex:panelGrid columns="2">
					<apex:actionRegion >
						<apex:selectList id="facilitylist" value="{!sSelectedFacility}" size="1" label="Facility" style="width: 250px;" html-autocomplete="off">
							<apex:selectOption itemValue="" itemLabel="- Select Facility -"/>
							<apex:selectOptions value="{!lFacilityList}" />
							<apex:actionSupport action="{!UpdateSelectedFacility}" event="onchange" status="ajaxStatus" rerender="messages, pb" />
							<apex:actionSupport action="{!UpdateSelectedFacility}" event="onkeydown" status="ajaxStatus" rerender="messages, pb" />
						</apex:selectList>
					</apex:actionRegion>

				<apex:outputPanel rendered="{!oFacilityCapacity.Id != null}">
					<apex:panelGrid columns="4">
						<apex:outputText value="Capacity information was last updated by" />
						<apex:outputLink target="_blank" style="color: #0532f3;" value="/{!oFacilityCapacity.LastModifiedBy.Id}">{!oFacilityCapacity.LastModifiedBy.Name}</apex:outputLink>
						<apex:outputText value="on {!sLastModifiedDate}" />
						<apex:outputLink target="_blank" style="color: #0532f3;" value="/{!oFacilityCapacity.Id}">VIEW HISTORY</apex:outputLink>
					</apex:panelGrid>
				</apex:outputPanel>

				</apex:panelGrid>

			</apex:pageBlockSection>

			<apex:pageBlockSection title="Capacity Values" collapsible="false" id="pbsValues" columns="1" rendered="{!sSelectedFacility != ''}">
				<apex:inputField id="oilfield" value="{!oFacilityCapacity.Total_Capacity_Oil__c}" required="true" style="width: 60px;" html-maxlength="5" html-title="Capacity must be between 0 and 9999" />
				<apex:inputField id="waterfield" value="{!oFacilityCapacity.Total_Capacity_Water__c}" required="true" style="width: 60px;" html-maxlength="5" html-title="Capacity must be between 0 and 9999" />
			</apex:pageBlockSection>

		</apex:pageBlock>
	</apex:form>

</apex:page>