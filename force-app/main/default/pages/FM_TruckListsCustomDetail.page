<apex:page controller="FM_TruckListsCustomCtrl" tabStyle="FM_Truck_Lists__tab">
    <style type="text/css">
        .actionIcon {
            cursor: pointer;
            padding-right: 2px;
        }
        /*Popup*/
        .custPopup
        {
            background-color: white;
            border-radius: 5px;
            border-style: solid;
            border-width: 1px;
            left: 50%;
            margin-left: -250px;
            padding:10px;
            position: fixed;
            top: 30%;
            width: 450px;
            z-index: 99;
        }
        .custPopup .apexp .bPageBlock .pbHeader table {
            padding: 0px;
        }
        .custPopup .apexp .bPageBlock .pbHeader table td.pbTitle {
            width: 100%;
        }
        .custPopup .apexp .bPageBlock .pbBody,
        .custPopup .apexp .bPageBlock .pbBottomButtons {
            margin:  0px !important;
            white-space: normal;
        }
        .custPopup .popupmessage span{
            height: 100%;
            overflow: auto;
        }
        .popupBackground{
            background-color:black;
            filter: alpha(opacity = 20);
            height: 100%;
            left: 0;
            opacity: 0.20;
            position: absolute;
            top: 0;
            width: 100%;
            z-index: 98;
        }
        .cleanLoad {
            background-color:#ADD8E6;
        }
    </style>
    <apex:form >
        <!-- Status Spinner -->
        <apex:actionstatus id="ajaxStatus">
            <apex:facet name="start">

                <div class="waitingSearchDiv" id="el_loading" style="background-color: #DCD6D6; height: 100%;opacity:0.65;width:100%;">
                    <div class="waitingHolder" style="top: 150px; width: 200px; height: 40px; border:1px solid black;  background-color: white;">
                        <p>
                            <img class="waitingImage" src="/img/loading.gif" title="Please Wait..." />
                            <span class="waitingDescription">Please Wait...</span>
                        </p>
                    </div>
                </div>

            </apex:facet>
        </apex:actionstatus>

        <apex:outputPanel id="trucklistdetail">
            <apex:pageBlock title="{!IF(truckTripGeneral.Truck_Trip_Status__c = 'New','Pending',truckTripGeneral.Truck_Trip_Status__c)} {!truckTripGeneral.Unit__r.Name} {!truckTripGeneral.Shift__c} {!truckTripGeneral.Run_Sheet_Date__c}">

                <apex:pageBlockSection columns="2">
                	<apex:outputField value="{!truckTripGeneral.Carrier__r.Carrier_Name_For_Fluid__c}"/>
                    <apex:outputField value="{!truckTripGeneral.Unit__c}"/>
                    <apex:outputField value="{!truckTripGeneral.Planned_Dispatch_Date__c}"/>
                    <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                    <apex:outputField value="{!truckTripGeneral.Shift__c}"/>
                    <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                    <apex:outputField value="{!truckTripGeneral.Truck_Trip_Status__c}"/>
                    <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                    <apex:outputField value="{!truckTripGeneral.TL_Phone_Display__c}"/>
                    <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                    <apex:outputField value="{!truckTripGeneral.TL_First_Booked_By__c}"/>
                    <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                    <apex:outputField value="{!truckTripGeneral.TL_Dispatched_By__c}"/>
                    <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>
                    <apex:outputField value="{!truckTripGeneral.TL_Dispatched_On__c}"/>
                    <apex:pageBlockSectionItem ></apex:pageBlockSectionItem>

                </apex:pageBlockSection>

                <apex:pageBlockSection title="Load Statistics" columns="1" collapsible="false">
                    <apex:outputText label="Oil Loads" value="{!CurrentWrapper.oil}" />
                    <apex:outputText label="Water Loads" value="{!CurrentWrapper.water}" />
                    <apex:outputText label="Total Loads" value="{!CurrentWrapper.total}" />
                    <apex:outputText label="Completed Loads" value="{!CurrentWrapper.completed}" />
                </apex:pageBlockSection>

                <apex:pageBlockSection title="Truck List Comments" columns="1" collapsible="false">
                    <apex:outputField value="{!truckTripGeneral.TL_Truck_List_Comments__c}" style="width: 700px;" />
                </apex:pageBlockSection>

                <apex:pageBlockSection title="Dispatch Comments" columns="1" collapsible="false">
                    <apex:outputText value="{!oComments.Dispatch_Comments__c}" style="width: 700px;" />
                </apex:pageBlockSection>

                <apex:pageBlockButtons >
                    <apex:commandButton value="Edit" action="{!showEdit}"/>
                    <apex:commandButton value="Back" action="{!back}"/>
                </apex:pageBlockButtons>
            </apex:pageBlock>
    	</apex:outputPanel>

        <apex:outputPanel id="trucktripslist">
            <apex:pageMessages />
            <apex:pageBlock title="Truck Trips" rendered="{!TruckTrips.size > 0}">
                <apex:pageBlockTable value="{!TruckTrips}" var="tt" > <!--columnsWidth="10px,10px,10px,10px,10px,220px,220px,220px,10px,10px,10px,10px"-->
                    <apex:column styleClass="{!IF(tt.truckTrip.Load_Type__c = 'Clean', 'cleanLoad', '')}">
                        <apex:facet name="header">Truck Trip Name</apex:facet>
                        <apex:outputLink value="/{!tt.truckTrip.Id}">{!tt.truckTrip.Name}
                        </apex:outputLink>
                    </apex:column>
                    <apex:column headerValue="Dispatch - Shift" styleClass="{!IF(tt.truckTrip.Load_Type__c = 'Clean', 'cleanLoad', '')}">
                        <apex:outputText value="{!tt.truckTrip.Shift__c} {!tt.truckTrip.Planned_Dispatch_Date__c}"/>
                    </apex:column>
                    <apex:column headerValue="Route" styleClass="{!IF(tt.truckTrip.Load_Type__c = 'Clean', 'cleanLoad', '')}">
                        <apex:outputField value="{!tt.truckTrip.Route__c}"/>
                    </apex:column>
                    <apex:column headerValue="Product" styleClass="{!IF(tt.truckTrip.Load_Type__c = 'Clean', 'cleanLoad', '')}">
                        <apex:outputField value="{!tt.truckTrip.Product__c}"/>
                    </apex:column>

                    <apex:column headerValue="Flow Rate (m3/day)" styleClass="{!IF(tt.truckTrip.Load_Type__c = 'Clean', 'cleanLoad', '')}">
                        <apex:outputField value="{!tt.truckTrip.Load_Request__r.Act_Flow_Rate__c}"/>
                    </apex:column>

                    <apex:column styleClass="{!IF(tt.TruckTrip.Sour__c, 'sourGas', IF(tt.isCleanLoad, 'cleanLoad', ''))}">
                        <apex:facet name="header">
                            Scada Level<br/>(m3)
                        </apex:facet>
                        <apex:outputText value="{!CEILING(tt.truckTrip.Load_Request__r.Equipment_Tank__r.SCADA_Tank_Level__c)}"/>
                        <br/>
                        <apex:outputField value="{!tt.truckTrip.Load_Request__r.Equipment_Tank__r.Latest_Tank_Reading_Date__c}"/>
                    </apex:column>

                    <apex:column headerValue="Load Request" style="white-space: nowrap" styleClass="{!IF(tt.truckTrip.Load_Type__c = 'Clean', 'cleanLoad', '')}">
                        <!-- <apex:outputField rendered="{!tt.truckTrip.Load_Request__r.Source_Location__c != null}" value="{!tt.truckTrip.Load_Request__r.Source_Location__r.Name}"/>
                        <apex:outputField rendered="{!tt.truckTrip.Load_Request__r.Source_Facility__c != null}" value="{!tt.truckTrip.Load_Request__r.Source_Facility__r.Name}"/> -->
                        <apex:outputField value="{!tt.truckTrip.Load_Request__r.Name}" />
                    </apex:column>

                    <apex:column headerValue="Comments" width="200px" styleClass="{!IF(tt.truckTrip.Load_Type__c = 'Clean', 'cleanLoad', '')}">
                        <apex:outputField value="{!tt.truckTrip.Standing_Comments__c}" />
                    </apex:column>

                    <!-- <apex:column headerValue="RM/County" style="white-space: nowrap">
                        <apex:outputField value="{!tt.truckTrip.Load_Request__r.RM_County__c}"/>
                    </apex:column> -->

                    <apex:column headerValue="Unit Configuration" styleClass="{!IF(tt.truckTrip.Load_Type__c = 'Clean', 'cleanLoad', '')}">
                        <apex:outputField value="{!tt.truckTrip.Unit_Configuration__c}"/>
                    </apex:column>

                    <apex:column headerValue="Sour" value="{!tt.truckTrip.Sour__c}" styleClass="{!IF(tt.truckTrip.Load_Type__c = 'Clean', 'cleanLoad', '')}"/>

                    <apex:column headerValue="Unit #" styleClass="{!IF(tt.truckTrip.Load_Type__c = 'Clean', 'cleanLoad', '')}">
                        <apex:outputField value="{!tt.truckTrip.Unit__c}" />
                    </apex:column>

                    <apex:column value="{!tt.truckTrip.Truck_Trip_Status__c}" styleClass="{!IF(tt.truckTrip.Load_Type__c = 'Clean', 'cleanLoad', '')}"/>

                    <apex:column headerValue="Action" width="10%" rendered="{!IsTrucktripCompatible}" styleClass="{!IF(tt.truckTrip.Load_Type__c = 'Clean', 'cleanLoad', '')}">
                        <apex:image value="/img/icon/generic16.png" alt="Rebook Trucktrip" title="Rebook Trucktrip" styleClass="actionIcon"
                                    rendered="{!tt.truckTrip.Truck_Trip_Status__c == DispatchedStatus || tt.truckTrip.Truck_Trip_Status__c == AddOnStatus}">
                            <apex:actionSupport event="onclick" action="{!RebookTruckTripPopupOpen}" reRender="trucktripslist,rebookPopup">
                                <apex:param name="truckTripId" value="{!tt.truckTrip.Id}" />
                            </apex:actionSupport>
                        </apex:image>
                        <apex:image value="/img/func_icons/remove12_on.gif" alt="Cancel Trucktrip" title="Cancel TruckTrip" styleClass="actionIcon"
                            rendered="{!tt.truckTrip.Truck_Trip_Status__c <> CancelledStatus && tt.truckTrip.Truck_Trip_Status__c <> RebookedStatus && tt.truckTrip.Truck_Trip_Status__c <> CompletedStatus}">
                            <apex:actionSupport event="onclick" action="{!openCancelDialog}" reRender="trucktripslist,cancelPopup">
                                <apex:param name="truckTripId" value="{!tt.truckTrip.Id}" />
                                <apex:param name="dispatchedTrips" assignTo="{!dispatchedTrips}" value="true" />
                            </apex:actionSupport>
                        </apex:image>
                        <apex:image value="/img/permissions_confirm16.gif" alt="Confirm Trucktrip" title="Confirm Trucktrip" styleClass="actionIcon"
                            rendered="{!(tt.truckTrip.Truck_Trip_Status__c == DispatchedStatus || tt.truckTrip.Truck_Trip_Status__c == AddOnStatus) && tt.truckTrip.Stuck__c == 0}">
                            <apex:actionSupport event="onclick" action="{!redirectToConfirmationCreationPage}" reRender="trucktripslist">
                                <apex:param name="truckTripId" value="{!tt.truckTrip.Id}" />
                            </apex:actionSupport>
                        </apex:image>
                    </apex:column>

                </apex:pageBlockTable>
            </apex:pageBlock>
            <apex:pageMessage severity="INFO" title="No truck trips found for filter criteria." rendered="{!TruckTrips.size = 0}"/>
        </apex:outputPanel>

        <!------ Rebook Reason Prompt ------>
        <apex:outputPanel id="rebookPopup" styleClass="popup-wrapper">
            <apex:outputPanel layout="block" rendered="{!showRebookPopup}">
                <apex:outputPanel styleClass="popupBackground" layout="block" />
                <apex:outputPanel styleClass="custPopup" layout="block" rendered="{!truckTripRebooked != null}">
                    <apex:pageMessage summary="{!rebookErrorMsg}" severity="error" rendered="{!isRebookError}"/>
                    <apex:pageBlock mode="edit" title="Rebook">
                        <apex:pageBlockSection collapsible="false" columns="1" showHeader="false" >
                            <apex:pageBlockSectionItem >
                                <apex:outputLabel value="Rebook Reason"/>
                                <apex:outputPanel styleClass="requiredInput" layout="block" >
                                    <apex:outputPanel styleClass="requiredBlock" layout="block"/>
                                    <apex:inputField value="{!truckTripGivenBack.Rebook_Reason__c}">
                                        <apex:actionSupport event="onchange" action="{!handleRebookReasonChange}" reRender="rebookPopup"/>
                                    </apex:inputField>
                                    <apex:inputField value="{!truckTripGivenBack.Rebook_Comments__c}" rendered="{!truckTripGivenBack.Rebook_Reason__c == 'Other'}"/>
                                </apex:outputPanel>
                            </apex:pageBlockSectionItem>
                            <apex:pageBlockSectionItem >
                                <apex:outputLabel value="Destination - Facility"/>
                                <apex:selectList id="facilityPicklist" value="{!truckTripRebooked.truckTrip.Facility_String_Hidden__c}" size="1" label="Facility" styleClass="facility-picklist changeDropdown">
                                    <apex:selectOptions value="{!lFacilityList}"/>
                                </apex:selectList>
                            </apex:pageBlockSectionItem>
                            <apex:pageBlockSectionItem rendered="{!truckTripRebooked.truckTrip.Load_Type__c != null}">
                                <apex:outputLabel value="Well Location"/>
                                <apex:inputField id="wellLookup" style="width: 200px;" value="{!truckTripRebooked.truckTrip.Well__c}" styleClass="changeDropdown" />
                            </apex:pageBlockSectionItem>
                            <apex:pageBlockSectionItem >
                                <apex:outputLabel value="Unit"/>
                                <apex:outputPanel styleClass="requiredInput" layout="block" >
                                    <apex:outputPanel styleClass="requiredBlock" layout="block"/>
                                    <apex:selectList id="unitPicklist" value="{!truckTripRebooked.truckTrip.Unit__c}" size="1" label="Facility" styleClass="changeDropdown">
                                        <apex:selectOption itemValue="" itemLabel="--None--"/>
                                        <apex:selectOptions value="{!unitSelectOptions}"/>
                                    </apex:selectList>
                                </apex:outputPanel>
                            </apex:pageBlockSectionItem>
                            <apex:pageBlockSectionItem >
                                <apex:outputLabel value="Shift"/>
                                <apex:outputPanel styleClass="requiredInput" layout="block" >
                                    <apex:outputPanel styleClass="requiredBlock" layout="block"/>
                                    <apex:selectList id="dispatchShiftPicklist" value="{!truckTripRebooked.dispatchShift}" size="1" label="Facility" styleClass="changeDropdown">
                                        <apex:selectOptions value="{!shiftSelectOptionList}" />
                                    </apex:selectList>
                                </apex:outputPanel>
                            </apex:pageBlockSectionItem>
                        </apex:pageBlockSection>
                        <apex:pageBlockButtons location="bottom" style="text-align:center">
                            <apex:commandButton value="Confirm" action="{!RebookTruckTripPopupConfirm}" rerender="rebookPopup,trucktripslist" status="ajaxStatus" />
                            <apex:commandButton value="Close" action="{!RebookTruckTripPopupCancel}" rerender="rebookPopup,trucktripslist" status="ajaxStatus" />
                        </apex:pageBlockButtons>
                    </apex:pageBlock>
                </apex:outputPanel>
                <apex:outputPanel styleClass="custPopup" layout="block" rendered="{!truckTripRebooked == null}">
                    <apex:pageMessage summary="Rebooked trucktrip wasn't created!" severity="error"/>
                </apex:outputPanel>
            </apex:outputPanel>
        </apex:outputPanel>

        <!------ CONFIRM CANCELLING LOAD REQUEST POPUP ------>
        <apex:outputPanel id="cancelPopup" styleClass="popup-wrapper">
            <apex:outputPanel layout="block" rendered="{!showCancelPopup}">
                <apex:outputPanel styleClass="popupBackground" layout="block" />
                <apex:outputPanel styleClass="custPopup" layout="block">
                    <apex:pageBlock mode="maindetial" title="Please enter reason for cancelling requested load" >
                        <apex:pageBlockSection collapsible="false" columns="1" showHeader="false">
                            <apex:selectList value="{!truckTripCancelled.Load_Request_Cancel_Reason__c}" label="" size="1">
                                <apex:selectOptions value="{!LoadRequestCancelReasons}" />
                                <apex:actionSupport event="onchange" action="{!handleCancelReasonChange}" reRender="cancelPopup"/>
                            </apex:selectList>
                            <apex:inputField value="{!truckTripCancelled.Load_Request_Cancel_Comments__c}" label="" rendered="{!truckTripCancelled.Load_Request_Cancel_Reason__c == 'Other'}"/>
                            <apex:outputText value="Note: Cancellation of the truck trip will result in cancellation of the load and the load cannot be rebooked. You will have to create a new Load Request to rebook the load." />
                        </apex:pageBlockSection>
                        <apex:pageBlockButtons location="bottom">
                            <apex:commandButton value="Confirm" action="{!confirmCancelDialog}" rerender="cancelPopup,truckTripInfo,trucktripslist" status="ajaxStatus" />
                            <apex:commandButton value="Close" action="{!closeCancelDialog}" rerender="cancelPopup,truckTripInfo,trucktripslist" status="ajaxStatus" />
                        </apex:pageBlockButtons>
                </apex:pageBlock>
                </apex:outputPanel>
            </apex:outputPanel>
        </apex:outputPanel>
    </apex:form>
</apex:page>