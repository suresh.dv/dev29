<apex:page controller="cms.CoreController" id="DSP_CMS_Index" standardStylesheets="false" extensions="DSP_CMS_CalendarController" applyHtmlTag="false" showHeader="false" cache="false">
<apex:composition template="{!page_template_reference}">
<html class="no-js whiteBackground" lang="en">
<apex:define name="header">
    <c:DSP_Header />
    <title>Calendar - Downstream Portal</title>
    <link rel="stylesheet" href="{!URLFOR($Resource.DSP_CSS, 'fullcalendar.css')}" />
    <style>
        button:focus {
            background-color:#ccc;
        }
    </style>
</apex:define>
<apex:define name="body">
    <c:DSP_CMS_Navigation />
    <div style="background-color:#FFF">
    <main class="custom-container">
        <div class="row">
            <!-- First div is a hack so the first card does not auto center, this seems to fix it -->
            <div class="col s12 m12 l12"></div>
            <!-- End hack -->
            <div class="col s12 m12 l12">
                <h2><i class="icon-padding medium mdi-action-event"></i>Calendar</h2>
            </div>
            <div class="col s12 m12 l12 new-event" id="newEventButton" style="display:none">
                <div class='btn new-event-button'><a href="{!$Setup.DSP_Settings__c.New_Event_Return_URL__c}"><span class="white-title-text">New Event</span></a></div>
            </div>
            <div class="col s12 m12 l6 new-event">
                <div id="calendar"></div>
            </div>
            <div class="col s12 m12 l6">
                <div id="calendarMonthView"></div>
            </div>

        </div>
    </main>
    </div>
    <c:DSP_CMS_Footer />
    <script>
        // dynamically get all events
        var eventRequest = {"action": 'CalendarEvents'};
        
        var dynamicEventDisplay = [];
        var allEvents;
        
        
        function getEvents()
        {
            $.orchestracmsRestProxy.doAjaxServiceRequest('DSP_CMS_CalendarService', eventRequest, function(textStatus, json, xhr)
            {
                if(json.events)
                {
                    allEvents = json.events;

                    for(var i = 0; i < allEvents.length; i++)
                    {
                        var eventObj2 = new Object();
                        eventObj2.title = allEvents[i].title;
                        eventObj2.start = allEvents[i].startStringIE;
                        eventObj2.end = allEvents[i].endStringIE;
                        eventObj2.url = allEvents[i].url;
                        eventObj2.allDay = allEvents[i].allDay;
                        
                        dynamicEventDisplay.push(eventObj2);
                        
                    }

                    processEvents();
                }
            },null,true);
        }
        
        function getAdminStatus()
        {
            $.orchestracmsRestProxy.doAjaxServiceRequest('DSP_CMS_UserService', eventRequest, function(textStatus, json, xhr)
            {
                if(json.isSiteAdmin)
                {
                    $('#newEventButton').css('display','block');
                }
            },null,true);
        }
        ///////////////////////////////////////////////
        //////// Calendar Initialization //////////////
        ///////////////////////////////////////////////
        function callbackWhenJQueryIsLoaded() {
            jqnew(document).ready(function(){
                getEvents();
                getAdminStatus();
            });
        }
            
        function processEvents() {
            jqnew('#calendar').fullCalendar({
                header: {
                  left: 'prev,next today',
                  center: 'title',
                  right: 'month,basicWeek,basicDay'
                },
                editable: false,
                eventLimit: true,
                eventColor: '#1976D2',
                events:dynamicEventDisplay,
                timeFormat: 'h(:mm)t'
            });
            // This stores the current month and year
            var currentMonth = getCurrentMonth();
            var currentYear = getCurrentYear();
            var currentEvents = getCurrentEvents();

            // Build html for current page
            var calendarViewHTML = getCalendarHTML();
            jqnew("#calendarMonthView").html(calendarViewHTML);

            // Update the current month and year and get a list of current months events
            jqnew(".fc-prev-button, .fc-next-button").click(function() {
                currentMonth = getCurrentMonth();
                currentYear = getCurrentYear();
                currentEvents = getCurrentEvents();

                // Reset the calendar HTML
                calendarViewHTML = getCalendarHTML();
                jqnew("#calendarMonthView").html(calendarViewHTML);
            });

            // Replace the â€” character if it is showing (unicode issue)
            jqnew(".fc-basicWeek-button, .fc-prev-button, .fc-next-button, .fc-today-button").click(function() {
                var weekBody = jqnew(".fc-center").html();
                weekBody = weekBody.replace(/â€”/ig, " - ");
                jqnew(".fc-center").html(weekBody);
            })

            // Returns the HTML for the view of events for current month
            function getCalendarHTML() {
                var className = 'list-odd';
                var html = '';
                html += '<div class="col s12 m12 l12 agenda-title">\n';
                html += '<h3 class="center-align"><span class="white-title-text">' + currentMonth +  ' ' + currentYear + '</span></h3>\n';
                html += '</div>\n';

                for(var i=0; i<currentEvents.length; i++) {
                    // Figure out if odd or even row for html markup
                    if(i % 2 === 0) {
                        className = 'list-odd';
                    } else {
                        className = 'list-even';
                    }

                    // Build out the html for this article
                    html += '<div class="col s12 m12 l12 ' + className + '">\n';

                        html += '<div class="col s12 m4 l4">\n';
                            html += '<div class="agenda-date">' + currentEvents[i].agendaDate + '</div>\n';
                            html += '<div class="agenda-time">' + currentEvents[i].agendaTime + '</div>\n';
                        html += '</div>\n';

                        html += '<div class="col s12 m4 l4">\n';
                        html += '<hr class="hide-on-med-and-up" />\n';
                            html += '<div class="agenda-event-title">' + currentEvents[i].title + '</div>\n';
                            html += '<div class="agenda-location">' + currentEvents[i].eventLocation + '</div>\n';
                            html += '<div class="agenda-owner"><em>' + currentEvents[i].owner + '</em></div>\n';
                        html += '<hr class="hide-on-med-and-up" />\n';
                        html += '</div>\n';

                        html += '<div class="col s12 m4 l4">\n';
                            html += '<div><p class="agenda-desc">' + currentEvents[i].eventDesc + '</p></div>\n';
                        html += '</div>\n';

                    html += '</div>\n';
                }

                return html;
            }
            // Returns an array of all the events for the current month
            function getCurrentEvents() {
                dynamicEventList = []
                for(var i = 0; i < allEvents.length; i++)
                {
                    if((allEvents[i].startString.indexOf(currentMonth) > -1 && allEvents[i].startString.indexOf(currentYear) > -1) || (allEvents[i].endString.indexOf(currentMonth) > -1 && allEvents[i].endString.indexOf(currentYear) > -1)){
                        var eventObj = new Object();
                        eventObj.title = allEvents[i].title;
                        eventObj.agendaDate = getAgendaDate(allEvents[i].startString, allEvents[i].endString);
                        eventObj.agendaTime = getAgendaTime(allEvents[i].startString, allEvents[i].endString, allEvents[i].allDay);
                        eventObj.url = allEvents[i].url;
                        if(allEvents[i].location != null)
                        {
                            eventObj.eventLocation = allEvents[i].location;
                        }
                        else
                        {
                            eventObj.eventLocation = '';
                        }
                        eventObj.owner = allEvents[i].createdBy;
                        if(allEvents[i].description != null)
                        {
                            eventObj.eventDesc = allEvents[i].description.replace(/\\r\\n/g, '\n');
                            eventObj.eventDesc = eventObj.eventDesc.replace(/\\/g, '');
                        }
                        else
                        {
                            eventObj.eventDesc = '';
                        }
                        dynamicEventList.push(eventObj);
                    }
                }
                return dynamicEventList;
            }

            // A function to get agenda view of the date
            function getAgendaDate(startTime, endTime) {
                var startDate = '';
                var endDate = '';
                if(startTime.substring(6, 7) === ' ') {
                    startDate = startTime.substring(5, 10);
                } else {
                    startDate = startTime.substring(5, 11);
                }

                if(endTime.substring(6, 7) === ' ') {
                    endDate = endTime.substring(5, 10);
                } else {
                    endDate = endTime.substring(5, 11);
                }

                if(endDate === startDate) {
                    return startDate;
                } else {
                    return startDate + ' - ' + endDate;
                }
                return;
            }
            // A function to return the agenda view of the time
            function getAgendaTime(startTimeString, endTimeString, allDay) {
                if(allDay === true) {
                    return 'All Day';
                } else {
                    var startTime = '';
                    var endTime = '';
                    if(startTimeString.substring(6, 7) === ' ') {
                        startTime = startTimeString.substring(16, 21);
                    } else {
                        startTime = startTimeString.substring(17, 22);
                    }
                    // Get the start time and add AM or PM onto it
                    if(startTime.substring(0, 2) < 12) {
                        // If we are at 00 convert to 12, for 12 AM
                        if(startTime.substring(0, 2) == '00') {
                            startTime = '12' + startTime.substring(2, 5);
                        }
                        startTime = startTime + 'AM';
                    } else if(startTime.substring(0, 2) > 12) {
                        startTime = startTime.substring(0, 2) - 12 + startTime.substring(2, 5) + 'PM';
                    } else {
                        startTime = startTime + 'PM';
                    }

                    if(endTimeString.substring(6, 7) === ' ') {
                        endTime = endTimeString.substring(16, 21);
                    } else {
                        endTime = endTimeString.substring(17, 22);
                    }
                    // Get the end time and add AM or PM onto it
                    if(endTime.substring(0, 2) < 12) {
                        // If we are at 00 convert to 12, for 12 AM
                        if(endTime.substring(0, 2) == '00') {
                            endTime = '12' + endTime.substring(2, 5);
                        }
                        endTime = endTime + 'AM';
                    } else if(endTime.substring(0, 2) > 12) {
                        endTime = endTime.substring(0, 2) - 12 + endTime.substring(2, 5) + 'PM';
                    } else {
                        endTime = endTime + 'PM';
                    }

                    // Here we remove the :00 from times if they exist and the 0s for single hour
                    startTime = startTime.replace(':00', '');  
                    endTime = endTime.replace(':00', '');
                    if(startTime.charAt(0) === '0') {
                        startTime = startTime.substring(1);
                    }
                    if(endTime.charAt(0)  === '0') {
                        endTime = endTime.substring(1);
                    }
                    return startTime + ' - ' + endTime;
                }
                return;
            }

            // Returns the current month as a string
            function getCurrentMonth(){
              var calDate = jqnew("#calendar").fullCalendar('getDate');
              var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

              // Here we return the date as a string
              return monthNames[calDate.month()];
            }

            // Returns the current year as a string
            function getCurrentYear(){
              var calDate = jqnew("#calendar").fullCalendar('getDate');

              // Here we return the date as a string
              return calDate.year();
            }
        }
    </script>
</apex:define>
</html>
</apex:composition>
</apex:page>