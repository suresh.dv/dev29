<apex:page showHeader="false" renderAs="pdf" applyHtmlTag="false" standardController="Change_Request__c" extensions="PrintableChangeRequestFormCtrl">
<head>
<style>
    @page  {
      size: A4; 
      margin-left:15mm;
      margin-right:15mm;
      //margin-top:190px;

        @top-center {
            content: element(headerContainer);
        }
    }
    body {
        font-family: Arial;    
        font-size:12px;        
    }
    div.headerContainer {
        padding: 10px;
        //position: running(headerContainer);
    }
    .page-break {
            display:block;
            //page-break-after:always;
        }
    .title {
        color:#3d629b;
        font-weight:bold;
        font-size:22px;
        text-align:center;
    } 
    .subHeader
    {
        font-size:13px;
        font-weight:bold;
    }
    .subHeaderUnderline{
        text-decoration:underline;
        font-size:13px;
        font-weight:bold;
    }
   .header 
   {
        color:#3d629b;
        font-weight:bold;
        
        font-size:18px;
   }
  
    table
    {
        margin-top:10px;
        width:100%;
        border:1px solid black;
    }
   tr td
   {
    border:0.5px solid black;
   }
    .noBorder
   {
      
        border: none;
   }
    </style>
</head>
<apex:form > 
<body>
    <div>
        <apex:image value="{!$Resource.HMGPLogo}" />
        <center><div class="header">PROJECT CHANGE NOTICE</div></center>
    </div>
    <div class="page-break">
        <table cellspacing="0" cellpadding="5" >
            <tr>
                <td class="subHeader" width="25%">Project Name:</td>
                <td colspan="3" width="75%">{!Change_Request__c.Project_Area__r.Name}</td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">Contractor:</td>
                <td width="25%">{!Change_Request__c.Contractor__c}</td>
                <td class="subHeader" width="25%">Initiator:</td>
                <td width="25%">{!Change_Request__c.Initiator__r.Name}</td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">Project AFE:</td>
                <td width="25%">{!Change_Request__c.Project_AFE__c}</td>
                <td class="subHeader" width="25%">Date:</td>
                <td width="25%">
                    <apex:outputText value="{0,date,dd' 'MMMMM' 'yyyy}">
                        <apex:param value="{!Change_Request__c.CreatedDate}" /> 
                    </apex:outputText>
                </td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">PCN #:</td>
                <td width="25%">{!Change_Request__c.PCN_Number__c}</td>
                <td class="subHeader" width="25%">PO #:</td>
                <td width="25%">{!Change_Request__c.Document_Number__c}</td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">EWR #:</td>
                <td width="25%">{!Change_Request__c.EWR_No__c}</td>
                <td width="25%"></td>
                <td width="25%"></td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">PCN Title:</td>
                <td colspan="3" width="75%">{!Change_Request__c.Title__c}</td>
            </tr>
            <tr>
                <td colspan="4" width="100%">
                    <span class="subHeader">PCN Description:</span>
                    <div style="text-align:justify; padding-top:10px;">{!Change_Request__c.Description__c}</div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <span class="subHeaderUnderline"><strong>Justification for Change:</strong></span>
                    <div style="white-space: nowrap; padding-top:10px; line-height:20px">
                        <apex:repeat value="{!JustificationOptions}" var="j">
                            <apex:image url="{!IF(CONTAINS(Change_Request__c.Justification__c, j), $Resource.TrueCheckbox, $Resource.FalseCheckbox)}"/>
                            {!j} &nbsp;
                        </apex:repeat>
                    </div>
                </td>
            </tr>
            <tr>
                <td width="25%">
                    <center><apex:image url="{!IF(Change_Request__c.Change_Type__c == 'Budget Change', $Resource.TrueCheckbox, $Resource.FalseCheckbox)}"/></center>
                </td>
                <td class="subHeader" width="25%">Budget Change</td>
                <td width="25%">
                    <center><apex:image url="{!IF(Change_Request__c.Change_Type__c == 'Forecast Change', $Resource.TrueCheckbox, $Resource.FalseCheckbox)}"/></center>
                </td>
                <td class="subHeader" width="25%">Forecast Change</td>
            </tr>
            <tr>
                <td colspan="4" class="subHeader"><center>CHANGE IMPACT</center></td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">Cost Impact:</td>
                <td colspan="3" width="75%"> &nbsp;&nbsp;&nbsp; $&nbsp;&nbsp;{!impactAss.Cost_Impact__c} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {!impactAss.Change_Impact_Hours__c}&nbsp;&nbsp;Hours</td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">Schedule Impact:</td>
                <td colspan="3" width="75%">{!impactAss.Scheduled_Impact_Comments__c}</td>
            </tr>
            <tr>
                <td colspan="4" class="subHeader"><center>PROJECT TIC IMPACT</center></td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">Cost Impact:</td>
                <td colspan="3" width="75%">{!impactAss.TIC_Cost__c}</td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">Schedule Impact:</td>
                <td colspan="3" width="75%">{!impactAss.TIC_Schedule_Impact__c}</td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">Requested by:</td>
                <td width="25%">
                    <apex:image url="{!IF(impactAss.Requested_By__c == 'Husky', $Resource.TrueCheckbox, $Resource.FalseCheckbox)}"/>Husky
                </td>
                <td colspan="2" width="50%">
                    <apex:image url="{!IF(impactAss.Requested_By__c == 'Contractor', $Resource.TrueCheckbox, $Resource.FalseCheckbox)}"/>Contractor
                </td>
            </tr>
            <tr>
                <td colspan="2" class="subHeader" width="50%">
                    <apex:image url="{!IF(Change_Request__c.Change_Status__c == 'Approved', $Resource.TrueCheckbox, $Resource.FalseCheckbox)}"/>Approved
                </td>
                <td colspan="2" class="subHeader" width="50%">
                    <apex:image url="{!IF(Change_Request__c.Change_Status__c == 'Rejected', $Resource.TrueCheckbox, $Resource.FalseCheckbox)}"/>Rejected
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <span class="subHeader">Comments:</span>
                    <div style="text-align:justify; padding-top:10px;">{!Change_Request__c.Comments__c}</div>
                </td>
            </tr>
            <tr>
                <td class="subHeader" width="25%"><center>Title</center></td>
                <td class="subHeader" width="50%" colspan="2"><center>Name</center></td>
                <td class="subHeader" width="25%"><center>Date</center></td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">Project Control Analyst:</td>
                <td width="50%" colspan="2">{!impactAss.Assessor__r.Name}</td>
                <td width="25%"></td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">Project Controls Lead:</td>
                <td width="50%" colspan="2">{!Change_Request__c.Project_Controls_Lead__r.Name}</td>
                <td width="25%"></td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">Contract Administrator:</td>
                <td width="50%" colspan="2">{!Change_Request__c.Contract_Admin__r.Name}</td>
                <td width="25%"></td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">Project Engineer:</td>
                <td width="50%" colspan="2">{!Change_Request__c.SPA__r.Name}</td>
                <td width="25%">
                    <apex:outputText value="{0,date,dd' 'MMMMM' 'yyyy}">
                        <apex:param value="{!Change_Request__c.Project_Engineer_Approval_Date__c}" /> 
                    </apex:outputText>
                </td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">Project Manager:</td>
                <td width="50%" colspan="2">{!Change_Request__c.Project_Manager__r.Name}</td>
                <td width="25%">
                    <apex:outputText value="{0,date,dd' 'MMMMM' 'yyyy}">
                        <apex:param value="{!Change_Request__c.Project_Manager_Approval_Date__c}" /> 
                    </apex:outputText>
                </td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">Project Engineering Manager:</td>
                <td width="50%" colspan="2">{!Change_Request__c.Project_Area__r.Project_Engineering_Manager__r.Name}</td>
                <td width="25%"></td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">Director:</td>
                <td width="50%" colspan="2">{!Change_Request__c.Director__r.Name}</td>
                <td width="25%"></td>
            </tr>
            <tr>
                <td class="subHeader" width="25%">VP, Midstream:</td>
                <td width="50%" colspan="2">{!Change_Request__c.Senior_VP__r.Name}</td>
                <td width="25%"></td>
            </tr>
        </table>
    </div>
</body>
</apex:form>
</apex:page>