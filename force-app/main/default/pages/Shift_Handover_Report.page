<apex:page controller="Shift_Handover_Report_Ctrl" docType="html-5.0" sidebar="false" tabStyle="Report" id="page">
<apex:includeScript value="{!$Resource.DateUtil}"/>
<apex:includeScript value="{!URLFOR($Resource.JQueryZip,'jquery-1.9.1.min.js')}"/>
<apex:includeScript value="{!URLFOR($Resource.JQueryZip,'jquery-ui-1.10.0/ui/minified/jquery-ui.min.js')}"/>
<apex:stylesheet value="{!URLFOR($Resource.JQueryZip,'jquery-ui-1.10.0/themes/base/jquery-ui.css')}"/>      
<style>
   .label
   {
        font-weight:bold;
        text-align:right;
   }
   .groupedRow{
        line-height: 20px;
        background-color: #d7ecf2;
        font-weight:bold;
    }
    .dataRow {
        line-height: 20px;
        
    }
   .sectionCaption {
        font-weight: bold;
        font-size: 12pt;
       
    }
            
    .headerRow {
       line-height: 20px;
       background-color: #41aacb;
    }        
    
    .headerCell {
        color: #878787;
        font-weight: bold;
        font-size: small;
        padding-left: 10px
    }
   
    .dataCell {
        font-weight: normal;
        font-size: small;
        color: #878787;
       
        padding-left: 10px;
        padding-right: 5px;
        vertical-align:top;
        word-wrap: break-word;
        line-height: 20px;
        text-align:left;
    }
    
    .trblBorder {
        border-top: 0.03cm solid #d7d7d7;
        border-right: 0.03cm solid #d7d7d7;
        border-bottom: 0.03cm solid #d7d7d7;
        border-left: 0.03cm solid #d7d7d7;
    }
   .rblBorder {
        border-right: 0.03cm solid #d7d7d7;
        border-bottom: 0.03cm solid #d7d7d7;
        border-left: 0.03cm solid #d7d7d7;
    }
    
    .footerRow {
        line-height: 30px;
        background-color:#818290;
        
    }
    .footerCell {
        border: 0.03cm solid #b7b7b7;
        color: white !important;
        
        padding-left: 20px;
        font-size: 12pt;
        font-weight: normal;
    }
   
</style>
<script>
    //prevent focus on the calendar input field since the calendar popup is displayed in the first load.
    function setFocusOnLoad() {}
 
    jQuery(function(){
        var $j = jQuery.noConflict();
        $j("input[id$=dpFrom]").datepicker({
            changeYear: true,
            changeMonth: true,
            dateFormat: "dd/mm/yy",
            
            onSelect: function(selectedDate, instance) {
                if (selectedDate != '')
                {
                    $j("input[id$=dpTo]").datepicker("option","minDate", selectedDate);
                    
                    var date = Date.parseExact(selectedDate, "dd/MM/yyyy");
                    date.setYear(date.getFullYear() + 1);
                    $j("input[id$=dpTo]").datepicker("option", "maxDate", date);
                  
                }
            }
          });
        
          
        $j("input[id$=dpTo]").datepicker({
            changeYear: true,
            changeMonth: true,
            dateFormat: "dd/mm/yy",
            onSelect: function(selected) {
               $j("input[id$=dpFrom]").datepicker("option","maxDate", selected)
           }  
            
          });
       
     });
     
    // ----------------------------------------------------------------------------
    // The product lookup popup
    var newWindow = null;
     
    // ----------------------------------------------------------------------------
    // Open the product lookup popup with appropriate parameters
    function openCustomLookup( url) 
    {
       
      // Open the popup
      newWindow = window.open(url, 'Popup', 
                              'height=700,width=600,left=100,top=100,resizable=no,scrollbars=yes,toolbar=no,status=no');
      if (window.focus) {
        newWindow.focus();
      }
      
      return false;
    }
    
     
    // ----------------------------------------------------------------------------
    // Close the popup
    function closeWindowLookup() 
    {
        
      if (newWindow != null) {
        newWindow.close();
      }
    }
    
    function clearFilter()
    {
        document.getElementById('page:form:pageBlock:criteriaSection:processArea:component:fieldName').value= '';
        document.getElementById('page:form:pageBlock:criteriaSection:team:component:fieldName').value= '';
        document.getElementById('page:form:pageBlock:criteriaSection:role:component:fieldName').value='';
        document.getElementById('page:form:pageBlock:criteriaSection:unit:component:fieldName').value='';
        document.getElementById('page:form:pageBlock:criteriaSection:resource:component:fieldName').value='';
    }
</script>
    <apex:sectionHeader title="Shift Handover Report" />
    
    <apex:form id="form">
    <apex:pageBlock id="pageBlock">
        
        <apex:pageBlockSection columns="1" id="criteriaSection">
            <apex:outputPanel >
             <fieldset  style="width:70%">
                <legend class="sectionCaption"> Search Criteria </legend>
                <table>
                    <tr>
                        <td><apex:outputText value="Team Allocation Date Range" styleClass="label"/></td>
                        <td colspan="3">
                            <apex:selectList size="1" onchange="rangeChanged(this)" id="dRange" value="{!dateRange}" style="width:210px" >
                                <apex:selectOption itemValue="Custom" itemLabel="Custom"/>
                                <apex:selectOption itemValue="Calendar Year" itemLabel="Calendar Year" itemDisabled="true"/>
                                <apex:selectOption itemValue="THIS_YEAR" itemLabel="Current Year"/>
                                <apex:selectOption itemValue="LAST_YEAR" itemLabel="Last Year"/>
                                <apex:selectOption itemValue="Calendar Quarter" itemLabel="Calendar Quarter" itemDisabled="true"/>
                                <apex:selectOption itemValue="THIS_QUARTER" itemLabel="Current Quarter"/>
                                <apex:selectOption itemValue="Q1" itemLabel="Quarter 1"/>
                                <apex:selectOption itemValue="Q2" itemLabel="Quarter 2"/>
                                <apex:selectOption itemValue="Q3" itemLabel="Quarter 3"/>
                                <apex:selectOption itemValue="Q4" itemLabel="Quarter 4"/>
                                <apex:selectOption itemValue="Calendar Month" itemLabel="Calendar Month" itemDisabled="true"/>
                                <apex:selectOption itemValue="LAST_MONTH" itemLabel="Last Month"/>
                                <apex:selectOption itemValue="THIS_MONTH" itemLabel="This Month"/>
                                
                                <apex:selectOption itemValue="Day" itemLabel="Day" itemDisabled="true"/>
                                <apex:selectOption itemValue="YESTERDAY" itemLabel="Yesterday"/>
                                <apex:selectOption itemValue="TODAY" itemLabel="Today"/>
                                <apex:selectOption itemValue="LAST_N_DAYS:7" itemLabel="Last 7 Days" />
                                <apex:selectOption itemValue="LAST_N_DAYS:30" itemLabel="Last 30 Days"/>
                                <apex:selectOption itemValue="LAST_N_DAYS:60" itemLabel="Last 60 Days"/>
                                <apex:selectOption itemValue="LAST_N_DAYS:90" itemLabel="Last 90 Days"/>
                                <apex:selectOption itemValue="LAST_N_DAYS:120" itemLabel="Last 120 Days"/>
                                
                            </apex:selectList>
                        </td>
                        <td class="label"><apex:outputLabel value="Process Area"/></td>
                        <td><c:CustomLookupComponent id="processArea" fieldName="{!selectedAreas}"  pageURL="/apex/ProcessAreaLookup" /></td>
                    
                 
                    </tr>
                    <tr>
                        <td class="label">From</td>
                        <td><apex:inputText value="{!dFrom}"  id="dpFrom" onchange="setCustomRange()" style="width:80px"/></td>
                        <td class="label">To</td>
                        <td><apex:inputText value="{!dTo}" id="dpTo" onchange="setCustomRange()" style="width:80px"/></td>
                        
                        <td class="label"><apex:outputText value="Unit" /></td>
                        <td><c:CustomLookupComponent id="unit" fieldName="{!selectedUnits}" pageURL="/apex/UnitLookup"/> </td>
                    </tr>
                    <tr>
                        <td class="label"><apex:outputText value="Priority" /></td>
                        <td colspan="3">
                            <apex:selectCheckboxes value="{!selectedPriority}" >
                                <apex:selectOptions value="{!priorityList}"/>
                            </apex:selectCheckboxes>
                           
                        </td> 
                        <td class="label">Shift Resource</td>
                        <td><c:CustomLookupComponent id="resource" fieldName="{!selectedOperators}"  pageURL="/apex/OperatorLookup"/></td>
                    </tr>
                    <tr>
                        <td class="label">Record Type</td>
                        <td>
                            <apex:selectList id="rtype" value="{!selectedDepartment}" size="1" onchange="clearFilter();">
                                <apex:selectOptions value="{!departmentList}" />
                               
                            </apex:selectList>
                        </td>
                        <td colspan="2"></td>
                        <td class="label"><apex:outputText value="Team" /></td>
                        <td><c:CustomLookupComponent id="team" fieldName="{!selectedTeams}"  pageURL="/apex/TeamLookup" /></td>
                    </tr>
                    <tr>
                        <td class="label"><apex:outputText value="Equipment Tag Description" /></td>
                        <td><apex:inputText value="{!equipmentTagDesc}" style="width:195px"/></td>
                        <td colspan="2"></td>
                        <td class="label"><apex:outputText value="Team Role" /></td>
                        <td><c:CustomLookupComponent id="role" fieldName="{!selectedRoles}"  pageURL="/apex/TeamRoleLookup" /></td>
                    </tr>
                </table>
               
               
             </fieldset>
             <div style="padding-top:20px">
                 <apex:commandButton value="Run Report" action="{!runReport}" rerender="resultTable" status="searchStatus"/>
                 <apex:commandLink value="Export Details" action="{!exportDetail}" target="_blank" id="btnExportDetail"
                            styleClass="btn" style="text-decoration:none; padding:5px;margin-bottom:10px" />
                 <span style="padding-left:400px">
                 <apex:actionStatus id="searchStatus"  startText="Loading please wait..............." startStyle="background-color:yellow; "/>
                 </span>
                 
             </div>
         </apex:outputPanel>
              
     </apex:pageBlockSection>
    <apex:outputPanel id="resultTable">    
        <apex:pageMessages />
        <apex:pageBlockSection title="Generated Report" columns="1">    
          <!--  <c:ShiftHandoverReportComponent resultMap="{!resultMap}" areaMap="{!areaMap}" resultNo="{!resultNo}" teamRoleMap="{!teamRoleMap}" mode="view"/> -->
              <apex:outputPanel >
                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr class="headerRow">
                        <td class="headerCell trblBorder">Process Area</td>
                        <td class="headerCell trblBorder">Team Allocation: Date</td>
                        <td class="headerCell trblBorder">Time</td>
                        <td class="headerCell trblBorder">Team Allocation: Shift</td>
                        <td class="headerCell trblBorder">Priority</td>
                        <td class="headerCell trblBorder" >Description</td>
                        <td class="headerCell trblBorder">Team</td>
                        <td class="headerCell trblBorder">Role</td>
                        <td class="headerCell trblBorder">Shift Resource</td>
                        <td class="headerCell trblBorder">Unit</td>
                        <td class="headerCell trblBorder">Equipment Tag Description</td>
                        <td class="headerCell trblBorder">Work Order #</td>
                        <td class="headerCell trblBorder">Record Type</td>
                        
                    </tr>
                    <apex:repeat value="{!areaMap}" var="area">
                       
                        <tr class="groupedRow">
                            <td class="subDetailColumnDataCell rblBorder" colspan="13">
                                <apex:image value="/img/icon/custom51_100/redcross16.png" /> Area: {!area} ({!areaMap[area]} records)
                            </td>
                        </tr>
                       
                          <apex:repeat value="{!resultMap[area]}" var="o" >
                              <tr class="dataRow">
                                  <td class="dataCell rblBorder">
                                    <a href="/{!o.Area__c}">{!o.Area__r.Name}</a>
                                  </td>
                                  <td class="dataCell rblBorder">
                                      <apex:outputText value="{0,date,dd/MM/yyyy}">
                                         <apex:param value="{!o.Team_Allocation__r.Date__c}" />
                                     </apex:outputText>
                                  </td>
                                  <td class="dataCell rblBorder">{!o.Time__c}</td>
                                  <td class="dataCell rblBorder">
                                      <a href="/{!o.Team_Allocation__r.Shift__c}">{!o.Team_Allocation__r.Shift__r.Name}</a>
                                  </td>
                                  <td class="dataCell rblBorder">{!o.Priority__c}</td>
                                  <td class="dataCell rblBorder"><apex:outputText value="{!o.Description__c}" escape="false"/></td>
                                  
                                  <td class="dataCell rblBorder">{!o.Shift_Resource_Team__c}</td>
                                  <td class="dataCell rblBorder">{!o.Shift_Resource_Role__c}</td>
                                  <td class="dataCell rblBorder">
                                      <a href="/{!o.Operator_Contact__c}">{!o.Operator_Contact__r.Name}</a>
                                  </td>
                                  <td class="dataCell rblBorder">
                                      <a href="/{!o.Unit__c}">{!o.Unit__r.Name}</a>
                                  </td>
                                  <td class="dataCell rblBorder"><apex:outputText value="{!o.Equipment_Tag_Description__c}" /></td>
                                  <td class="dataCell rblBorder">{!o.Work_Order_Number__c}</td>
                                  <td class="dataCell rblBorder">{!o.RecordType.Name}</td>
                                  
                              </tr>
                          </apex:repeat>
                          
                    </apex:repeat>
                   
                    <tr class="footerRow">
                         <td colspan="13" class="footerCell">Grand Totals ({!resultNo} records)</td>
                    </tr>
           
                </table>
            </apex:outputPanel>
        </apex:pageBlockSection>  
    </apex:outputPanel> 
    </apex:pageBlock>
  
        
    </apex:form> 
<script>
    function setCustomRange()
    {
        document.getElementById('page:form:pageBlock:criteriaSection:dRange').value= "Custom";
    }
    function rangeChanged(dRange)
    {
    
        var range = dRange.value;
        var dFromField = document.getElementById('page:form:pageBlock:criteriaSection:dpFrom');
        var dToField = document.getElementById('page:form:pageBlock:criteriaSection:dpTo');
        var today = Date.today();
        if (range.indexOf('LAST_N_DAYS') > -1)
        {
            dToField.value = formatDate(today);
            var n = range.substring(range.indexOf(':')+1, range.length);
            dFromField.value = lastNDays(n-1);
        }
        else if (range == 'YESTERDAY')
        {
            dToField.value = dFromField.value = formatDate(today.addDays(-1));
        }
        else if (range == 'TODAY')
        {
            dToField.value = dFromField.value = formatDate(today);
        }
        else if (range == 'THIS_YEAR')
        {
            dToField.value = formatDate(new Date(today.getFullYear(), 11, 31));
            dFromField.value = formatDate(new Date(today.getFullYear(), 0, 1));
        }
        else if (range == 'LAST_YEAR')
        {
            dToField.value = formatDate(new Date(today.getFullYear()-1, 11, 31));
            dFromField.value = formatDate(new Date(today.getFullYear()-1, 0, 1));
        }
        else if (range == 'THIS_QUARTER')
        {
            
            if(today.getMonth() <= 2)
            {
                dToField.value = formatDate(new Date(today.getFullYear(), 2, 31));
                dFromField.value = formatDate(new Date(today.getFullYear(), 0, 1));
            }
            else if(today.getMonth() > 2 && today.getMonth() <=5)
            {
                dToField.value = formatDate(new Date(today.getFullYear(), 5, 30));
                dFromField.value = formatDate(new Date(today.getFullYear(), 3, 1));
            }
            else if(today.getMonth() >5 && today.getMonth() <= 8)
            {
                dToField.value = formatDate(new Date(today.getFullYear(), 8, 30));
                dFromField.value = formatDate(new Date(today.getFullYear(), 6, 1));
            }
            else
            {
                dToField.value = formatDate(new Date(today.getFullYear(), 11, 31));
                dFromField.value = formatDate(new Date(today.getFullYear(), 9, 1));
            }
        }
       else if (range == 'Q1')
        {
            dToField.value = formatDate(new Date(today.getFullYear(), 2, 31));
            dFromField.value = formatDate(new Date(today.getFullYear(), 0, 1));
        }
        else if (range == 'Q2')
        {
            dToField.value = formatDate(new Date(today.getFullYear(), 5, 30));
            dFromField.value = formatDate(new Date(today.getFullYear(), 3, 1));
        }
        else if (range == 'Q3')
        {
            dToField.value = formatDate(new Date(today.getFullYear(), 8, 30));
            dFromField.value = formatDate(new Date(today.getFullYear(), 6, 1));
        }
        else if (range == 'Q4')
        {
            dToField.value = formatDate(new Date(today.getFullYear(), 11, 31));
            dFromField.value = formatDate(new Date(today.getFullYear(), 9, 1));
        }
        else if (range == 'THIS_MONTH')
        {
            dToField.value = formatDate(new Date(today.getFullYear(), today.getMonth(), Date.getDaysInMonth(today.getFullYear(), today.getMonth())));
            dFromField.value = formatDate(new Date(today.getFullYear(), today.getMonth(), 1));
        }
        else if(range == 'LAST_MONTH')
        {
            var lastMonth = today.getMonth();
            if (today.getMonth() == 0)
            {
                dToField.value = formatDate(new Date(today.getFullYear()-1, 11, 31));
                dFromField.value = formatDate(new Date(today.getFullYear()-1, 11, 1));
            }   
            else
            {
                dToField.value = formatDate(new Date(today.getFullYear(), today.getMonth()-1, Date.getDaysInMonth(today.getFullYear(), today.getMonth()-1)));
                dFromField.value = formatDate(new Date(today.getFullYear(), today.getMonth()-1, 1));
            }         
            
        } 
    }
    function lastNDays(n)
    {
        return formatDate((n).days().ago());
    }
    function formatDate(date)
    {
        return date.toString('dd/MM/yyyy');
    }
</script>
</apex:page>