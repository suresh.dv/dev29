<apex:page standardController="Opportunity" extensions="OpportunityProductMultilineItem" action="{!priceBookCheck}" lightningStylesheets="true">

    <apex:sectionHeader Title="Manage {!$ObjectType.Product2.LabelPlural}" subtitle="{!opportunity.Name}"/>
    <apex:messages style="color:red"/>

    <style>
        .search{
            font-size:14pt;
            margin-right: 20px;    
        }
        .fyi{
            color:red;
            font-style:italic;
        }
        .label{
            margin-right:10px;
            font-weight:bold;
        }
        .table-opp-rel-prod td {
        	min-width: 100px; 
        }        
        .table-opp-rel-prod select {
        	width: 100%;
        }  
        .table-opp-rel-prod td.min-w-200 {
        	min-width: 200px;
        }
        .table-opp-rel-prod td .requiredInput {
        	margin-left: 10px;
        }
        
    </style>
    
    <script type='text/javascript'>
    	
        // This script assists the search bar functionality
        // It will execute a search only after the user has stopped typing for more than 1 second
        // To raise the time between when the user stops typing and the search, edit the following variable:
        
        var waitTime = 1;        
    
        var countDown = waitTime + 1;
        var started = false;
        
        function resetTimer(){
        
            countDown = waitTime + 1;
            
            if(started == false) {
                started = true;
                runCountDown();
            }
        }
        
        function runCountDown(){
        
            countDown--;
            
            if(countDown <= 0){
                fetchResults();
                started = false;
            }
            else{
                window.setTimeout(runCountDown,1000);
            }
        }    
    </script>
  	
    <apex:form >
        <apex:outputPanel id="mainBody" styleClass="table-opp-rel-prod">        
            <!-- this is the upper table-->
            <apex:pageBlock title="Selected {!$ObjectType.Product2.LabelPlural}" id="selected">

                <!-- Default User -->
                <apex:pageblockTable value="{!shoppingCart}" var="s">
                
                    <apex:column >
                        <apex:commandLink value="Remove" action="{!removeFromShoppingCart}" reRender="selected,searchResults" immediate="true">
                            <!-- this param is how we send an argument to the controller, so it knows which row we clicked 'remove' on -->
                            <apex:param value="{!s.PriceBookEntryId}" assignTo="{!toUnselect}" name="toUnselect"/>
                        </apex:commandLink>
                    </apex:column>
                    
                    <apex:column headerValue="{!$ObjectType.Product2.LabelPlural}" value="{!s.PriceBookEntry.Product2.Name}"/>
                    
                    <apex:column headerValue="{!$ObjectType.OpportunityLineItem.Fields.Quantity.Label}">
                        <apex:inputField value="{!s.Quantity}" style="width:70px" required="true" onkeyup="refreshTotals();"/>
                    </apex:column>
                    
                     <apex:column headerValue="{!$ObjectType.OpportunityLineItem.Fields.Unit__c.Label}">
                        <apex:inputField value="{!s.Unit__c}" style="width:82px" required="true" onkeyup="refreshTotals();"/>
                    </apex:column>
                    
                    <apex:column headerValue="{!$ObjectType.OpportunityLineItem.Fields.Additive__c.Label}">
                        <apex:inputField value="{!s.Additive__c}" style="width:83px" onkeyup="refreshTotals();"/>
                    </apex:column>
                    
                    <apex:column headerValue="{!$ObjectType.OpportunityLineItem.Fields.Anti_Strip__c.Label}">
                        <apex:inputField value="{!s.Anti_Strip__c}" style="width:60px" onkeyup="refreshTotals();"/>
                    </apex:column>
                    
                    <apex:column headerValue="{!$ObjectType.OpportunityLineItem.Fields.Price_Type__c.Label}">
                        <apex:inputField value="{!s.Price_Type__c}" style="width:60px" onkeyup="refreshTotals();"/>
                    </apex:column>
                    
                    <apex:column headerValue="{!$ObjectType.OpportunityLineItem.Fields.Sales_Price__c.Label}">
                        <apex:inputField value="{!s.Sales_Price__c}" style="width:70px" onkeyup="refreshTotals();"/>
                    </apex:column>
                    
                    <apex:column headerValue="{!$ObjectType.OpportunityLineItem.Fields.Axle_5_Price__c.Label}">
                        <apex:inputField value="{!s.Axle_5_Price__c}" style="width:70px" onkeyup="refreshTotals();"/>
                    </apex:column>
                    
                    <apex:column headerValue="{!$ObjectType.OpportunityLineItem.Fields.Axle_6_Price__c.Label}">
                        <apex:inputField value="{!s.Axle_6_Price__c}" style="width:70px" onkeyup="refreshTotals();"/>
                    </apex:column>
                    
                    <apex:column headerValue="{!$ObjectType.OpportunityLineItem.Fields.Axle_7_Price__c.Label}" >
                        <apex:inputField value="{!s.Axle_7_Price__c}" style="width:70px" onkeyup="refreshTotals();"/>
                    </apex:column>
                    
                    <apex:column headerValue="{!$ObjectType.OpportunityLineItem.Fields.Axle_8_Price__c.Label}">
                        <apex:inputField value="{!s.Axle_8_Price__c}" style="width:70px" onkeyup="refreshTotals();"/>
                    </apex:column>
                    
                    <apex:column headerValue="{!$ObjectType.OpportunityLineItem.Fields.Currency__c.Label}">
                        <apex:inputField value="{!s.Currency__c}" style="width:70px" required="true" onkeyup="refreshTotals();"/>
                    </apex:column>
                    
                </apex:pageblockTable>
                
                <apex:pageBlockButtons >
                    <apex:commandButton action="{!onSave}" value="Save"/>
                    <apex:commandButton action="{!onCancel}" value="Cancel" immediate="true"/>
                </apex:pageBlockButtons>
            </apex:pageBlock>
    
            <!-- this is the lower table: search bar and search results -->
            <apex:pageBlock >
                <apex:outputPanel styleClass="search">
                    Search for {!$ObjectType.Product2.LabelPlural}:
                </apex:outputPanel>

                <apex:actionRegion renderRegionOnly="false" immediate="true">                
                    <apex:actionFunction name="fetchResults" action="{!updateAvailableList}" reRender="searchResults" status="searchStatus"/>
                    <apex:inputText value="{!searchString}" onkeydown="if(event.keyCode==13){this.blur();}else{resetTimer();}" style="width:300px"/>
                    &nbsp;&nbsp;
                    <i>
                        <apex:actionStatus id="searchStatus" startText="searching..." stopText=" "/>
                    </i>
                </apex:actionRegion>
            
                <br/>
                <br/>
            
                <apex:outputPanel id="searchResults">
                    <apex:pageBlockTable value="{!AvailableProducts}" var="a">
                        <apex:column headerValue="{!$ObjectType.Product2.Fields.Name.Label}" value="{!a.Product2.Name}" />
                        
                        <apex:column headerValue="{!$ObjectType.Product2.Fields.Family.Label}" value="{!a.Product2.Family}"/>
                        
                        <apex:column headerValue="{!$ObjectType.Product2.Fields.Description.Label}" value="{!a.Product2.Description}"/>
                        
                        <apex:column >
                            <apex:commandButton value="Select" action="{!addToShoppingCart}" reRender="selected,searchResults" immediate="true">
                                <apex:param value="{!a.Id}" assignTo="{!toSelect}" name="toSelect"/>
                            </apex:commandButton>
                        </apex:column>
                    </apex:pageBlockTable>
                    
                    <apex:outputPanel styleClass="fyi" rendered="{!overLimit}">
                        <br/>
                        Your search returned over 100 results, use a more specific search string if you do not see the desired {!$ObjectType.Product2.Label}.
                        <br/>
                    </apex:outputPanel>
                </apex:outputPanel>
            </apex:pageBlock>
        </apex:outputPanel>
    </apex:form>
</apex:page>