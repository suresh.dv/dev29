<apex:page standardController="Work_Order_Activity__c" 
    extensions="WOEquipmentHierarchyCtrl" 
    tabStyle="Work_Order_Activity__c">

<!--01-Mar-17 Miro Zelina - included Well Events-->
<!--04-Jul-17 Miro Zelina - included System & Sub-System -->
<!--27-Jul-17 Miro Zelina - modification for hide related list and buttons if well event is on work order activity-->
	<apex:variable var="type" value="{!Type}"/>

	<apex:outputPanel rendered="{! IF(type = '',false,true) }">

	<apex:form >
		<apex:pageBlock >

            <apex:pageBlockButtons location="top" rendered="{!type != 'WellEvent'}">
                <apex:commandLink action="{!CASE(type,
                			                 'Location',URLFOR($Action.Equipment__c.Add_Equipment_Form, Work_Order_Activity__c.Location__c),
                			                 'Facility',URLFOR($Action.Equipment__c.Add_Equipment_Form_Facility, Work_Order_Activity__c.Facility__c),
                			                 'WellEvent',URLFOR($Action.Equipment__c.Add_Equipment_Form_Well_Event, Work_Order_Activity__c.Well_Event__c),
                			                 'System',URLFOR($Action.Equipment__c.Add_Equipment_Form_System, Work_Order_Activity__c.System__c),
                			                 'SubSystem',URLFOR($Action.Equipment__c.Add_Equipment_Form_SubSystem, Work_Order_Activity__c.Sub_System__c),
                                             'Functional Equipment Level',URLFOR($Action.Equipment__c.Add_Equipment_Form_FEL, Work_Order_Activity__c.Functional_Equipment_Level__c),
                                             'Yard',URLFOR($Action.Equipment__c.Add_Equipment_Form_Yard, Work_Order_Activity__c.Yard__c),
                			                  NULL)}"
                	value="Add Equipment Form" target="_parent" styleClass="btn" 
                	style="text-decoration:none; padding:4x;"/>

                <apex:commandLink action="{!CASE(type,
                			                 'Location',URLFOR($Action.Equipment__c.Transfer_Equipment_to_this_Location,Work_Order_Activity__c.Location__c),
                			                 'Facility',URLFOR($Action.Equipment__c.Transfer_Equipment_TO_this_Facility, Work_Order_Activity__c.Facility__c),
                			                 'WellEvent',URLFOR($Action.Equipment__c.Transfer_Equipment_TO_this_Well_Event, Work_Order_Activity__c.Well_Event__c),
                			                 'System',URLFOR($Action.Equipment__c.Transfer_Equipment_TO_this_System, Work_Order_Activity__c.System__c),
                			                 'SubSystem',URLFOR($Action.Equipment__c.Transfer_Equipment_TO_this_Sub_System, Work_Order_Activity__c.Sub_System__c),
                                             'Functional Equipment Level',URLFOR($Action.Equipment__c.Transfer_Equipment_TO_this_FEL, Work_Order_Activity__c.Functional_Equipment_Level__c),
                                             'Yard',URLFOR($Action.Equipment__c.Transfer_Equipment_TO_this_Yard, Work_Order_Activity__c.Yard__c),
                			                  NULL)}"
                	value="{!CASE(type,
                		        'Location','Transfer Equipment TO this Location',
                		        'Facility','Transfer Equipment TO this Facility',
                		        'WellEvent','Transfer Equipment TO this Well Event',
                		        'System','Transfer Equipment TO this System',
                		        'SubSystem','Transfer Equipment TO this Sub-System',
                                'Functional Equipment Level','Transfer Equipment TO this FEL',
                                'Yard','Transfer Equipment TO this Yard',
                		        NULL
                		   )}"
                	target="_parent" styleClass="btn" 
                	style="text-decoration:none; padding:4x;"/>
                	
                <apex:commandLink action="{!CASE(type,
                			                 'Location',URLFOR($Action.Equipment__c.Transfer_Equipment_from_this_Location,Work_Order_Activity__c.Location__c),
                			                 'Facility',URLFOR($Action.Equipment__c.Transfer_Equipment_FROM_this_Facility, Work_Order_Activity__c.Facility__c),
                			                 'WellEvent',URLFOR($Action.Equipment__c.Transfer_Equipment_FROM_this_Well_Event, Work_Order_Activity__c.Well_Event__c),
                			                 'System',URLFOR($Action.Equipment__c.Transfer_Equipment_FROM_this_System, Work_Order_Activity__c.System__c),
                			                 'SubSystem',URLFOR($Action.Equipment__c.Transfer_Equipment_FROM_this_Sub_System, Work_Order_Activity__c.Sub_System__c),
                                             'Functional Equipment Level',URLFOR($Action.Equipment__c.Transfer_Equipment_FROM_this_FEL, Work_Order_Activity__c.Functional_Equipment_Level__c),
                                             'Yard',URLFOR($Action.Equipment__c.Transfer_Equipment_FROM_this_Yard, Work_Order_Activity__c.Yard__c),
                			                  NULL)}"
                	value="{!CASE(type,
                		        'Location','Transfer Equipment FROM this Location',
                		        'Facility','Transfer Equipment FROM this Facility',
                		        'WellEvent','Transfer Equipment FROM this Well Event',
                		        'System','Transfer Equipment FROM this System',
                		        'SubSystem','Transfer Equipment FROM this Sub-System',
                                'Functional Equipment Level','Transfer Equipment FROM this FEL',
                                'Yard','Transfer Equipment FROM this Yard',
                		        NULL
                		   )}"
                	target="_parent" 
                	styleClass="btn" style="text-decoration:none; padding:4x;"/>

            </apex:pageBlockButtons>

            <apex:outputPanel id="emptyEquipmentMessage" rendered="{!EquipmentHierarchy == null || EquipmentHierarchy.Size == 0}">
                <apex:outputText value="This location has no equipment to display"/>
            </apex:outputPanel>
            <apex:outputPanel id="equipmentHierarchy" rendered="{!EquipmentHierarchy != null && EquipmentHierarchy.Size > 0}">

                <apex:pageBlockTable value="{!EquipmentHierarchyVisible}" var="node">
                    <apex:column style="width:15px;">
                        <apex:image value="{!IF(node.Closed, '/img/alohaSkin/setup/setup_plus_lev1.gif',  '/img/alohaSkin/setup/setup_minus_lev1.gif')}"  
                                    rendered="{!node.HasChildren}"
                                    width="11" height="11" 
                                    styleClass="setupImage"
                                    style="float:left;">
                            <apex:actionSupport event="onclick" 
                                                action="{!toggleNodeState}" 
                                                immediate="true" 
                                                reRender="equipmentHierarchy">
                                <apex:param name="nodeId" value="{!node.equipment.Id}"/>
                            </apex:actionSupport>
                        </apex:image>                    
                    </apex:column>


                    <apex:column style="padding-left: {!node.level * 20}px;" headerValue="Equipment">
                        <apex:outputLink value="/{!node.Equipment.Id}" target="_blank">{!node.Equipment.Name}</apex:outputLink>
                    </apex:column>

                    <apex:column style="padding-left: {!node.level * 20}px;" value="{!node.Equipment.Description_of_Equipment__c}"/>
                    <apex:column style="padding-left: {!node.level * 20}px;" value="{!node.Equipment.Manufacturer__c}"/>
                    <apex:column style="padding-left: {!node.level * 20}px;" value="{!node.Equipment.Model_Number__c}"/>
                    <apex:column style="padding-left: {!node.level * 20}px;" value="{!node.Equipment.Manufacturer_Serial_No__c}"/>
                    <apex:column style="padding-left: {!node.level * 20}px;" value="{!node.Equipment.Tag_Number__c}"/>
                </apex:pageBlockTable>
            </apex:outputPanel>

        </apex:pageBlock>
	</apex:form>

	<apex:outputPanel rendered="{!IF(type = 'Location',true,false)}">
        <base target="_top" />
		<apex:relatedList subject="{!Work_Order_Activity__c.Location__c}" list="Missing_Equipment_Data_Forms__r"/>
		<apex:relatedList subject="{!Work_Order_Activity__c.Location__c}" list="Equipment_Transfer_Forms__r"/>
		<apex:relatedList subject="{!Work_Order_Activity__c.Location__c}" list="Equipment_Transfer_Forms1__r"/>
	</apex:outputPanel>

	<apex:outputPanel rendered="{!IF(type = 'Facility',true,false)}">
		<base target="_top" />
        <apex:relatedList subject="{!Work_Order_Activity__c.Facility__c}" list="Add_Equipment_Forms__r"/>
		<apex:relatedList subject="{!Work_Order_Activity__c.Facility__c}" list="Equipment_Transfer_Requests__r"/>
		<apex:relatedList subject="{!Work_Order_Activity__c.Facility__c}" list="Equipment_Transfer_Requests1__r"/>
	</apex:outputPanel>
	
	<!--apex:outputPanel rendered="{!IF(type = 'WellEvent',true,false)}">
		<base target="_parent" />
        <apex:relatedList subject="{!Work_Order_Activity__c.Well_Event__c}" list="Add_Equipment_Forms__r"/>
		<apex:relatedList subject="{!Work_Order_Activity__c.Well_Event__c}" list="Equipment_Transfer_Requests__r"/>
		<apex:relatedList subject="{!Work_Order_Activity__c.Well_Event__c}" list="Equipment_Transfer_Requests1__r"/>
	</apex:outputPanel-->
	
			<apex:outputPanel rendered="{!IF(type = 'System',true,false)}">
		<base target="_parent" />
        <apex:relatedList subject="{!Work_Order_Activity__c.System__c}" list="Add_Equipment_Forms__r"/>
		<apex:relatedList subject="{!Work_Order_Activity__c.System__c}" list="Equipment_Transfer_Requests__r"/>
		<apex:relatedList subject="{!Work_Order_Activity__c.System__c}" list="Equipment_Transfer_Requests1__r"/>
	</apex:outputPanel>
	
	<apex:outputPanel rendered="{!IF(type = 'SubSystem',true,false)}">
		<base target="_parent" />
        <apex:relatedList subject="{!Work_Order_Activity__c.Sub_System__c}" list="Add_Equipment_Forms__r"/>
		<apex:relatedList subject="{!Work_Order_Activity__c.Sub_System__c}" list="Equipment_Transfer_Requests__r"/>
		<apex:relatedList subject="{!Work_Order_Activity__c.Sub_System__c}" list="Equipment_Transfer_Requests1__r"/>
	</apex:outputPanel>

    <apex:outputPanel rendered="{!IF(type = 'Functional Equipment Level',true,false)}">
        <base target="_parent" />
        <apex:relatedList subject="{!Work_Order_Activity__c.Functional_Equipment_Level__c}" list="Add_Equipment_Forms__r"/>
        <apex:relatedList subject="{!Work_Order_Activity__c.Functional_Equipment_Level__c}" list="Equipment_Transfer_Requests__r"/>
        <apex:relatedList subject="{!Work_Order_Activity__c.Functional_Equipment_Level__c}" list="Equipment_Transfer_Requests1__r"/>
    </apex:outputPanel>

     <apex:outputPanel rendered="{!IF(type = 'Yard',true,false)}">
        <base target="_parent" />
        <apex:relatedList subject="{!Work_Order_Activity__c.Yard__c}" list="Add_Equipment_Forms__r"/>
        <apex:relatedList subject="{!Work_Order_Activity__c.Yard__c}" list="Equipment_Transfer_Requests__r"/>
        <apex:relatedList subject="{!Work_Order_Activity__c.Yard__c}" list="Equipment_Transfer_Requests1__r"/>
    </apex:outputPanel>
	
	</apex:outputPanel>

	<apex:outputPanel rendered="{! IF(type = '',true,false) }">
		<apex:pageMessage summary="Location, Facility, Well Event, System, Sub-System, Functional Equipment Level or Yard is missing for this Activity. Please go back to Work Order level to see Equipment Hierarchy." severity="info" strength="3" />
	</apex:outputPanel>

</apex:page>